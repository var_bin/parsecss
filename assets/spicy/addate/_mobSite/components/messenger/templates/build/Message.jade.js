this["JST"] = this["JST"] || {};

this["JST"]["Message"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),message = locals_.message,_t = locals_._t,blurred = locals_.blurred;
if ( message.separate)
{
var date = (["today", "yesterday"].indexOf(message.date) > -1) ? _t("messenger", "text.conversation_started." + message.date) : message.date;
buf.push("<div" + (jade.attr("data-message-date", message.date, true, false)) + " class=\"message cf message-date\">" + (jade.escape(null == (jade_interp = date) ? "" : jade_interp)) + "</div>");
}
var messageClass = "";
switch (true){
case message.data.autoreplySubject && !message.data.autoreplyDescription:
messageClass = "msg-autoreply"
  break;
case message.data.autoreplyDescription && !message.data.autoreplySubject:
messageClass = "msg-autoreply"
  break;
case message.data.userMatchAlerts:
messageClass = "msg-new-unit"
  break;
case message.data.type === 'flirtcast':
messageClass = "msg-flirtcast"
  break;
case message.direction === "in":
messageClass = ""
  break;
}
buf.push("<div data-message=\"1\"" + (jade.cls(['message','cf',["message-"+message.direction, message.loading ? "loading" : "", message.data.isModerated? "msg-scammer" : ""]], [null,null,true])) + "><div data-message-text=\"1\"" + (jade.cls(['message-text',[messageClass]], [null,true])) + "><div class=\"message-item-text\">");
if ( blurred)
{
var offset = 8
if ( message.text.length > offset)
{
buf.push(null == (jade_interp = message.text.substr(0, offset)) ? "" : jade_interp);
}
buf.push("<span class=\"upgrade-text\"></span>");
}
else
{
if ( message.data.autoreplySubject && !message.data.autoreplyDescription)
{
buf.push(null == (jade_interp = _t("messenger", "text.autoreply") + ": "+message.text) ? "" : jade_interp);
}
else if ( message.data.autoreplyDescription && !message.data.autoreplySubject)
{
buf.push((null == (jade_interp = _t("messenger", "text.autoreply") + ": "+message.text) ? "" : jade_interp) + "<div>_t(\"messenger\", \"text.this_is_autoreply\")</div>");
}
else
{
buf.push(null == (jade_interp = message.text) ? "" : jade_interp);
}
}
buf.push("</div><div class=\"time\">" + (jade.escape(null == (jade_interp = message.time) ? "" : jade_interp)) + "</div></div></div>");;return buf.join("");
};