this["JST"] = this["JST"] || {};

this["JST"]["Messenger"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),isUserAdmin = locals_.isUserAdmin,prev_exist = locals_.prev_exist,_t = locals_._t,upgrade_communication_show = locals_.upgrade_communication_show,upgrade_communication = locals_.upgrade_communication,userId = locals_.userId,upgrade_read = locals_.upgrade_read,smileList = locals_.smileList,smileBlank = locals_.smileBlank;
buf.push("<div id=\"Messenger\"><div><div" + (jade.cls(['msg-box',isUserAdmin ? 'admin-page' : ''], [null,true])) + "><div id=\"messageHistory\" class=\"msg-history\"><div id=\"messengerMore\" data-message-more=\"1\"" + (jade.cls(['msg-view-more',(!prev_exist) ? 'hidden' : ''], [null,true])) + "><span>" + (jade.escape(null == (jade_interp = _t("messenger", "button.previous")) ? "" : jade_interp)) + "</span></div></div></div></div>");
if ( (!isUserAdmin))
{
buf.push("<div data-input-block=\"1\" class=\"msg-input cf\">");
if ( upgrade_communication_show && upgrade_communication)
{
buf.push("<div class=\"message-upgrade fbx-field-check\"><button data-link=\"1\"" + (jade.attr("data-href", upgrade_communication+userId, true, false)) + " class=\"field-check\"><label><input type=\"checkbox\"/></label><div class=\"switcher\"><span>ON</span><span>OFF</span></div></button><label class=\"field-label\">" + (jade.escape(null == (jade_interp = _t("messenger", "button.upgrade_communication")) ? "" : jade_interp)) + "</label></div>");
}
if ( upgrade_read)
{
buf.push("<div class=\"message-upgrade\"><a" + (jade.attr("href", upgrade_read, true, false)) + " class=\"button\">" + (jade.escape(null == (jade_interp = _t("messenger", "button.upgrade_to_read")) ? "" : jade_interp)) + "</a></div>");
}
if ( (!isUserAdmin))
{
buf.push("<div id=\"messengerSmileList\" data-messenger-smiles=\"1\" class=\"msg-smiles\"><ul>");
// iterate smileList || []
;(function(){
  var $$obj = smileList || [];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var smile = $$obj[$index];

buf.push("<li class=\"msg-smile\"><img" + (jade.attr("data-smile", smile, true, false)) + (jade.attr("src", smileBlank, true, false)) + " class=\"msg-emoji\"/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var smile = $$obj[$index];

buf.push("<li class=\"msg-smile\"><img" + (jade.attr("data-smile", smile, true, false)) + (jade.attr("src", smileBlank, true, false)) + " class=\"msg-emoji\"/></li>");
    }

  }
}).call(this);

buf.push("</ul></div><div class=\"submit\"><button id=\"messengerSend\" data-messenger-send=\"1\"></button></div><div class=\"input cf\"><div class=\"input-wrap\"><textarea id=\"messengerInput\" tabindex=\"0\" name=\"new_message_input\" data-messenger-field=\"1\" class=\"message-input\"></textarea></div></div>");
}
buf.push("</div>");
}
buf.push("</div>");;return buf.join("");
};