var ToolbarView = Backbone.View.extend({

    events: {
        "click [data-back]": "back",
        "click [data-menu]": "showMenu",
        "click #toolbarSearchForm": "showSearchForm",
        "click #toolbarSearchResultList": "showSearchResultList",
        "click #toolbarSearchResultGallery": "showSearchResultGallery",
        "click #toolbarLiveCams": "showSearchResultLivecam",
        "click #toolbarSearchMap": "showSearchMap",
        "click #toolbarAccountBack": "accountBackEvent",
        "click #userProfileGalleryButton": "showUserProfileGallery",
        "click #userProfileInfoButton": "showUserProfileInfo",
        'click [data-external-vod]' : 'redirectToExternalVod',
        'click [data-link]': 'openUrl',
        'click button': 'touchToolbar'
    },

    regions: {
        activityCounter: "[data-total-counter]"
    },

    coregOptions: {
        id: 'advanced-search',
        container : '#coreg-placement-advanced-search',
        tpl : 'SearchFormCoreg'
    },

    initialize: function() {
        this.model.set('livecamAnchor', app.appData().get('livecamSearchButton') ? app.appData().get('livecamSearchButton').toolbar : false);
        this.listenTo(Backbone, "ControllerRun", this.activeMenu);
        this.listenTo(Backbone, "LookProfile", this.userInfo);
        this.listenTo(Backbone, "LookGallery", this.userGallery);
        this.listenTo(Backbone, "AccountPage", this.accountPage);
        this.listenTo(Backbone, "AccountBack", this.accountBack);
        this.listenTo(Backbone, "HideToolbar", this.hideToolbar);
        this.listenTo(Backbone, "ShowToolbar", this.showToolbar);
        this.listenTo(Backbone, "CustomToolbar", this.renderCustomToolbar);
        this.listenTo(Backbone, "ChatWithUserPageReady", this.chatWithUserPageReady);
        this.listenTo(this.model, 'change:activity', this.updateActivityCounter);
        this.listenTo(Backbone, 'search.setDefaultSearchType', this.setDefaultSearchType);
        this.listenTo(Backbone, 'showSearchResultList showSearchResultGallery showSearchMap toolbar:liveCamButtonActive', this.activeSearchType);
        Backbone.on('chatRoom.joinRoom', this.chatRoomReady);
        Backbone.on('chatRoom.leaveRoom', this.chatRoomsListReady);
    },

    render: function() {
        this.$el.html(tpl.render("Toolbar", this.model.toJSON()));
        return this;
    },

    activeMenu: function(options) {
        this.$('[data-block]').hide();
        var id = '#' + options.name.toLowerCase() + '-' + options.action.toLowerCase();
        var currentBlock = this.$(id);

        if( ! currentBlock.length)
            currentBlock = this.$('#default-index');

        currentBlock.show();
    },

    touchToolbar: function() {
        Backbone.trigger('touchToolbar');
    },

    back: function() {
        if(window.history.length == 1) {
            app.router.navigate("/search", {trigger: true});
            return;
        }
        window.history.back();
    },

    showMenu: function(e) {
        if ($("#mainLayout").hasClass('side-opened')) {
            Backbone.trigger('UserMenu.hide', e);
        } else {
            Backbone.trigger('UserMenu.show', e);
        }
    },

    userInfo: function(user) {
        this.$('#toolbarUserName').text(user.login);
    },

    userGallery: function(photoData) {
        this.$('#currentIndex').text(photoData.currentImage + ' ');
        this.$('#count').text(' ' + photoData.length);
    },

    showSearchForm: function() {
        Backbone.trigger('showSearchForm');
        if (!$('#searchFormContainer').hasClass('hidden')) {
            var self = this;
            Backbone.trigger('Coregistration.showNext', {
                id : self.coregOptions.id,
                container : self.coregOptions.container,
                tpl : self.coregOptions.tpl,
                async : true,
                onRender : function() {
                    var $input = $(self.coregOptions.container + ' [data-element=checkbox] input');
                    $input.on('change', function() {
                        var $container = $(this).closest('[data-check]');
                        if ($(this).attr('checked')) {
                            $container.addClass('active');
                        } else {
                            $container.removeClass('active');
                        }
                    });
                }
            });
        }
    },

    showSearchResultList: function(e) {
        Backbone.trigger('showSearchResultList', 'list');
    },

    showSearchResultGallery: function(e) {
        Backbone.trigger('showSearchResultGallery', 'photo');
    },

    showSearchResultLivecam: function(e) {
        app.router.navigate('/search/type:livecam', {trigger: true});
        this.$('#search-index aside.cside button').removeClass('active');
        this.$('#toolbarLiveCams').addClass('active');
    },

    showSearchMap: function(e) {
        Backbone.trigger('showSearchMap', 'map');
    },

    activeSearchType: function(type) {
        this.$('#search-index aside.cside button').removeClass('active');

        switch (type) {
            case 'list':
                this.$('#toolbarSearchResultList').addClass('active');
                break;
            case 'photo':
                this.$('#toolbarSearchResultGallery').addClass('active');
                break;
            case 'map':
                this.$('#toolbarSearchMap').addClass('active');
                break;
            case 'livecam':
                this.$('#toolbarLiveCams').addClass('active');
                break;
            default:
                break;
        }
    },

    setDefaultSearchType: function() {
        this.region.$el().find('#search-index aside.cside button').removeClass('active').first().addClass('active');
    },

    accountPage: function(pageName) {
        this.activeMenu({
            name: 'account',
            action: 'page'
        });
        this.$('#account-page header').html(pageName);
    },

    chatWithUserPageReady: function(user) {
        var $elem = this.$('#chat-with'),
            naughtyInfo;

        if( ! user)
            return;

        $elem.find('[data-userLogin]').html(user.login);
        var status = '';
        if(user.statuses && user.statuses.onlineStatus) {
            status = 'user-online'
        }
        else if(user.statuses && user.statuses.recentlyOnlineStatus) {
            status = 'user-recent'
        }

        $elem.find('[data-userHeadBlock]').removeClass('user-online user-recent').addClass(status);
        $elem.find('[data-userPhoto]').attr('src', user.photoUrl);
        $elem.find('[data-userLink]').attr('data-href', '#user/view/id/'+user.id);
        $elem.find('[data-userLink]').removeClass('blurred blurred-heavy');
        naughtyInfo = app.appData().photoHolderInfo(user.photoAttributes.level);
        if (naughtyInfo) {
            $elem.find('[data-userLink]').addClass(naughtyInfo.naughtyClass);
        }
    },

    chatRoomReady: function(params) {
        var $elem = this.$('#custom-chatRoom');
        $elem.find('[data-roomTitle]').html(params.title);
        $elem.find('[data-leaveRoom]').attr('data-leaveRoom', params.id);
        this.$('[data-block]').hide();
        $elem.show();
    },

    chatRoomsListReady: function() {
        var $elem = this.$('#rooms-index');
        this.$('[data-block]').hide();
        $elem.show();
    },

    accountBackEvent: function(){
        Backbone.trigger('AccountBack');
    },

    showUserProfileGallery: function(e){
        $('[data-tab=info]').removeClass('active');
        $('[data-tab=gallery]').addClass('active');
        Backbone.trigger('UserProfileBase:scalePhoto');
    },

    showUserProfileInfo: function(e){
        $('[data-tab=gallery]').removeClass('active');
        $('[data-tab=info]').addClass('active');
    },

    accountBack: function() {
        this.activeMenu({
            name: 'account',
            action: 'index'
        });
    },

    hideToolbar: function() {
        $("#toolbar").hide();
    },

    showToolbar: function() {
        $("#toolbar").show();
    },

    renderCustomToolbar: function(data) {
        this.showUserProfileGallery();
    },

    openUrl: function(event) {
        var $el = $(event.target).closest('[data-link]'),
            href = $el.attr('data-href');
        if(href){
            if(href.charAt(0) === '#') {
                app.router.navigate(href, {
                    trigger: true
                });
            } else {
                window.location = href;
            }
        }
        return false;
    },

    updateActivityCounter: function(e, data) {
        var totalMenuCounter = data.counters.view + data.counters.wink  + data.counters.mail + getWCCounters();
        var elem = this.regions.activityCounter.$el();
        if(totalMenuCounter > 0) {
            elem.html(totalMenuCounter).show();
        } else {
            elem.hide();
        }

    },

    redirectToExternalVod: function(event) {
        var $el = $(event.currentTarget),
            $loader = $('#layoutContent');
        $loader.addClass('loading');
        $.ajax({
            url:  '/externalVod/index',
            type: 'post',
            dataType: 'json',
            data: app.csrfToken.name + '=' + app.csrfToken.value + '&externalVodId=' + $el.attr('data-external-vod')
        }).done(function(response) {
            if (response.status == 'success') {
                window.location.href = response.data.redirectUrl;
            }
        });
        $loader.removeClass('loading');
    }

});
