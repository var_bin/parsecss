this["JST"] = this["JST"] || {};

this["JST"]["Flirtcast"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,messages = locals_.messages;
buf.push("<div data-flirtcast-main=\"data-flirtcast-main\" class=\"b-flirtcast\"><div class=\"ftcst-title\">" + (jade.escape(null == (jade_interp = _t("flirtcast", "Send Flirtcast")) ? "" : jade_interp)) + "</div>");
var random = Math.floor(Math.random() * messages.length);
buf.push("<textarea id=\"flirtcastMessage\"" + (jade.attr("data-id", random, true, false)) + ">" + (jade.escape(null == (jade_interp = messages[random]) ? "" : jade_interp)) + "</textarea><button id=\"flirtcastNext\" class=\"refresh\"></button><a id=\"flirtcastSend\" class=\"button\">" + (jade.escape(null == (jade_interp = _t("flirtcast", "Send Flirtcast")) ? "" : jade_interp)) + "</a></div><div data-flirtcast-result=\"data-flirtcast-result\" class=\"b-flirtcast-result-message\"></div>");;return buf.join("");
};