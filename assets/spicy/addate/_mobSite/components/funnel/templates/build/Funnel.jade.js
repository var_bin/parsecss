this["JST"] = this["JST"] || {};

this["JST"]["Funnel"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),userAttributes = locals_.userAttributes,_t = locals_._t,defaultSearchParams = locals_.defaultSearchParams;
buf.push("<div class=\"b-index\"><div class=\"bg\"></div><div class=\"overlay\"></div><div class=\"b-head\"><h1 class=\"head-logo\"></h1></div><form id=\"FunnelForm\" class=\"form-funnel\">");
if ( userAttributes)
{
buf.push("<div class=\"funel-box\"><div class=\"item\"><h2 class=\"title\">" + (jade.escape(null == (jade_interp = _t("funnel", "title.welcome_to")) ? "" : jade_interp)) + "<label id=\"welcomMsg\"></label></h2></div><div class=\"item\"><label class=\"full-width\"><input type=\"text\"" + (jade.attr("value", userAttributes.screenname, true, false)) + " name=\"userAttributes[screenname]\" id=\"funnelScreenname\" placeholder=\"_t(&quot;funnel&quot;, &quot;title.screenname&quot;)\" onfocus=\"this.placeholder=''\" onblur=\"this.placeholder='_t('funnel', 'title.screenname')'\" class=\"screenname\"/></label></div><div class=\"item\"><label class=\"full-width\"><input type=\"text\" placeholder=\"Whats on my mind\" id=\"funnelChatupline\" onfocus=\"this.placeholder=''\" onblur=\"this.placeholder='Whats on my mind'\"/></label></div></div><div class=\"funel-box\"><div class=\"item\"><h2 class=\"title\">" + (jade.escape(null == (jade_interp = _t("funnel", "title.looking_for") + "?") ? "" : jade_interp)) + "</h2></div><div class=\"item\"><input type=\"hidden\"" + (jade.attr("value", defaultSearchParams.gender, true, false)) + " name=\"defaultSearchParams[gender]\" data-control=\"data-control\" id=\"funnelGender\"/><select class=\"looking-for\">");
if (defaultSearchParams.gender == 1)
{
buf.push("<option value=\"1\" selected=\"selected\">" + (jade.escape(null == (jade_interp = _t("funnel", "value.male")) ? "" : jade_interp)) + "</option><option value=\"2\">" + (jade.escape(null == (jade_interp = _t("funnel", "value.female")) ? "" : jade_interp)) + "</option>");
}
else
{
buf.push("<option value=\"1\">" + (jade.escape(null == (jade_interp = _t("funnel", "value.male")) ? "" : jade_interp)) + "</option><option value=\"2\" selected=\"selected\">" + (jade.escape(null == (jade_interp = _t("funnel", "value.female")) ? "" : jade_interp)) + "</option>");
}
buf.push("</select></div><div class=\"item\"><label class=\"full-width\"><div data-ui-location=\"data-ui-location\" class=\"select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div class=\"value\"><input name=\"defaultSearchParams[locationInput]\" type=\"text\"" + (jade.attr("value", defaultSearchParams.locationInput, true, false)) + " autocomplete=\"off\" data-control=\"data-control\" id=\"funnelLocale\" class=\"input-value\"/><div class=\"select-location location\"><div data-list=\"data-list\" id=\"locationSelectData\" class=\"b-dropdown-menu location\"></div></div></div></div></div></label></div><div class=\"item\"><button id=\"funnelSaveBtn\" class=\"button full-width\">" + (jade.escape(null == (jade_interp = _t("funnel", "title.start_searching") + "!") ? "" : jade_interp)) + "</button></div></div>");
}
buf.push("</form></div>");;return buf.join("");
};