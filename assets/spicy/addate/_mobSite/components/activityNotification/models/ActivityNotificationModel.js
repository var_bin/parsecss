var ActivityNotificationModel = Backbone.Model.extend({
    urlRoot: '/activity/removeNotification',

    defaults: {},
    counter: 0,
    readyData: {},
    onlineData: {},
    existing: {
        view: [],
        wink: [],
        mail: [],
        newsFeed: [],
        webcam: [],
        vod: [],
        addUserLike: [],
        match: []
    },
    recentlySent: {
        view: [],
        wink: [],
        mail: [],
        newsFeed: [],
        webcam: [],
        vod: [],
        addUserLike: [],
        match: []
    },

    recentlySentTypes: ['view', 'wink'],

    cacheKey: 'RecentlySentNotification',

    initialize: function() {
        this.storage = new LocalStorage('sessionStorage');
        var user = app.appData().get('user');
        this.set('memberShipUrl', app.appData().get('notificationPopup').bannerUrl);
        var isEmailConfirmed = app.appData().get("isEmailConfirmed");
        var counter = (typeof isEmailConfirmed != 'undefined' && isEmailConfirmed == false) ? 1 : 0;
        this.set('counter', counter);
        this.set('readyData', app.appData().get('newActivities'));
        this.set('defaults', {accountStatus: user.account_status});
        this.setEmailData(app.appData());
        this.listenTo(app.appData(), "change:notification", this.onlineDataProcess);
        this.trigger('ready');
    },


    onlineDataProcess: function(model) {
        var notification  = model.get('notification');
        if(!this.addNotification(notification))
            return false;
        this.set("onlineData", notification, {trigger: true});

    },

    addWCActivity: function (notification) {
        var date = new Date();
        var formatedDate = date.getDate() + ',' + (date.getMonth()+1) + ',' + date.getDay();
        var wcActivity = {
            action: "webcam",
            date: notification.date,
            gender: notification.gender.toLowerCase(),
            id: notification.fromId,
            login: notification.login,
            screenname: notification.login,
            photoAttributes: notification.photoAttributes,
            photo: notification.photo_url,
            age: notification.age,
            link: "/livecam/view/id/"+notification.login+"/promocode/" + app.appData().get('livecam').activity
        };
        var wcActivityCookie;
        try {
            wcActivityCookie = JSON.parse(getCookie('wcActivity'));
        } catch (e) {
            wcActivityCookie = {};
        }
        if (!wcActivityCookie) wcActivityCookie = {};
        if (!wcActivityCookie[formatedDate]) wcActivityCookie[formatedDate] = {};
        if (wcActivityCookie[formatedDate][notification.fromId]) return;
        wcActivityCookie[formatedDate][notification.fromId] = wcActivity;

        var expires = new Date();
        expires.setMinutes(expires.getMinutes() + 20);
        document.cookie = "wcActivity=" + escape(JSON.stringify(wcActivityCookie)) + ";expires=" + expires;
    },

    addNotification: function(notification) {
        if (notification.type === 'webcam') {
            this.addWCActivity(notification);
        }
        if (notification.type === 'vod' || this.existing[notification.type].indexOf(notification.fromId) !== -1){
            return false;
        }

        var recentlySent = this.getRecentlySent();
        if(recentlySent[notification.type].indexOf(notification.fromId) !== -1) {
            return false;
        }

        this.existing[notification.type].push(notification.fromId);
        this.updateCounter();
        return true;
    },

    removeNotification: function(notification) {
        this.existing[notification.type].splice(this.existing[notification.type].indexOf(notification.fromId) ,1);
        this.updateCounter();

        if(this.recentlySentTypes.indexOf(notification.type) === -1) {
            return;
        }
        var recentlySent = this.getRecentlySent();
        if(recentlySent[notification.type].indexOf(notification.fromId) !== -1) {
            return;
        }
        recentlySent[notification.type].push(notification.fromId);
        this.setRecentlySent(recentlySent);

    },

    updateCounter: function() {
        var isEmailConfirmed = app.appData().get("isEmailConfirmed");
        var counter = (typeof isEmailConfirmed != 'undefined' && isEmailConfirmed == false) ? 1 : 0;
        _.each(this.existing, function(notifications, type) {
            counter += notifications.length;
        });
        if (app.appData().get('isFlirtcastEnabled')
            && -1 !== _.indexOf(app.appData().getComponentFeature('flirtcast', 'placement'), 'activityNotification')
        ) {
            counter++;
        }
        this.set('counter', counter, {trigger: true});
    },

    getRecentlySent: function() {
        return this.storage.getItem(this.cacheKey) || this.recentlySent;
    },

    setRecentlySent: function(recentlySent) {
        this.storage.setItem(this.cacheKey, recentlySent);
    },

    setEmailData: function(model) {
        var isEmailConfirmed = app.appData().get("isEmailConfirmed");
        if (typeof isEmailConfirmed != 'undefined' && isEmailConfirmed == false) {
            this.set('counter', this.get('counter') + 1);
            var emailValidation = {
                isEmailConfirmed: isEmailConfirmed,
                userEmail: app.appData().get("user").email,
                emailService: app.appData().get("user").emailService
            };
            this.set('emailValidation', emailValidation);
        }
    }
 
});
