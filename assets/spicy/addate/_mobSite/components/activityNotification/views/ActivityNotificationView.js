var ActivityNotificationView = Backbone.View.extend({

    modelData: {},

    events: {
        "click [data-popup-open]": "openPopup",
        "click [data-popup-close]": "closePopup",
        "click [data-chat]": "goChat",
        "click [data-notification-close]": "closeNotification",
        'click #resendConfirmMail': 'resendConfirmMail',
        'click #btnChangeEmail': 'showChangeEmailForm',
        'click #sendChangeEmail': 'changeEmail',
        'click #sentConfimMailOkBtn': 'sentConfimMailOkBtn'
    },

    regions: {
        flirtcast: '#flirtcastContainer'
    },

    initialize: function() {
        this.permission = new PermissionModel();
        this.listenTo(this.model, "change:onlineData", this.addOnlineNotification);
        this.listenTo(this.model, "change:counter", this.changeCounter);
        this.listenTo(this.model, "ready", this.renderNotification);
        this.listenTo(this.permission, "getPermission", this.permissionAction);
        this.listenTo(Backbone, "hideNotifications", this.hideRegion);
        this.listenTo(Backbone, "showNotifications", this.showRegion);
        this.listenTo(Backbone, 'flirtcastForm', this.showFlirtcastNotification);
        this.listenTo(Backbone, "SendFlirtcast:error", this.hideFlirtcastNotification);
    },

    render: function() {
        var modelData = this.model.toJSON();
        this.$el.html(tpl.render("ActivityNotification", modelData));

        this.$counter = this.$('[data-notification-counter]');
        this.bookmark = this.$('#activityNotificationBookmark');
        this.popup = this.$('#activityNotificationPopup');
        this.container = this.$('#activityNotificationContainer');

        if (-1 !== _.indexOf(app.appData().getComponentFeature('flirtcast', 'placement'), 'activityNotification')) {
            this.regions.flirtcast.render(FlirtcastView, {
                model: new FlirtcastModel(),
                params: []
            });
        }

        var notifications   =  this.model.get('readyData');

        _.each(notifications, function(notification){
            if(this.model.addNotification(notification)) {
                this.addNotification(notification);
            }
        }, this);

        this.model.updateCounter();
    },

    hideRegion: function(){
        this.$el.addClass('hidden');
    },

    showRegion: function(){
        this.$el.removeClass('hidden');
    },

    changeCounter: function() {
        var counter = this.model.get('counter');
        this.$counter.html(counter);
        if(counter) {
            this.bookmark.removeClass('hidden');
        } else {
            this.bookmark.addClass('hidden');
            this.closePopup();
        }
    },

    addOnlineNotification: function() {
        this.addNotification(this.model.get('onlineData'));
    },

    addNotification: function(model){
        var talksMessengerConfig = app.appData().get('talksMessengerConfig'),
            talksEnabled = talksMessengerConfig && talksMessengerConfig.enabled,
            naughtyModeData = {};

        this.modelData = model;
        if(this.modelData.photo_url){
            naughtyModeData = app.appData().photoHolderInfo(this.modelData.photoAttributes.level);
        }

        this.modelData = _.extend(this.modelData, {
            naughtyMode: naughtyModeData,
            showRp: naughtyModeData.naughtyMode && naughtyModeData.naughtyMode.showRp,
            talksEnabled: talksEnabled,
            promo: app.appData().get('livecam').notifier
        });

        this.container.append(tpl.render("ActivityNotificationWidget", this.modelData));
    },

    openPopup: function() {
        this.popup.removeClass("hidden");
        $('#mainLayout').addClass('popup-active');
        Backbone.trigger('showNotificationPopup');
    },

    closePopup: function() {
        this.popup.addClass('hidden');
        $('#mainLayout').removeClass('popup-active');
    },

    closeNotification: function(e) {
        if($(e.currentTarget).hasClass('item-close')){
            e.preventDefault();
            e.stopPropagation();
        } else {
            this.closePopup();
        }
        var element = $(e.currentTarget).parents('[data-notification-item]'),
            removeData = {
                fromId: element.attr('data-from'),
                type:   element.attr('data-type')
            };
        this.model.unset({data: removeData});
        this.model.removeNotification(removeData);
        element.remove();
    },

    getNotificatiorData: function(elem) {
        var notificatorElement = $(elem).parents('[data-notification-item]');
        return {
            fromId:  notificatorElement.attr('data-from'),
            type:    notificatorElement.attr('data-type'),
            msgType: notificatorElement.attr('data-msgType')
        }
    },

    goChat: function(e) {
        e.preventDefault();
        e.stopPropagation();
        var notificationData = this.getNotificatiorData(e.currentTarget);
        this.permission.notification(notificationData.fromId, notificationData.msgType, null, notificationData.type);
        this.closePopup();
    },

    resendConfirmMail: function(event) {
        var userEmail = app.appData().get("user").email,
            userId = app.appData().get("user").id,
            jsonData = {};

        jsonData[app.csrfToken.name] = app.csrfToken.value;
        jsonData['RegistrationCompleteForm'] = {'resendEmail': userEmail},
        jsonData['id'] = userId;

        $.ajax({
            url      : '/user/resendConfirmMail',
            type     : 'post',
            data     : jsonData,
            dataType : 'json',
            success  : function (response) {
                if (response.status == 'success' && response.meta.code == 200) {
                    $('.email-valid-window-resend-email').removeClass('hidden');
                }
            }
        });
        return false;
    },

    showChangeEmailForm: function(event) {
        $('.email-valid-window-change-email').toggleClass('hidden');
        return false;
    },

    changeEmail: function() {
        var $newEmailField = $('#newEmail'),
            userId = app.appData().get("user").id,
            jsonData = {};

        jsonData[app.csrfToken.name] = app.csrfToken.value;
        jsonData['RegistrationCompleteForm'] = {'resendEmail': $newEmailField.val()},
        jsonData['id'] = userId;

        $newEmailField.removeClass('invalid-email');
        $('.email-valid-window-resend-email').addClass('hidden');
        $.ajax({
            url      : '/user/resendConfirmMail',
            type     : 'post',
            data     : jsonData,
            dataType : 'json',
            success  : function (response) {
                if (response.status == 'success' && response.meta.code == 200) {
                    $('.email-valid-window-resend-email').removeClass('hidden');
                    $('.email-valid-window-change-email').addClass('hidden');
                    $('#linkUserEmail').text($newEmailField.val()).attr('href', 'mailto:' + $newEmailField.val());
                } else if (response.status == 'error') {
                    $newEmailField.addClass('invalid-email');
                }
            }
        });
    },

    sentConfimMailOkBtn: function(event) {
        $('.email-valid-window-resend-email').addClass('hidden');
    },

    showFlirtcastNotification: function() {
        if (this.container.children('#flirtcastContainer.hidden').length) {
            this.container.children('#flirtcastContainer').removeClass('hidden');
            this.model.set('counter', this.model.get('counter') + 1, {trigger: true});
        }
    },

    hideFlirtcastNotification: function() {
        if (this.container.children('#flirtcastContainer').length) {
            this.container.children('#flirtcastContainer').addClass('hidden');
            this.model.set('counter', this.model.get('counter') - 1, {trigger: true});
            this.closePopup();
        }
    }

});
