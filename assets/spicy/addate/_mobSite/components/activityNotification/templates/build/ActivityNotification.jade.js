this["JST"] = this["JST"] || {};

this["JST"]["ActivityNotification"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),counter = locals_.counter,emailValidation = locals_.emailValidation,_t = locals_._t,defaults = locals_.defaults,memberShipUrl = locals_.memberShipUrl;
if (counter > 0)
{
buf.push("<div id=\"activityNotificationBookmark\"><div data-popup-open=\"data-popup-open\" data-notification-counter=\"data-notification-counter\" class=\"b-notification-icon\">" + (jade.escape(null == (jade_interp = counter) ? "" : jade_interp)) + "</div></div>");
}
else
{
buf.push("<div id=\"activityNotificationBookmark\" class=\"hidden\"><div data-popup-open=\"data-popup-open\" data-notification-counter=\"data-notification-counter\" class=\"b-notification-icon\">" + (jade.escape(null == (jade_interp = counter) ? "" : jade_interp)) + "</div></div>");
}
buf.push("<div id=\"activityNotificationPopup\" class=\"hidden\"><div class=\"overlay active\"><div data-popup-notification=\"data-popup-notification\" class=\"popup-notification\"><div class=\"notif-title\"><button data-popup-close=\"data-popup-close\" class=\"popup-close\"></button><span data-notification-counter=\"data-notification-counter\">" + (jade.escape(null == (jade_interp = counter) ? "" : jade_interp)) + "</span></div><div class=\"notif-users-wrap\"><ul id=\"activityNotificationContainer\" class=\"notif-users\">");
if (emailValidation && emailValidation.isEmailConfirmed === false)
{
buf.push("<li><div class=\"email-validation-b\"><div class=\"email-valid-info\">" + (null == (jade_interp = _t("activityNotification", "text.email-valid-info", {'{userEmail}': emailValidation.userEmail})) ? "" : jade_interp) + "<div class=\"email-vaild-btns\">");
if (emailValidation.emailService)
{
buf.push("<a" + (jade.attr("href", emailValidation.emailService, true, false)) + " target=\"_blank\" class=\"email-valid-btn check-email\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "text.check-email-btn")) ? "" : jade_interp)) + "</a>");
}
buf.push("<a id=\"resendConfirmMail\" href=\"#\" class=\"email-valid-btn resend-btn\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "text.email-resend-btn")) ? "" : jade_interp)) + "</a><a id=\"btnChangeEmail\" href=\"#\" class=\"email-valid-btn change-btn\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "text.email-change-btn")) ? "" : jade_interp)) + "</a></div></div><div class=\"email-valid-window-change-email hidden\"><div class=\"email-valid-window-change-email-content\"><form onsubmit=\"return false;\"><input id=\"newEmail\" type=\"text\"" + (jade.attr("placeholder", _t("activityNotification", "text.activity-new-email-placeholder"), true, false)) + "/><button id=\"sendChangeEmail\" type=\"submit\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "text.send-change-email-btn")) ? "" : jade_interp)) + "</button></form></div></div><div class=\"email-valid-window-resend-email resend-mail hidden\"><div class=\"email-valid-window-change-email-content\"><div class=\"email-valid-window-info\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "text.email-confirm-ok-message")) ? "" : jade_interp)) + "</div><a id=\"sentConfimMailOkBtn\" href=\"#\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "text.confim-mail-ok-btn")) ? "" : jade_interp)) + "</a></div></div></div></li>");
}
if (defaults.accountStatus.type == 'free')
{
buf.push("<li class=\"notif-banner\">" + (null == (jade_interp = _t("userMenu", "text.membership-text")) ? "" : jade_interp) + "<a" + (jade.attr("href", memberShipUrl, true, false)) + " class=\"button\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.membership-button")) ? "" : jade_interp)) + "</a></li>");
}
buf.push("<li id=\"flirtcastContainer\" class=\"srh-results-flirtcast\"></li></ul></div></div></div></div>");;return buf.join("");
};