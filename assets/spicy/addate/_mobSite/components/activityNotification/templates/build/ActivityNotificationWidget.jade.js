this["JST"] = this["JST"] || {};

this["JST"]["ActivityNotificationWidget"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),type = locals_.type,params = locals_.params,txtId = locals_.txtId,gender = locals_.gender,fromId = locals_.fromId,msgType = locals_.msgType,login = locals_.login,promo = locals_.promo,userLink = locals_.userLink,naughtyMode = locals_.naughtyMode,photo_url = locals_.photo_url,_t = locals_._t,buttons = locals_.buttons;
var needButtons = (type !== 'mail')
var notifyKey = ['notification', type];
if (params && params.subType) notifyKey.push(params.subType);
if (txtId) notifyKey.push(txtId);
notifyKey.push(gender);
buf.push("<li data-notification-item=\"data-notification-item\"" + (jade.attr("data-from", fromId, true, false)) + (jade.attr("data-type", type, true, false)) + (jade.attr("data-msgType", msgType || 'chat', true, false)) + " class=\"item-holder\"><div class=\"wrap-link\"><a" + (jade.attr("href", (type == "webcam" ? "/livecam/view/id/"+login+"/promocode/"+promo : userLink(fromId)), true, false)) + " data-popup-close=\"data-popup-close\" data-notification-close=\"data-notification-close\"" + (jade.attr("data-chat", type==='mail' ? 'data-chat' : false, true, false)) + (jade.cls(['pic',naughtyMode ? naughtyMode.naughtyClass: ''], [null,true])) + "><img" + (jade.attr("src", photo_url, true, false)) + "/>");
if ((naughtyMode.naughtyLevel == 1))
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "title.naughty_photo")) ? "" : jade_interp)) + "</span>");
}
if ((naughtyMode.naughtyLevel == 2))
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "title.explicit_Nudity")) ? "" : jade_interp)) + "</span>");
}
buf.push("</a><div class=\"content-holder\"><a" + (jade.attr("href", (type == "webcam" ? "/livecam/view/id/"+login+"/promocode/"+promo : userLink(fromId)), true, false)) + " data-popup-close=\"data-popup-close\" data-notification-close=\"data-notification-close\"" + (jade.attr("data-chat", type==='mail' ? 'data-chat' : false, true, false)) + "><span class=\"screenname\">" + (jade.escape(null == (jade_interp = login) ? "" : jade_interp)) + "</span>");
if ( (type == "webcam"))
{
buf.push("<span class=\"notif-desc\">Video chat request</span>");
}
else
{
buf.push("<span class=\"notif-desc\">" + (jade.escape(null == (jade_interp = _t("activityNotification", notifyKey.join('.'))) ? "" : jade_interp)) + "</span>");
}
buf.push("</a><button data-notification-close=\"data-notification-close\" class=\"item-close\"></button>");
if(needButtons && buttons)
{
buf.push("<div class=\"user-buttons activity-btns\">");
if ( (type == "webcam"))
{
// iterate ['Join live show', 'View profile']
;(function(){
  var $$obj = ['Join live show', 'View profile'];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var buttonText = $$obj[$index];

buf.push("<a" + (jade.attr("href", "/livecam/view/id/"+login+"/promocode/"+promo, true, false)) + " target=\"_blank\" class=\"a-chat-cam\"><button class=\"usr-action wc-button\"><span>" + (jade.escape(null == (jade_interp = buttonText) ? "" : jade_interp)) + "</span></button></a>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var buttonText = $$obj[$index];

buf.push("<a" + (jade.attr("href", "/livecam/view/id/"+login+"/promocode/"+promo, true, false)) + " target=\"_blank\" class=\"a-chat-cam\"><button class=\"usr-action wc-button\"><span>" + (jade.escape(null == (jade_interp = buttonText) ? "" : jade_interp)) + "</span></button></a>");
    }

  }
}).call(this);

}
else if ((params && params.subType && params.subType == 'onSmsChat'))
{
buf.push("<button data-popup-close=\"data-popup-close\" data-smsChat=\"data-smsChat\" class=\"btn-sms\"><span>" + (jade.escape(null == (jade_interp = _t("activityNotification", "Sms Chat")) ? "" : jade_interp)) + "</span></button>");
}
else
{
buf.push("<button data-chat=\"data-chat\" class=\"btn-chat usr-action message\"><span>" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.message")) ? "" : jade_interp)) + "</span></button>");
}
if ( (type != "webcam"))
{
buf.push("<button" + (jade.attr("data-winkbutton", fromId, true, false)) + (jade.cls(['btn-wink','usr-action','wink',(buttons.wink.activated)?"active":""], [null,null,null,true])) + "><span>" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.wink")) ? "" : jade_interp)) + "</span></button>");
}
buf.push("</div>");
}
buf.push("</div></div></li>");;return buf.join("");
};