this["JST"] = this["JST"] || {};

this["JST"]["FunnelPhotoUploadMotivation"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,profile = locals_.profile,users = locals_.users,user = locals_.user,src = locals_.src;
buf.push("<div class=\"b-index\"><div class=\"bg\"></div><div class=\"overlay\"></div><div class=\"b-head\"><h1 class=\"head-logo\"></h1><div class=\"lside\"><button id=\"toolbar-back\" class=\"tbr-button tbr-button-back\"></button></div></div><div class=\"form-funnel\"><div class=\"item\"><h2 class=\"funnel-title\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "title.upload_photo")) ? "" : jade_interp)) + "</h2></div><div class=\"item\"><ul class=\"list\"><li>" + (jade.escape(null == (jade_interp = _t("funnelPhotoUploadMotivation", "text.members_with_photo_in_search")) ? "" : jade_interp)) + "</li>");
if (profile.genderKey === 1)
{
buf.push("<li>" + (jade.escape(null == (jade_interp = _t("funnelPhotoUploadMotivation", "text.no_answers_without_photo_male")) ? "" : jade_interp)) + "</li>");
}
if (profile.genderKey === 2)
{
buf.push("<li>" + (jade.escape(null == (jade_interp = _t("funnelPhotoUploadMotivation", "text.no_answers_without_photo_female")) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div><div class=\"item\">");
if ( users)
{
// iterate [0,1,2]
;(function(){
  var $$obj = [0,1,2];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

if ( users[key])
{
user = users[key]
{
buf.push("<div data-image-grid-item=\"1\" class=\"pic\"><img" + (jade.attr("src", src=user.photos.url, true, false)) + "/><div class=\"tags\"><div class=\"tag new\">" + (jade.escape(null == (jade_interp = _t("funnelPhotoUploadMotivation", "text.new")) ? "" : jade_interp)) + "</div></div><div class=\"status online\"></div><div class=\"usr-info-holder\"></div><a href=\"#\" class=\"name\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span>,<span class=\"age\">" + (jade.escape(null == (jade_interp = user.age) ? "" : jade_interp)) + "</span></a><div class=\"pic-counter-holder\"><div class=\"pic-counter\">" + (jade.escape(null == (jade_interp = user.photos.count) ? "" : jade_interp)) + "</div></div></div>");
}
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

if ( users[key])
{
user = users[key]
{
buf.push("<div data-image-grid-item=\"1\" class=\"pic\"><img" + (jade.attr("src", src=user.photos.url, true, false)) + "/><div class=\"tags\"><div class=\"tag new\">" + (jade.escape(null == (jade_interp = _t("funnelPhotoUploadMotivation", "text.new")) ? "" : jade_interp)) + "</div></div><div class=\"status online\"></div><div class=\"usr-info-holder\"></div><a href=\"#\" class=\"name\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span>,<span class=\"age\">" + (jade.escape(null == (jade_interp = user.age) ? "" : jade_interp)) + "</span></a><div class=\"pic-counter-holder\"><div class=\"pic-counter\">" + (jade.escape(null == (jade_interp = user.photos.count) ? "" : jade_interp)) + "</div></div></div>");
}
}
    }

  }
}).call(this);

}
buf.push("<div id=\"photoUploadButtons\"></div></div></div></div>");;return buf.join("");
};