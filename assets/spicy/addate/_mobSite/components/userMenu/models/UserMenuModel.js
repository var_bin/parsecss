var UserMenuModel = Backbone.Model.extend({
    initialize: function() {
        this.listenTo(app.appData(), 'change:activity', this.setData);
        this.listenTo(app.appData(), 'change:user', this.setData);
        this.setData(app.appData());
    },

    setData: function(model) {
        var data = model.toJSON();
        data.livecamAnchor = app.appData().get('livecam') ? app.appData().get('livecam').search : false;
        data.externalVods = model.get("externalVod");
        if (data.externalVods) {
            data.externalVods["vodFormUrl"] = document.location.protocol + "//" + document.location.host + "/externalVod/redirectToVod/externalVodId/";
        }
        this.set(data);
    }

});
