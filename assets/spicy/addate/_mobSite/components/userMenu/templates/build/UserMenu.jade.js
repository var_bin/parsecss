this["JST"] = this["JST"] || {};

this["JST"]["UserMenu"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user,naughtyMode = locals_.naughtyMode,_t = locals_._t,activity = locals_.activity,isAllowChatRooms = locals_.isAllowChatRooms,externalVods = locals_.externalVods,livecamAnchor = locals_.livecamAnchor,userMenu = locals_.userMenu,undefined = locals_.undefined,phoneInfo = locals_.phoneInfo;
jade_mixins["vodLink"] = function(type, externalVod, vodId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if ( (type == 'vodLink'))
{
buf.push("<a" + (jade.attr("href", externalVod.vodFormUrl + vodId, true, false)) + " target=\"_blank\" class=\"a-item\"><span class=\"a-item-inner\">Premium Videos</span></a>");
}
else
{
buf.push("<a" + (jade.attr("href", externalVod.paymentUrl, true, false)) + " class=\"a-item\"><span class=\"a-item-inner\">Premium Videos</span></a>");
}
};
buf.push("<div class=\"smn-box\"><div id=\"siteLogo\" class=\"smn-logo\"></div><ul class=\"smn-list\"><li class=\"divider\">People</li><!-- add Naughty mode--><li class=\"aside-profile\"><a href=\"/#profile\" class=\"a-my-account-link\"><div class=\"av-wrap\"><img id=\"menuAvatar\"" + (jade.attr("src", user.photoUrl, true, false)) + "/></div><div class=\"aside-screenname\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + "</div></a></li>");
if(naughtyMode.available)
{
buf.push("<li class=\"item item-naughty-mode\"><span class=\"nm-text\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.naughty_mode") + ":") ? "" : jade_interp)) + "</span><a id=\"userMenuNaughtyMode\" href=\"/#site/naughtyMode\" class=\"nm-button-holder\"><button data-mode='1'" + (jade.cls(['nm-button','normal',(naughtyMode.naughtyMode == 1)?'':'hidden'], [null,null,true])) + ">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.normal")) ? "" : jade_interp)) + "</button><button data-mode='2'" + (jade.cls(['nm-button','sexy',(naughtyMode.naughtyMode == 2)?'':'hidden'], [null,null,true])) + ">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.sexy")) ? "" : jade_interp)) + "</button><button data-mode='3'" + (jade.cls(['nm-button','no-limit',(naughtyMode.naughtyMode == 3)?'':'hidden'], [null,null,true])) + ">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.no_limits")) ? "" : jade_interp)) + "</button></a></li>");
}
buf.push("<!-- end Naughty mode--><li class=\"item item-search\"><a href=\"/#search\" class=\"a-item\"><span class=\"a-item-inner\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.search")) ? "" : jade_interp)) + "</span></a></li><li class=\"divider\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.activity")) ? "" : jade_interp)) + "</li><li class=\"item item-messenger\"><a href=\"/#chat/recipients\" class=\"a-item\"><span class=\"a-item-inner\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.chat")) ? "" : jade_interp)));
if (activity.counters.mail)
{
buf.push("<span id=\"userMenuCounterMail\" class=\"counter\">" + (jade.escape(null == (jade_interp = activity.counters.mail) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span id=\"userMenuCounterMail\" class=\"counter hidden\"></span>");
}
buf.push("</span></a></li><li class=\"item item-activity\"><a href=\"/#newsFeed\" class=\"a-item\"><span class=\"a-item-inner\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.activity")) ? "" : jade_interp)) + "<span id=\"userMenuCounterActivity\" class=\"counter hidden\"></span></span></a></li><li class=\"item item-friends\"><a href=\"/#favorites\" class=\"a-item\"><span class=\"a-item-inner\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.friends") + " | ") ? "" : jade_interp)) + "<span id=\"userMenuCounterFavourite\">" + (jade.escape((jade_interp = user.friendsCount || 0) == null ? '' : jade_interp)) + "</span></span></a></li>");
if (isAllowChatRooms)      
{
buf.push("<li class=\"item item-chatrooms\"><a href=\"/#rooms\" class=\"a-item\"><span class=\"a-item-inner\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.chatrooms")) ? "" : jade_interp)) + "</span></a></li>");
}
if (externalVods && externalVods.vodIds && externalVods.vodIds.length)
{
// iterate externalVods.vodIds
;(function(){
  var $$obj = externalVods.vodIds;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var vodId = $$obj[$index];

buf.push("<li class=\"item item-vod\">");
if (user.account_status.type == 'paid' || externalVods.paidPermissions)
{
jade_mixins["vodLink"]('vodLink', externalVods, vodId);
}
else
{
jade_mixins["vodLink"]('payLink', externalVods, vodId);
}
buf.push("</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var vodId = $$obj[$index];

buf.push("<li class=\"item item-vod\">");
if (user.account_status.type == 'paid' || externalVods.paidPermissions)
{
jade_mixins["vodLink"]('vodLink', externalVods, vodId);
}
else
{
jade_mixins["vodLink"]('payLink', externalVods, vodId);
}
buf.push("</li>");
    }

  }
}).call(this);

}
if ( (livecamAnchor))
{
buf.push("<li class=\"item item-livecams\"><a" + (jade.attr("href", '/livecam/?promocode=' + (livecamAnchor) + '', true, false)) + " target=\"_blank\" class=\"a-item\"><span class=\"a-item-inner\">Who's on cam</span></a></li>");
}
var showOpenxBanner = true
if (user.account_status.type === "free")
{
showOpenxBanner = false
buf.push("<li class=\"item\">");
var banClass = "maleBan trad"
if (user.gender == "female")
{
buf.push("<banClass>= \"femaleBan\"</banClass>");
}
buf.push("<div" + (jade.cls(['smn-upgrade-area',banClass], [null,true])) + "><div class=\"smn-upgrade-area-item\"><div class=\"text\">" + (null == (jade_interp = _t("userMenu", "text.membership-text")) ? "" : jade_interp) + "</div><a" + (jade.attr("href", userMenu.membershipUrl, true, false)) + " class=\"lnk-submit get-button\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.membership-button")) ? "" : jade_interp)) + "</a></div><div class=\"smn-upgrade-area-bg\"></div></div></li>");
}
if(user.account_status.type === "paid")
{
if(user.account_features.top_in_search !== undefined && ! user.account_features.top_in_search.active)
{
buf.push("<li class=\"item\"><a" + (jade.attr("href", user.account_features.top_in_search.upgrade, true, false)) + " class=\"smn-upgrade-area\"><div class=\"text\">" + (null == (jade_interp = _t("userMenu", "text.features_vip", {"{br}":"<br>", "{b}":"<b>", "{/b}":"</b>"})) ? "" : jade_interp) + "</div><div class=\"button\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.activate_feature")) ? "" : jade_interp)) + "</div></a></li>");
}
else if (user.account_features.free_communication !== undefined && ! user.account_features.free_communication.active)
{
buf.push("<li class=\"item\"><a" + (jade.attr("href", user.account_features.free_communication.upgrade, true, false)) + " class=\"smn-upgrade-area\"><div class=\"text\">" + (null == (jade_interp = _t("userMenu", "text.features_free_communication", {"{br}":"<br>", "{b}":"<b>", "{/b}":"</b>"})) ? "" : jade_interp) + "</div><div class=\"button\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.activate_feature")) ? "" : jade_interp)) + "</div></a></li>");
}
}
if (showOpenxBanner === true)
{
buf.push("<li class=\"item openx-banner-item\"><div id=\"openx-banner-placement-sidebarmenu\" class=\"sidebarmenu-banner-openx\"></div></li>");
}
buf.push("<li class=\"item item-web\"><a id=\"mobToWeb\" href=\"#\" class=\"a-item\"><span class=\"a-item-inner\">" + (jade.escape(null == (jade_interp = _t("userMenu", "text.web_version")) ? "" : jade_interp)) + "</span></a></li></ul>");
if ( (phoneInfo.phoneNumber))
{
buf.push("<div class=\"smn-list\"><div class=\"item\"><div style=\"padding-left:18px;\" class=\"a-item-inner a-item\"><p>" + (null == (jade_interp = _t("userMenu", "text.hotline_credit_card")) ? "" : jade_interp) + "</p>");
if (phoneInfo.allowTwoPhones)
{
if (!phoneInfo.hideLocal)
{
buf.push("<p>" + (null == (jade_interp = _t("userMenu", "text.local") + ': ' + phoneInfo.phoneNumber) ? "" : jade_interp) + "</p>");
}
buf.push("<p>" + (null == (jade_interp = _t("userMenu", "text.intnl") + ': ' + phoneInfo.countryCode + phoneInfo.delimiter + phoneInfo.phoneNumber) ? "" : jade_interp) + "</p>");
}
else
{
buf.push("<p>" + (null == (jade_interp = phoneInfo.countryCode + phoneInfo.delimiter + phoneInfo.phoneNumber) ? "" : jade_interp) + "</p>");
}
buf.push("</div></div></div>");
}
buf.push("</div>");;return buf.join("");
};