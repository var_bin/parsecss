this["JST"] = this["JST"] || {};

this["JST"]["User"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),paid = locals_.paid,upgrade = locals_.upgrade,_t = locals_._t,user = locals_.user,lookingFor = locals_.lookingFor,userPrimaryPhoto = locals_.userPrimaryPhoto,naughtyLevel = locals_.naughtyLevel,userPhotoCount = locals_.userPhotoCount,naughtyClass = locals_.naughtyClass,buttons = locals_.buttons,activities = locals_.activities;
var number = 0
paid = (upgrade.length == 0)
jade_mixins["infoRow"] = function(key, title){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div" + (jade.cls(['usv-about-row',(number % 2 == 0) ? 'even' : ''], [null,true])) + "><p class=\"title\">" + (jade.escape(null == (jade_interp = _t('user', title)) ? "" : jade_interp)) + "</p><p class=\"data\">" + (jade.escape((jade_interp = (user[key]) ? user[key] : _t("user", "title.not_given")) == null ? '' : jade_interp)) + "</p></div>");
number++
};
jade_mixins["infoLookingForRow"] = function(key, title){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div" + (jade.cls(['usv-about-row',(number % 2 == 0) ? 'even' : ''], [null,true])) + "><p class=\"title\">" + (jade.escape(null == (jade_interp = _t('user', title)) ? "" : jade_interp)) + "</p><p class=\"data\">" + (jade.escape((jade_interp = (lookingFor[key]) ? ((key == 'age') ?  lookingFor.age.from + "-" + lookingFor.age.to : lookingFor[key].ucfirst()) : _t("user", "title.not_given")) == null ? '' : jade_interp)) + "</p></div>");
number++
};





jade_mixins["infoRowLocation"] = function(title){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div" + (jade.cls(['usv-about-row',(number % 2 == 0) ? 'even' : ''], [null,true])) + "><p class=\"title\">" + (jade.escape(null == (jade_interp = _t('user', title)) ? "" : jade_interp)) + "</p><p class=\"data\">" + (jade.escape((jade_interp = (user.geo) ? user.geo.city + ", " + user.geo.country : _t("user", "title.not_given")) == null ? '' : jade_interp)) + "</p></div>");
number++
};
jade_mixins["infoFullRow"] = function(key, title, last){
var block = (this && this.block), attributes = (this && this.attributes) || {};
var cls
if(last)
{
cls = 'last'
}
else
{
cls = (number % 2 == 0) ? 'even' : ''
}
buf.push("<div" + (jade.cls(['usv-about-row',cls], [null,true])) + "><p class=\"title full-row\">" + (jade.escape(null == (jade_interp = _t('user', title)) ? "" : jade_interp)) + "</p><p>" + (jade.escape((jade_interp = (user[key]) ? user[key] : _t("user", "title.not_given")) == null ? '' : jade_interp)) + "</p></div>");
number++
};
jade_mixins["userImage"] = function(){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<img" + (jade.attr("src", (userPrimaryPhoto && userPrimaryPhoto.avatar) ? userPrimaryPhoto.avatar : user.photoUrl, true, false)) + (jade.attr("alt", user.login, true, false)) + "/>");
if(naughtyLevel == 1)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("user", "title.naughty_photo")) ? "" : jade_interp)) + "</span>");
}
if(naughtyLevel == 2)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("user", "title.explicit_Nudity")) ? "" : jade_interp)) + "</span>");
}
};
jade_mixins["tags"] = function(){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div class=\"tags\">");
if(user.statuses && user.statuses.newStatus)
{
buf.push("<div class=\"tag new\">" + (jade.escape(null == (jade_interp = _t("user", "title.new")) ? "" : jade_interp)) + "</div>");
}
buf.push("</div>");
};
jade_mixins["header"] = function(title){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<header>" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</header>");
number++
};
buf.push("<div" + (jade.cls(['user-view',paid ? 'full' : 'free'], [null,true])) + "><div class=\"usv-info\">");
if(paid)
{
buf.push("<div class=\"photo-preloader pic\">");
if (userPhotoCount)
{
buf.push("<a" + (jade.attr("href", '/#photo/gallery/id/'+user.id, true, false)) + ">");
jade_mixins["userImage"]();
buf.push("</a>");
}
else
{
jade_mixins["userImage"]();
}
buf.push("<div class=\"pic-counter-holder\"><div class=\"pic-counter\">" + (jade.escape((jade_interp = userPhotoCount) == null ? '' : jade_interp)) + "</div></div></div>");
}
else
{
if(upgrade && upgrade.photo_big)
{
buf.push("<a" + (jade.attr("href", upgrade.photo_big, true, false)) + (jade.attr("data-popup", (naughtyClass.length) ? 'yes' : 'no', true, false)) + " class=\"user_link\"><div class=\"photo-preloader pic faded\">");
jade_mixins["userImage"]();
buf.push("</div></a>");
}
}
buf.push("<div" + (jade.cls(['details',paid ? 'full' : ''], [null,true])) + "><div class=\"info-wrap\">");
if ((!paid))
{
buf.push("<div class=\"pic-small\">");
if(upgrade && upgrade.photo_big)
{
buf.push("<a" + (jade.attr("href", upgrade.photo_big, true, false)) + ">");
jade_mixins["userImage"]();
buf.push("</a>");
}
else
{
if (userPhotoCount)
{
buf.push("<a" + (jade.attr("href", '/#photo/gallery/id/'+user.id, true, false)) + (jade.attr("data-popup", (naughtyClass.length) ? 'yes' : 'no', true, false)) + ">");
jade_mixins["userImage"]();
buf.push("</a>");
}
else
{
jade_mixins["userImage"]();
}
}
buf.push("<div class=\"user-info\">");
jade_mixins["tags"]();
buf.push("<div" + (jade.cls(['status',user.statuses.onlineStatus ? 'online' : ''], [null,true])) + "></div></div></div>");
}
buf.push("<div class=\"user-info\">");
if ((paid))
{
jade_mixins["tags"]();
buf.push("<div" + (jade.cls(['status',user.statuses.onlineStatus ? 'online' : ''], [null,true])) + "></div><div class=\"name\"><span class=\"name-wrap\"><span class=\"name-inner\"><a href=\"#\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + ",</a></span></span><span class=\"age\">" + (jade.escape(null == (jade_interp = user.age + " " + _t("user", "text.years")) ? "" : jade_interp)) + "</span></div><div class=\"from\">" + (jade.escape((jade_interp = user.geo.city) == null ? '' : jade_interp)) + " | " + (jade.escape((jade_interp = user.geo.country) == null ? '' : jade_interp)) + "</div>");
}
else
{
buf.push("<div class=\"name\"><span class=\"name-wrap\"><span class=\"name-inner\"><a href=\"#\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + "</a></span></span></div><div class=\"age\">" + (jade.escape(null == (jade_interp = user.age + " " + _t("user", "text.years")) ? "" : jade_interp)) + "</div><div class=\"from\">" + (jade.escape((jade_interp = user.geo.city) == null ? '' : jade_interp)) + " | " + (jade.escape((jade_interp = user.geo.country) == null ? '' : jade_interp)) + "</div><div class=\"photos\">" + (jade.escape((jade_interp = userPhotoCount) == null ? '' : jade_interp)) + "</div>");
}
buf.push("</div></div><div class=\"user-buttons\"><a" + (jade.attr("href", '/#chat/with/'+user.id, true, false)) + " class=\"usr-action message\">" + (jade.escape(null == (jade_interp = _t("user", "button.chat")) ? "" : jade_interp)) + "</a><button" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.cls(['usr-action','wink',(buttons.wink.activated) ? "active" : ""], [null,null,true])) + "><span class=\"initial_state\">" + (jade.escape(null == (jade_interp = _t("user", "button.wink")) ? "" : jade_interp)) + "</span><span class=\"final_state\">" + (jade.escape(null == (jade_interp = _t("user", "button.winked")) ? "" : jade_interp)) + "</span></button><button" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.cls(['usr-action','favourite',(buttons.favorite.activated) ? "active" : ""], [null,null,true])) + "><span class=\"initial_state\">" + (jade.escape(null == (jade_interp = _t("user", "button.add_friend")) ? "" : jade_interp)) + "</span><span class=\"final_state\">" + (jade.escape(null == (jade_interp = _t("user", "text.friends")) ? "" : jade_interp)) + "</span></button></div></div></div><div id=\"swipeGallery\" class=\"glr-wrap hidden\"><div class=\"glr-header\"></div><div class=\"glr-inner\"></div></div><div class=\"usv-tabs-container\"><ul data-tabs-list=\"1\" class=\"usv-tabs-list\"><li data-tabs-opener=\"tab1\">" + (jade.escape(null == (jade_interp = _t("user", "text.about_user") + " ") ? "" : jade_interp)));
if(user.data_gender == "male")
{
buf.push(jade.escape(null == (jade_interp = _t("user", "text.him")) ? "" : jade_interp));
}
else
{
buf.push(jade.escape(null == (jade_interp = _t("user", "text.her")) ? "" : jade_interp));
}
buf.push("</li><li data-tabs-opener=\"tab2\">" + (jade.escape(null == (jade_interp = _t("user", "text.activity_history") + " | " + activities.total) ? "" : jade_interp)) + "</li></ul><div data-tabs-content=\"data-tabs-content\" class=\"usv-tabs-content\"><div data-tab=\"tab1\" class=\"tab\"><div class=\"usv-about\">");
jade_mixins["infoFullRow"]('user', (user.data_gender == "male") ? "user.his_mind" : "user.her_mind");
jade_mixins["infoFullRow"]('description', 'In her own words', true);
jade_mixins["header"](_t("user", "title.appearance"));
jade_mixins["infoRow"]('height', 'Height');
jade_mixins["infoRow"]('weight', 'Weight');
jade_mixins["infoRow"]('build', 'Body type');
jade_mixins["infoRow"]('hair_color', 'Hair color');
jade_mixins["infoRow"]('eye_color', 'Eyes color');
jade_mixins["infoRow"]('pierced', 'Pirsing');
jade_mixins["infoRow"]('tattoo', 'Tatoo');
jade_mixins["header"](_t("user", "text.lifestyle"));
jade_mixins["infoRow"]('sexual_orientation', 'Orientation');
jade_mixins["infoRow"]('marital_status', 'Marital status');
jade_mixins["infoRow"]('children', 'Children');
jade_mixins["infoRow"]('living', 'Living');
jade_mixins["infoRow"]('income', 'Income');
jade_mixins["infoRow"]('smoke', 'Smoke');
jade_mixins["infoRow"]('drink', 'Drink');
jade_mixins["header"](_t("user", "title.looking_for"));
jade_mixins["infoLookingForRow"]('gender', 'Gender');
jade_mixins["infoLookingForRow"]('age', 'Age');
jade_mixins["infoRowLocation"]('Location');
if(lookingFor.show === "payment-link")
{
var banClass = "maleBan trad"
if (user.gender == "female")
{
banClass = "femaleBan"
}
buf.push("<div" + (jade.cls(['usv-about-lock',banClass], [null,true])) + "><div class=\"usv-about-lock-wrap\"><div class=\"box-inner\"><p>" + (jade.escape(null == (jade_interp = _t("user", "text.membership")) ? "" : jade_interp)) + "<br/><span>" + (jade.escape(null == (jade_interp = _t("user", "text.view_more_results")) ? "" : jade_interp)) + "</span></p><a" + (jade.attr("href", lookingFor.upgrade, true, false)) + " class=\"get-button\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_it_now") + "!") ? "" : jade_interp)) + "</a></div><div class=\"usv-about-lock-bg\"></div></div></div>");
}
buf.push("</div></div><div data-tab=\"tab2\" class=\"tab\"><div class=\"urv-activity\"><div class=\"activity-history\">");
if(typeof activities.list === "object")
{
var currentDate = ""
// iterate activities.list
;(function(){
  var $$obj = activities.list;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var act = $$obj[$index];

if(currentDate != act.date)
{
currentDate = act.date
buf.push("<div class=\"activity-time\"><span class=\"day\">");
switch (act.date){
case "today":
buf.push(jade.escape(null == (jade_interp = _t("user", "text.today")) ? "" : jade_interp));
  break;
case "yesterday":
buf.push(jade.escape(null == (jade_interp = _t("user", "text.yesterday")) ? "" : jade_interp));
  break;
default:
buf.push("" + (jade.escape((jade_interp = act.date) == null ? '' : jade_interp)) + "");
  break;
}
buf.push("</span></div>");
}
buf.push("<div" + (jade.cls(['activity-item',act.action], [null,true])) + "><div class=\"activity cf\"><div class=\"photo\"><img" + (jade.attr("src", act.photoUrl, true, false)) + " alt=\"users photo\"/></div><div class=\"activity-icon\"></div></div><div class=\"time\"> " + (jade.escape((jade_interp = act.time) == null ? '' : jade_interp)) + "</div><div class=\"activity-text\">");
switch (act.type){
case "outgoing":
buf.push(null == (jade_interp = _t('user', 'user.you.'+act.action+'.'+user.data_gender)) ? "" : jade_interp);
  break;
default:
buf.push(null == (jade_interp = _t('user', 'user.'+user.data_gender+'.'+act.action)) ? "" : jade_interp);
  break;
}
buf.push("</div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var act = $$obj[$index];

if(currentDate != act.date)
{
currentDate = act.date
buf.push("<div class=\"activity-time\"><span class=\"day\">");
switch (act.date){
case "today":
buf.push(jade.escape(null == (jade_interp = _t("user", "text.today")) ? "" : jade_interp));
  break;
case "yesterday":
buf.push(jade.escape(null == (jade_interp = _t("user", "text.yesterday")) ? "" : jade_interp));
  break;
default:
buf.push("" + (jade.escape((jade_interp = act.date) == null ? '' : jade_interp)) + "");
  break;
}
buf.push("</span></div>");
}
buf.push("<div" + (jade.cls(['activity-item',act.action], [null,true])) + "><div class=\"activity cf\"><div class=\"photo\"><img" + (jade.attr("src", act.photoUrl, true, false)) + " alt=\"users photo\"/></div><div class=\"activity-icon\"></div></div><div class=\"time\"> " + (jade.escape((jade_interp = act.time) == null ? '' : jade_interp)) + "</div><div class=\"activity-text\">");
switch (act.type){
case "outgoing":
buf.push(null == (jade_interp = _t('user', 'user.you.'+act.action+'.'+user.data_gender)) ? "" : jade_interp);
  break;
default:
buf.push(null == (jade_interp = _t('user', 'user.'+user.data_gender+'.'+act.action)) ? "" : jade_interp);
  break;
}
buf.push("</div></div>");
    }

  }
}).call(this);

}
buf.push("</div></div></div></div></div></div><div class=\"banner-bottom\"><div id=\"openx-banner-placement-userbottom\" class=\"userbottom-banner-openx\"></div></div><div class=\"banner-bottom-height\"></div>");;return buf.join("");
};