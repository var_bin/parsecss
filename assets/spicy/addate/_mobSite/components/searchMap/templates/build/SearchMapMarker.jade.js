this["JST"] = this["JST"] || {};

this["JST"]["SearchMapMarker"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),naughty = locals_.naughty,imgSrc = locals_.imgSrc,name = locals_.name,_t = locals_._t;
buf.push("<div" + (jade.cls(['map-marker',naughty ? "holder" : ""], [null,true])) + "><div class=\"image-wrapper\"><img" + (jade.attr("src", imgSrc, true, false)) + (jade.attr("title", name, true, false)) + "/>");
if(naughty)
{
buf.push("<div class=\"naughty-overlay\"><div class=\"naughty-message\">" + (jade.escape(null == (jade_interp = _t("searchMap", "text.naughty_photo")) ? "" : jade_interp)) + "</div></div>");
}
buf.push("</div></div>");;return buf.join("");
};