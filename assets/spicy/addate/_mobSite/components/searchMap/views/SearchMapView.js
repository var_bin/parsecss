var SearchMapView = Backbone.View.extend({

    markers: [],

    initialize: function() {
        this.listenTo(Backbone, "showSearchMap", this.renderMap);
    },

    renderMap: function() {

        var self = this,
            attributes = self.model.get('attributes'),
            mapOptions = {
                panControl: false,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                scaleControl: false,
                mapTypeControl: false,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

        if (attributes.latitude && attributes.longitude) {
            // search in concrete city
            mapOptions.center = new google.maps.LatLng(attributes.latitude, attributes.longitude);
            mapOptions.zoom = 10;
            this.map = new google.maps.Map(document.getElementById("searchMap"), mapOptions);
        } else {
            // search in any city of specified country
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({address: attributes.countryName}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    mapOptions.center = results[0].geometry.location;
                    mapOptions.zoom = 6;
                    self.map = new google.maps.Map(document.getElementById("searchMap"), mapOptions);
                }
            });
        }

        this.clearMarkers();
        Resources.load('googleMapRichMarker', function() {
            _.each(self.model.collection.models, function(user){
                self.renderMarker(user);
            });
        });

        function handleNoGeolocation(errorFlag) {
            var options = {
                map: map,
                position: new google.maps.LatLng(attributes.latitude, attributes.longitude),
                content: errorFlag ? 'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.'
            };
            var infowindow = new google.maps.InfoWindow(options);
            map.setCenter(options.position);
        }

        var controlDiv = document.createElement('div');
        controlDiv.className = 'custom_geolocation';
        controlDiv.appendChild(document.createElement('span'));
        google.maps.event.addDomListener(controlDiv, 'click', function() {
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = new google.maps.LatLng(position.coords.latitude,
                        position.coords.longitude);
                    var infowindow = new google.maps.InfoWindow({
                        map: self.map,
                        position: pos,
                        content: '&nbsp;'
                    });
                    self.map.setCenter(pos);
                }, function() {
                    handleNoGeolocation(true);
                });
            } else {
                handleNoGeolocation(false);
            }
        });
        this.map.controls[google.maps.ControlPosition.LEFT_TOP].push(controlDiv);
    },

    renderMarker: function(user) {
        var self = this,
            userData = user.toJSON();

        marker = new RichMarker({
            position: new google.maps.LatLng(userData.geo.latitude, userData.geo.longitude),
            map: this.map,
            content: tpl.render('SearchMapMarker', {
                imgSrc: userData.photos.url,
                name: userData.login,
                naughty: self.isNaughty(user)
            })
        });

        google.maps.event.addListener(marker, 'click', function() {
            if (userData.promocode) {
                window.location.href = '/livecam/view/id/' + userData.login + '/s/1/promocode/' + userData.promocode;
            } else {
                app.router.navigate('/#user/view/id/' + userData.id, {
                    trigger: true
                });
            }
            return false;
        });

        this.markers.push(marker);
    },

    clearMarkers: function() {
        while (this.markers.length) {
            var marker = this.markers.pop();
            marker.onRemove();
        }
    },

    isNaughty: function(model) {
        var naughtyLevel = false;
        if(model.attributes.photos.count > 0 && model.attributes.photos.attributes) {
            naughtyLevel = app.appData().photoHolderInfo(model.attributes.photos.attributes.level).naughtyLevel
        }
        return (naughtyLevel) ? true : false;
    }

});
