$(function() {
    //prototype doesn't work on android properly fucking bitch
    //check user agent
    var userAgent = navigator.userAgent.toLowerCase();
    var isAndroid = userAgent.indexOf("android") > -1;
    if (isAndroid) {
        var androidVersion = parseFloat(userAgent.match(/android\s+([\d\.]+)/)[1]);
        if (androidVersion <= 2.3) {
            $('.form-box--payment').addClass('android-old-version');
        }
    }

    //init variables
    var $continue          = $('#continueButton');
    var $back              = $('#toolbarBack');
    var $backText          = $('#toolbarBackText');
    var $forms             = $("js-payment-form");
    var $packages          = $("[data-pay-package]");
    var $packagesIds       = $("input.js-package-input-id");
    var timeout            = null;
    var $creditCardForm    = $("#creditCardPaymentForm");
    var $smsForm           = $("#smsPaymentForm");
    var smsProcessing      = false;
    var $changePaymentDetails = $('#changePaymentDetails');

    /**
     * Hide form if is visible on browser history trigger
     */
    window.onpopstate = function() {
        if ($('.js-payment-form').is(":visible")) {
            triggerBackText();
            formHide();
        }
    };

    //show 3Day package description
    if ($('.active').attr('data-pay-package') == '3DAY1') {
        triggerPackageDesc($('.active'));
    }

    /**
     * Double click on package
     */
    $packages.on("dblclick", function(e) {
        var $el    = $(this);
        var formId = $el.attr('data-form');
        setActive($(e.currentTarget));

        if ($(this).hasClass('js-feature')) {
            return false;
        }

        triggerPackageDesc($el);
        triggerFullPrice($el);
        triggerXXXVideoText($el);
        boxClick(formId);
    });

    /**
     * Click on package
     */
    $packages.on("click", function(e) {
        var $el = $(this);
        //show features list
        if ($el.hasClass('js-feature')) {
            $('.benefits').hide();
            $el.next('.benefits').show();
        }

        triggerPackageDesc($el);
        triggerXXXVideoText($el);
        setActive($(e.currentTarget));
        if ($el.hasClass('clickablePackage')) {
            //Continue button click analog
            displayForm();
        }
    });

    $packages.each(function() {
        if ($(this).hasClass('active')) {
            $(this).next('.benefits').show();
        }
    });

    $('div.pay-one-click>a.pay-button').live('click', function(){
        var $el = $(this);
        if ($el.attr('clicked') !=1 ) {
            $el.attr('clicked',1);
            $("#creditCardPaymentForm").submit();
        }
    });
    /**
     * Continue button click
     */
    $continue.on('click', function(e) {
        e.stopPropagation();
        displayForm();
    });

    /**
     * Back toolbar button
     */
    $back.on("click", function() {

        if ($('.js-payment-form').is(':visible')) {
            triggerBackText();
            formHide();
            history.back();
            setPackageHash('');
        } else {
            window.location = $back.attr('data-back-url');
        }

        hidePaymentDetails(false);
    });

    $changePaymentDetails.on('click', function(e) {
        e.stopPropagation();
        displayForm();

        var hidePaymentForm = $('#CreditCardPaymentForm_hidePaymentForm');
        hidePaymentForm.val(1);
        $('#creditCardPaymentDetails').removeClass('hidden');

        hidePaymentDetails(true);
    });

    function hidePaymentDetails($hidden)
    {
        var $changeDetailForm = $('.change-detail-one-click');
        var $button = $('.pay-button-wrap.pay-one-click');
        if ($hidden) {
            $changeDetailForm.addClass('hidden');
            $button.addClass('hidden');
        } else {
            $changeDetailForm.removeClass('hidden');
            $button.removeClass('hidden');
        }
        $('#CreditCardPaymentForm_hidePaymentForm').val($hidden ? 0 : 1);
    }

    /**
     * Empty errors on keyup on payment forms
     */
    $forms.find("input[type=text], input[type=number]").on("keyup", function(e) {
        var $el = $(e.currentTarget).parent();
        if($el.hasClass("field-error")) {
            $el.removeClass("field-error")
        }

    });

    /**
     * Expire date select
     */
    $("[data-pay-select]").each(function() {
        var $el = $(this),
            $select = $el.find("select"),
            select = $select[0],
            $val = $el.find("[data-pay-select-value]");
        $select.on("change", function() {
            $val.html(select.options[select.selectedIndex].innerHTML);
        });
        $select.trigger("change");
    });

    /**
     * Check card number and show hide long form
     * Unused!
     */
    $creditCardForm.find("input[name='CreditCardPaymentForm[card_number]']").blur(function() {
        var formData = {
            'ajax':             'subscription',
            'validatorActions': 'getSourceType'
        };

        $('#creditCardPaymentForm').find('input[type="tel"], input[type="radio"], input[type="text"], input[type="hidden"], input[type="number"], select').each(function(){
            formData[$(this).attr('name')] = $(this).val();
        });

        $.ajax({
            url:        "/pay/pay",
            type:       "post",
            data:       formData,
            dataType:   "json",
            error:      function() {},
            success:    function(response) {
                if (response.getSourceType !== undefined) {
                    if (response.getSourceType.def !== undefined) return;
                    var longFields = $creditCardForm.find('.longOnly');
                    if (response.getSourceType.sourceType == 'SHORT') {
                        longFields.hide();
                    }else{
                        longFields.show();
                    }
                }
            },
            complete:   function(){}
        });
    }).keydown(allowMaxlengthOnly);

    /**
     * Fill first & last name from Card Holder name
     */
    $creditCardForm.find("input[name='CreditCardPaymentForm[card_holder]']").blur(function() {
        var nameBlock = $creditCardForm.find("input[name='CreditCardPaymentForm[name_first]']");
        if (nameBlock === undefined) return false;

        var surnameBlock = $creditCardForm.find("input[name='CreditCardPaymentForm[name_last]']");
        if (surnameBlock === undefined) return false;

        var cardHolder = $.trim($(this).val()).replace(/\s+/g, ' ');
        $(this).val(cardHolder);

        if (cardHolder == "") return false;

        var names = cardHolder.split(" ", 2);
        if (names.length > 1) {
            nameBlock.val(names[0]);
            surnameBlock.val(names[1]);
        } else {
            nameBlock.val(names[0]);
            surnameBlock.val(names[0]);
        }

        return true;
    }).keydown(function(event) {
        var keyCode = event.keyCode;

        return (keyCode <= 48 || keyCode >= 57);
    });

    /**
     * Check cvv code
     */
    $creditCardForm.find("input[name='CreditCardPaymentForm[security_number]']").keydown(allowMaxlengthOnly);

    /**
     * Change payment details on one click
     */
    $('#change_payment_details').click(function() {
        var hidePaymentForm = $('#CreditCardPaymentForm_hidePaymentForm');
        hidePaymentForm.val(hidePaymentForm.val() == 1 ? 0 : 1);
        $('#creditCardPaymentDetails').toggleClass('hidden');
    });

    /**
     * Disable pay button to prevent double click
     */
    $creditCardForm.live("submit", function() {
        $creditCardForm.find("input[type=submit]").attr("disabled", "disabled");
    });

    /**
     * Check cvv code
     */
    $smsForm.find("input[name='SmsPaymentForm[phone_number]']").keydown(allowNumbersOnly);

    /**
     * Submit of sms form
     */
    $smsForm.submit(function(){
        if (smsProcessing === true) {
            return false;
        }

        smsProcessing = true;

        var form        = $(this);
        $.ajax({
            url:        "/pay/sms/",
            type:       "post",
            data:       form.serialize(),
            dataType:   "json",
            error:      function() {},
            success:    function(response) {
                if (response.status == true) {
                    window.location.href = response.url;
                }
            },
            complete:   function() {
                smsProcessing = false;
            }
        });

        return false;
    });

    /**
     * Select operator for psms
     */
    $('div.field-select select').each(function() {
        if ($(this).val() != '') {
            $(this).closest('.field-select').children('div.field-select-value').text($(this).val());
        }
    }).change(function() {
        $(this).closest('.field-select').children('div.field-select-value').text($(this).val());
    });

    /**
     * Change sms package amount by operator
     */
    $('#psmsOperatorSelect').change(function() {
        var selectedOperator = $(this).val();

        if (smsOperatorsList[selectedOperator]) {
            var amount     = smsOperatorsList[selectedOperator].amount.toString().split('.');
            var curBlock   = $('.pay-data-box-single[data-form=paySmsForm].active');
            var priceBlock = curBlock.find('.pay-data-box-single__price .price');

            priceBlock.find('b').text(amount[0]);
            priceBlock.find('.fractional-price').text(amount[1]);
            $("#paySmsForm #packageAmount").html(smsOperatorsList[selectedOperator].amount);
            $('.weekly-psms-price').html(smsOperatorsList[selectedOperator].amount);
            var priceBox = $(".pay-data-box-single[data-pay-package='1DAY5'] .pay-data-box-single__price .price");
            priceBox.children('b').html(amount[0]);
            priceBox.children('.fractional-price').html(amount[1]);
            $(".pay-data-box-single[data-pay-package='1DAY5'] .packageChargePrice").val(currency + smsOperatorsList[selectedOperator].amount);
        }
    }).change();

    /**
     * Displaying Form and other form dependent elements
     */
    function displayForm()
    {
        $('.pay-data-box-single input[type=radio]').each(function() {
            if ($(this).is(':checked')) {
                var formId = $(this).parent('div').attr('data-form');
                hideAll();
                triggerFullPrice($(this).closest('div.pay-data-box-single'));
                boxClick(formId);
                setPackageHash($(this).attr('id'));
            }
        });
    }

    /**
     * Allow only numbers
     * @param event
     * @returns {boolean}
     */
    function allowNumbersOnly(event)
    {
        var keyCode = event.keyCode;

        return ((keyCode >= 8 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105));
    }

    /**
     * Allow only numbers
     * @param event
     * @returns {boolean}
     */
    function allowMaxlengthOnly(event)
    {
        var keyCode = event.keyCode;

        if (!allowNumbersOnly(event)) {
            return false;
        }

        return (keyCode >= 8 && keyCode < 48) || (($(this).val().length + 1) <= $(this).attr('maxlength'));
    }

    /**
     * Set active selected package
     *
     * @param $el
     */
    function setActive($el)
    {
        var $radio = $el.find("input[type='radio']");

        if(!$el.hasClass("active")) {
            $packages.removeClass("active");
            $packagesIds.attr("checked", false);
            $el.addClass("active");
            $radio.attr("checked", true);
        }
    }

    /**
     * Show form with trigger toolbar text
     *
     * @param formId
     */
    function boxClick(formId)
    {
        if (formId == 'paySmsForm') {
            $(".cc-payment-info").addClass("hidden");
        }
        if (!$('#'+formId).is(':visible')) {
            clearTimeout(timeout);
            timeout = setTimeout(function() {
                formShow(formId);
                triggerBackText();
            }, 200);
        }
    }

    /**
     * Show form and hide all packages except current
     *
     * @param formId
     */
    function formShow(formId)
    {
        history.pushState({'form':formId}, formId, null);

        hideAll();
        $('#'+formId).removeClass("hidden");
    }

    /**
     * Hide forms and show packages
     */
    function formHide()
    {
        //Hide forms
        $('.js-payment-form').addClass('hidden');
        showAll();
    }

    /**
     * Show discount & packages & continue button & payment statement & compliance
     */
    function showAll()
    {
        $(".membership-discount-payment").removeClass("hidden");
        $('.pay-data-box-single').removeClass('hidden');
        $('.benefits').hide();
        $(".js-feature.active").next('.benefits').show();
        $(".pay-data-box-title").removeClass("hidden");
        $('#continueButton').parent('div').removeClass('hidden');
        $('.cc-payment-info').removeClass('hidden');
    }

    /**
     * Hide all packages & discount & titles & continue button
     */
    function hideAll()
    {
        var packageSelector = '.pay-data-box-single' + ($packages.hasClass('clickablePackage') ? ':not(.active)': '');
        $(packageSelector).addClass('hidden');
        $('.benefits').hide();
        $('.payment-form').addClass('hidden');
        $(".pay-data-box-title").addClass("hidden");
        $('#continueButton').parent('div').addClass('hidden');
        $(".membership-discount-payment").addClass("hidden");
    }

    /**
     * Change text on back button in toolbar
     */
    function triggerBackText()
    {
        var text1 = $back.attr('data-trigger-text');
        var text2 = $backText.html();
        $back.attr('data-trigger-text', text2);
        $backText.html(text1);
    }

    /**
     * Show description to selected package if exists
     * @param $el
     */
    function triggerPackageDesc($el)
    {
        var packageType = $el.attr('data-pay-package');
        $('.js-package-description').addClass('hidden');
        $('#'+packageType+'_description').removeClass('hidden');
    }

    /**
     * Fill full amount on payment form
     * @param $el
     */
    function triggerFullPrice($el)
    {
        $("#packageAmount, .packageAmount").html($el.find(".packageChargePrice").val());
        $(".you-will-pay-block").html($el.find(".youWillPayBlock").html());
    }

    function triggerXXXVideoText($el)
    {
        if ($el.attr('data-form') == 'payForm') {
            $(".info-vod").show();
        } else {
            $(".info-vod").hide();
        }
    }

    function setPackageHash(packageId)
    {
        location.hash = 'packageId=' + packageId.replace(/^package/, '');
    }

    //Show filled form after decline page
    var hashValue = location.hash;
    hashValue = hashValue.replace(/^#/, '').split('=');

    if (hashValue.length > 0 && hashValue[0] == 'packageId' && hashValue[1].length > 0) {
        var pkgId = 'package'+hashValue[1];
        var activePkg = $('#'+pkgId).parent('div');
        setActive(activePkg);
        activePkg.dblclick();
    }
});
