this["JST"] = this["JST"] || {};

this["JST"]["Profile"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,model = locals_.model,user = locals_.user;
var months = {
1: _t("profile", "text.january"),
2: _t("profile", "text.february"),
3: _t("profile", "text.march"),
4: _t("profile", "text.april"),
5: _t("profile", "text.may"),
6: _t("profile", "text.june"),
7: _t("profile", "text.july"),
8: _t("profile", "text.august"),
9: _t("profile", "text.september"),
10: _t("profile", "text.october"),
11: _t("profile", "text.november"),
12: _t("profile", "text.december")
};
var selected = function(current, i) {
current = parseInt(current, 10)
return (current == i);
}
jade_mixins["select"] = function(dataName, data, start, end, month){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div data-select=\"data-select\" class=\"field-select\"><div data-select-value=\"data-select-value\" class=\"field-select-value\"></div><div class=\"field-select-control\"><select" + (jade.attr("data-name", dataName, true, false)) + ">");
for(var i= start; i <= end; i++)
{
if(selected(data, i))
{
buf.push("<option" + (jade.attr("value", i, true, false)) + " selected=\"selected\">" + (jade.escape((jade_interp = month ? months[i] : i) == null ? '' : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", i, true, false)) + ">" + (jade.escape((jade_interp = month ? months[i] : i) == null ? '' : jade_interp)) + "</option>");
}
}
buf.push("</select></div></div>");
};
jade_mixins["selectInline"] = function(dataName, data, start, end){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div data-select=\"data-select\"" + (jade.attr("id", dataName, true, false)) + " class=\"field-select field-select-inline\"><div data-select-value=\"data-select-value\" class=\"field-select-value\"></div><div class=\"field-select-control\"><select" + (jade.attr("data-name", dataName, true, false)) + ">");
for(var i= start; i <= end; i++)
{
if(selected(data, i))
{
buf.push("<option" + (jade.attr("value", i, true, false)) + " selected=\"selected\">" + (jade.escape((jade_interp = i) == null ? '' : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", i, true, false)) + ">" + (jade.escape((jade_interp = i) == null ? '' : jade_interp)) + "</option>");
}
}
buf.push("</select></div></div>");
};
jade_mixins["selectParams"] = function(dataName, options, current){
var block = (this && this.block), attributes = (this && this.attributes) || {};
options = options ? options : model.userAttributesDictionaries.fields[dataName];
current = current ? current : model.profile[dataName];
buf.push("<div data-select=\"data-select\" class=\"field-select\"><div data-select-value=\"data-select-value\" class=\"field-select-value\"></div><div class=\"field-select-control\"><select" + (jade.attr("data-name", dataName, true, false)) + ">");
// iterate options
;(function(){
  var $$obj = options;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

if(selected(current, key))
{
buf.push("<option" + (jade.attr("value", key, true, false)) + " selected=\"selected\">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", key, true, false)) + ">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

if(selected(current, key))
{
buf.push("<option" + (jade.attr("value", key, true, false)) + " selected=\"selected\">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", key, true, false)) + ">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
    }

  }
}).call(this);

buf.push("</select></div></div>");
};
buf.push("<div id=\"openx-banner-placement-profiletop\" class=\"profiletop-banner-openx\"></div><div id=\"Profile\" class=\"my-profile\"><div class=\"mpr-header cf\"><div class=\"usv-info\"><div id=\"profilePhoto\"></div><div id=\"goToProfilePhotos\" class=\"details\"><div class=\"user-info\"><div class=\"status online\"></div><div id=\"photos_button\" class=\"photos\">" + (jade.escape(null == (jade_interp = _t("profile", "text.photos")) ? "" : jade_interp)) + "<span class=\"photo-count\">" + (jade.escape((jade_interp = model.myPhotos.photoCount) == null ? '' : jade_interp)) + "</span></div><div class=\"name\"><span class=\"name-wrap\"><span class=\"name-inner\"><a href=\"#\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + "</a></span></span></div></div></div></div><section class=\"mpr-section cf\"><div id=\"profileDate\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label label-age\"><span>" + (jade.escape(null == (jade_interp = _t("profile", "text.years", {'{n}': model.profile.age})) ? "" : jade_interp)) + "</span></div><div class=\"field\"><div class=\"fbx-item fbx-table\"><table><tbody><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-month") + ":") ? "" : jade_interp)) + "</label></td><td>");
jade_mixins["select"]('birthdayDate.month', model.profile.birthdayDate.month, 1, 12, true);
buf.push("</td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-day") + ":") ? "" : jade_interp)) + "</label></td><td>");
jade_mixins["select"]('birthdayDate.day', model.profile.birthdayDate.day, 1, 31, false);
buf.push("</td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-year") + ":") ? "" : jade_interp)) + "</label></td><td>");
jade_mixins["select"]('birthdayDate.year', model.profile.birthdayDate.year, 1935, 1996, false);
buf.push("</td></tr></tbody></table></div></div></div><div id=\"profileLocated\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label label-away\"><span>" + (jade.escape((jade_interp = model.profile.location) == null ? '' : jade_interp)) + "</span></div><div class=\"field\"><div class=\"fbx-item fbx-table\"><table><tbody><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-located") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-panel-testarea\"><div class=\"fbx-table-field\"><div class=\"field-text\"><div data-ui-location=\"data-ui-location\" class=\"field location\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"><input data-name=\"location\" type=\"text\"" + (jade.attr("value", model.profile.location, true, false)) + " data-select-location=\"no\" class=\"input-value\"/></div><div data-list=\"data-list\" class=\"select location\"></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div></div></td></tr></tbody></table></div></div></div><div id=\"profileMyMind\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label text-area\">" + (jade.escape(null == (jade_interp = _t("profile", "text.what-on-my-mind")) ? "" : jade_interp)) + "</div><div class=\"field\"><div class=\"fbx-panel-testarea\"><textarea data-name=\"chat_up_line\" class=\"text-input\">" + (jade.escape((jade_interp = model.profile.chat_up_line) == null ? '' : jade_interp)) + "</textarea></div></div></div><div id=\"profileMyWords\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label text-area\">" + (jade.escape(null == (jade_interp = _t("profile", "text.in-my-own-words")) ? "" : jade_interp)) + "</div><div class=\"field\"><div class=\"fbx-panel-testarea\"><textarea data-name=\"description\" class=\"text-input\">" + (jade.escape((jade_interp = model.profile.description) == null ? '' : jade_interp)) + "</textarea></div></div></div><div id=\"profileAboutMe\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label\">" + (jade.escape(null == (jade_interp = _t("profile", "text.about-me")) ? "" : jade_interp)) + "</div><div class=\"field\"><div class=\"fbx-item fbx-table\"><table><tbody><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-orientation") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('sexual_orientation');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-marital-status") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('marital_status');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-children") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('children');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-living") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('living');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-race") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('race');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-religion") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('religion');
buf.push("</div></td></tr></tbody></table></div></div></div><div id=\"profileParameters\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label\">" + (jade.escape(null == (jade_interp = _t("profile", "text.physical-parameters")) ? "" : jade_interp)) + "</div><div class=\"field\"><div class=\"fbx-item fbx-table\"><table><tbody><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.physical-parameters") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('build');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-height") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('height');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-weight") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('weight');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-hair-color") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('hair_color');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-eyes-color") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('eye_color');
buf.push("</div></td></tr></tbody></table></div></div></div><div id=\"profileLifeStyle\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label\">" + (jade.escape(null == (jade_interp = _t("profile", "text.life-style")) ? "" : jade_interp)) + "</div><div class=\"field\"><div class=\"fbx-item fbx-table\"><table><tbody><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-tattoo") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('tattoo');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-piercing") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('pierced');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "title.income") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('income');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-smoke") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('smoke');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "text.select-drink") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('drink');
buf.push("</div></td></tr></tbody></table></div></div></div><div id=\"profileLookingFor\" data-accordion=\"data-accordion\" class=\"fbx-panel fbx-panel-collapse\"><div data-accordion-label=\"data-accordion-label\" class=\"label\">" + (jade.escape(null == (jade_interp = _t("profile", "text.looking-for")) ? "" : jade_interp)) + "</div><div class=\"field\"><div class=\"fbx-item fbx-table\"><table><tbody><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "title.gender") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('lookingForGender', model.userAttributesDictionaries.fields.gender, model.profile.lookingFor.gender);
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "title.age") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectInline"]('lookingForAge[from]', model.profile.lookingFor.age.from, 18, 78);
buf.push("<div class=\"field-select-separator\">to</div>");
jade_mixins["selectInline"]('lookingForAge[to]', model.profile.lookingFor.age.to, 18, 78);
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "title.city") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\"><div class=\"field-text\"><input data-name=\"lookingForLocation\" type=\"text\"" + (jade.attr("value", model.profile.lookingFor.location, true, false)) + " data-select-location=\"no\"/></div></div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("profile", "title.search_radius") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('lookingForDistance', model.userAttributesDictionaries.fields.distances, model.profile.lookingFor.distance);
buf.push("</div></td></tr></tbody></table></div></div></div><div data-checkbox=\"data-checkbox\" class=\"fbx-list-padding border-top\"><div id=\"coreg-placement-advanced-activity\" class=\"fbx-item fbx-field-check coreg-placement-on-advanced-activity\"></div></div><div class=\"fbx-item fbx-field-submit\"><button id=\"profileSave\" class=\"button\">" + (jade.escape(null == (jade_interp = _t("profile", "text.save")) ? "" : jade_interp)) + "</button></div></section></div></div><div id=\"profilePhotoList\" style=\"display: none;\"></div>");;return buf.join("");
};