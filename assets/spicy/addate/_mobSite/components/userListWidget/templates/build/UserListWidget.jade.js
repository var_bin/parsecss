this["JST"] = this["JST"] || {};

this["JST"]["UserListWidget"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user,naughtyMode = locals_.naughtyMode,userLink = locals_.userLink,_t = locals_._t,config = locals_.config;
var userClass = [];
if(user.marks && user.marks.highlight_profile && !user.livecam) userClass.push("b-user-highlight")
if(user.livecam) userClass.push("livecam")
buf.push("<div" + (jade.cls(['srh-user','cf',userClass], [null,null,true])) + ">");
if ( (user.livecam))
{
buf.push("<span class=\"livecams-ico-top\"><span class=\"livecams-ico-block\"></span><span class=\"livecams-ico\"><span class=\"livecams-ico-inner\"><span class=\"livecams-ico-handler\"></span><span class=\"livecams-ico-lines\"></span></span><span class=\"livecams-ico-arrow\"></span></span></span>");
}
buf.push("<div data-image-grid-item=\"1\"" + (jade.cls(['pic','portrait',naughtyMode.naughtyClass], [null,null,true])) + "><a" + (jade.attr("href", (user.promocode ? '/livecam/view/id/' + user.login + '/s/1/promocode/' + user.promocode : userLink(user.id)), true, false)) + " data-cached-search=\"data-cached-search\"><img" + (jade.attr("src", user.photos.url, true, false)) + "/>");
if(naughtyMode.naughtyLevel == 1)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.naughty_photo")) ? "" : jade_interp)) + "</span>");
}
if(naughtyMode.naughtyLevel == 2)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.explicit_nudity")) ? "" : jade_interp)) + "</span>");
}
buf.push("</a><div class=\"tags\">");
if(user.statuses && user.statuses.newStatus)
{
buf.push("<div class=\"tag new\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.new")) ? "" : jade_interp)) + "</div>");
}
buf.push("</div>");
if(user.statuses.onlineStatus)
{
buf.push("<div class=\"status online\"></div>");
}
if(user.photos.showCount)
{
buf.push("<div class=\"pic-counter-holder\"><div class=\"pic-counter\">" + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "</div></div>");
}
buf.push("</div><div class=\"details\"><div class=\"user-info cf\"><div class=\"name\"><span class=\"name-wrap mob\"><span class=\"name-inner\"><a" + (jade.attr("href", (user.promocode ? '/livecam/view/id/' + user.login + '/s/1/promocode/' + user.promocode : userLink(user.id)), true, false)) + " data-cached-search=\"data-cached-search\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</a></span></span></div><div class=\"age\">" + (jade.escape(null == (jade_interp = user.age) ? "" : jade_interp)) + "</div>");
if ( (!user.livecam))
{
buf.push("<div class=\"from\">" + (jade.escape((jade_interp = user.geo.city) == null ? '' : jade_interp)) + " | " + (jade.escape((jade_interp = user.geo.country) == null ? '' : jade_interp)) + "</div>");
}
buf.push("<div class=\"user-activity\">");
if(user.activity && user.activity.action)
{
if(user.activity.action == "newsFeed")
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("userListWidget","text.notification_newsFeed_"+user.activity.type+"_"+user.activity.txtId+"_"+user.gender)) ? "" : jade_interp)) + "</span>");
}
else if(user.activity.type == "outgoing")
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("userListWidget","text.you_"+user.activity.action+"_user_w_time", {"{n}":user.gender, "{time}":user.activity.time})) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("userListWidget","text.user_"+user.activity.action+"_you_w_time", {"{n}":user.gender, "{time}":user.activity.time})) ? "" : jade_interp)) + "</span>");
}
}
else if (user.livecam)
{
buf.push("<div class=\"b-live-cam\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.on_cam_now")) ? "" : jade_interp)) + "</div><div class=\"b-live-cam-sponsor\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.this_is_cam_profile")) ? "" : jade_interp)) + "</div>");
}
else if(typeof config === 'undefined' || config.showTextNoActivity)
{
buf.push(jade.escape(null == (jade_interp = _t("userListWidget", "text.no_activity")) ? "" : jade_interp));
}
buf.push("</div></div>");
if ( (!user.livecam))
{
buf.push("<div class=\"user-buttons\"><a data-messagebutton=\"\"" + (jade.attr("href", '/#chat/with/'+user.id, true, false)) + " data-cached-search=\"data-cached-search\" class=\"usr-action message\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "title.chat")) ? "" : jade_interp)) + "</a><button" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.cls(['usr-action','wink',(user.buttons.wink.activated)?"active":""], [null,null,true])) + "><span class=\"initial_state\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.wink")) ? "" : jade_interp)) + "</span><span class=\"final_state\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.winked")) ? "" : jade_interp)) + "</span></button><button" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.cls(['usr-action','favourite',(user.buttons.favorite.activated)?"active":""], [null,null,true])) + "><span class=\"initial_state\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.add_friend")) ? "" : jade_interp)) + "</span><span class=\"final_state\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.friends")) ? "" : jade_interp)) + "</span></button></div>");
}
else
{
buf.push("<div class=\"user-buttons\"><a" + (jade.attr("href", (user.promocode ? '/livecam/view/id/' + user.login + '/s/1/promocode/' + user.promocode : userLink(user.id)), true, false)) + " class=\"livecam-ico-link\"><span class=\"livecam-ico-button\"><span class=\"livecams-ico\"><span class=\"livecams-ico-inner\"><span class=\"livecams-ico-handler\"></span><span class=\"livecams-ico-lines\"></span></span><span class=\"livecams-ico-arrow\"></span></span></span><span class=\"livecam-ico-button-text\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.watch_live_sex_show")) ? "" : jade_interp)) + "</span></a></div>");
}
buf.push("</div></div>");;return buf.join("");
};