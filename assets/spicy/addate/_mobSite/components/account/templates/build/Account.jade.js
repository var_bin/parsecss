this["JST"] = this["JST"] || {};

this["JST"]["Account"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,upgradeButton = locals_.upgradeButton,hasBillingHistory = locals_.hasBillingHistory,paymentStatus = locals_.paymentStatus,userId = locals_.userId,l10n = locals_.l10n,email = locals_.email;
jade_mixins["checkbox"] = function(name, checked, title){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div data-element=\"checkbox\" class=\"fbx-item fbx-field-check\"><div data-check=\"data-check\" class=\"field-check\"><label><input type=\"checkbox\"" + (jade.attr("name", name, true, false)) + (jade.attr("checked", checked, true, false)) + "/></label><div class=\"switcher\"><span>ON</span><span>OFF</span></div></div><label class=\"field-label\">" + (jade.escape(null == (jade_interp = _t("account", title)) ? "" : jade_interp)) + "</label></div>");
};
buf.push("<div id=\"openx-banner-placement-accounttop\" class=\"accounttop-banner-openx\"></div><div class=\"form-box\"><div id=\"accountMenu\" class=\"settinds-menu\"><div class=\"fbx-item fbx-info\"><ul><li class=\"cf\"><div class=\"info-button\">");
if(upgradeButton.type == 'main' || upgradeButton.type == 'features')
{
buf.push("<a" + (jade.attr("href", upgradeButton.url, true, false)) + " data-role=\"upgrade-button\" class=\"button upgrade\">" + (jade.escape(null == (jade_interp = _t("account", "button.upgrade_free")) ? "" : jade_interp)) + "</a>");
}
buf.push("</div>");
if ((hasBillingHistory === true))
{
buf.push("<div class=\"info-button\"><a href=\"/#billingHistory\" class=\"button history\">" + (jade.escape(null == (jade_interp = _t("account", "button.billing_history")) ? "" : jade_interp)) + "</a></div>");
}
buf.push("<div class=\"info-control\"><span>" + (jade.escape(null == (jade_interp = _t("account", "title.status") + ": ") ? "" : jade_interp)) + (jade.escape(null == (jade_interp = _t("account", "title."+ paymentStatus.type)) ? "" : jade_interp)) + "</span></div></li><li class=\"cf\"><div class=\"info-control\"><span>" + (jade.escape(null == (jade_interp = _t("account", "title.user_id") + ": " + userId) ? "" : jade_interp)) + "</span></div></li><li class=\"cf\"><div class=\"info-control\"><span>" + (jade.escape(null == (jade_interp = _t("account", "title.language")) ? "" : jade_interp)) + "</span></div><div class=\"field-inner\"><div data-select-value=\"data-select-value\" class=\"form-box-select-wrap\"><select id=\"profileLanguageSelect\" name=\"profileLanguageSelect\">");
if ( l10n.languagesList)
{
// iterate l10n.languagesList
;(function(){
  var $$obj = l10n.languagesList;
  if ('number' == typeof $$obj.length) {

    for (var language = 0, $$l = $$obj.length; language < $$l; language++) {
      var title = $$obj[language];

if(language == l10n.locale)
{
buf.push("<option" + (jade.attr("value", language, true, false)) + " selected=\"selected\">" + (jade.escape(null == (jade_interp = _t("account", "locale."+title)) ? "" : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", language, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("account", "locale."+title)) ? "" : jade_interp)) + "</option>");
}
    }

  } else {
    var $$l = 0;
    for (var language in $$obj) {
      $$l++;      var title = $$obj[language];

if(language == l10n.locale)
{
buf.push("<option" + (jade.attr("value", language, true, false)) + " selected=\"selected\">" + (jade.escape(null == (jade_interp = _t("account", "locale."+title)) ? "" : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", language, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("account", "locale."+title)) ? "" : jade_interp)) + "</option>");
}
    }

  }
}).call(this);

}
buf.push("</select><span class=\"value\"><span id=\"selectedLanguage\">" + (jade.escape(null == (jade_interp = _t("account", "locale."+l10n.languagesList[l10n.locale])) ? "" : jade_interp)) + "</span></span></div></div><button id=\"accountLanguageSave\" class=\"button button-save\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</button></li></ul></div><div class=\"fbx-item fbx-list\"><ul><li><a data-page=\"accountChangePassword\" class=\"list-control\">" + (jade.escape(null == (jade_interp = _t("account", "title.change_password")) ? "" : jade_interp)) + "</a></li><li><a data-page=\"accountChangeEmail\" class=\"list-control\">" + (jade.escape(null == (jade_interp = _t("account", "title.change_email")) ? "" : jade_interp)) + "</a></li><li><a data-page=\"accountActivityNotifications\" class=\"list-control\">" + (jade.escape(null == (jade_interp = _t("account", "title.new_activity_notifications")) ? "" : jade_interp)) + "</a></li><li><a data-page=\"accountEmailNotifications\" class=\"list-control\">" + (jade.escape(null == (jade_interp = _t("account", "email_or_sms_notification.email")) ? "" : jade_interp)) + "</a></li><li><a data-page=\"accountPushNotifications\" class=\"list-control\">" + (jade.escape(null == (jade_interp = _t("account", "email_or_sms_notification.push")) ? "" : jade_interp)) + "</a></li><li><a data-page=\"accountMobileNotifications\" class=\"list-control\">" + (jade.escape(null == (jade_interp = _t("account", "email_or_sms_notification.sms")) ? "" : jade_interp)) + "</a></li><li class=\"no-before\"><a href=\"/site/logout\" class=\"list-control\">" + (jade.escape(null == (jade_interp = _t("account", "button.logout")) ? "" : jade_interp)) + "</a></li></ul></div><div class=\"fbx-item fbx-list\"><div id=\"coreg-placement-account-settings\" class=\"coreg-placement-on-account\"></div></div></div><div id=\"accountChangePassword\" data-page-content=\"data-page-content\" class=\"hidden\"><div class=\"change-password\"><div class=\"form-box\"><div class=\"bx-item fbx-field-text\"><label>" + (jade.escape(null == (jade_interp = _t("account", "title.old_password") + "s:") ? "" : jade_interp)) + "</label><div class=\"field-inner\"><input id=\"oldPassword\" type=\"password\"/><div data-error=\"oldPassword\" class=\"field-hint-error\"></div></div></div><div class=\"bx-item fbx-field-text\"><label>" + (jade.escape(null == (jade_interp = _t("account", "title.new_password") + ":") ? "" : jade_interp)) + "</label><div class=\"field-inner\"><input id=\"newPassword\" type=\"password\"/><div data-error=\"newPassword\" class=\"field-hint-error\"></div></div></div><div class=\"bx-item fbx-field-check-holder\">");
jade_mixins["checkbox"]('ShowPassword', false, _t("account", "text.show_password"));
buf.push("</div></div><div class=\"fbx-item fbx-field-submit\"><button id=\"accountPasswordSave\" class=\"button\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</button></div></div></div><div id=\"accountChangeEmail\" data-page-content=\"data-page-content\" class=\"hidden\"><div class=\"change-email\"><div class=\"form-box\"><div id=\"accountEmail\" class=\"fbx-item fbx-email\">" + (jade.escape((jade_interp = email) == null ? '' : jade_interp)) + "</div><div class=\"bx-item fbx-field-text\"><label>" + (jade.escape(null == (jade_interp = _t("account", "title.your_password") + ":") ? "" : jade_interp)) + "</label><div class=\"field-inner\"><input id=\"password\" type=\"password\"/><div data-error=\"password\" class=\"field-hint-error\"></div></div></div><div class=\"bx-item fbx-field-text\"><label>" + (jade.escape(null == (jade_interp = _t("account", "title.new_email_address") + ":") ? "" : jade_interp)) + "</label><div class=\"field-inner\"><input id=\"email\" type=\"text\"/><div data-error=\"email\" class=\"field-hint-error\"></div></div></div></div><div class=\"fbx-item fbx-field-submit\"><button id=\"accountEmailSave\" class=\"button\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</button></div></div></div><div id=\"accountActivityNotifications\" data-page-content=\"data-page-content\" class=\"hidden\"><div class=\"activity-notifications\"><div id=\"notificationSettings\"></div><div id=\"notificationNewsFeed\"></div><div id=\"coreg-placement-activity-notifications\" class=\"coreg-placement-on-account\"></div><div class=\"fbx-item fbx-field-submit\"><button id=\"accountNotificationsSave\" class=\"button\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</button></div></div></div><div id=\"accountEmailNotifications\" data-page-content=\"data-page-content\" class=\"hidden\"></div><div id=\"accountPushNotifications\" data-page-content=\"data-page-content\" class=\"hidden\"></div><div id=\"accountMobileNotifications\" data-page-content=\"data-page-content\" class=\"hidden\"></div></div><div id=\"openx-banner-placement-accountbottom\" class=\"accountbottom-banner-openx\"></div>");;return buf.join("");
};