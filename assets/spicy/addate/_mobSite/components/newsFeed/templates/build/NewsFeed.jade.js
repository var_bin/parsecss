this["JST"] = this["JST"] || {};

this["JST"]["NewsFeed"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,date = locals_.date,model = locals_.model,chatLink = locals_.chatLink,userLink = locals_.userLink;
var months = {
1: _t("newsFeed", "text.january"),
2: _t("newsFeed", "text.february"),
3: _t("newsFeed", "text.march"),
4: _t("newsFeed", "text.april"),
5: _t("newsFeed", "text.may"),
6: _t("newsFeed", "text.june"),
7: _t("newsFeed", "text.july"),
8: _t("newsFeed", "text.august"),
9: _t("newsFeed", "text.september"),
10: _t("newsFeed", "text.october"),
11: _t("newsFeed", "text.november"),
12: _t("newsFeed", "text.december")
};
var days = {
1: _t("newsFeed", "text.monday"),
2: _t("newsFeed", "text.tuesday"),
3: _t("newsFeed", "text.wednesday"),
4: _t("newsFeed", "text.thursday"),
5: _t("newsFeed", "text.friday"),
6: _t("newsFeed", "text.saturday"),
7: _t("newsFeed", "text.sunday")
};
for (date in model)
{
date = date.split(',');
buf.push("<div class=\"activity-time\">");
if ( date.length > 1)
{
buf.push("<span class=\"day\">" + (jade.escape((jade_interp = date[0]) == null ? '' : jade_interp)) + " " + (jade.escape((jade_interp = months[date[1]]) == null ? '' : jade_interp)) + ", " + (jade.escape((jade_interp = days[date[2]]) == null ? '' : jade_interp)) + "</span>");
}
buf.push("</div>");
// iterate model[date]
;(function(){
  var $$obj = model[date];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var activity = $$obj[$index];

buf.push("<div" + (jade.cls(['activity-item',activity.action], [null,true])) + ">");
if (activity.isUserAdmin || activity.action == 'mail')
{
var link = chatLink(activity.user_id);
}
else
{
var link = userLink(activity.user_id);
}
buf.push("<a" + (jade.attr("href", ( activity.link ? activity.link : link ), true, false)) + (jade.attr("target", ( activity.link ? '_blank' : '_self'), true, false)) + " class=\"activity cf\"><div" + (jade.cls(['photo',activity.naughtyMode.naughtyClass], [null,true])) + "><img" + (jade.attr("src", activity.photo, true, false)) + "/>");
if(activity.naughtyMode.naughtyLevel == 1)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("newsFeed", "text.naughty_photo")) ? "" : jade_interp)) + "</span>");
}
if(activity.naughtyMode.naughtyLevel == 2)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("newsFeed", "text.explicit_nudity")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div></a><div class=\"time\"><div class=\"activity-icon\"></div></div><div class=\"activity-text\"><div class=\"activity-text-wrap\">");
if (activity.action == 'webcam')
{
buf.push("<a" + (jade.attr("href", (activity.link ? activity.link : link), true, false)) + (jade.attr("target", (activity.link ? '_blank' : '_self'), true, false)) + " class=\"screenname\">" + (jade.escape(null == (jade_interp = activity.screenname + ', ' + activity.age) ? "" : jade_interp)) + "</a>&nbsp;<span class=\"activity-name\">Video chat request</span>");
}
else
{
buf.push("<a" + (jade.attr("href", link, true, false)) + " class=\"screenname\">" + (jade.escape((jade_interp = activity.screenname) == null ? '' : jade_interp)) + " &nbsp;<span class=\"activity-name\">" + (jade.escape(null == (jade_interp = _t('newsFeed', 'newsFeed.'+activity.gender+'.'+activity.action)) ? "" : jade_interp)) + "</span></a>");
}
buf.push("</div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var activity = $$obj[$index];

buf.push("<div" + (jade.cls(['activity-item',activity.action], [null,true])) + ">");
if (activity.isUserAdmin || activity.action == 'mail')
{
var link = chatLink(activity.user_id);
}
else
{
var link = userLink(activity.user_id);
}
buf.push("<a" + (jade.attr("href", ( activity.link ? activity.link : link ), true, false)) + (jade.attr("target", ( activity.link ? '_blank' : '_self'), true, false)) + " class=\"activity cf\"><div" + (jade.cls(['photo',activity.naughtyMode.naughtyClass], [null,true])) + "><img" + (jade.attr("src", activity.photo, true, false)) + "/>");
if(activity.naughtyMode.naughtyLevel == 1)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("newsFeed", "text.naughty_photo")) ? "" : jade_interp)) + "</span>");
}
if(activity.naughtyMode.naughtyLevel == 2)
{
buf.push("<span class=\"b-message-holder\">" + (jade.escape(null == (jade_interp = _t("newsFeed", "text.explicit_nudity")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div></a><div class=\"time\"><div class=\"activity-icon\"></div></div><div class=\"activity-text\"><div class=\"activity-text-wrap\">");
if (activity.action == 'webcam')
{
buf.push("<a" + (jade.attr("href", (activity.link ? activity.link : link), true, false)) + (jade.attr("target", (activity.link ? '_blank' : '_self'), true, false)) + " class=\"screenname\">" + (jade.escape(null == (jade_interp = activity.screenname + ', ' + activity.age) ? "" : jade_interp)) + "</a>&nbsp;<span class=\"activity-name\">Video chat request</span>");
}
else
{
buf.push("<a" + (jade.attr("href", link, true, false)) + " class=\"screenname\">" + (jade.escape((jade_interp = activity.screenname) == null ? '' : jade_interp)) + " &nbsp;<span class=\"activity-name\">" + (jade.escape(null == (jade_interp = _t('newsFeed', 'newsFeed.'+activity.gender+'.'+activity.action)) ? "" : jade_interp)) + "</span></a>");
}
buf.push("</div></div></div>");
    }

  }
}).call(this);

};return buf.join("");
};