this["JST"] = this["JST"] || {};

this["JST"]["NewsFeedLayout"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<div id=\"openx-banner-placement-activitytop\" class=\"activitytop-banner-openx\"></div><div class=\"page-activity\"><div class=\"urv-activity\"><div id=\"newsFeedHistory\" class=\"activity-history\"></div></div><div id=\"noActivity\" class=\"no-activity\"><p>" + (jade.escape(null == (jade_interp = _t("newsFeed", "newsFeed.No_activity_yet")) ? "" : jade_interp)) + "</p></div></div><div class=\"banner-bottom\"><div id=\"openx-banner-placement-activitybottom\" class=\"activitybottom-banner-openx\"></div></div><div class=\"banner-bottom-height\"></div>");;return buf.join("");
};