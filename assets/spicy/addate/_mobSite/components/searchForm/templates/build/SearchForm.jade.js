this["JST"] = this["JST"] || {};

this["JST"]["SearchForm"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),params = locals_.params,_t = locals_._t,searchTypes = locals_.searchTypes;
function selected(current, i) {
current = parseInt(current, 10)
return (current == i);
}
jade_mixins["selectInline"] = function(name, data, start, end, fromTo){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div" + (jade.attr("data-select", fromTo, true, false)) + " class=\"field-select field-select-inline\"><div data-select-value=\"data-select-value\" class=\"field-select-value\"></div><div class=\"field-select-control\"><select" + (jade.attr("name", name, true, false)) + ">");
for(var i= start; i <= end; i++)
{
if(selected(data, i))
{
buf.push("<option" + (jade.attr("value", i, true, false)) + " selected=\"selected\">" + (jade.escape((jade_interp = i) == null ? '' : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", i, true, false)) + ">" + (jade.escape((jade_interp = i) == null ? '' : jade_interp)) + "</option>");
}
}
buf.push("</select></div></div>");
};
jade_mixins["selectParams"] = function(name, options, current){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div data-select=\"data-select\" class=\"field-select\"><div data-select-value=\"data-select-value\" class=\"field-select-value\"></div><div class=\"field-select-control\"><select" + (jade.attr("name", name, true, false)) + ">");
// iterate options
;(function(){
  var $$obj = options;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

if(selected(current, key))
{
buf.push("<option" + (jade.attr("value", key, true, false)) + " selected=\"selected\">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", key, true, false)) + ">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

if(selected(current, key))
{
buf.push("<option" + (jade.attr("value", key, true, false)) + " selected=\"selected\">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
else
{
buf.push("<option" + (jade.attr("value", key, true, false)) + ">" + (jade.escape((jade_interp = value) == null ? '' : jade_interp)) + "</option>");
}
    }

  }
}).call(this);

buf.push("</select></div></div>");
};
buf.push("<form id=\"searchForm\"><input type=\"hidden\" name=\"country\"" + (jade.attr("value", params.country, true, false)) + "/><div class=\"fbx-item fbx-title\">" + (jade.escape(null == (jade_interp = _t("searchForm", "text.search-parameters")) ? "" : jade_interp)) + "</div><div class=\"fbx-item fbx-table\"><table><tbody><tr><td><label>" + (jade.escape(null == (jade_interp = _t("searchForm", "text.select-gender") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('gender', {1: _t("searchForm", "text.select-male"), 2: _t("searchForm", "text.select-female")}, params.gender);
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("searchForm", "text.select-aged-between") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectInline"]('ageFrom', params.ageFrom, 18, 78, 'from');
buf.push("<div class=\"field-select-separator\">" + (jade.escape(null == (jade_interp = _t("searchForm", "text.to")) ? "" : jade_interp)) + "</div>");
jade_mixins["selectInline"]('ageTo', params.ageTo, 18, 78, 'to');
buf.push("</div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("searchForm", "text.select-located") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\"><div class=\"field-text\"><input type=\"text\"" + (jade.attr("value", params.location, true, false)) + " name=\"location\"/></div></div></td></tr><tr><td><label>" + (jade.escape(null == (jade_interp = _t("searchForm", "text.select-distance") + ":") ? "" : jade_interp)) + "</label></td><td><div class=\"fbx-table-field\">");
jade_mixins["selectParams"]('distance', {20: "20", 50: "50", 100: "100"}, params.distance);
buf.push("</div></td></tr></tbody></table></div><div data-radio=\"data-radio\" class=\"fbx-list-padding border-top\">");
// iterate params.types
;(function(){
  var $$obj = params.types;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var searchType = $$obj[$index];

buf.push("<div class=\"fbx-item fbx-field-check\"><div data-check=\"data-check\" class=\"field-check\"><label><input type=\"radio\" name=\"type\"" + (jade.attr("value", searchType, true, false)) + (jade.attr("checked", (params.type==searchType), true, false)) + "/></label><div class=\"switcher\"><span>ON</span><span>OFF</span></div></div><label class=\"field-label\">" + (jade.escape(null == (jade_interp = _t("searchForm", searchTypes[searchType])) ? "" : jade_interp)) + "</label></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var searchType = $$obj[$index];

buf.push("<div class=\"fbx-item fbx-field-check\"><div data-check=\"data-check\" class=\"field-check\"><label><input type=\"radio\" name=\"type\"" + (jade.attr("value", searchType, true, false)) + (jade.attr("checked", (params.type==searchType), true, false)) + "/></label><div class=\"switcher\"><span>ON</span><span>OFF</span></div></div><label class=\"field-label\">" + (jade.escape(null == (jade_interp = _t("searchForm", searchTypes[searchType])) ? "" : jade_interp)) + "</label></div>");
    }

  }
}).call(this);

buf.push("</div><div data-checkbox=\"data-checkbox\" class=\"fbx-list-padding border-top\"><div id=\"coreg-placement-advanced-search\" class=\"fbx-item fbx-field-check coreg-placement-on-advanced-search\"></div></div><div class=\"fbx-item fbx-field-submit\"><button id=\"searchFormSubmit\" class=\"button\">" + (jade.escape(null == (jade_interp = _t("searchForm", "text.save")) ? "" : jade_interp)) + "</button></div></form>");;return buf.join("");
};