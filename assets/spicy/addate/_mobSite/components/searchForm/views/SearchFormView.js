var SearchFormView = Backbone.View.extend({
    events: {
        'click #searchFormSubmit': 'submit',
        'click input[name=location]': 'selectLocation'
    },

    coregOptions: {
        'id': 'advanced-search',
        'container' : '#coreg-placement-advanced-search',
        'tpl' : 'SearchFormCoreg'
    },

    initialize: function() {
        this.listenTo(Backbone, 'LocationSelected', this.locationSelected);
    },

    render: function() {
        var modelData   = this.model.toJSON();
        var searchTypes = {
            "near_you"    : "text.near-you",
            "new_members" : "text.new-members",
            "online"      : "text.online-now",
            "livecam"     : "text.livecam",
            'all_members' : 'text.all-members'
        };
        _.extend(modelData, {"searchTypes" : searchTypes});
        this.$el.html(tpl.render("SearchForm", modelData));
        this.selectWrap();
        this.radioButton();
        this.initInputEvents();
        return this;
    },

    submit: function(e) {
        e.preventDefault();
        Backbone.trigger('showSearchForm');
        var query = getFormQuery(this.$("#searchForm"));
        if(query) app.router.navigate("/search/"+query, {trigger: true});
    },

    selectLocation: function(e){
        Backbone.trigger('SelectLocation', {
            location: $(e.target).val(),
            anyCityItem: false
        });
    },

    locationSelected: function(options) {
        this.$('input[name=location]').val(options.location);
    }

});
