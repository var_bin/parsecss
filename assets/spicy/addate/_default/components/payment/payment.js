$(document).ready(function()
{
    var validators = {
        'isPartialCharge' : function(data){
            $.each(data, function(k, v){
                var value = v ? 'true' : 'false';
                $('#subscription input#package' + k).parents('li').removeAttr('partial').attr('partial', value);
                $('#subscription .package.active').click();
            })
        }
    };

    if (typeof currentTab != 'undefined' && currentTab == "sms" && window.location.hash == '') {
        window.location.hash = "pay_via_sms";
    }

    $(".packages-list li").click(function(){
        if (typeof($(this).attr('lang')) != "undefined")
        {
            $(".packages-list li").removeClass("active").find(':radio').attr("checked", null);

            var details = $.parseJSON($(this).attr('lang'));

            if (typeof(details.package) != "undefined")
            {
                $("._package").hide();
                $("._package_" + details.package).show();
            }

            if (typeof(details.package_type) != "undefined")
            {
                $("._package_type").hide();
                $("._package_type_" + details.package_type).show();
            }

            $('.package[lang="'+$(this).attr('lang')+'"]').addClass("active").find(':radio').attr('checked', 'checked');
        }
        else
        {
            var parent = $(this).closest('.packages-list');
            parent.find(':radio').attr("checked", null);
            $(this).find(':radio').attr('checked', 'checked');
            parent.find("li").removeClass("active");
            $(this).addClass("active");
        }

        if ($('.repeat_' + package_type) !== undefined) {
            $('.footer-repeat').hide();
            $('.repeat_' + package_type).show();
        }
    });
    $("[data-tabs=tab]").click(function(){
        $(this).parent().find("[data-tabs=tab]").removeClass("active current");
        var tabs_content = $(this).parent().next().find("[data-tabs=content]");
        tabs_content.removeClass("active current").eq($(this).index()).addClass("active current");
        $(this).addClass("active current");
        
        // set active package
        var checked_package = tabs_content.eq($(this).index()).find(".packages-list li input[type=radio].checked").eq(0);
        if(checked_package.length > 0)
        {
            checked_package.closest("li").click();
        }
        
        window.location.hash = "pay_via_" + $(this).find("[type=radio]").val();
    });

    if( window.location.hash != '' && window.location.hash === '#pay_via_sms' ){
        $("[type=radio][value=sms]").click();
    }

    var smsProcessing = false;
    $("#smsPaymentForm").submit(function(){
        if( smsProcessing === true ) return false;

        smsProcessing = true;

        var form        = $(this);
        $.ajax({
            url:        "/pay/sms/",
            type:       "post",
            data:       form.serialize(),
            dataType:   "json",
            error:      function(){},
            success:    function( response ){
                if( response.status == true ){
                    form.find(".sms_form_content").hide();
                    form.find(".sms_form_response iframe").attr("src", response.url).parent().show();
                }
            },
            complete:   function(){
                smsProcessing = false;
            }
        });

        return false;
    });

    $("#subscription input[name='CreditCardPaymentForm[card_number]']").change(function(){
        var formData = {
            'ajax':             'subscription',
            'validatorActions': 'isPartialCharge'
        };

        $('#subscription').find('input[type="radio"], input[type="text"], input[type="hidden"], input[type="number"], select').each(function(){
            formData[$(this).attr('name')] = $(this).val();
        });

        $.ajax({
            url:        "/pay/pay",
            type:       "post",
            data:       formData,
            dataType:   "json",
            error:      function(){},
            success:    function(response){
                if( response.getSourceType !== undefined ){
                    $.each(response, function(key, data){
                        if (typeof(validators[key]) == 'function'){
                            validators[key](data);
                        }
                    })
                }
            },
            complete:   function(){}
        });

        return;
    });
    $('div.select select').each(function(e){
        if($(this).val() != '')
        {
            $(this).parent().children('span.value').text($(this).val());
        }
    });
	$('div.select select').change(function(e){
	
		$(this).parent().children('span.value').text($(this).val());
	});

    // Change sms package amount by operator
    $('#psmsOperatorSelect').change(function(e){
        var selectedOperator = $(this).val();

        if (smsOperatorsList[selectedOperator]) {
            var curTab = $(this).closest('.b-tab-content');
            curTab.find('.b-packege-mobile .amount').text(smsOperatorsList[selectedOperator].amount);

            $('#SmsPaymentForm_amount').val(smsOperatorsList[selectedOperator].amount);
            $('.weekly-psms-price').html(smsOperatorsList[selectedOperator].amount);
        }
    });
    $('#psmsOperatorSelect').change();

    //disable pay button to prevent double click
    $("#subscription").live("submit", function (){
        $("#subscription input[type=submit]").attr("disabled", "disabled");
    });

    document.querySelector(".frm_security_number").addEventListener("keypress", function (evt) {
        if (evt.which > 57) {
            evt.preventDefault();
        } else {
            if ($(this).val().length == 3 && evt.which > 46) evt.preventDefault();
        }
    });
});
