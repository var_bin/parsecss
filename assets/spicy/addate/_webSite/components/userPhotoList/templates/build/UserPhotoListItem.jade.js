this["JST"] = this["JST"] || {};

this["JST"]["UserPhotoListItem"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),aspectTotal = locals_.aspectTotal,total = locals_.total,photos = locals_.photos,_t = locals_._t;
jade_mixins["photo"] = function(data){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<img" + (jade.attr("src", data.src, true, false)) + " style=\"opacity: 0; width: 100%;\"/>");
};
buf.push("<div" + (jade.attr("data-photo-row", aspectTotal, true, false)) + " style=\"display: none; overflow: hidden; clear: both; margin-bottom: 1%;\">");
if ( total > 1)
{
var per = 101 - total;
// iterate photos
;(function(){
  var $$obj = photos;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div" + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "width: "+(val.aspect/aspectTotal*per)+"%; "+((total !== key + 1)?"margin-right: 1%;":""), true, false)) + " class=\"photo-holder\"></div><a" + (jade.attr("href", (!val.userUpgrade)?val.url:val.userUpgrade, true, false)) + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "background-image: url("+val.src+"); width: "+(val.aspect/aspectTotal*per)+"%; "+((total !== key + 1)?"margin-right: 1%;":""), true, false)) + (jade.cls(['photo-item',(val.naughty.naughtyClass)?val.naughty.naughtyClass:""], [null,true])) + ">");
jade_mixins["photo"](val);
if(!val.userUpgrade)
{
if(val.naughty.naughtyLevel == 1)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Naughty photo")) ? "" : jade_interp)) + "</span></div>");
}
else if(val.naughty.naughtyLevel == 2)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Explicit Nudity")) ? "" : jade_interp)) + "</span></div>");
}
}
buf.push("</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div" + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "width: "+(val.aspect/aspectTotal*per)+"%; "+((total !== key + 1)?"margin-right: 1%;":""), true, false)) + " class=\"photo-holder\"></div><a" + (jade.attr("href", (!val.userUpgrade)?val.url:val.userUpgrade, true, false)) + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "background-image: url("+val.src+"); width: "+(val.aspect/aspectTotal*per)+"%; "+((total !== key + 1)?"margin-right: 1%;":""), true, false)) + (jade.cls(['photo-item',(val.naughty.naughtyClass)?val.naughty.naughtyClass:""], [null,true])) + ">");
jade_mixins["photo"](val);
if(!val.userUpgrade)
{
if(val.naughty.naughtyLevel == 1)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Naughty photo")) ? "" : jade_interp)) + "</span></div>");
}
else if(val.naughty.naughtyLevel == 2)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Explicit Nudity")) ? "" : jade_interp)) + "</span></div>");
}
}
buf.push("</a>");
    }

  }
}).call(this);

}
else
{
// iterate photos
;(function(){
  var $$obj = photos;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div" + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "background-image: url("+val.src+");", true, false)) + " class=\"photo-holder\"></div><a" + (jade.attr("href", (!val.userUpgrade)?val.url:val.userUpgrade, true, false)) + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "background-image: url("+val.src+");", true, false)) + (jade.cls(['photo-item','single-photo',(val.naughty.naughtyClass)?val.naughty.naughtyClass:""], [null,null,true])) + ">");
jade_mixins["photo"](val);
if(!val.userUpgrade)
{
if(val.naughty.naughtyLevel == 1)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Naughty photo")) ? "" : jade_interp)) + "</span></div>");
}
else if(val.naughty.naughtyLevel == 2)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Explicit Nudity")) ? "" : jade_interp)) + "</span></div>");
}
}
buf.push("</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div" + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "background-image: url("+val.src+");", true, false)) + " class=\"photo-holder\"></div><a" + (jade.attr("href", (!val.userUpgrade)?val.url:val.userUpgrade, true, false)) + (jade.attr("data-aspect", val.aspect, true, false)) + (jade.attr("style", "background-image: url("+val.src+");", true, false)) + (jade.cls(['photo-item','single-photo',(val.naughty.naughtyClass)?val.naughty.naughtyClass:""], [null,null,true])) + ">");
jade_mixins["photo"](val);
if(!val.userUpgrade)
{
if(val.naughty.naughtyLevel == 1)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Naughty photo")) ? "" : jade_interp)) + "</span></div>");
}
else if(val.naughty.naughtyLevel == 2)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "Explicit Nudity")) ? "" : jade_interp)) + "</span></div>");
}
}
buf.push("</a>");
    }

  }
}).call(this);

}
buf.push("</div>");;return buf.join("");
};