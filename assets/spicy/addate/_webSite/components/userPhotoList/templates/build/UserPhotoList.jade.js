this["JST"] = this["JST"] || {};

this["JST"]["UserPhotoList"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),photos = locals_.photos;
buf.push("<div id=\"userPhotosHidden\" style=\"position: absolute; height; 0; width: 0; overflow: hidden;\">");
// iterate photos
;(function(){
  var $$obj = photos;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var val = $$obj[$index];

buf.push("<img" + (jade.attr("src", val.normal, true, false)) + " data-photo=\"data-photo\"/>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var val = $$obj[$index];

buf.push("<img" + (jade.attr("src", val.normal, true, false)) + " data-photo=\"data-photo\"/>");
    }

  }
}).call(this);

buf.push("</div>");;return buf.join("");
};