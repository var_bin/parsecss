this["JST"] = this["JST"] || {};

this["JST"]["ProfileEditor"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),profile = locals_.profile,_t = locals_._t,formParams = locals_.formParams;
if ( profile)
{
buf.push("<div id=\"openx-banner-placement-profileright\" class=\"banner-zone-right openx-banner\"></div><div class=\"b-features\"></div><div class=\"b-row mind\"><div class=\"b-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.what_mind")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><div data-ui-textarea=\"data-ui-textarea\" class=\"field\"><div class=\"textarea-wrap\"><textarea name=\"chat_up_line\" data-resize=\"auto\" data-control=\"data-control\" id=\"status\" class=\"textarea\">" + (jade.escape(null == (jade_interp = profile.chat_up_line) ? "" : jade_interp)) + "</textarea></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div><div class=\"b-row own-words\"><div class=\"b-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.in_my_words")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><div data-ui-textarea=\"data-ui-textarea\" class=\"field\"><div class=\"textarea-wrap\"><textarea name=\"description\" data-resize=\"auto\" data-control=\"data-control\" id=\"about\" class=\"textarea\">" + (jade.escape(null == (jade_interp = profile.description) ? "" : jade_interp)) + "</textarea></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div><div class=\"b-row birthday\"><div class=\"b-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.birthday")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><div data-ui-date-select=\"data-ui-date-select\" class=\"field\">");
// iterate formParams.dateFormat
;(function(){
  var $$obj = formParams.dateFormat;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"field day select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.day), true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><ul class=\"list\">");
for(var i = 1; i <= 31; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"field month select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.month), true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><ul class=\"list\"><li data-item=\"1\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.january")) ? "" : jade_interp)) + "</li><li data-item=\"2\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.february")) ? "" : jade_interp)) + "</li><li data-item=\"3\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.march")) ? "" : jade_interp)) + "</li><li data-item=\"4\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.april")) ? "" : jade_interp)) + "</li><li data-item=\"5\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.may")) ? "" : jade_interp)) + "</li><li data-item=\"6\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.june")) ? "" : jade_interp)) + "</li><li data-item=\"7\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.july")) ? "" : jade_interp)) + "</li><li data-item=\"8\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.august")) ? "" : jade_interp)) + "</li><li data-item=\"9\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.september")) ? "" : jade_interp)) + "</li><li data-item=\"10\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.october")) ? "" : jade_interp)) + "</li><li data-item=\"11\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.november")) ? "" : jade_interp)) + "</li><li data-item=\"12\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.december")) ? "" : jade_interp)) + "</li></ul></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"field year select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><ul class=\"list\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"field day select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.day), true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><ul class=\"list\">");
for(var i = 1; i <= 31; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"field month select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.month), true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><ul class=\"list\"><li data-item=\"1\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.january")) ? "" : jade_interp)) + "</li><li data-item=\"2\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.february")) ? "" : jade_interp)) + "</li><li data-item=\"3\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.march")) ? "" : jade_interp)) + "</li><li data-item=\"4\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.april")) ? "" : jade_interp)) + "</li><li data-item=\"5\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.may")) ? "" : jade_interp)) + "</li><li data-item=\"6\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.june")) ? "" : jade_interp)) + "</li><li data-item=\"7\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.july")) ? "" : jade_interp)) + "</li><li data-item=\"8\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.august")) ? "" : jade_interp)) + "</li><li data-item=\"9\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.september")) ? "" : jade_interp)) + "</li><li data-item=\"10\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.october")) ? "" : jade_interp)) + "</li><li data-item=\"11\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.november")) ? "" : jade_interp)) + "</li><li data-item=\"12\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.december")) ? "" : jade_interp)) + "</li></ul></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"field year select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><ul class=\"list\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div>");
}
    }

  }
}).call(this);

buf.push("<input type=\"hidden\" name=\"birthday\" data-control=\"data-control\" data-date-select-value=\"1\" id=\"birthday\"/><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div><div class=\"b-row location\"><div class=\"b-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.location")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><div data-ui-location=\"data-ui-location\" class=\"field location select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"><input name=\"location\" type=\"text\"" + (jade.attr("value", profile.location, true, false)) + " autocomplete=\"off\" data-control=\"data-control\" class=\"input-value\"/></div><div data-list=\"data-list\" class=\"select location\"></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div><div id=\"coregPlacementEditProfile\" class=\"b-row edit-profile-placements-coreg\"></div><div class=\"btn-row\"><div id=\"saveStatus\" style=\"display:none;\" class=\"info-save-status\"></div><button data-btn-undo=\"1\" class=\"btn-cancel btn-text-def\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "button.cancel")) ? "" : jade_interp)) + "</button><button data-btn-save=\"1\" class=\"btn-save btn-text-green\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "button.save")) ? "" : jade_interp)) + "</button></div>");
};return buf.join("");
};