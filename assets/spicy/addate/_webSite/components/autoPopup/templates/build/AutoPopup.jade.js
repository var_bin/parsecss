this["JST"] = this["JST"] || {};

this["JST"]["AutoPopup"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),popupType = locals_.popupType,_t = locals_._t,data = locals_.data;
if ( popupType === "paySuccess")
{
buf.push("<div class=\"b-popup b-popup-payment\"><a data-close=\"data-close\"" + (jade.attr("title", _t("autoPopup", "text.close"), true, false)) + " class=\"btn-close\"></a>");
if ( data.features && (data.membership || data.offer || data.lifetimeOffer))
{
buf.push("<p class=\"title\">" + (null == (jade_interp = _t('autoPopup','text.pay-success-membership-and-features', {"{upgradepackage}":data.features, "{sitename}":data.siteName, "{br}":"<br/>"})) ? "" : jade_interp) + "</p>");
}
else if ( (data.features || data.membership || data.offer || data.lifetimeOffer))
{
if ( data.features)
{
buf.push("<p class=\"title\">" + (null == (jade_interp = _t('autoPopup', 'text.pay-success-features-only',{"{upgradepackage}":data.features,"{sitename}":data.siteName,"{br}":"<br />"})) ? "" : jade_interp) + "</p>");
}
if ((data.membership || data.offer || data.lifetimeOffer))
{
buf.push("<p class=\"title\">" + (null == (jade_interp = _t('autoPopup', 'text.pay-success-membership-only',{"{sitename}":data.siteName,"{br}":"<br />"})) ? "" : jade_interp) + "</p>");
}
}
else
{
buf.push("<p class=\"title\">" + (null == (jade_interp = _t('autoPopup', 'text.pay-success')) ? "" : jade_interp) + "</p>");
}
if ( data.isUnlimMobAccessEnabled)
{
buf.push("<div class=\"mobile-benefits\"><div class=\"mobile-benefits__title\">" + (null == (jade_interp = _t('autoPopup', 'text.You also get access to mobile version at with the following exclusive benefits', {"{mobSiteName}":data.mobSiteName})) ? "" : jade_interp) + "</div><div class=\"mobile-benefits__list\"><div class=\"mobile-benefits__item\"><div class=\"mobile-benefits__icon mobile-benefits__icon--map\"></div><div class=\"mobile-benefits__item__title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'text.Interactive map showing best matches nearby')) ? "" : jade_interp)) + "</div></div><div class=\"mobile-benefits__item\"><div class=\"mobile-benefits__icon mobile-benefits__icon--communication\"></div><div class=\"mobile-benefits__item__title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'text.Unlimited communication even on the go')) ? "" : jade_interp)) + "</div></div><div class=\"mobile-benefits__item\"><div class=\"mobile-benefits__icon mobile-benefits__icon--search\"></div><div class=\"mobile-benefits__item__title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'text.Advanced search option')) ? "" : jade_interp)) + "</div></div></div></div>");
}
if ( data.smschat)
{
buf.push("<p class=\"text\">" + (null == (jade_interp = _t('autoPopup', 'text.pay-success-smschat-only',{"{product}":_t('autoPopup', data.smschat),"{br}":"<br />"})) ? "" : jade_interp) + "</p>");
}
buf.push("<form class=\"form\">");
if ( data.doAskMsisdn)
{
buf.push("<div name=\"error_block\" class=\"b-data-row\"><h2 class=\"title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Good news!')) ? "" : jade_interp)) + "</h2><p>" + (null == (jade_interp = _t('autoPopup', 'Now you are able to chat via SMS! To use this feature fill in the field below.')) ? "" : jade_interp) + "</p><div class=\"b-value\"><input type=\"text\" name=\"msisdn\"" + (jade.attr("placeholder", _t('autoPopup', 'Enter your mobile number here'), true, false)) + "/><p class=\"b-error\">" + (null == (jade_interp = _t('autoPopup', 'The mobile number you entered is not valid')) ? "" : jade_interp) + "</p><input type=\"submit\" name=\"add_new_msisdn\"" + (jade.attr("value", _t('autoPopup', 'text.continue'), true, false)) + " class=\"btn green chat-now\"/></div></div>");
}
else
{
buf.push("<button id=\"popupContinue\" class=\"btn-text-green btn-continue\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'text.continue')) ? "" : jade_interp)) + "</button>");
}
buf.push("</form><div class=\"moneyback-guarantee\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'moneyBack.tip.text1')) ? "" : jade_interp)) + "<div class=\"moneyback-guarantee__hidden\">");
if ( data.showDescription)
{
buf.push("<p>" + (null == (jade_interp = _t('autoPopup', 'moneyBack.tip.text2', {"{siteDescriptor}" : data.siteDescriptorImage})) ? "" : jade_interp) + "</p><p>" + (jade.escape(null == (jade_interp = _t('autoPopup', 'moneyBack.tip.text3')) ? "" : jade_interp)) + "</p>");
}
buf.push("<p>" + (jade.escape(null == (jade_interp = _t('autoPopup', 'moneyBack.tip.text4')) ? "" : jade_interp)) + "</p></div></div>");
if ((data.repeatAt && data.simpleCancel && data.cardId))
{
buf.push("<div class=\"cancel-repeat-block\"><div class=\"active\"><p>" + (jade.escape(null == (jade_interp = _t('autoPopup', 'autoPopup.note', {"{repeatAt}":data.repeatAt})) ? "" : jade_interp)) + "</p><div class=\"b-center\"><input id=\"cancel-repeat\" type=\"submit\"" + (jade.attr("value", (_t('autoPopup', 'autoPopup.cancel-repeat')).ucfirst(), true, false)) + (jade.attr("data-card-id", data.cardId, true, false)) + " name=\"simple_cancel\" class=\"btn btn-cancel\"/></div></div><div class=\"hide\"><p>" + (jade.escape(null == (jade_interp = _t('autoPopup', 'text.cancel-repeat.complete', {"{package}":(data.features ? (data.membership ? data.features + '&' + data.membership : data.features) : data.membership)})) ? "" : jade_interp)) + "</p></div></div>");
}
buf.push("<div class=\"information-title\">" + (null == (jade_interp = _t("autoPopup", "text.pay_success_information", {"{br}":"<br>"})) ? "" : jade_interp) + "</div><div class=\"information-description\">" + (jade.escape(null == (jade_interp = _t("autoPopup", "text.pay-success-description")) ? "" : jade_interp)) + "</div></div>");
}
if ( popupType === "addMsisdn")
{
buf.push("<div class=\"b-popup b-sms-chat chat-feature\"><div class=\"b-popup-head\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Try SMS chat feature')) ? "" : jade_interp)) + "</span><div class=\"meta-button-wrap\"><button id=\"popupClose\"" + (jade.attr("title", _t("autoPopup", "text.close"), true, false)) + " class=\"btn-meta b-close\"></button></div></div><div class=\"b-popup-content\"><form class=\"form\">");
if ((data.isShowAdditionalTextsBySourceGBR))
{
buf.push("<h2 class=\"title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Enter mobile number to enable SMS messaging!')) ? "" : jade_interp)) + "</h2>");
}
else
{
buf.push("<h2 class=\"title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Enter mobile number to enable SMS messaging with other members!')) ? "" : jade_interp)) + "</h2>");
}
buf.push("<div name=\"error_block\" class=\"b-data-row\"><div class=\"b-value\"><input type=\"text\" name=\"msisdn\"" + (jade.attr("placeholder", _t('autoPopup', 'Enter your mobile number here'), true, false)) + "/><p class=\"b-error\">" + (null == (jade_interp = _t('autoPopup', 'The mobile number you entered is not valid')) ? "" : jade_interp) + "</p></div><input type=\"submit\" name=\"add_new_msisdn\"" + (jade.attr("value", _t('autoPopup', 'Save'), true, false)) + " class=\"btn green save-btn\"/><label class=\"show-again\"><input type=\"checkbox\" name=\"do_not_ask_again\" data-notificationsettings-checkbox=\"1\"/><span>" + (jade.escape(null == (jade_interp = _t('autoPopup', "Don't show me again")) ? "" : jade_interp)) + "</span></label></div></form></div></div>");
}
if ( popupType === "addMsisdnSuccess")
{
buf.push("<div class=\"b-popup b-sms-chat thanks\"><div class=\"b-popup-head\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Thanks for joining SMS Chat!')) ? "" : jade_interp)) + "</span><div class=\"meta-button-wrap\"><button id=\"popupClose\"" + (jade.attr("title", _t("autoPopup", 'text.close'), true, false)) + " class=\"btn-meta b-close\"></button></div></div><div class=\"b-popup-content step-4\"><div class=\"form\"><h2 class=\"title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Your phone has been successfully saved.')) ? "" : jade_interp)) + "</h2><button id=\"popupContinue\" class=\"btn green close-btn\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'text.close')) ? "" : jade_interp)) + "</button></div></div></div>");
}
if ( popupType === "smsChatOneClick")
{
buf.push("<div class=\"b-popup b-sms-chat chat-feature2\"><div class=\"b-popup-head\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Special communication offer')) ? "" : jade_interp)) + "</span><div class=\"meta-button-wrap\"><button id=\"popupClose\"" + (jade.attr("title", _t("autoPopup", 'text.close'), true, false)) + " class=\"btn-meta b-close\"></button></div></div><div class=\"b-popup-content\"><div class=\"b-sms-price\">" + (null == (jade_interp = _t('autoPopup', '2.99$/3 SMS')) ? "" : jade_interp) + "</div><form class=\"form\"><h2>" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Lots of ladies here are ready to chat via SMS.')) ? "" : jade_interp)) + "</h2><p class=\"description\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'Activate SMS package and start texting now!')) ? "" : jade_interp)) + "</p><div name=\"error_block\" class=\"b-data-row error\"><input type=\"submit\" name=\"add_new_msisdn\"" + (jade.attr("value", _t('autoPopup', 'Activate'), true, false)) + " class=\"btn green save\"/><div class=\"b-value\"><input type=\"text\" name=\"msisdn\"" + (jade.attr("value", (data.msisdn || ''), true, false)) + (jade.attr("placeholder", _t('autoPopup', 'Enter your mobile number here'), true, false)) + "/><div class=\"b-error\">" + (null == (jade_interp = _t('autoPopup', 'The mobile number you entered is not valid')) ? "" : jade_interp) + "</div></div></div></form><div class=\"terms\">" + (jade.escape(null == (jade_interp = _t('autoPopup', 'After using all SMS messages available')) ? "" : jade_interp)) + "</div></div></div>");
};return buf.join("");
};