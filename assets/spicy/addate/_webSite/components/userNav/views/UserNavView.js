var UserNavView = Backbone.View.extend({
    events: {
        "click #userNavPhoto, #userNavDropdown": "toggleMenu",
        "click [data-nav-icon]": function(event) {
            event.stopPropagation();
            this.toggleMenu(event);
        },
        "click #site-logout" : "processLogout",
        'click .btn.btn-upgrade': 'upgradeButtonEvent'
    },

    initialize: function() {

        this.model = new UserNavModel();

        $(document).click(function(event) {
            if ($(event.target).closest("#userNavPhoto").length) return;
            $("#userNavDropdown").removeClass("active");
            event.stopPropagation();
        });

        this.listenTo(this.model, 'change', this.render);
    },

    render: function() {
        var self = this;

        this.model.set({showHelpSection: app.appData().get('helpSectionLinker')});

        var model = this.model.toJSON();
        model.upgradeButton = app.appData().get('upgradeButton');
        model.hideButtonHint = app.appData().get('hideButtonHint');
        this.$el.html(tpl.render('UserNav', model));
        this.$dom = {
            photo: this.$('#userNavPhoto'),
            dropdown: this.$('#userNavDropdown')
        };

        return this;
    },

    toggleMenu: function(event) {
        if(!this.$dom.dropdown.hasClass('active')) {
            this.$dom.dropdown.addClass('active');
        } else {
            this.$dom.dropdown.removeClass('active');
        }
    },

    upgradeButtonEvent: function(event) {
        var url = 'https://' + document.location.hostname + $("#userNav .btn.btn-upgrade").attr("data-upgrade-url");
        if (app.appData().paymentRouteHandler != undefined) {
            app.appData().paymentRouteHandler(url);
        } else {
            document.location.href = url;
        }
    },

    processLogout: function() {
        var params = {
            id: 'coreg-logout-popup',
            link: $("#site-logout").attr('href'),
            refresh: true
        };
        Backbone.trigger("Coregistration:showPopup", params);

        var $menu = this.$el.find('#userNavDropdown');
        $menu.removeClass("active");

        return false;
    }
});
