var UserNavModel = Backbone.Model.extend({
    defaults: function() {
        return {
            user: app.appData().get('user')
        };
    },

    initialize: function() {
        this.listenTo(app.appData(), 'change:user', this.setUser);

        this.setUser();
    },

    setUser: function() {
        this.set(app.appData().get('user'));
    }
});
