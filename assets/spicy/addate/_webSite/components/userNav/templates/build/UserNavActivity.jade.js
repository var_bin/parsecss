this["JST"] = this["JST"] || {};

this["JST"]["UserNavActivity"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),activity = locals_.activity,userLink = locals_.userLink,_t = locals_._t;
// iterate activity
;(function(){
  var $$obj = activity;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var user = $$obj[$index];

// iterate user
;(function(){
  var $$obj = user;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var activities = $$obj[$index];

if ( activities)
{
buf.push("<div class=\"b-action-item\"><div class=\"image-centerer\"><div class=\"image-holder\"><img" + (jade.attr("src", activities.photo_url, true, false)) + " width=\"60\" height=\"60\" class=\"photo\"/></div><div" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"overlay\"></div></div><div class=\"b-user-info\"><h2 class=\"b-screenname\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = activities.login) ? "" : jade_interp)) + "</span></a></h2><div class=\"b-age\">" + (jade.escape(null == (jade_interp = activities.gender) ? "" : jade_interp)) + "</div></div><div class=\"b-action-date\">" + (jade.escape(null == (jade_interp = activities.date) ? "" : jade_interp)) + "</div><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"btn-activity btn-chat\">" + (jade.escape(null == (jade_interp = _t("userNav", "button.message")) ? "" : jade_interp)) + "</a></div><div class=\"b-btn-activity-grid\"><a href=\"#\"" + (jade.attr("data-winkbutton", activities.id, true, false)) + " class=\"btn-activity btn-wink\"><span class=\"wink\">" + (jade.escape(null == (jade_interp = _t("userNav", "Wink")) ? "" : jade_interp)) + "</span><span class=\"winked\">" + (jade.escape(null == (jade_interp = _t("userNav" ,"winked")) ? "" : jade_interp)) + "</span></a></div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var activities = $$obj[$index];

if ( activities)
{
buf.push("<div class=\"b-action-item\"><div class=\"image-centerer\"><div class=\"image-holder\"><img" + (jade.attr("src", activities.photo_url, true, false)) + " width=\"60\" height=\"60\" class=\"photo\"/></div><div" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"overlay\"></div></div><div class=\"b-user-info\"><h2 class=\"b-screenname\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = activities.login) ? "" : jade_interp)) + "</span></a></h2><div class=\"b-age\">" + (jade.escape(null == (jade_interp = activities.gender) ? "" : jade_interp)) + "</div></div><div class=\"b-action-date\">" + (jade.escape(null == (jade_interp = activities.date) ? "" : jade_interp)) + "</div><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"btn-activity btn-chat\">" + (jade.escape(null == (jade_interp = _t("userNav", "button.message")) ? "" : jade_interp)) + "</a></div><div class=\"b-btn-activity-grid\"><a href=\"#\"" + (jade.attr("data-winkbutton", activities.id, true, false)) + " class=\"btn-activity btn-wink\"><span class=\"wink\">" + (jade.escape(null == (jade_interp = _t("userNav", "Wink")) ? "" : jade_interp)) + "</span><span class=\"winked\">" + (jade.escape(null == (jade_interp = _t("userNav" ,"winked")) ? "" : jade_interp)) + "</span></a></div></div></div>");
}
    }

  }
}).call(this);

    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var user = $$obj[$index];

// iterate user
;(function(){
  var $$obj = user;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var activities = $$obj[$index];

if ( activities)
{
buf.push("<div class=\"b-action-item\"><div class=\"image-centerer\"><div class=\"image-holder\"><img" + (jade.attr("src", activities.photo_url, true, false)) + " width=\"60\" height=\"60\" class=\"photo\"/></div><div" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"overlay\"></div></div><div class=\"b-user-info\"><h2 class=\"b-screenname\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = activities.login) ? "" : jade_interp)) + "</span></a></h2><div class=\"b-age\">" + (jade.escape(null == (jade_interp = activities.gender) ? "" : jade_interp)) + "</div></div><div class=\"b-action-date\">" + (jade.escape(null == (jade_interp = activities.date) ? "" : jade_interp)) + "</div><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"btn-activity btn-chat\">" + (jade.escape(null == (jade_interp = _t("userNav", "button.message")) ? "" : jade_interp)) + "</a></div><div class=\"b-btn-activity-grid\"><a href=\"#\"" + (jade.attr("data-winkbutton", activities.id, true, false)) + " class=\"btn-activity btn-wink\"><span class=\"wink\">" + (jade.escape(null == (jade_interp = _t("userNav", "Wink")) ? "" : jade_interp)) + "</span><span class=\"winked\">" + (jade.escape(null == (jade_interp = _t("userNav" ,"winked")) ? "" : jade_interp)) + "</span></a></div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var activities = $$obj[$index];

if ( activities)
{
buf.push("<div class=\"b-action-item\"><div class=\"image-centerer\"><div class=\"image-holder\"><img" + (jade.attr("src", activities.photo_url, true, false)) + " width=\"60\" height=\"60\" class=\"photo\"/></div><div" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"overlay\"></div></div><div class=\"b-user-info\"><h2 class=\"b-screenname\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = activities.login) ? "" : jade_interp)) + "</span></a></h2><div class=\"b-age\">" + (jade.escape(null == (jade_interp = activities.gender) ? "" : jade_interp)) + "</div></div><div class=\"b-action-date\">" + (jade.escape(null == (jade_interp = activities.date) ? "" : jade_interp)) + "</div><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a" + (jade.attr("href", userLink(activities.id), true, false)) + " class=\"btn-activity btn-chat\">" + (jade.escape(null == (jade_interp = _t("userNav", "button.message")) ? "" : jade_interp)) + "</a></div><div class=\"b-btn-activity-grid\"><a href=\"#\"" + (jade.attr("data-winkbutton", activities.id, true, false)) + " class=\"btn-activity btn-wink\"><span class=\"wink\">" + (jade.escape(null == (jade_interp = _t("userNav", "Wink")) ? "" : jade_interp)) + "</span><span class=\"winked\">" + (jade.escape(null == (jade_interp = _t("userNav" ,"winked")) ? "" : jade_interp)) + "</span></a></div></div></div>");
}
    }

  }
}).call(this);

    }

  }
}).call(this);
;return buf.join("");
};