this["JST"] = this["JST"] || {};

this["JST"]["UserNav"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),upgradeButton = locals_.upgradeButton,hideButtonHint = locals_.hideButtonHint,user = locals_.user,_t = locals_._t,photoUrl = locals_.photoUrl,login = locals_.login,showHelpSection = locals_.showHelpSection,allowedBlocked = locals_.allowedBlocked;
buf.push("<div class=\"upgrade-block\">");
if ( upgradeButton.enable)
{
if ( !hideButtonHint)
{
if ( user.account_status.type == "paid")
{
buf.push("<!--full membership--><div class=\"full-upgrade\">" + (jade.escape(null == (jade_interp = _t("userNav", "Full Membership")) ? "" : jade_interp)) + "</div>");
}
else
{
buf.push("<!--full membership: upgrade now--><span class=\"current-payment-status\">" + (jade.escape(null == (jade_interp = _t("userNav", "Free Membership")) ? "" : jade_interp)) + "</span>");
}
buf.push("<!--show upgrate button-->");
}
buf.push("<!--free user-->");
if ( user.account_status.type == "free")
{
var upgradeUrl = "/pay/membership?via=toolbar_upgrade_button"
buf.push("<!--user with discount-->");
if ( user.discount.id)
{
var discountAmount = user.discount.type == "percent" ? user.discount.percent + "%" : user.discount.amount
buf.push("<!-- full membership: upgrade now--><div class=\"btn-upgrade-wrap discount\"><a" + (jade.attr("data-upgrade-url", upgradeUrl, true, false)) + " class=\"btn btn-upgrade\">" + (jade.escape(null == (jade_interp = _t("userNav", "Upgrade now")) ? "" : jade_interp)) + "</a>");
if ( !hideButtonHint)
{
buf.push("<span class=\"discount-label\">" + (jade.escape(null == (jade_interp = "-" + discountAmount) ? "" : jade_interp)) + "</span><div class=\"upgrade-hint-wrap\"><div class=\"upgrade-hint membership\"><span class=\"upgrade-hint__title\">" + (jade.escape(null == (jade_interp = _t("userNav", "You are 1 click away from:")) ? "" : jade_interp)) + "</span><ul class=\"upgrade-hint__features\"><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Chatting with the")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "hottest members")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Viewing members'")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "tempting photos")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Watching members'")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "explicit videos")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Using")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "advanced search")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Getting more")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "naughty action")) ? "" : jade_interp)) + "</b></li></ul><a" + (jade.attr("data-upgrade-url", upgradeUrl, true, false)) + " class=\"btn btn-upgrade\"><b>" + (jade.escape(null == (jade_interp = _t("userNav", "Upgrade now")) ? "" : jade_interp)) + "</b><span>" + (jade.escape(null == (jade_interp = _t("userNav","Use your discount", {"{discount}":discountAmount})) ? "" : jade_interp)) + "</span></a></div></div>");
}
buf.push("</div>");
}
else
{
buf.push("<div class=\"btn-upgrade-wrap\"><a" + (jade.attr("data-upgrade-url", upgradeUrl, true, false)) + " class=\"btn btn-upgrade\">" + (jade.escape(null == (jade_interp = _t("userNav", "Upgrade now")) ? "" : jade_interp)) + "</a>");
if ( !hideButtonHint)
{
buf.push("<div class=\"upgrade-hint-wrap\"><div class=\"upgrade-hint membership\"><div class=\"upgrade-hint__title\">" + (jade.escape(null == (jade_interp = _t("userNav", "You are 1 click away from:")) ? "" : jade_interp)) + "<ul class=\"upgrade-hint__features\"><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Chatting with the")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "hottest members")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Viewing members'")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "tempting photos")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Watching members'")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "explicit videos")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Using")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "advanced search")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Getting more")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "naughty action")) ? "" : jade_interp)) + "</b></li></ul></div></div></div>");
}
buf.push("</div>");
}
}
else
{
if ( user.hasFeatures)
{
var features = user.account_features
if ( (features.free_communication || features.top_in_search || features.highlight_profile || features.invisible_mode || features.special_delivery))
{
var GetMoreUrl = "/pay/features?via=toolbar_getmore_button"
buf.push("<!--full membership: get more--><div class=\"btn-upgrade-wrap\"><a" + (jade.attr("data-upgrade-url", GetMoreUrl, true, false)) + " class=\"btn btn-upgrade\">" + (jade.escape(null == (jade_interp = _t("userNav", "Get more")) ? "" : jade_interp)) + "</a>");
if ( !hideButtonHint)
{
buf.push("<div class=\"upgrade-hint-wrap\"><div class=\"upgrade-hint\"><div class=\"upgrade-hint__title\">" + (jade.escape(null == (jade_interp = _t("userNav", "You are 1 click away from:")) ? "" : jade_interp)) + "</div><ul class=\"upgrade-hint__features\">");
if ( features.top_in_search)
{
buf.push("<li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Top in")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "hottest search")) ? "" : jade_interp)) + "</b></li>");
}
if ( features.highlight_profile)
{
buf.push("<li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Highlighted")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "tempting profile")) ? "" : jade_interp)) + "</b></li>");
}
if ( features.invisible_mode)
{
buf.push("<li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Invisible")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "explicit mode")) ? "" : jade_interp)) + "</b></li>");
}
if ( features.special_delivery)
{
buf.push("<li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Special")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "advanced delivery")) ? "" : jade_interp)) + "</b></li>");
}
if ( features.free_communication)
{
buf.push("<li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "Free")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "naughty communication")) ? "" : jade_interp)) + "</b></li>");
}
buf.push("</ul></div></div>");
}
buf.push("</div>");
}
else
{
var GetMoreUrl = "/pay/liveCamCredits?via=toolbar_offer_button"
buf.push("<div class=\"btn-upgrade-wrap\"><a" + (jade.attr("data-upgrade-url", GetMoreUrl, true, false)) + " class=\"btn btn-upgrade\">" + (jade.escape(null == (jade_interp = _t("userNav", "Exclusive offer")) ? "" : jade_interp)) + "</a>");
if ( !hideButtonHint)
{
buf.push("<span class=\"discount-label\">-30 %</span><div class=\"upgrade-hint-wrap\"><div class=\"upgrade-hint\">");
var discountSum="30 %"
buf.push("<div class=\"upgrade-hint__title\">" + (jade.escape(null == (jade_interp = _t("userNav", "You are 1 click away from:")) ? "" : jade_interp)) + "</div><ul class=\"upgrade-hint__features\"><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.watch")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "free HD shows 24/7")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "hook up with the")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "hottest models")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "enjoy")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "private shows even on the go")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "have")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "virtual sex via cam-2-cam")) ? "" : jade_interp)) + "</b></li><li class=\"upgrade-hint__features__item\">" + (jade.escape(null == (jade_interp = _t("userNav", "watch up to")) ? "" : jade_interp)) + "<b>" + (jade.escape(null == (jade_interp = _t("userNav", "5000 hours of pre-recorded shows")) ? "" : jade_interp)) + "</b></li><a" + (jade.attr("data-upgrade-url", GetMoreUrl, true, false)) + " class=\"btn btn-upgrade\"><b>" + (jade.escape(null == (jade_interp = _t("userNav", "Exclusive offer")) ? "" : jade_interp)) + "</b><span>" + (jade.escape(null == (jade_interp = _t("userNav","Use your discount", {"{discount}":discountSum})) ? "" : jade_interp)) + "</span></a></ul></div></div>");
}
buf.push("</div>");
}
}
}
}
buf.push("</div><div id=\"userNavPhoto\" class=\"photo-holder\"><span class=\"overlay\"></span><div class=\"image-holder\"><img" + (jade.attr("src", photoUrl, true, false)) + (jade.attr("title", login, true, false)) + " class=\"photo\"/></div></div><div id=\"userNavPhoto\" class=\"b-screenname\">" + (jade.escape(null == (jade_interp = login) ? "" : jade_interp)) + "</div><a href=\"/#account\" class=\"b-settingsbutton\"><div class=\"b-settings-tooltip\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.settings")) ? "" : jade_interp)) + "</div></a><div id=\"userNavDropdown\" class=\"b-dropdown-menu\"><ul class=\"nav\"><li class=\"item\"><a href=\"/#account\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.settings")) ? "" : jade_interp)) + "</a></li><li class=\"item\"><a href=\"/#profile\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.profile")) ? "" : jade_interp)) + "</a></li>");
if ((showHelpSection))
{
buf.push("<li class=\"item\"><a href=\"/#help\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.help_section")) ? "" : jade_interp)) + "</a></li>");
}
if (allowedBlocked)
{
buf.push("<li class=\"item\"><a href=\"/#blocked\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("blocked", "title.blocked_members")) ? "" : jade_interp)) + "</a></li>");
}
buf.push("<li class=\"item\"><a href=\"/#site/contactUs\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.contact_us")) ? "" : jade_interp)) + "</a></li><li class=\"item\"><a id=\"site-logout\" href=\"/site/logout\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.log_out")) ? "" : jade_interp)) + "</a></li></ul></div>");;return buf.join("");
};