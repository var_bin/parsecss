this["JST"] = this["JST"] || {};

this["JST"]["UserListWidget"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user,naughtyMode = locals_.naughtyMode,_t = locals_._t,undefined = locals_.undefined,userLink = locals_.userLink,isSiteCompliance = locals_.isSiteCompliance,config = locals_.config,naughtyClass = locals_.naughtyClass,currentUser = locals_.currentUser,niche = locals_.niche;
buf.push("<li" + (jade.cls(['b-grid-item',(user.photos.count > 1 || (user.statuses && user.statuses.newStatus)) ? 'bottom-shadow' : ''], [null,true])) + ">");
if(!user.upgradeBanner)
{
var userClass = [];
if(naughtyMode.naughtyClass) userClass.push(naughtyMode.naughtyClass)
if(user.marks && user.marks.highlight_profile && !user.livecam) userClass.push("b-user-highlight")
if(user.livecam) userClass.push("livecam")
buf.push("<div data-userwidget=\"\"" + (jade.cls(['b-user',userClass], [null,true])) + ">");
if (user.livecam)
{
buf.push("<span class=\"livecams-ico-block\"></span><span class=\"livecams-ico\"><span class=\"livecams-ico-inner\"><span class=\"livecams-ico-handler\"></span><span class=\"livecams-ico-lines\"></span></span><span class=\"livecams-ico-arrow\"></span></span>");
}
buf.push("<div class=\"b-photo\"><div data-photo=\"\" class=\"b-photo-link\"><span class=\"overlay\"></span>");
if(user.statuses && user.statuses.newStatus)
{
buf.push("<span" + (jade.attr("title", _t("userListWidget", "title.new_user"), true, false)) + " class=\"b-new\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.new")) ? "" : jade_interp)) + "</span>");
}
if(user.statuses && user.statuses.onlineStatus)
{
buf.push("<span class=\"online-status online\"></span>");
}
if(user.statuses && user.statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"online-status\"></span>");
}
buf.push("<div class=\"image-holder\"><img" + (jade.attr("src", user.photos.url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"photo\"/>");
if(naughtyMode.naughtyLevel == 1)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.naughty_photo")) ? "" : jade_interp)) + "</span></div>");
}
else if(naughtyMode.naughtyLevel == 2)
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.explicit_nudity")) ? "" : jade_interp)) + "</span></div>");
}
if(naughtyMode.naughtyMode)
{
buf.push("<div id=\"naughtyMode\" class=\"naughty-menu\"><div class=\"naughty-menu-wrap\">");
if(naughtyMode.naughtyLevel == 1)
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_sexy")) ? "" : jade_interp)) + "</div><button data-nm-trigger=\"2\" class=\"btn-switch\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.sexy")) ? "" : jade_interp)) + "</button>");
}
if(naughtyMode.naughtyLevel == 2)
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_no_limits")) ? "" : jade_interp)) + "</div><button data-nm-trigger=\"3\" class=\"btn-switch\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.no_limits")) ? "" : jade_interp)) + "</button>");
}
buf.push("</div></div>");
}
if((user.marks && ((user.marks.hotUser && user.marks.hotUser.upgradeUrl) || (user.marks.top_in_search && user.marks.top_in_search.upgradeUrl)
|| (user.marks.free_communication && user.marks.free_communication.show))) || user.invisibleMode)
{
buf.push("<div class=\"b-features\">");
if(user.marks && user.marks.hotUser && user.marks.hotUser.upgradeUrl)
{
buf.push("<a" + (jade.attr("href", user.marks.hotUser.upgradeUrl, true, false)) + (jade.attr("title", _t("userListWidget", "title.hot_user"), true, false)) + " class=\"item hot\">&nbsp;</a>");
}
if(user.marks && user.marks.top_in_search && user.marks.top_in_search.upgradeUrl)
{
buf.push("<a" + (jade.attr("href", user.marks.top_in_search.upgradeUrl, true, false)) + (jade.attr("title", _t("userListWidget", "title.top_in_search"), true, false)) + " class=\"item top\">&nbsp;</a>");
}
if(user.marks && user.marks.free_communication && user.marks.free_communication.show)
{
buf.push("<a" + (jade.attr("href", user.marks.free_communication.upgradeUrl, true, false)) + (jade.attr("title", _t("userListWidget", "title.free_communication"), true, false)) + " class=\"item free-m\">&nbsp;</a>");
}
if(user.invisibleMode)
{
buf.push("<span" + (jade.attr("title", _t("userListWidget", "title.invisible_mode"), true, false)) + " class=\"item invisible\">&nbsp;</span>");
}
buf.push("</div>");
if(user.statuses)
{
if(user.statuses.onlineStatus)
{
buf.push("<span class=\"status online\"><div class=\"status-text\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.online")) ? "" : jade_interp)) + "</div></span>");
}
else if(user.statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"status r-online\"><div class=\"status-text\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.recently_online")) ? "" : jade_interp)) + "</div></span>");
}
}
}
buf.push("</div></div></div><div class=\"b-user-info\"><h2 class=\"b-screenname\"><a" + (jade.attr("href", (user.livecam !== undefined) ? user.userViewLink : userLink(user.id), true, false)) + " data-screenname=\"\"" + (jade.attr("target", (user.livecam !== undefined)?'_blank':'_self', true, false)) + " class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span></a></h2><div class=\"b-main-info\"><div class=\"b-age\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "placeholder.n_years", {"{n}": user.age})) ? "" : jade_interp)) + "</div>");
if (user.livecam !== undefined)
{
buf.push("<div class=\"b-live-cam\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.on_cam_now")) ? "" : jade_interp)) + "</div><div class=\"b-live-cam-sponsor\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.this_is_cam_profile")) ? "" : jade_interp)) + "</div><div class=\"b-live-cam-chat\"><a" + (jade.attr("href", user.userViewLink, true, false)) + (jade.attr("target", (user.livecam !== undefined)?'_blank':'_self', true, false)) + " class=\"b-live-cam-btn btn-text-green\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.join_chat")) ? "" : jade_interp)) + "</a></div>");
}
if (user.livecam === undefined)
{
buf.push("<div class=\"b-location\"><div class=\"location\">");
if(user.geo && user.geo.city && !user.isEntertainmentProfile)
{
buf.push("<span class=\"city\">" + (jade.escape((jade_interp = user.geo.city) == null ? '' : jade_interp)) + ",&nbsp;</span>");
}
if(user.geo && user.geo.country)
{
buf.push("<span class=\"country\">" + (jade.escape(null == (jade_interp = user.geo.country) ? "" : jade_interp)) + "</span>");
}
buf.push("</div></div>");
}
if (user.livecam === undefined)
{
buf.push("<div class=\"b-user-about\">");
if(user.about && !isSiteCompliance)
{
buf.push("<div class=\"about-icon\">" + (jade.escape((jade_interp = user.about) == null ? '' : jade_interp)) + "</div>");
}
buf.push("</div>");
}
if (user.livecam === undefined)
{
buf.push("<div data-btn-activity=\"1\" class=\"b-btn-activity\"></div>");
}
buf.push("</div></div>");
if (user.livecam === undefined)
{
buf.push("<div class=\"b-recent\">");
if(user.activity && user.activity.action)
{
buf.push("<div class=\"b-recent-act b-recent-act--message\">");
if(user.activity.action == "newsFeed")
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("userListWidget","text.notification_newsFeed_"+user.activity.type+"_"+user.activity.txtId+"_"+user.gender)) ? "" : jade_interp)) + "</span>");
}
else if(user.activity.type == "outgoing")
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("userListWidget","text.you_"+user.activity.action+"_user_w_time", {"{n}":user.gender, "{time}":user.activity.time})) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("userListWidget","text.user_"+user.activity.action+"_you_w_time", {"{n}":user.gender, "{time}":user.activity.time})) ? "" : jade_interp)) + "</span>");
}
buf.push("</div>");
}
else if(typeof config === 'undefined' || config.showTextNoActivity)
{
buf.push("<div class=\"b-recent-act b-recent-act--message\">" + (jade.escape(null == (jade_interp = _t("userListWidget","text.no_activity")) ? "" : jade_interp)) + "</div>");
}
buf.push("</div>");
}
buf.push("</div>");
}
else
{
buf.push("<div" + (jade.cls(['user-motivation','column',naughtyClass], [null,null,true])) + "><div class=\"header\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "title.want_more")) ? "" : jade_interp)) + "</span></div><div class=\"um-content square\"><div" + (jade.cls(['main',(currentUser.gender === "male")?"one-img-w":"one-img-m",niche], [null,true,true])) + "></div><div class=\"action-line\"><div class=\"action-text\"><span class=\"text\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.get_a_full_membership")) ? "" : jade_interp)) + "</span></div><div class=\"action-btn\"><a" + (jade.attr("href", user.upgradeBanner, true, false)) + " class=\"btn-send btn-text-green\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.get_it_now")) ? "" : jade_interp)) + "</a></div></div></div></div>");
}
buf.push("</li>");;return buf.join("");
};