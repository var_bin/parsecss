this["JST"] = this["JST"] || {};

this["JST"]["UserListWidgetActivity"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),pageType = locals_.pageType,user = locals_.user,_t = locals_._t,talksEnabled = locals_.talksEnabled,userLink = locals_.userLink;
if(pageType === "blocked")
{
buf.push("<div class=\"b-btn-activity-grid\"><button" + (jade.attr("data-blockbutton", user.id, true, false)) + (jade.attr("title", user.buttons.block.activated?_t("blocked", "tooltip.unblock"):_t("blocked", "tooltip.block_" + user.gender), true, false)) + (jade.cls(['btn-blocked','btn-text-def',user.buttons.block.activated?"activated":""], [null,null,true])) + "><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("blocked", "button.block")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("blocked", "button.unblock")) ? "" : jade_interp)) + "</span></button></div>");
}
else if(user.buttons.message)
{
if(user.usersLastActions && user.usersLastActions[user.id].action == "newsFeed" && user.usersLastActions[user.id].type == "onSmsChatFeed")
{
buf.push("<div class=\"b-btn-activity-grid\"><span" + (jade.attr("onclick", "Backbone.trigger('SmsChat.Start', 'addMsisdnFromNotificationFeedList', '"+user.id+"')", true, false)) + " class=\"btn-activity btn-sms-chat\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "Sms Chat")) ? "" : jade_interp)) + "</span></div>");
}
else if (talksEnabled)
{
buf.push("<div class=\"b-btn-activity-grid\"><button href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('Talks.Start', '" + user.id + "')", true, false)) + " class=\"btn-activity btn-message\"></button></div>");
}
else
{
buf.push("<div class=\"b-btn-activity-grid\"><button data-messagebutton=\"\"" + (jade.attr("title", _t("userListWidget", "button.message"), true, false)) + " class=\"btn-activity btn-message\"></button></div>");
}
if(user.buttons.favorite)
{
buf.push("<div class=\"b-btn-activity-grid\"><button" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.attr("title", _t("userListWidget", "button.friend"), true, false)) + (jade.cls(['btn-activity','btn-favorite','btn-text-def',(user.buttons.favorite.activated)?"activated":""], [null,null,null,true])) + "><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.add_friend")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.friends")) ? "" : jade_interp)) + "</span></button></div>");
}
if(user.buttons.wink)
{
buf.push("<div class=\"b-btn-activity-grid\"><button" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.attr("title", _t("userListWidget", "button.wink"), true, false)) + (jade.cls(['btn-activity','btn-wink','btn-text-def',(user.buttons.wink.activated)?"activated":""], [null,null,null,true])) + "><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.wink")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.winked")) ? "" : jade_interp)) + "</span></button></div>");
}
if(user.photos.count > 1 && user.photos.showCount)
{
buf.push("<div class=\"b-btn-activity-grid\"><a" + (jade.attr("href", userLink(user.id), true, false)) + " class=\"link\"><div class=\"b-photos-count\">" + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "</div></a></div>");
}
};return buf.join("");
};