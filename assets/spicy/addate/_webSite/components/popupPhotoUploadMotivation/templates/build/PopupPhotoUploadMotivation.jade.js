this["JST"] = this["JST"] || {};

this["JST"]["PopupPhotoUploadMotivation"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,users = locals_.users,user = locals_.user,params = locals_.params;
buf.push("<div class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\"><div id=\"funnel-popup-item\" class=\"b-popup b-popup-funnel step-2\"><div class=\"b-popup-content\"><div class=\"header\"><a id=\"PhotoMotivationClose\" href=\"#\"" + (jade.attr("title", _t("popupPhotoUploadMotivation", "button.close"), true, false)) + " class=\"btn-close active\"></a><h1 class=\"title\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "title.upload_photo")) ? "" : jade_interp)) + "</h1></div><div class=\"content\"><div class=\"b-funnel-list-wrap\"><div class=\"list-title\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "title.pay_attention")) ? "" : jade_interp)) + "</div><ul class=\"b-funnel-list\"><li>" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "text.members_with_photo_in_search")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "text.no_answers_without_photo")) ? "" : jade_interp)) + "</li></ul></div><div id=\"PhotoMotivationCloseDragDrop\"></div><div id=\"photoMotivationSearchSamplesList\" class=\"b-search-samples active\">");
if ( users)
{
// iterate [0,1,2]
;(function(){
  var $$obj = [0,1,2];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

if ( users[key])
{
user = users[key]
buf.push("<div class=\"b-user-single\"><div class=\"b-photo\"><div class=\"b-photo-link\"><span class=\"overlay\">&nbsp;</span><span" + (jade.attr("title", _t("popupPhotoUploadMotivation", "tooltip.new_user"), true, false)) + " class=\"b-new\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "text.new")) ? "" : jade_interp)) + "</span><div class=\"image-holder\"><img" + (jade.attr("src", user.photos.url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"photo labels-custom\"/></div><span title=\"photos quantity\" class=\"b-photos-count\">" + (jade.escape(null == (jade_interp = _t('popupPhotoUploadMotivation', 'value.n_photos', {"{n}": user.photos.count})) ? "" : jade_interp)) + "</span></div></div><div class=\"b-user-info\"><h2 class=\"b-screenname\"><div class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span></div></h2><div class=\"b-main-info\"><div class=\"b-age\">" + (jade.escape(null == (jade_interp = _t('popupPhotoUploadMotivation', 'value.n_years', {"{n}": user.age})) ? "" : jade_interp)) + "</div><div class=\"b-location\"><div class=\"location\"><span class=\"city\">" + (jade.escape(null == (jade_interp = user.geo.city+",") ? "" : jade_interp)) + "</span><span class=\"country\">" + (jade.escape(null == (jade_interp = _t("searchForm", user.geo.country)) ? "" : jade_interp)) + "</span></div></div><div class=\"b-miles-away\">" + (jade.escape(null == (jade_interp = _t('popupPhotoUploadMotivation', 'value.n_miles_from_you', {"{n}": user.geo.distanceToMe})) ? "" : jade_interp)) + "</div></div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

if ( users[key])
{
user = users[key]
buf.push("<div class=\"b-user-single\"><div class=\"b-photo\"><div class=\"b-photo-link\"><span class=\"overlay\">&nbsp;</span><span" + (jade.attr("title", _t("popupPhotoUploadMotivation", "tooltip.new_user"), true, false)) + " class=\"b-new\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "text.new")) ? "" : jade_interp)) + "</span><div class=\"image-holder\"><img" + (jade.attr("src", user.photos.url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"photo labels-custom\"/></div><span title=\"photos quantity\" class=\"b-photos-count\">" + (jade.escape(null == (jade_interp = _t('popupPhotoUploadMotivation', 'value.n_photos', {"{n}": user.photos.count})) ? "" : jade_interp)) + "</span></div></div><div class=\"b-user-info\"><h2 class=\"b-screenname\"><div class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span></div></h2><div class=\"b-main-info\"><div class=\"b-age\">" + (jade.escape(null == (jade_interp = _t('popupPhotoUploadMotivation', 'value.n_years', {"{n}": user.age})) ? "" : jade_interp)) + "</div><div class=\"b-location\"><div class=\"location\"><span class=\"city\">" + (jade.escape(null == (jade_interp = user.geo.city+",") ? "" : jade_interp)) + "</span><span class=\"country\">" + (jade.escape(null == (jade_interp = _t("searchForm", user.geo.country)) ? "" : jade_interp)) + "</span></div></div><div class=\"b-miles-away\">" + (jade.escape(null == (jade_interp = _t('popupPhotoUploadMotivation', 'value.n_miles_from_you', {"{n}": user.geo.distanceToMe})) ? "" : jade_interp)) + "</div></div></div></div>");
}
    }

  }
}).call(this);

}
buf.push("</div><div class=\"b-nav-btns\">");
if ( params.returnUrl)
{
buf.push("<a id=\"PhotoMotivationBack\" class=\"btn btn-muted btn-prev btn-text-def\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "button.back")) ? "" : jade_interp)) + "</a>");
}
buf.push("<button id=\"PhotoMotivationSave\" data-fb-photo-upload=\"1\" class=\"btn btn-next btn-text-green\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "button.save")) ? "" : jade_interp)) + "</button></div></div></div></div></div></div><div class=\"b-loader\"></div></div>");;return buf.join("");
};