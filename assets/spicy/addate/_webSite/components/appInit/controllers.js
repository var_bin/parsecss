var searchController = new Backbone.Controller({
    name: 'search',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this,
            url;

        if(params.urlRoot) {
            url = params.urlRoot;
        }
        params = params || {};
        params = _.extend(params, {
            type: 'search',
            urlRoot: url
        });

        Resources.load('user');
        this.region.render(Backbone.View.extend({
            regions: {
                userList: '#userListContainer',
                footer: '#userListFooter',
                flirtcast: '#userListFlirtcast',
                searchForm: '#searchFormContainer'
            },

            render: function() {
                this.$el.html(tpl.render('UserListLayout', {type: self.name}));
                if (params.query) params.query+="&cam:1";
                this.regions.userList.render(UserListView, {
                    loadingRegion: this.region,
                    parentRegions: this.regions,
                    model: new UserListModel({
                        query: params.query ? params.query : '',
                        urlRoot: params.urlRoot ? params.urlRoot : ''
                    }, {
                        collection: new UserListCollection()
                    }),
                    params: params
                });

                this.regions.searchForm.render(SearchFormView, {
                    model: new SearchFormModel(app.appData().get('searchForm')),
                    params: params
                });

                this.regions.flirtcast.render(FlirtcastView, {
                    model: new FlirtcastModel(),
                    params: params
                });

                return this;
            }
        }));
    }
});

var spVideosController = new Backbone.Controller({
    name: 'spVideos',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;
        params = params || {};
        this.region.close();
        Resources.load('spVideos', function() {
            self.region.render(SpVideosView, {
                model: new SpVideosModel(),
                params: params
            });
        });
    }
});

var vodController = new Backbone.Controller({
    name: 'vod',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;
        params = params || {};
        this.region.close();
        Resources.load('vod', function() {
            self.region.render(VodView, {
                model: new VodModel(),
                params: params
            });
        });
    }
});

var wallController = new Backbone.Controller({
    name: 'wall',

    initialize: function() {
        this.region = app.regions.content;

        $(window).scroll(function (){
            if ($(this).scrollTop() > 200){
                $(".to-top").fadeIn();
            } else{
                $(".to-top").fadeOut();
            }
        });
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('wall', function() {
            self.region.render(WallView, {
                model: new WallModel(),
                params: params
            });
        });
    }
});

var viewsController = new Backbone.Controller({
    name: 'views',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('activityTypes', function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: '#userListContainer',
                },

                render: function() {
                    this.$el.html(tpl.render('UserListLayout', {type: self.name}));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: 'views',
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var messengerController = new Backbone.Controller({
    name: 'messenger',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('activityTypes', function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: '#userListContainer',
                },

                render: function() {
                    this.$el.html(tpl.render('UserListLayout', {type: self.name}));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: 'messenger',
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var blockedController = new Backbone.Controller({
    name: "blocked",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};
        params = _.extend(params, {
            type: "blocked"
        });

        Resources.load("activityTypes", function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: "#userListContainer",
                },

                render: function() {
                    this.$el.html(tpl.render("UserListLayout", {type: self.name}));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: "blocked",
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var winksController = new Backbone.Controller({
    name: 'winks',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('activityTypes', function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: '#userListContainer',
                },

                render: function() {
                    this.$el.html(tpl.render('UserListLayout', {type: self.name}));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: 'winks',
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var favoritesController = new Backbone.Controller({
    name: 'favorites',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('activityTypes', function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: '#userListContainer',
                },

                render: function() {
                    this.$el.html(tpl.render('UserListLayout', {type: self.name}));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: 'favorites',
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var newsFeedController = new Backbone.Controller({
    name: 'newsFeed',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('activityTypes', function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: '#userListContainer',
                },

                render: function() {
                    this.$el.html(tpl.render('UserListLayout', {type: self.name}));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: 'newsFeed',
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var accountController = new Backbone.Controller({
    name: 'account',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('account', function() {
            self.region.render(AccountView, {
                model: new AccountModel(),
                params: params
            });
        });
    },
    unsubscribeAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('unsubscribe', function() {
            self.region.render(UnsubscribeView, {
                model: new UnsubscribeModel(),
                params: params
            });
        });
    }
});

var billingHistoryController = new Backbone.Controller({
    name: 'billingHistory',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('billingHistory', function() {
            self.region.render(BillingHistoryView, {
                model: new BillingHistoryModel(),
                params: params
            });
        });
    }
});

var profileController = new Backbone.Controller({
    name: 'Profile',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('profile', function() {
            self.region.render(ProfileView, {
                model: new ProfileModel(),
                params: params
            });
        });
    }
});

var userController = new Backbone.Controller({
    name: 'user',

    initialize: function() {
        this.region = app.regions.content;
    },

    viewAction: function(params) {
        var self = this;

        app.regions.popup.close();
        this.region.close();

        Resources.load('user', function() {
            self.region.render(UserView, {
                model: new UserModel(),
                params: params
            });
        });
    },

    massBlockedAction: function(params) {
        var self = this;

        this.region.close();
        Resources.load("reCaptcha", function() {
            self.region.render(ReCaptchaView, {
                loadingRegion: self.region,
                model: new ReCaptchaModel(),
                params: params
            });
        });
    }
});

var siteController = new Backbone.Controller({
    name: 'site',

    initialize: function() {
        this.region = app.regions.content;
    },

    contactUsAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('contactUs', function() {
            self.region.render(ContactUsView, {
                model: new ContactUsModel(),
                params: params
            });
        });
    }
});

var photoUploadMotivationController = new Backbone.Controller({
    name: 'photoUploadMotivation',

    initialize: function() {
        this.region = app.regions.popup;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('popupPhotoUploadMotivation', function() {
            self.region.render(PopupPhotoUploadMotivationView, {
                model: new PopupPhotoUploadMotivationModel(),
                params: params
            });
        });

        //load search
        searchController.run({action: 'index'});
    }
});

var funnelController = new Backbone.Controller({
    name: 'funnel',

    initialize: function() {
        this.region = app.regions.popup;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('funnel', function() {
            self.region.render(FunnelView, {
                model: new FunnelModel(),
                params: params
            });

            var funnelModel = new FunnelModel();
            funnelModel.fetch({
                success: function(model) {
                    var modelData = model.toJSON();
                    var defaultSearchParams = modelData['defaultSearchParams'];
                    var queryString = '';

                    for(var searchParam in defaultSearchParams){
                        var paramName = (searchParam == 'locationInput') ? 'location' : searchParam;
                        if($.isArray(defaultSearchParams[searchParam])) {
                            for(var innerParams in defaultSearchParams[searchParam]){
                                queryString += paramName+'[]:'+defaultSearchParams[searchParam][innerParams]+'&';
                            }
                        } else {
                            queryString += paramName+':'+defaultSearchParams[searchParam]+'&';
                        }
                    }

                    searchController.run({
                        action: 'index',
                        options:{query: queryString, urlRoot: "/searchFunnel"}
                    });
                }
            });
        });
    }
});

var photoController = new Backbone.Controller({
    name: 'photo',

    initialize: function() {
        this.region = app.regions.popup;
    },

    galleryAction: function(params) {
        var self = this;

        params.open = params.open && parseInt(params.open) || 0;

        Resources.load('photoGallery', function() {
            var renderGallery = function() {
                self.region.render(UserPhotoGalleryView, {
                    model: new UserPhotoGalleryModel({
                        id: params.id,
                        open: params.open
                    })
                });
            };

            if (!app.regions.content.isRendered()) {
                if (params.id) {
                    Resources.load('user', function () {
                        userController.run({
                            action: 'view',
                            options: {id: params.id}
                        });
                        renderGallery();
                    });
                } else {
                    renderGallery();
                }
            } else {
                renderGallery();
            }
        });
    }
});

var roomsController = new Backbone.Controller({
    name: "rooms",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        this.region.close();
        Resources.load("rooms", _.bind(function() {
            this.region.render(RoomsView, {
                model: new RoomsModel(),
                params: params
            });
        }, this));
    }
});

var helpController = new Backbone.Controller({
    name: 'help',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;
        params   = params || {};

        this.region.close();

        Resources.load('helpSection', function() {
            self.region.render(HelpSectionView, {
                model : new HelpSectionModel(),
                params: params
            });
        });
    }
});

var smsChatFeedController = new Backbone.Controller({
    name: "smsChatFeed",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("activityTypes", function() {
            Backbone.trigger('Toolbar.changeActivityMenu', [self.name]);

            self.region.render(Backbone.View.extend({
                initialize: function() {
                    this.listenTo(Backbone, 'userCollection', this.showHeader);
                },

                regions: {
                    userList: "#userListContainer",
                },

                render: function() {
                    this.$el.html(tpl.render("UserListLayout", {type: self.name}));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: "smsChatFeed",
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                },

                showHeader:function(length) {
                    if(length) {
                        this.$('.b-activity-push').show();
                    }
                }
            }));
        });
    }
});

var declineController = new Backbone.Controller({
    name: 'decline',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        app.regions.content.close();
        this.region.close();
        this.region.render(declineView, {});

    }
});
