window.app = new Backbone.App({
    regions: {
        content: '#layoutContent',
        activityNotification: '#activityNotification',
        toolbar: '#toolbar',
        userNav: '#userNav',
        popup: '#layoutPopup',
        ear: "#ear",
        talks: '#cTalks',
        naughtyMode: "#naughtyMode"
    },

    showPopup: function(params) {
        this.regions.popup.render(PopupSimpleView, {
            model: new PopupSimpleModel({
                continueUrl : params.url
            }),
            template : params.template,
            data     : params
        });
    },

    ready: function() {
        var self = this;

        _.defer(function() {
            self.regions.userNav.render(UserNavView, {
                model: new UserNavModel()
            });
        });

        _.defer(function() {
            self.regions.activityNotification.render(ActivityNotificationView, {
                model: new ActivityNotificationModel()
            });
        }, 10);

        _.defer(function() {
            self.regions.naughtyMode.render(NaughtyModeView, {
                model: new NaughtyMode()
            });
        });

        this.regions.ear.render(FeedbackShowView, {
            model: new FeedbackShowModel()
        });

        this.regions.toolbar.render(ToolbarView, {
            model: new ToolbarModel(this.appData().get('toolbar'))
        });

        _.defer(function() {
            var talksMessengerConfig = app.appData().get('talksMessengerConfig');

            if (talksMessengerConfig && talksMessengerConfig.enabled) {
                self.regions.talks.render(TalksView, {
                    model: new TalksModel()
                });
            }
        });

        Resources.load(["Coregistration, Banner"], function() {
            new CoregistrationView({
                model: new CoregistrationModel
            });
            new BannerOpenxView({
                model: new BannerOpenx
            });
            new BannerPopunderView({
                model: new BannerPopunder
            });
        });
        if (typeof CoregistrationPopupView != "undefined") {
            new CoregistrationPopupView({
                model: new CoregistrationPopupModel()
            });
        }

        if (self.unreadActions) {
            self.unreadActions().fetch();
        }

        if (app.appData().get('photoMotivationFrequencyShows')) {
            var photoMotivationShows = 0;
            var photoMotivationIteration = 0;
            var photoMotivationFrequency = app.appData().get('photoMotivationFrequencyShows');
            Backbone.on("userListViewRendered userViewRendered", function() {
                var event = _.first(Backbone.trigger.arguments);
                if (app.appData().get('user').photoCounts ||
                    photoMotivationShows >= photoMotivationFrequency.maxShows ||
                    !photoMotivationFrequency[event] ||
                    ++photoMotivationIteration < photoMotivationFrequency[event]
                ) {
                    return;
                }

                app.regions.content.loading();

                photoMotivationShows++;
                photoMotivationIteration = 0;
                var scenario = (event == 'userViewRendered') ? 'ViewProfile' : 'Search';
                Resources.load("popupPhotoUploadMotivation", function () {
                    app.regions.popup.render(PopupPhotoUploadMotivationView, {
                        model: new PopupPhotoUploadMotivationModel(),
                        scenario: scenario
                    });
                });
            });

            Backbone.on("PopupPhotoUploadMotivationView::popupRendered", function() {
                app.regions.content.ready();
            });
        }
    }
});

$(function() {
    app.initialize();

    $(document).on('click', 'a[href^="/#"]', function(event) {
        event.preventDefault();
        app.router.navigate($(this).attr('href'), {
            trigger: true
        });
    });
});
