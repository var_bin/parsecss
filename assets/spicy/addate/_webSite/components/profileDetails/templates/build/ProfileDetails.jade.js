this["JST"] = this["JST"] || {};

this["JST"]["ProfileDetails"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,profile = locals_.profile,formParams = locals_.formParams;
buf.push("<div class=\"data-column-wrap\">");
var fieldsList = ["sexual_orientation", "marital_status", "children", "living", "race", "religion", "height", "weight", "build", "hair_color", "eye_color", "tattoo", "pierced", "smoke", "drink", "education", "income"];
{
// iterate fieldsList
;(function(){
  var $$obj = fieldsList;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field select-output-dropdown dark\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><ul data-items-list=\"data-items-list\" class=\"list\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field select-output-dropdown dark\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><ul data-items-list=\"data-items-list\" class=\"list\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  }
}).call(this);

buf.push("<div id=\"coregPlacementProfileDetails\" class=\"data-single coreg-placement-profile-details\"></div><div class=\"data-single ft-rt\"><div class=\"btn-row\"><div id=\"saveStatus\" style=\"display:none;\" class=\"info-save-status\"></div><button data-btn-undo=\"1\" class=\"btn-cancel btn-text-def\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "button.cancel")) ? "" : jade_interp)) + "</button><button data-btn-save=\"1\" class=\"btn-save btn-text-green\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "button.save")) ? "" : jade_interp)) + "</button></div></div>");
}
buf.push("</div>");;return buf.join("");
};