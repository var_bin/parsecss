this["JST"] = this["JST"] || {};

this["JST"]["Coregistration"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),coregistration = locals_.coregistration;
if ( (coregistration && coregistration.coreg_id))
{
buf.push("<label class=\"coreg-wrapper\"><input type=\"checkbox\" name=\"coreg_profile\" id=\"coreg_profile\" value=\"\"/><span></span><label class=\"coreg-field-label\">" + (jade.escape(null == (jade_interp = coregistration.label_translation) ? "" : jade_interp)) + "<span class=\"tip button\">?</span><div class=\"tip-content\">" + (null == (jade_interp = coregistration.termsString) ? "" : jade_interp) + "</div></label></label>");
};return buf.join("");
};