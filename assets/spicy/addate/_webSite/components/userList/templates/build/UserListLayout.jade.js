this["JST"] = this["JST"] || {};

this["JST"]["UserListLayout"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),type = locals_.type;
if ( type === "search")
{
buf.push("<div class=\"w-search-form\"><div class=\"r-frame\"><div id=\"searchFormContainer\" class=\"b-search-form b-fixed\"></div></div></div>");
}
buf.push("<div" + (jade.cls(['b-grid-content',(type !== "search")?"activity-search":""], [null,true])) + "><ul class=\"rows-holder\"><li class=\"row grid-row\"><div class=\"l-frame\"><div class=\"b-search\"><div class=\"b-search-results\"><div id=\"openx-banner-placement-userlisttop\" class=\"userlisttop-banner-openx\"></div><ul id=\"userListContainer\" class=\"b-search-grid\"><li id=\"userListFlirtcast\" style=\"display: none;\" class=\"b-grid-item\"></li></ul><div id=\"openx-banner-placement-userlistbottom\" class=\"userlistbottom-banner-openx\"></div></div></div><footer id=\"userListFooter\"></footer></div></li></ul></div>");;return buf.join("");
};