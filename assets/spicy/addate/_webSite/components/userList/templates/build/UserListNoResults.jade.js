this["JST"] = this["JST"] || {};

this["JST"]["UserListNoResults"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),type = locals_.type,_t = locals_._t;
buf.push("<li class=\"b-grid-item related-search\"><div class=\"rel-search\"><div class=\"b-search-noresult\"><div class=\"b-search-noresult__message\"><div>");
if ( (type == "winks"))
{
buf.push(jade.escape(null == (jade_interp = _t("userList", "text.no_winks")) ? "" : jade_interp));
}
else if ( (type == "views"))
{
buf.push(jade.escape(null == (jade_interp = _t("userList", "text.no_views")) ? "" : jade_interp));
}
else if ( (type == "favorites"))
{
buf.push(null == (jade_interp = _t("userList", "text.no_favorites", {"{a}": "<a href=\"/#search\">", "{/a}" : "</a>"})) ? "" : jade_interp);
}
else if ( (type == "newsFeed"))
{
buf.push(jade.escape(null == (jade_interp = _t("userList", "text.newsFeed_header")) ? "" : jade_interp));
}
else if ( (type == "smsChatFeed"))
{
buf.push(jade.escape(null == (jade_interp = _t("userList", "text.smsChatFeed_header")) ? "" : jade_interp));
}
else if ( (type == "blocked"))
{
buf.push(jade.escape(null == (jade_interp = _t("blocked", "text.no_users")) ? "" : jade_interp));
}
else
{
buf.push(jade.escape(null == (jade_interp = _t("userList", "text.no_matches")) ? "" : jade_interp));
}
buf.push("</div>");
if ( (type))
{
buf.push("<button id=\"userBackToSearchButton\" class=\"userBackToSearchButton b-search-noresult__button btn-text-green\">" + (jade.escape(null == (jade_interp = _t("userList", "text.back-to-search")) ? "" : jade_interp)) + "</button>");
}
buf.push("</div></div></div></li>");;return buf.join("");
};