this["JST"] = this["JST"] || {};

this["JST"]["UserListCompletion"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<li class=\"b-grid-item related-search\"><div class=\"rel-search\"><div class=\"b-completion-divider\">" + (jade.escape(null == (jade_interp = _t("userList", "title.related_results")) ? "" : jade_interp)) + "</div></div></li>");;return buf.join("");
};