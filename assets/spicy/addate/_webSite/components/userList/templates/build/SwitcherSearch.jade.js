this["JST"] = this["JST"] || {};

this["JST"]["SwitcherSearch"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<div class=\"content-settings\"><div id=\"userListSort\" class=\"switcher\"><div class=\"switcher-cell\"><a data-item=\"near_you\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.all_members")) ? "" : jade_interp)) + "</a></div><div class=\"switcher-cell\"><a data-item=\"online\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.online_now")) ? "" : jade_interp)) + "</a></div><div class=\"switcher-cell\"><a data-item=\"new_members\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.new_members")) ? "" : jade_interp)) + "</a></div></div></div>");;return buf.join("");
};