this["JST"] = this["JST"] || {};

this["JST"]["SwitcherActivity"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<div class=\"content-settings\"><div class=\"switcher\"><div data-nav-item=\"messenger\" class=\"switcher-cell\"><a href=\"/#messenger\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.messenger")) ? "" : jade_interp)) + "<span data-counter=\"mail\" class=\"action-counter active\"></span></a></div><div data-nav-item=\"views\" class=\"switcher-cell\"><a href=\"/#views\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.views")) ? "" : jade_interp)) + "<span data-counter=\"view\" class=\"action-counter active\"></span></a></div><div data-nav-item=\"winks\" class=\"switcher-cell\"><a href=\"/#winks\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.winks")) ? "" : jade_interp)) + "<span data-counter=\"wink\" class=\"action-counter active\"></span></a></div><div data-nav-item=\"newsFeed\" class=\"switcher-cell\"><a href=\"/#newsFeed\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.newsFeed")) ? "" : jade_interp)) + "<span data-counter=\"newsFeed\" class=\"action-counter active\"></span></a></div></div></div>");;return buf.join("");
};