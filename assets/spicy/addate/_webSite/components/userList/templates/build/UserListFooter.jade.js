this["JST"] = this["JST"] || {};

this["JST"]["UserListFooter"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,requirements = locals_.requirements,termsAndConditions = locals_.termsAndConditions,privacyPolicy = locals_.privacyPolicy;
jade_mixins["link"] = function(linkData, type){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if ( linkData && linkData.link)
{
buf.push("<li class=\"item\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "return !window.open('"+linkData.link+"','"+type+"','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=750,height=600');", true, false)) + " class=\"link\">" + (jade.escape(null == (jade_interp = _t("userList", "text.footer_link_"+type)) ? "" : jade_interp)) + "</a></li>");
}
};
buf.push("<div class=\"link-holder\"><ul class=\"link-list\">");
jade_mixins["link"](requirements, "requirementsComplianceStatement");
jade_mixins["link"](termsAndConditions, "terms");
jade_mixins["link"](privacyPolicy, "privacypolicy");
buf.push("</ul></div>");;return buf.join("");
};