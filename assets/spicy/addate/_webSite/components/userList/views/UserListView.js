var UserListView = Backbone.View.extend({
    scrollPosition: 0,

    bannersOpenxPlacements : [],

    positionCounter: 0,
    
    activityBanner: ['mail','wink','newsFeed','view','favorites'],
    
    toBufferCounter: 0,

    events: {
        "click #userBackToSearchButton": "backToSearch",
        "click [data-blockbutton]" : function(event) {
            event.preventDefault();
            var $buttonElement = $(event.currentTarget);

            var uid = $buttonElement.data("blockbutton");
            var blockStatus = $buttonElement.hasClass("activated");

            var model = new BlockStatusModel({
                uid: uid,
                status: blockStatus
            });

            if ($buttonElement.hasClass("loading")) {
                return;
            }
            $buttonElement.addClass("loading");

            model.saveAjax({
                success: function() {
                    if (blockStatus) {
                        $buttonElement.removeClass("activated")
                                      .removeClass("loading");
                    } else {
                        $buttonElement.addClass("activated")
                                      .removeClass("loading");
                    }
                },
                error: function() {
                    $buttonElement.removeClass("loading");
                }
            });
        }
    },

    initialize: function(options) {
        this.buffer = [];
        this.completionStarted = false;
        this.loadingRegion = options.loadingRegion;

        this.listenTo(this.model.collection, 'add', this.toBuffer);
        this.listenTo(this.model.collection, 'ready', this.renderUsers);
        this.listenTo(this.model, 'sync', this.renderFooter);
        this.listenTo(this.model, 'sync', this.processBanners);

        this.model.fetch();

        $(window).on('scroll.UserList', $.proxy(this.scroll, this));
    },

    renderFooter: function() {
        var modelData = this.model.toJSON();
        if ((modelData.requirements && modelData.requirements.link) ||
            (modelData.termsAndConditions && modelData.termsAndConditions.link) ||
            (modelData.privacyPolicy && modelData.privacyPolicy.link)) {
            this.options.parentRegions.footer.render(Backbone.View.extend({
                render: function() {
                    //this.$el.html(tpl.render('UserListFooter', modelData));
                    this.region.ready();

                    return this;
                }
            }));
        }
    },

    renderUsers: function() {
        var modelData = this.model.toJSON(),
            type = modelData.attributes && modelData.attributes.type || null;

        if (type) {
            Backbone.trigger('Search.CurrentType', type);
        }

        if (!this.model.collection.length) {
            this.buffer.push(tpl.render('UserListNoResults', this.model));
            this.toggleMotivationalText();
        }

        this.$el.append(this.buffer);
        this.loadingRegion.ready();
        this.buffer = [];
        if ($('#alertEmailValidationBlock').length && $('#alertEmailValidationBlock').css('display') == 'block') {
            $('.b-grid-content').addClass('validation-toolbar');
        }

        if (this.model.isRequesting() && !this.model.has('activity')) {
            Backbone.trigger('userListViewRendered');
        }
    },

    backToSearch: function() {
        app.router.navigate("/search", { trigger: true });
    },

    toBuffer: function(model, f) {
        var data = model.toJSON();

        if (!this.completionStarted && data.isCompletionUser) {
            if (data.userIndex === 0) {
                this.buffer.push(tpl.render('UserListNoResults'));
                this.toggleMotivationalText();
            }

            this.buffer.push(tpl.render('UserListCompletion'));
            this.completionStarted = true;
        }

        data.usersLastActions = this.model.toJSON().usersLastActions;

        var widget = this.region.widget(UserListWidgetView, {
            model: new UserListWidgetModel(data),
            params: this.options.params || {},
            niche: this.model.get('niche')
        });

        var bannersox = this.model.get("bannersox");

        if (bannersox && bannersox.properties &&
            bannersox.properties.frequency &&
            bannersox.properties.position &&
            bannersox.data &&
            bannersox.data.placements &&
            data ) {

            this.positionCounter++;
            var freq = bannersox.properties.frequency;
            var pos  = bannersox.properties.position;
            var placements = bannersox.data.placements;

            if ( placements.length &&
                ((this.positionCounter / freq) % 1 === 0 || this.positionCounter === pos)
            ) {
                var bannersOpenxPlacementId = placements.shift();
                var bannersOpenxPlacement = tpl.render('BannerUserListItem', {'placementId':bannersOpenxPlacementId});
                this.buffer.push(bannersOpenxPlacement);
                this.bannersOpenxPlacements.push(bannersOpenxPlacementId);
                this.bannersOpenxPlacements = _.uniq(this.bannersOpenxPlacements);
                if(this.positionCounter !== pos)
                    this.positionCounter++;
            }
        }
        this.toBufferCounter++;
        this.buffer.push(widget.el);
    },

    scroll: function(event) {
        var currentScroll = $(window).scrollTop(),
            params = this.model.get('params');
        if (this.model.get('showMoreUsersInSearch')) {
            if (!this.model.fetching &&
                params &&
                (!params.more || this.model.userBuffer) &&
                currentScroll >= this.scrollPosition &&
                this.region.$el().outerHeight() - $(window).height() - currentScroll < 100) {
                this.model.more(this.loadingRegion);
            }
        }

        this.scrollPosition = currentScroll;
    },

    remove: function() {
        $(window).off('scroll.UserList');
    },

    processBanners: function() {
        var bannersOpenxPlacements = [];
        var activity = false;
        if(!$.isEmptyObject(this.model.get('activity')) &&
            $.inArray(this.model.get('activity'), this.activityBanner)!==-1){
            activity = true;
        }
        if(activity){
            bannersOpenxPlacements.push('userlisttop');
        }
        if(this.toBufferCounter && activity){
            bannersOpenxPlacements.push('userlistbottom');
        }
        for (var i = 0; i < this.bannersOpenxPlacements.length; i++) {
            bannersOpenxPlacements.push(this.bannersOpenxPlacements[i]);
        }
        Backbone.trigger('BannerOpenx.process', {
            'id' : bannersOpenxPlacements.join(','),
            'container': '#openx-banner-placement-',
            'asyncLoading': false,
            'success': function(response) {
                if(response && response.div_id){
                    $("."+response.div_id).parents('li.search-banner-openx').show();
                }
            }
        });
    },

    toggleMotivationalText: function() {
        $("[data-motivational-text]").toggle();
    }
});
