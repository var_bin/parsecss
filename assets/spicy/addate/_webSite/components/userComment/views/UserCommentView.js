// User comment view
var UserCommentView = Backbone.View.extend({
    // Object types
    objectType  : 0,
    objectId    : 0,
    action      : '',
    reverse     : false,

    events: {
        "click .comment-input-btn": "addComment",
        "keyup .comment-input-text": "processKey"
    },

    initialize: function(options) {
        this.collection = new UserComments([], {
            objectType: options.objectType,
            objectId: options.objectId,
            reverse: options.reverse
        });

        this.objectType = options.objectType || 0;
        this.objectId = options.objectId || 0;

        var comments = this.collection;
        comments.on("reset", this.setReset, this);
        comments.on("add", this.setCreate, this);
        comments.on("sync", this.processRequest, this);
        comments.on("invalid", this.showError, this);
        comments.on("error", this.showServerError, this);

        var reqData = {};
        if (this.options.reverse) {
            reqData['reverse'] = 1;
        }
        this.collection.fetch({reset: true, data: reqData});
    },

    render: function() {
        var commentsHtml = this.renderComments(this.collection.toJSON());

        var commentForm = tpl.render('commentForm', {
            user: app.appData().get('user')
        });

        if (this.options.reverse) {
            this.$el.html(commentForm + commentsHtml);
        } else {
            this.$el.html(commentsHtml + commentForm);
        }

        this.initForm(true);
        return this;
    },

    initForm: function(reset) {
        this.$el.find('.comment-input-text').prop('disabled', null);
        this.$el.find('.comment-input-btn').show();
        if (reset) {
            this.$el.find('.comment-input-text').val('');
        }
    },

    blockForm: function() {
        this.$el.find('.comment-input-btn').hide();
        this.$el.find('.comment-input-text').prop('disabled', true);;
    },

    setReset: function() {
        this.options.action = 'reset';
    },

    setCreate: function() {
        this.options.action = 'create'
    },

    processRequest: function(model_or_collection, resp, options) {
        switch (this.options.action) {
            case 'reset':
                this.render();
                break;
            case 'create':
                this.appendComment(model_or_collection, this.collection, options);
                break;
            default:
                this.appendComments(model_or_collection, resp, options);
        }
        this.options.action = '';
    },

    renderComments: function(models) {
        if (!$.isArray(models)) {
            models = [models]
        }
        return tpl.render('comments', {
            comments: models
        });
    },

    renderComment: function(model) {
        return tpl.render('comment', model);
    },

    appendComment: function(model, collection, options) {
        var contentHTML = '',
            commentsBlock = this.$el.find('.single-vod-comments__list'),
            commentHead = $('.single-vod-comments__title');

        if (commentsBlock.length > 0) {
            contentHTML = tpl.render('comment', {comment: model.toJSON(), isFirstComment: false});
            if (this.options.reverse) {
                commentsBlock.prepend(contentHTML);
            } else {
                commentsBlock.append(contentHTML);
            }
        } else {
            contentHTML = tpl.render('comment', {comment: model.toJSON(), isFirstComment: true});
            this.$el.find('.comment-form').before(contentHTML);
        }
        commentHead.text($('.comment-item').length + commentHead.text().replace(/\d/g, ""))

        this.initForm(true);
    },

    appendComments: function(collection, resp, options) {
        var comments = [];
        resp.data.comments.forEach(function(comment, i, arr) {
            comments.push(collection.get(comment.id).toJSON());
        });
        var contentHTML = this.renderComments(comments);

        if (this.options.reverse) {
            this.$el.append(contentHTML);
        } else {
            this.$el.prepend(contentHTML);
        }
    },

    showError: function() {
        this.initForm(false);
    },

    showServerError: function() {
        this.initForm(false);
        console.log('Error');
    },

    addComment: function() {
        this.blockForm();
        var newComment = this.collection.create(
            {'comment': this.$el.find('.comment-input-text').val()},
            {wait: true, type: "post"}
        );
        if (!newComment || !newComment.isValid()) {
            this.showError();
        }
    },

    processKey: function(event) {
        if (event.which === 13) {
            this.addComment();
        }
    }
});
