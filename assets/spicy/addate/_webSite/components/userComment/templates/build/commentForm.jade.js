this["JST"] = this["JST"] || {};

this["JST"]["commentForm"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user;
buf.push("<div class=\"b-comment__item b-comment__table comment-form\"><div class=\"b-comment__item-avatar b-comment__table-cell-ava\"><img" + (jade.attr("src", '' + (user.photoUrl) + '', true, false)) + " class=\"b-comment__item-avatar-item\"/></div><div class=\"b-comment__item-info-massage search-box-b b-comment__table-cell\"><div class=\"search-box-b__content\"></div><input type=\"text\" class=\"search-box-b__item comment-input-text\"/><div class=\"b-comment__item-info-massage-icon icon-Icon_2 comment-input-btn\"></div></div></div>");;return buf.join("");
};