this["JST"] = this["JST"] || {};

this["JST"]["comments"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,comments = locals_.comments,userLink = locals_.userLink;
buf.push("<h3 class=\"single-vod-comments__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'title.comments', {'{n}' : comments.length})) ? "" : jade_interp)) + "</h3>");
if ( (comments.length > 0))
{
buf.push("<div class=\"single-vod-comments__container\"><div class=\"single-vod-comments__list\">");
// iterate comments
;(function(){
  var $$obj = comments;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var commentItem = $$obj[$index];

buf.push("<div class=\"b-comment__item comment-item\"><a" + (jade.attr("href", '' + (userLink(commentItem.user.id)) + '', true, false)) + " class=\"b-comment__item-avatar\"><img" + (jade.attr("src", '' + (commentItem.user.photos.url) + '', true, false)) + " class=\"b-comment__item-avatar-item\"/></a><div class=\"b-comment__item-info-massage\"><!--span.b-comment__item-info-massage-nick(href='#{userLink(commentItem.user.id)}')= commentItem.user.login--><div class=\"b-comment__item-info-massage-content\">" + (jade.escape(null == (jade_interp = commentItem.comment) ? "" : jade_interp)) + "</div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var commentItem = $$obj[$index];

buf.push("<div class=\"b-comment__item comment-item\"><a" + (jade.attr("href", '' + (userLink(commentItem.user.id)) + '', true, false)) + " class=\"b-comment__item-avatar\"><img" + (jade.attr("src", '' + (commentItem.user.photos.url) + '', true, false)) + " class=\"b-comment__item-avatar-item\"/></a><div class=\"b-comment__item-info-massage\"><!--span.b-comment__item-info-massage-nick(href='#{userLink(commentItem.user.id)}')= commentItem.user.login--><div class=\"b-comment__item-info-massage-content\">" + (jade.escape(null == (jade_interp = commentItem.comment) ? "" : jade_interp)) + "</div></div></div>");
    }

  }
}).call(this);

buf.push("</div></div>");
};return buf.join("");
};