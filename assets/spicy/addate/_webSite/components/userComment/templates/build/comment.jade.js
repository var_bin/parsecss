this["JST"] = this["JST"] || {};

this["JST"]["comment"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),isFirstComment = locals_.isFirstComment,userLink = locals_.userLink,comment = locals_.comment;
if (isFirstComment)
{
buf.push("<div class=\"single-vod-comments__container\"><div class=\"single-vod-comments__list\"><div class=\"b-comment__item comment-item\"><a" + (jade.attr("href", '' + (userLink(comment.user.id)) + '', true, false)) + " class=\"b-comment__item-avatar\"><img" + (jade.attr("src", '' + (comment.user.photos.url) + '', true, false)) + " class=\"b-comment__item-avatar-item\"/></a><div class=\"b-comment__item-info-massage\"><!--span.b-comment__item-info-massage-nick(href='#{userLink(comment.user.id)}')= comment.user.login--><div class=\"b-comment__item-info-massage-content\">" + (jade.escape(null == (jade_interp = comment.comment) ? "" : jade_interp)) + "</div></div></div></div></div>");
}
else
{
buf.push("<div class=\"b-comment__item comment-item\"><a" + (jade.attr("href", '' + (userLink(comment.user.id)) + '', true, false)) + " class=\"b-comment__item-avatar\"><img" + (jade.attr("src", '' + (comment.user.photos.url) + '', true, false)) + " class=\"b-comment__item-avatar-item\"/></a><div class=\"b-comment__item-info-massage\"><!--span.b-comment__item-info-massage-nick(href='#{userLink(comment.user.id)}')= comment.user.login--><div class=\"b-comment__item-info-massage-content\">" + (jade.escape(null == (jade_interp = comment.comment) ? "" : jade_interp)) + "</div></div></div>");
};return buf.join("");
};