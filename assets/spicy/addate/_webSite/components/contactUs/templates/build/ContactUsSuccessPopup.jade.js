this["JST"] = this["JST"] || {};

this["JST"]["ContactUsSuccessPopup"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<div class=\"b-popup contact-us\"><button" + (jade.attr("title", _t("contactUs", "button.close"), true, false)) + " href=\"#\" id=\"popupClose\" class=\"btn-close grey\"></button><p>" + (jade.escape(null == (jade_interp = _t("contactUs", "text.request_submitted")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("contactUs", "text.thanks")) ? "" : jade_interp)) + "</p><button id=\"popupContinue\" class=\"btn btn-text-green\">" + (jade.escape(null == (jade_interp = _t("contactUs", "button.continue")) ? "" : jade_interp)) + "</button></div>");;return buf.join("");
};