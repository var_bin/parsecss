this["JST"] = this["JST"] || {};

this["JST"]["PopupSimple"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),template = locals_.template;
buf.push("<div class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\">" + (null == (jade_interp = template) ? "" : jade_interp) + "</div></div><div class=\"b-loader\"></div></div>");;return buf.join("");
};