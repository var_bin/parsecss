this["JST"] = this["JST"] || {};

this["JST"]["PopupStopAction"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,dictionary = locals_.dictionary,cancelBillingPageBeforeRemove = locals_.cancelBillingPageBeforeRemove,stage = locals_.stage,error = locals_.error,password = locals_.password,paymentStatus = locals_.paymentStatus,siteInfo = locals_.siteInfo,notificationSettings = locals_.notificationSettings,notificationSubscription = locals_.notificationSubscription,profileStatus = locals_.profileStatus,reasons = locals_.reasons,viaPhoneCode = locals_.viaPhoneCode,email = locals_.email,hasBeenCancelled = locals_.hasBeenCancelled,userSubscriptionExpire = locals_.userSubscriptionExpire,viaCode = locals_.viaCode,code = locals_.code,hotLineCall = locals_.hotLineCall,publicId = locals_.publicId;
jade_mixins["popupTitle"] = function(title, replacement){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<h1 class=\"title\"><span class=\"you-sure\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+"."+title, replacement)) ? "" : jade_interp)) + "</span></h1>");
};
jade_mixins["cancelProgress"] = function(step){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div class=\"steps-wrap\"><span class=\"step-number\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.stage") + " " + step) ? "" : jade_interp)) + "</span><ul class=\"step-map\">");
for(var i = 1; i <= 5; i++)
{
var itemClass = (i == step) ? "active" : ((i < step) ? "pass" : "")
buf.push("<li" + (jade.cls(['items',"item-"+i+" "+itemClass], [null,true])) + "></li>");
}
buf.push("</ul></div>");
};
buf.push("<div id=\"remove-account-popup\" class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\"><div class=\"b-popup b-remove-profile cancel-billing\"><button id=\"close-popup\"" + (jade.attr("title", _t("popupStopAction", "button.close"), true, false)) + " class=\"btn-close\"></button>");
if ( (dictionary == "remove-account" && cancelBillingPageBeforeRemove == true))
{
buf.push("<div class=\"b-popup-content cancel-billing\">");
jade_mixins["popupTitle"](dictionary);
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.remove-account.cancel-repeat-billing")) ? "" : jade_interp)) + "</p></div>");
}
else if ( (typeof stage == "undefined" || stage == "checkPassword"))
{
buf.push("<div class=\"b-popup-content step-1\">");
jade_mixins["popupTitle"]("check-password-stage-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".check-password-main-text")) ? "" : jade_interp)) + "</p><div class=\"form\"><span class=\"desc\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.your-password")) ? "" : jade_interp)) + "</span><p id=\"password-error\" class=\"desc\">" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</p><input type=\"password\" id=\"password\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.password"), true, false)) + (jade.attr("value", password, true, false)) + "/><a id=\"remind-password\" href=\"#\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.remind-password")) ? "" : jade_interp)) + "</a></div><div class=\"footer\">");
jade_mixins["cancelProgress"](1);
buf.push("<button id=\"check-password\" class=\"btn next btn-text-green\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div>");
}
else
{
if ((typeof stage != "undefined"))
{
switch (stage){
case "statusMembership":
buf.push("<div class=\"b-popup-content b-step step-2 active\">");
jade_mixins["popupTitle"]("status-membership-stage-header");
buf.push("<div class=\"b-settings-row member-status\"><div class=\"b-value\">");
if ( paymentStatus.type == "free")
{
buf.push("<span class=\"value\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.free-membership")) ? "" : jade_interp)) + "<span class=\"info\">" + (jade.escape(null == (jade_interp = " " + _t("popupStopAction", "text.click-to-upgrade-your-account", {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</span></span><a" + (jade.attr("href", paymentStatus.url, true, false)) + " class=\"btn btn-text-orange\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.get-full-membership")) ? "" : jade_interp)) + "</a>");
}
else if ( paymentStatus.type == "paid")
{
buf.push("<span class=\"value\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.full-membership")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div></div><div data-notification-settings=\"1\" class=\"b-settings-row b-settings-new-activity\"><div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.new_activity_notifications")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><ul class=\"b-options-list\">");
// iterate notificationSettings.settings
;(function(){
  var $$obj = notificationSettings.settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

buf.push("<li>");
// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</span></label>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</span></label>");
    }

  }
}).call(this);

buf.push("</li>");
    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

buf.push("<li>");
// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</span></label>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</span></label>");
    }

  }
}).call(this);

buf.push("</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div><div data-notification-subscription=\"1\" class=\"b-settings-row b-settings-notifications\"><div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_sms_notification")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><ul class=\"b-options-list\"><li class=\"header\"><div class=\"label\"></div><div class=\"option\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification.email")) ? "" : jade_interp)) + "</div><div class=\"option\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification.sms")) ? "" : jade_interp)) + "</div><div class=\"option\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification.push")) ? "" : jade_interp)) + "</div></li>");
// iterate notificationSubscription.settings
;(function(){
  var $$obj = notificationSubscription.settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

buf.push("<li><div class=\"label\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification_"+name)) ? "" : jade_interp)) + "</div>");
// iterate ["email", "mobile", "push"]
;(function(){
  var $$obj = ["email", "mobile", "push"];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

buf.push("<div class=\"option\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<label><input type=\"checkbox\" data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span></span></label>");
}
buf.push("</div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

buf.push("<div class=\"option\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<label><input type=\"checkbox\" data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span></span></label>");
}
buf.push("</div>");
    }

  }
}).call(this);

buf.push("</li>");
    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

buf.push("<li><div class=\"label\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification_"+name)) ? "" : jade_interp)) + "</div>");
// iterate ["email", "mobile", "push"]
;(function(){
  var $$obj = ["email", "mobile", "push"];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

buf.push("<div class=\"option\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<label><input type=\"checkbox\" data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span></span></label>");
}
buf.push("</div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

buf.push("<div class=\"option\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<label><input type=\"checkbox\" data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span></span></label>");
}
buf.push("</div>");
    }

  }
}).call(this);

buf.push("</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div><div class=\"b-settings-row b-settings-options\"><div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.choose-options-below")) ? "" : jade_interp)) + "</div><p id=\"error-container\" class=\"desc\">" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</p><div class=\"b-value\"><ul id=\"status-membership-user-answer\" class=\"b-options-list\"><li><label><input type=\"radio\" name=\"name\" value=\"off-mailing\"/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-2")) ? "" : jade_interp)) + "</span></label></li><li><label><input type=\"radio\" name=\"name\"" + (jade.attr("value", (profileStatus == "active") ? "hide" : "show", true, false)) + "/><span>");
if ( profileStatus == "active")
{
buf.push(jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-3-hide")) ? "" : jade_interp));
}
else
{
buf.push(jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-3-show")) ? "" : jade_interp));
}
buf.push("</span></label></li><li><label><input type=\"radio\" name=\"name\" value=\"hide-off\"/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-4")) ? "" : jade_interp)) + "</span></label></li><li><label><input type=\"radio\" name=\"name\"" + (jade.attr("value", dictionary, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-1")) ? "" : jade_interp)) + "</span></label></li></ul></div></div><div id=\"coreg-placement-remove-account-stage-statusMembership\" class=\"coreg-placement-remove\"></div><div class=\"footer\">");
jade_mixins["cancelProgress"](2);
buf.push("<button id=\"close-popup\" class=\"btn cancel btn-text-def\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"status-membership\" class=\"btn next btn-text-green\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div>");
  break;
case "userReason":
buf.push("<div class=\"b-popup-content b-step step-3 active\">");
jade_mixins["popupTitle"]("user-reason-stage-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-main-text")) ? "" : jade_interp)) + "</p><div class=\"b-value\"><p id=\"error-container\" class=\"desc\">" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</p><ul id=\"user-reason-answer\" class=\"b-options-list\">");
// iterate reasons
;(function(){
  var $$obj = reasons;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var reason = $$obj[$index];

buf.push("<li" + (jade.attr("data-has-sub", (reason.children.length > 0) ? "1" : "0", true, false)) + "><label><input type=\"radio\" name=\"name\"" + (jade.attr("value", reason.reasonId, true, false)) + (jade.attr("data-action", reason.action, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+reason.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</span></label>");
if ( reason.children.length > 0)
{
buf.push("<ul data-sub=\"1\" class=\"b-options-list inactive\">");
// iterate reason.children
;(function(){
  var $$obj = reason.children;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var child = $$obj[$index];

buf.push("<li><input type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</span></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var child = $$obj[$index];

buf.push("<li><input type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</span></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
if ( reason.action == "other")
{
buf.push("<div data-sub=\"1\" data-additional=\"1\" class=\"b-input inactive\"><div class=\"b-input-wrap\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.your-comment")) ? "" : jade_interp)) + "</span><textarea name=\"frm-messenger-text\" data-reason=\"1\" data-resize=\"auto\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.enter-your-text-here"), true, false)) + " class=\"message-input\"></textarea></div></div>");
}
buf.push("</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var reason = $$obj[$index];

buf.push("<li" + (jade.attr("data-has-sub", (reason.children.length > 0) ? "1" : "0", true, false)) + "><label><input type=\"radio\" name=\"name\"" + (jade.attr("value", reason.reasonId, true, false)) + (jade.attr("data-action", reason.action, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+reason.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</span></label>");
if ( reason.children.length > 0)
{
buf.push("<ul data-sub=\"1\" class=\"b-options-list inactive\">");
// iterate reason.children
;(function(){
  var $$obj = reason.children;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var child = $$obj[$index];

buf.push("<li><input type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</span></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var child = $$obj[$index];

buf.push("<li><input type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</span></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
if ( reason.action == "other")
{
buf.push("<div data-sub=\"1\" data-additional=\"1\" class=\"b-input inactive\"><div class=\"b-input-wrap\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.your-comment")) ? "" : jade_interp)) + "</span><textarea name=\"frm-messenger-text\" data-reason=\"1\" data-resize=\"auto\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.enter-your-text-here"), true, false)) + " class=\"message-input\"></textarea></div></div>");
}
buf.push("</li>");
    }

  }
}).call(this);

buf.push("</ul></div><div id=\"coreg-placement-remove-account-stage-userReason\" class=\"coreg-placement-remove\"></div><div class=\"footer\">");
jade_mixins["cancelProgress"](3);
buf.push("<button id=\"close-popup\" class=\"btn cancel btn-text-def\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"user-reason\" class=\"btn next btn-text-green\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div>");
  break;
case "userStory":
buf.push("<div class=\"b-popup-content b-step step-4 active\">");
jade_mixins["popupTitle"]("user-story-stage-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-story-main-text", {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</p><p id=\"error-container\" class=\"desc\">" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</p><div class=\"b-input\"><div class=\"b-input-wrap\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.write-your-story")) ? "" : jade_interp)) + "</span><textarea name=\"frm-messenger-text\" id=\"user-story-text\" data-resize=\"auto\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.enter-your-text-here"), true, false)) + " class=\"message-input\"></textarea></div></div><div class=\"footer\">");
jade_mixins["cancelProgress"](4);
buf.push("<button id=\"close-popup\" class=\"btn cancel btn-text-def\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"user-story\" class=\"btn next btn-text-green\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.save-and-continue")) ? "" : jade_interp)) + "</button></div></div>");
  break;
case "areYouSure":
buf.push("<div class=\"b-popup-content b-step step-4 active\">");
jade_mixins["popupTitle"]("are-you-sure-stage-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-main-text")) ? "" : jade_interp)) + "</p><ul class=\"b-loss\"><li>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-1")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-2", {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-3")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-4")) ? "" : jade_interp)) + "</li></ul><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-main-text-part-3")) ? "" : jade_interp)) + "</p><div class=\"b-settings-row b-settings-options\"><div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-answer-option-header")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><p id=\"error-container\" class=\"desc\">" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</p><ul id=\"are-you-sure-answer\" class=\"b-options-list\"><li><label><input type=\"radio\" name=\"name\" value=\"yes\"/><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-answer-option-1")) ? "" : jade_interp)) + "</span></label></li><li><label><input type=\"radio\" name=\"name\" value=\"no\"/><span>" + (jade.escape(null == (jade_interp = _t('popupStopAction', "title."+dictionary+".are-you-sure-answer-option-2")) ? "" : jade_interp)) + "</span></label></li></ul></div></div><div class=\"footer\">");
jade_mixins["cancelProgress"](4);
buf.push("<button id=\"close-popup\" class=\"btn cancel btn-text-def\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"are-you-sure\" class=\"btn next btn-text-green\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div>");
  break;
case "stop":
if ( (dictionary == "cancel-billing" && viaPhoneCode == true))
{
buf.push("<div class=\"b-popup-content via-phone-code\">");
jade_mixins["popupTitle"]("via-phone-code-header", {"{sitename}": siteInfo.siteName});
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.cancel-billing.via-phone-code-body-text-1")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.cancel-billing.via-phone-code-body-text-2", {"{email address}": email})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.cancel-billing.via-phone-code-body-text-3")) ? "" : jade_interp)) + "</p></div>");
}
else if ( (dictionary == "cancel-billing" && hasBeenCancelled == true))
{
buf.push("<div class=\"b-popup-content b-step has-been-cancelled\">");
jade_mixins["popupTitle"]("has-been-cancelled-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.cancel-billing.has-been-cancelled-body-text-1", {"{mm/dd/yy}": userSubscriptionExpire})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.cancel-billing.has-been-cancelled-body-text-2")) ? "" : jade_interp)) + "</p></div>");
}
else if ( ((dictionary == "cancel-billing" || dictionary == "remove-account") && viaCode == true && code == null))
{
buf.push("<div class=\"b-popup-content b-step via-code\">");
jade_mixins["popupTitle"]("via-code-header", {"{sitename}": siteInfo.siteName});
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".via-code-body-text-1")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".via-code-body-text-2", {"{email address}": email})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".via-code-body-text-3")) ? "" : jade_interp)) + "</p></div>");
}
else if ( (hotLineCall == true))
{
buf.push("<div class=\"b-popup-content b-step step-5 active\">");
jade_mixins["popupTitle"]("hot-line-call-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-main-text-part-3")) ? "" : jade_interp)) + "<span class=\"hightlight-id\">" + (jade.escape(null == (jade_interp = " " + publicId) ? "" : jade_interp)) + "</span></p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-main-text-part-1")) ? "" : jade_interp)) + "</p>");
if ( siteInfo.contact)
{
buf.push("<div class=\"phone-number\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.local") + ": " + siteInfo.contact.localContactPhone) ? "" : jade_interp)) + "</div><div class=\"phone-number\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.intnl") + ": " + siteInfo.contact.intnlContactPhone) ? "" : jade_interp)) + "</div>");
}
else
{
buf.push("<div class=\"phone-number\">" + (jade.escape(null == (jade_interp = siteInfo.phone.code + " " + siteInfo.phone.phoneNumber) ? "" : jade_interp)) + "</div>");
}
buf.push("<div class=\"phone-number-sep\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "or")) ? "" : jade_interp)) + "</div><div class=\"skype-btn-holder\"><a" + (jade.attr("href", "skype:" + siteInfo.phone.code + siteInfo.phone.phoneNumber, true, false)) + " class=\"skype-btn\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-confirm")) ? "" : jade_interp)) + "</a></div><p class=\"note\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-main-text-part-2")) ? "" : jade_interp)) + "</p><div class=\"footer\">");
jade_mixins["cancelProgress"](5);
buf.push("</div></div>");
}
else
{
buf.push("<div class=\"b-popup-content b-step step-5 active\">");
jade_mixins["popupTitle"]("stop-stage-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".stop-subscription-main-text-part-1")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".stop-subscription-main-text-part-2", {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".stop-subscription-custom-text-1")) ? "" : jade_interp)) + "</p><div id=\"coreg-placement-remove-account-stage-stop\" class=\"coreg-placement-on-remove-account-stage stop checkbox-info\"></div><div class=\"footer\">");
jade_mixins["cancelProgress"](5);
buf.push("<button id=\"close-popup\" class=\"btn cancel gray short\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</span></button><button id=\"confirm\" class=\"btn next green\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".stop-subscription-confirm")) ? "" : jade_interp)) + "</span></button></div></div>");
}
  break;
case "emailSecurityCheck":
buf.push("<div class=\"b-popup-content email-verificate\">");
jade_mixins["popupTitle"]("email-security-check-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".email-security-check-body-text-1-valid-email")) ? "" : jade_interp)) + "<div class=\"form\"><p id=\"email-error\" class=\"desc\">" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</p><input type=\"text\" id=\"email\"" + (jade.attr("value", email, true, false)) + "/></div></p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".email-security-check-body-text-2")) ? "" : jade_interp)) + "</p><div class=\"footer\"><button id=\"close-popup\" class=\"btn cancel grey\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</span></button><button id=\"check-email\" class=\"btn next green\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</span></button></div></div>");
  break;
case "enterCode":
buf.push("<div class=\"b-popup-content enter-code\">");
jade_mixins["popupTitle"]("enter-code-header");
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".enter-code-body-text-1")) ? "" : jade_interp)) + "</p><div class=\"form\"><p id=\"code-error\" class=\"desc\">" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</p><input type=\"text\" id=\"code\" maxlength=\"8\"/></div><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".enter-code-body-text-2", {"{email address}": email})) ? "" : jade_interp)) + "</p><div class=\"footer\"><button id=\"check-code\" class=\"btn next green\"><span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</span></button></div></div>");
  break;
}
}
}
buf.push("</div></div></div></div>");;return buf.join("");
};