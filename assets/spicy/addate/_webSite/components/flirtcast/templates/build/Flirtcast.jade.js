this["JST"] = this["JST"] || {};

this["JST"]["Flirtcast"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),displaySuccess = locals_.displaySuccess,approved = locals_.approved,successMessage = locals_.successMessage,_t = locals_._t,messageId = locals_.messageId,messageText = locals_.messageText;
var fcBlockClass = "";
if(displaySuccess)
{
if(approved)
{
fcBlockClass = "default-message"
}
else
{
fcBlockClass = "user-message"
}
}
buf.push("<div" + (jade.cls(['flirtcast-block',fcBlockClass], [null,true])) + "><div class=\"flirtcast-content\">");
if ( displaySuccess)
{
buf.push("<div class=\"flirtcast-form\"><div class=\"textarea-wrap text\">" + (jade.escape(null == (jade_interp = successMessage) ? "" : jade_interp)) + "</div><div class=\"buttons-row\"><button data-close=\"data-close\" class=\"close btn-text-def\">" + (jade.escape(null == (jade_interp = _t("flirtcast", "button.close")) ? "" : jade_interp)) + "</button></div></div>");
}
else
{
buf.push("<div class=\"flirtcast-form\"><div class=\"fc-faces\"><div class=\"face one\"></div><div class=\"face two\"></div><div class=\"face three\"></div><div class=\"face four\"></div><div class=\"face five\"></div></div><span" + (jade.attr("title", _t("flirtcast", "tooltip.click_to_write"), true, false)) + " class=\"btn-info\"></span><div id=\"flirtcastError\" class=\"error\"></div><div class=\"textarea-wrap\"><textarea id=\"flirtcastMessage\"" + (jade.attr("data-id", messageId, true, false)) + ">" + (jade.escape(null == (jade_interp = messageText) ? "" : jade_interp)) + "</textarea></div><div class=\"buttons-row\"><button id=\"flirtcastNext\" class=\"btn-next btn-text-def\">" + (jade.escape(null == (jade_interp = _t("flirtcast", "button.next_message")) ? "" : jade_interp)) + "</button><button id=\"flirtcastSend\" class=\"btn-send btn-text-green\">" + (jade.escape(null == (jade_interp = _t("flirtcast", "button.send")) ? "" : jade_interp)) + "</button></div></div>");
}
buf.push("</div></div>");;return buf.join("");
};