this["JST"] = this["JST"] || {};

this["JST"]["ToolbarActivity"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),type = locals_.type,talksEnabled = locals_.talksEnabled,onclick = locals_.onclick,_t = locals_._t,banner = locals_.banner,activity = locals_.activity,userLink = locals_.userLink;
var url = "", upgradeClass = "", upgradeText = "";
switch (type){
case "favourite":
url = "/#favorites"
  break;
case "mail":
if ( talksEnabled)
{
url = "javascript:void(0)"
onclick = true
}
else
{
url = "/#messenger"
}
upgradeClass = "free-m"
upgradeText=_t("toolbar", "text.get_communication_upgrade")
  break;
case "newsFeed":
url = "/#newsFeed"
  break;
case "smsChatFeed":
url = "/#smsChatFeed"
  break;
case "view":
url = "/#views"
upgradeClass = "invisible"
upgradeText=_t("toolbar", "text.view_other_members")
  break;
case "wink":
url = "/#winks"
  break;
}
if(banner && banner.bannerUrl)
{
buf.push("<div class=\"b-upgrade\"><a" + (jade.attr("href", banner.bannerUrl, true, false)) + "><span" + (jade.cls(['gcu-icon',upgradeClass], [null,true])) + "></span><span class=\"gcu-text\">" + (jade.escape(null == (jade_interp = upgradeText) ? "" : jade_interp)) + "</span></a></div>");
}
buf.push("<ul class=\"nav\">");
// iterate activity
;(function(){
  var $$obj = activity;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var user = $$obj[$index];

buf.push("<li" + (jade.cls([user.naughtyData.naughtyClass?user.naughtyData.naughtyClass:""], [true])) + ">");
if(type == "smsChatFeed" && user.action != "newsFeed")
{
buf.push("<a" + (jade.attr("href", userLink(user.id), true, false)) + "><div class=\"sms-chat\"><div class=\"base\"><span class=\"image-holder\"><img width=\"30\" height=\"30\"" + (jade.attr("src", user.photo_url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"photo\"/></span><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span><span class=\"action\">" + (jade.escape(null == (jade_interp = _t("toolbar","text.notification_newsFeed_onSmsChatFeed_1_"+user.gender)) ? "" : jade_interp)) + "</span><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('SmsChat.Start', 'addMsisdnFromNotificationFeedList', '"+user.id+"')", true, false)) + " class=\"btn-activity btn-sms-chat\">SMS chat</a></div><div class=\"b-btn-activity-grid\"><a" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.cls(['btn-activity','btn-wink',(user.buttons.wink.activated?"active":"")], [null,null,true])) + "><span class=\"wink\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.wink")) ? "" : jade_interp)) + "</span><span class=\"winked\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.winked")) ? "" : jade_interp)) + "</span></a></div></div></div></div></a>");
}
else
{
buf.push("<a" + (jade.attr("href", (talksEnabled && type == "mail")?url:userLink(user.id), true, false)) + (jade.attr("onclick", (onclick)?"Backbone.trigger('Talks.Start', '" + user.id + "')":"", true, false)) + "><img width=\"30\" height=\"30\"" + (jade.attr("src", user.photo_url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"photo\"/><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span>");
if(user.action == "newsFeed" && user.type)
{
buf.push("<span class=\"action\">" + (jade.escape(null == (jade_interp = _t("toolbar","text.notification_newsFeed_"+user.type+"_"+user.txtId+"_"+user.gender)) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span class=\"date\">" + (jade.escape(null == (jade_interp = user.date) ? "" : jade_interp)) + "</span>");
}
buf.push("</a>");
}
buf.push("</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var user = $$obj[$index];

buf.push("<li" + (jade.cls([user.naughtyData.naughtyClass?user.naughtyData.naughtyClass:""], [true])) + ">");
if(type == "smsChatFeed" && user.action != "newsFeed")
{
buf.push("<a" + (jade.attr("href", userLink(user.id), true, false)) + "><div class=\"sms-chat\"><div class=\"base\"><span class=\"image-holder\"><img width=\"30\" height=\"30\"" + (jade.attr("src", user.photo_url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"photo\"/></span><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span><span class=\"action\">" + (jade.escape(null == (jade_interp = _t("toolbar","text.notification_newsFeed_onSmsChatFeed_1_"+user.gender)) ? "" : jade_interp)) + "</span><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('SmsChat.Start', 'addMsisdnFromNotificationFeedList', '"+user.id+"')", true, false)) + " class=\"btn-activity btn-sms-chat\">SMS chat</a></div><div class=\"b-btn-activity-grid\"><a" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.cls(['btn-activity','btn-wink',(user.buttons.wink.activated?"active":"")], [null,null,true])) + "><span class=\"wink\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.wink")) ? "" : jade_interp)) + "</span><span class=\"winked\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.winked")) ? "" : jade_interp)) + "</span></a></div></div></div></div></a>");
}
else
{
buf.push("<a" + (jade.attr("href", (talksEnabled && type == "mail")?url:userLink(user.id), true, false)) + (jade.attr("onclick", (onclick)?"Backbone.trigger('Talks.Start', '" + user.id + "')":"", true, false)) + "><img width=\"30\" height=\"30\"" + (jade.attr("src", user.photo_url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"photo\"/><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span>");
if(user.action == "newsFeed" && user.type)
{
buf.push("<span class=\"action\">" + (jade.escape(null == (jade_interp = _t("toolbar","text.notification_newsFeed_"+user.type+"_"+user.txtId+"_"+user.gender)) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span class=\"date\">" + (jade.escape(null == (jade_interp = user.date) ? "" : jade_interp)) + "</span>");
}
buf.push("</a>");
}
buf.push("</li>");
    }

  }
}).call(this);

buf.push("</ul><div class=\"b-view-more\"><a" + (jade.attr("href", url, true, false)) + (jade.attr("onclick", (onclick)?"Backbone.trigger('Talks.Start')":"", true, false)) + " class=\"view-more btn green\">" + (jade.escape(null == (jade_interp = _t("toolbar", "text.see_all")) ? "" : jade_interp)) + "</a></div>");;return buf.join("");
};