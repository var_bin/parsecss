this["JST"] = this["JST"] || {};

this["JST"]["Toolbar"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),randNum = locals_.randNum,_t = locals_._t,siteName = locals_.siteName,siteLogo = locals_.siteLogo,talksEnabled = locals_.talksEnabled,isAllowChatRooms = locals_.isAllowChatRooms,externalVod = locals_.externalVod,user = locals_.user,isSmsChat = locals_.isSmsChat,internalVodAnchor = locals_.internalVodAnchor,xSalesCoregSites = locals_.xSalesCoregSites,csrfTokenName = locals_.csrfTokenName,csrfTokenValue = locals_.csrfTokenValue,livecamAnchor = locals_.livecamAnchor,isShowEmailValidationBar = locals_.isShowEmailValidationBar,isEmailConfirmed = locals_.isEmailConfirmed,nonVirginToolbar = locals_.nonVirginToolbar,userEmail = locals_.userEmail,emailService = locals_.emailService,hideValidationBarButtons = locals_.hideValidationBarButtons,usersForPopup = locals_.usersForPopup;
var randNumKey = "title.confirmation_message_motiv_"+randNum
var randNumMsg = _t("toolbar", randNumKey)
var prefixConfirmMSg = _t("toolbar", "title.confirmation_message")
var randConfMsg = prefixConfirmMSg+' '+randNumMsg+". "
jade_mixins["vodLink"] = function(type, className, externalVod, vodId){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if ( (type == 'vodLink'))
{
buf.push("<a" + (jade.attr("href", externalVod.vodFormUrl + externalVod.vodIds[vodId], true, false)) + " target=\"_blank\"" + (jade.cls(['site-nav-item','site-nav-vod',className], [null,null,true])) + "><span class=\"title\"><span class=\"title-holder\">Premium Videos</span></span><span data-counter=\"vod\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='vod']\" data-activity=\"vod\" class=\"b-dropdown-menu\"></div>");
}
else if ( (type == 'spVideos'))
{
buf.push("<a href=\"/#spVideos\" class=\"site-nav-item site-nav-vod18 site-nav-vod\"><span class=\"title\"><span class=\"title-holder\">Premium Videos</span></span><span data-counter=\"vod\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='vod']\" data-activity=\"vod\" class=\"b-dropdown-menu\"></div>");
}
else
{
buf.push("<a" + (jade.attr("href", externalVod.paymentUrl, true, false)) + " class=\"site-nav-item site-nav-vodxxx site-nav-vod\"><span class=\"title\"><span class=\"title-holder\">XXX<span class=\"title-holder-inner\">Videos</span></span></span><span data-counter=\"vod\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='vod']\" data-activity=\"vod\" class=\"b-dropdown-menu\"></div>");
}
};
buf.push("<div class=\"b-site-nav\"><a href=\"/#\"" + (jade.attr("title", siteName, true, false)) + " class=\"b-logo\"><span class=\"logo\"><img" + (jade.attr("src", siteLogo, true, false)) + (jade.attr("alt", siteName, true, false)) + "/></span></a><ul class=\"b-nav-list\"><li data-nav-item=\"messenger\"><a" + (jade.attr("href", (talksEnabled)?"/#":"/#messenger", true, false)) + (jade.attr("title", _t("toolbar", "title.message"), true, false)) + (jade.attr("talksEnabled", (talksEnabled)?"talks":"", true, false)) + " class=\"site-nav-item site-nav-messenger\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.messages")) ? "" : jade_interp)) + "</span></span><span data-counter=\"mail\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='mail']\" data-activity=\"mail\" class=\"b-dropdown-menu\"></div></li><li data-nav-item=\"views\"><a href=\"/#views\" class=\"site-nav-item site-nav-views\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.views")) ? "" : jade_interp)) + "</span></span><span data-counter=\"view\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='view']\" data-activity=\"view\" class=\"b-dropdown-menu\"></div></li><li data-nav-item=\"winks\"><a href=\"/#winks\" class=\"site-nav-item site-nav-winks\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.winks")) ? "" : jade_interp)) + "</span></span><span data-counter=\"wink\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='wink']\" data-activity=\"wink\" class=\"b-dropdown-menu\"></div></li><li data-nav-item=\"favorites\"><a href=\"/#favorites\" class=\"site-nav-item site-nav-favorites\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.friends")) ? "" : jade_interp)) + "</span></span><span data-counter=\"favourite\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='favourite']\" data-activity=\"favourite\" class=\"b-dropdown-menu\"></div></li><li data-nav-item=\"newsFeed\"><a href=\"/#newsFeed\" class=\"site-nav-item site-nav-newsfeed\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.what_new")) ? "" : jade_interp)) + "</span></span><span data-counter=\"newsFeed\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='newsFeed']\" data-activity=\"newsFeed\" class=\"b-dropdown-menu\"></div></li>");
if (isAllowChatRooms)
{
buf.push("<li data-nav-item=\"chatRooms\"><a href=\"/#rooms\" class=\"site-nav-item site-nav-chatrooms\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.сhatrooms")) ? "" : jade_interp)) + "</span></span><span data-counter=\"chatRooms\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='chatRooms']\" data-activity=\"chatRooms\" class=\"b-dropdown-menu\"></div></li>");
}
if (externalVod)
{
if (user.account_status.type == 'paid' || (user.account_status.type != 'paid' && externalVod.alwaysShowIcon) || externalVod.paidPermissions)
{
if (externalVod.vodIds && externalVod.vodIds.length)
{
buf.push("<li data-nav-item=\"vod\">");
if (user.account_status.type == 'paid' || externalVod.paidPermissions)
{
jade_mixins["vodLink"].call({
block: function(){
jade_mixins["vodIco"]();
}
}, 'vodLink', 'site-nav-vodxxx', externalVod, 0);
}
else if (externalVod.spVideosSplit)
{
jade_mixins["vodLink"].call({
block: function(){
jade_mixins["vodIco"]();
}
}, 'spVideos', 'site-nav-vodxxx', externalVod, 0);
}
else
{
jade_mixins["vodLink"].call({
block: function(){
jade_mixins["vodIco"]();
}
}, 'payLink', 'site-nav-vodxxx', externalVod, 0);
}
buf.push("</li>");
if (externalVod.vodIds.length == 2)
{
buf.push("<li data-nav-item=\"vod\">");
if (user.account_status.type == 'paid' || externalVod.paidPermissions)
{
jade_mixins["vodLink"].call({
block: function(){
jade_mixins["vodIco"]();
}
}, 'vodLink', 'site-nav-vodxxx', externalVod, 1);
}
else if (externalVod.spVideosSplit)
{
jade_mixins["vodLink"].call({
block: function(){
jade_mixins["vodIco"]();
}
}, 'spVideos', 'site-nav-vodxxx', externalVod, 1);
}
else
{
jade_mixins["vodLink"].call({
block: function(){
jade_mixins["vodIco"]();
}
}, 'payLink', 'site-nav-vodxxx', externalVod, 1);
}
buf.push("</li>");
}
}
}
}
if (isSmsChat)
{
buf.push("<li data-nav-item=\"smsChat\"><a href=\"/#smsChatFeed\" target=\"_blank\" class=\"site-nav-item site-nav-sms\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = _t("toolbar", "tooltip.smschat")) ? "" : jade_interp)) + "</span></span><span data-counter=\"smsChatFeed\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='smsChatFeed']\" data-activity=\"smsChatFeed\" class=\"b-dropdown-menu\"></div></li>");
}
if ( (internalVodAnchor))
{
buf.push("<li data-nav-item=\"vod\"><a href=\"/#vod\" target=\"_blank\" class=\"site-nav-item site-nav-vod\"><span class=\"title\"><span class=\"title-holder\">Premium Videos</span></span><span data-counter=\"vod\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='vod']\" data-activity=\"vod\" class=\"b-dropdown-menu\"></div></li>");
}
if (xSalesCoregSites)
{
// iterate xSalesCoregSites
;(function(){
  var $$obj = xSalesCoregSites;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var coreg = $$obj[$index];

buf.push("<li data-nav-item=\"coreg\"><a" + (jade.attr("href", 'data:text/html;charset=utf-8,<form id="coregForm" action="' + xSalesCoregSites.coregFormUrl + '" method="post"><input type="hidden" name="'+csrfTokenName+'"value="'+csrfTokenValue+'"><input type="hidden" name="coregPermission" value="' + coreg.permissionName + '"></form><script>document.getElementById("coregForm").submit();</script>', true, false)) + " target=\"_blank\" class=\"site-nav-item site-nav-coreg\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = coreg.siteName) ? "" : jade_interp)) + "</span></span><span data-counter=\"coreg\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='coreg']\" data-activity=\"coreg\" class=\"b-dropdown-menu\"></div></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var coreg = $$obj[$index];

buf.push("<li data-nav-item=\"coreg\"><a" + (jade.attr("href", 'data:text/html;charset=utf-8,<form id="coregForm" action="' + xSalesCoregSites.coregFormUrl + '" method="post"><input type="hidden" name="'+csrfTokenName+'"value="'+csrfTokenValue+'"><input type="hidden" name="coregPermission" value="' + coreg.permissionName + '"></form><script>document.getElementById("coregForm").submit();</script>', true, false)) + " target=\"_blank\" class=\"site-nav-item site-nav-coreg\"><span class=\"title\"><span class=\"title-holder\">" + (jade.escape(null == (jade_interp = coreg.siteName) ? "" : jade_interp)) + "</span></span><span data-counter=\"coreg\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='coreg']\" data-activity=\"coreg\" class=\"b-dropdown-menu\"></div></li>");
    }

  }
}).call(this);

}
if ( (livecamAnchor))
{
buf.push("<li data-nav-item=\"webCams\"><a" + (jade.attr("href", "/livecam/?promocode=" + (livecamAnchor) + "", true, false)) + " target=\"_blank\" class=\"site-nav-item site-nav-webcams\"><span class=\"title\"><span class=\"title-holder\">Web Cams</span></span><span data-counter=\"webCams\" class=\"counter\">0</span></a><div data-ui=\"autoClose\" data-ui-control=\"[data-activity='webCams']\" data-activity=\"webCams\" class=\"b-dropdown-menu\"></div></li>");
}
buf.push("</ul>");
if (isShowEmailValidationBar)
{
if (typeof isEmailConfirmed !== 'undefined' && isEmailConfirmed === false)
buf.push("<div id=\"alertEmailValidationBlock\"" + (jade.cls(['email-validation-b',nonVirginToolbar ? 'newToolbar' : ''], [null,true])) + "><div class=\"email-validation-wrap\">");
if(!nonVirginToolbar)
{
buf.push("<div class=\"email-validation-wrap--left\">" + (null == (jade_interp = _t("toolbar", "title.email-validation-alert", {'{userEmail}': userEmail})) ? "" : jade_interp) + "</div>");
}
else
{
buf.push("<div class=\"email-validation-wrap--left\">" + (null == (jade_interp = randConfMsg) ? "" : jade_interp) + "<a" + (jade.attr("href", emailService, true, false)) + " target=\"_blank\" class=\"verify\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.confirmation_verify")) ? "" : jade_interp)) + "</a></div>");
}
if (!hideValidationBarButtons)
{
buf.push("<div class=\"email-validation-wrap--right\"><form id=\"change-email-form\" action=\"/user/resendConfirmMail\" method=\"post\" onsubmit=\"return false;\" class=\"hidden\"><div class=\"email-validation-label\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.email-validation-label")) ? "" : jade_interp)) + "</div><div class=\"email-validation-field\"><input id=\"newEmail\" type=\"text\"/></div><button id=\"btnChangeEmail\" type=\"button\" class=\"email-validation-send-btn\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.email-validation-send-btn")) ? "" : jade_interp)) + "</button></form><div class=\"email-validation-message hidden\"><span>" + (jade.escape(null == (jade_interp = _t("toolbar", "title.email-validation-message")) ? "" : jade_interp)) + "</span></div><div class=\"email-validation-nav\"><ul><li><a id=\"resendConfirmMail\" href=\"#\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.resend-confirm-mail-link")) ? "" : jade_interp)) + "</a></li><li><a id=\"changeEmail\" href=\"#\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.change-email-link")) ? "" : jade_interp)) + "</a></li><li><a id=\"checkMailBox\"" + (jade.attr("href", emailService, true, false)) + " target=\"_blank\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.chack-mail-box-link")) ? "" : jade_interp)) + "</a></li></ul></div></div>");
}
if(!nonVirginToolbar)
{
buf.push("<a id=\"closeEmailValidation\" href=\"#\" class=\"email-validation-close-btn\"></a>");
}
else
{
buf.push("<a id=\"closeEmailValidationShowPopup\" href=\"#\" class=\"email-validation-close-btn\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.hide-bar")) ? "" : jade_interp)) + "</a><div id=\"сonfirmPopup\" class=\"popup\"><div class=\"b-popup b-success-page\"><div id=\"closePopup\" class=\"closeButton\"></div><div class=\"b-popup-content\"><div class=\"must-confirm-wrap\"><div class=\"confirm-info\"><div class=\"confirm-info-title\">You Must Confirm Email To:<ul class=\"confirm-info-list\"><li class=\"confirm-info-list-item\"><span class=\"confirm-info-list-icon\"></span>View your new local matches</li><li class=\"confirm-info-list-item\"><span class=\"confirm-info-list-icon\"></span>Receive private pics from horny women</li><li class=\"confirm-info-list-item\"><span class=\"confirm-info-list-icon\"></span>Allow women to send you private videos</li><li class=\"confirm-info-list-item\"><span class=\"confirm-info-list-icon\"></span>See who’s viewed your profile</li><li class=\"confirm-info-list-item\"><span class=\"confirm-info-list-icon\"></span>Get laid tonight!</li></ul></div></div><div class=\"confirm-user-motiv\">");
if(typeof usersForPopup !== "undefined" && usersForPopup !== null)
{
// iterate usersForPopup
;(function(){
  var $$obj = usersForPopup;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var user = $$obj[$index];

buf.push("<div class=\"confirm-user-motiv-wrap\"><span class=\"image-holder\"><img" + (jade.attr("src", user.photos.url, true, false)) + "/></span><div class=\"confirm-user-motiv-stripe\"><span class=\"confirm-user-motiv-status\"></span><div class=\"confirm-user-motiv-photo\"><span class=\"confirm-user-motiv-photo-icon\"></span> " + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "</div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var user = $$obj[$index];

buf.push("<div class=\"confirm-user-motiv-wrap\"><span class=\"image-holder\"><img" + (jade.attr("src", user.photos.url, true, false)) + "/></span><div class=\"confirm-user-motiv-stripe\"><span class=\"confirm-user-motiv-status\"></span><div class=\"confirm-user-motiv-photo\"><span class=\"confirm-user-motiv-photo-icon\"></span> " + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "</div></div></div>");
    }

  }
}).call(this);

}
buf.push("</div></div><div class=\"b-confirm active\"><p class=\"complete-reg\">Click on the 'Activate my account' button in the email we've just sent to:</p><a" + (jade.attr("href", emailService, true, false)) + " target=\"_blank\" class=\"complete-email\">" + (jade.escape(null == (jade_interp = userEmail) ? "" : jade_interp)) + "</a><div class=\"b-buttons\"><a id=\"checkMailBox\"" + (jade.attr("href", emailService, true, false)) + " target=\"_blank\" class=\"btn known-domain green\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.chack-mail-box-link")) ? "" : jade_interp)) + "</a></div><div class=\"b-complete-staps\"><a id=\"resendConfirmMail\" href=\"#\" class=\"resend-email\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.resend-confirm-mail-link")) ? "" : jade_interp)) + "</a></div><form id=\"change-email-form\" action=\"/user/resendConfirmMail\" method=\"post\" onsubmit=\"return false;\" class=\"hidden\"><div class=\"email-validation-label\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.email-validation-label")) ? "" : jade_interp)) + "</div><div class=\"email-validation-field\"><input id=\"newEmail\" type=\"text\"/></div><button id=\"btnChangeEmail\" type=\"button\" class=\"btn green email-validation-send-btn\">" + (jade.escape(null == (jade_interp = _t("toolbar", "title.email-validation-send-btn")) ? "" : jade_interp)) + "</button></form><div class=\"b-resend-success email-validation-message hidden\"><span>" + (jade.escape(null == (jade_interp = _t("toolbar", "title.email-validation-message")) ? "" : jade_interp)) + "</span></div></div><div class=\"b-important\"><h2>important<ul><li>It takes 5-10 minutes for the activation email to arrive.</li><li>Check your email account's 'Spam/Junk' folder to make sure our email hasn't landed there.</li><li>Remember to mark it as 'Not junk/Spam' to ensure that you receive all important emails from " + (jade.escape((jade_interp = siteName) == null ? '' : jade_interp)) + "</li></ul></h2></div></div></div></div>");
}
buf.push("</div></div>");
}
buf.push("</div>");;return buf.join("");
};