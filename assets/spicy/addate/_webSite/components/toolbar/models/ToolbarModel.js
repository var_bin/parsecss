var ToolbarModel = Backbone.Model.extend({
    defaults: {
        activity: {
            activities: {},
            counters: {}
        }
    },

    initialize: function() {
        var appModel = app.appData();
        this.listenTo(app.appData(), "change:activity", this.setActivity);
        this.setActivity(appModel);
        this.setLivecamAnchor(appModel);
        this.setInternalVodAnchor(appModel);
        this.setExternalVods(appModel);
        this.setUser(appModel);
        this.setEmailData(appModel);
        this.setCoregSites(appModel);
    },

    setActivity: function(model) {
        this.set("activity", model.get("activity"))
            .trigger("change:activity", this);
    },

    setLivecamAnchor: function(model) {
        var livecamAnchor = model.get("livecam") ? model.get("livecam").toolbar : false;
        this.set("livecamAnchor", livecamAnchor);
    },

    setInternalVodAnchor: function(model) {
        var internalVodAnchor = model.get("internalVod") ? model.get("internalVod") : false;
        this.set("internalVodAnchor", internalVodAnchor);
    },

    setExternalVods: function(model) {
        var externalVods = model.get("externalVod");
        if (externalVods) {
            externalVods["vodFormUrl"] = document.location.protocol + "//" + document.location.host + "/externalVod/redirectToVod/externalVodId/";
        }
        this.set("externalVod", externalVods);
    },

    setUser: function(model) {
        this.set("user", model.get("user"));
    },

    setEmailData: function(model) {
        this.set('isShowEmailValidationBar', model.get('isShowEmailValidationBar'));
        var isEmailConfirmed = model.get("isEmailConfirmed");
        if (typeof isEmailConfirmed != 'undefined' && isEmailConfirmed == false) {
            this.set('isEmailConfirmed', isEmailConfirmed);
            this.set('userEmail', model.get("user").email);
            this.set('emailService', model.get("user").emailService);
        } 
        
        var nonVirginToolbar = model.get("nonVirginToolbar");
        if (typeof nonVirginToolbar != 'undefined' && nonVirginToolbar == true) {
            this.set('hideValidationBarButtons', model.get('hideValidationBarButtons'));
            this.set('nonVirginToolbar', nonVirginToolbar);
            this.set('usersForPopup', model.get('popupUsers'));
            this.set('randNum', Math.floor((Math.random() * 5) + 1));
        }
    },

    setCoregSites: function(model) {
        var xSalesCoregSites = model.get('xSalesCoregSites');
        if (xSalesCoregSites) {
            xSalesCoregSites["coregFormUrl"] = document.location.protocol + "//" + document.location.host + "/coreg/redirectCoregAutoLogin";
            this.set('xSalesCoregSites', xSalesCoregSites);
        }
    }

});
