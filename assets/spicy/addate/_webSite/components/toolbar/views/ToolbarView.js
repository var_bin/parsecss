var ToolbarView = Backbone.View.extend({
    events: {
        'click [data-nav-item] > a': 'clickItem',
        'click #closeEmailValidation': 'closeEmailValidation',
        'click #resendConfirmMail': 'resendConfirmMail',
        'click #changeEmail': 'showChangeEmailForm',
        'click #checkMailBox': 'checkMailBox',
        'click #btnChangeEmail': 'changeEmail',
        'click [data-coreg]': 'redirectToCoregSite',
        'click #closeEmailValidationShowPopup': 'showConfirmPopup',
        'click #closePopup': 'closeConfirmPopup'
    },

    initialize: function() {
        var self = this;

        this.listenTo(this.model, 'change:activity', this.renderActivity);
        this.listenTo(Backbone, 'ControllerRun', this.activeMenu);
        this.listenTo(app.appData(), "change:user", this.changeLogin);

        $(document).on('click.Toolbar', ':not(#toolbar [data-nav-item])', function() {
            self.$('[data-activity]').removeClass('active');
        });
    },

    render: function() {
        var talksMessengerConfig = app.appData().get('talksMessengerConfig'),
            talksEnabled = talksMessengerConfig && talksMessengerConfig.enabled;

        this.model.attributes = _.extend(this.model.attributes, {
            talksEnabled: talksEnabled
        });
        var model = this.model.toJSON();
        model.csrfTokenName = app.appData().get('csrfToken').name;
        model.csrfTokenValue = app.appData().get('csrfToken').value;
        model.isAllowChatRooms = app.appData().get('isAllowChatRooms');
        this.$el.html(tpl.render('Toolbar', model));
        if (model.isShowEmailValidationBar == true) {
            $('#layoutContent').addClass('valid-email-is-show');
        }
        this.renderActivity(this.model);

        return this;
    },

    remove: function() {
        $(document).off('click.Toolbar');
    },

    renderActivity: function(model) {
        var activity = model.get('activity') || [];
        _.each(activity.counters, this.counter, this);
        _.each(activity.activities, this.activity, this);
    },

    activeMenu: function(options) {
        var name = options.name || null;
        this.$('[data-nav-item].active').removeClass('active');
        if(name) this.$('[data-nav-item="'+name+'"]').addClass('active');
    },

    counter: function(value, name) {
        var $counter = this.$('[data-counter="'+name+'"]');

        value = parseInt(value);
        $counter.text(value);

        if(value > 0) {
            $counter.addClass('active');
        } else {
            $counter.removeClass('active');
        }
    },

    activity: function(value, name) {
        var $activity = this.$("[data-activity='"+name+"']"),
            talksMessengerConfig = app.appData().get('talksMessengerConfig'),
            talksEnabled = talksMessengerConfig && talksMessengerConfig.enabled;

        if(value.length === 0) {
            $activity.html('');
        } else {
            _.each(value, function(num, key){
                value[key].naughtyData = app.appData().photoHolderInfo(
                    (value[key].photoAttributes || {}).level
                );
            });

            $activity.html(tpl.render('ToolbarActivity', {
                activity: value,
                type: name,
                banner: this.model.get('tooltipBanners')[name],
                talksEnabled: talksEnabled
            }));
        }
    },

    clickItem: function(event) {
        $el = $(event.currentTarget);
        var $dataCounter = $el.find("[data-counter]");
        if($dataCounter.attr('data-counter') != 'vod' && $dataCounter.hasClass("active")) {
            event.preventDefault();
            event.stopPropagation();

            if($el.parent().find("[data-activity]").hasClass("active")){
                $el.parent().find("[data-activity]").removeClass("active");
            } else {
                app.appData().fetchActivityById(
                    $el.parent().find("[data-activity]").data('activity')
                );
                $(this.el).find('[data-nav-item]').children().removeClass('active');
                $el.parent().find("[data-activity]").addClass("active");
            }
        } else {
            if($el.attr("talksEnabled") == "talks") {
                Backbone.trigger('Talks.Start');
                event.preventDefault();
                event.stopPropagation();
            }
         }
    },

    redirectToCoregSite: function(event) {
        var $el = $(event.currentTarget),
            $loader = $('#layoutContent'),
            jsonData = {'coregPermission': $el.attr('data-coreg')};

        $loader.addClass('loading');
        jsonData[app.csrfToken.name] = app.csrfToken.value;
        $.ajax({
            url:  '/coreg/internalRedirect',
            type: 'post',
            dataType: 'json',
            data: jsonData
        }).done(function(response) {
            if (response.status == 'success') {
                window.location.href = response.data.redirectUrl;
            }
        });
        $loader.removeClass('loading');
    },

    closeEmailValidation: function(event) {
        $('#alertEmailValidationBlock').hide();
        $('.w-search-form').add('.b-grid-content').removeClass('validation-toolbar');
        $('.w-search-form').removeClass('new-toolbar');
        $('#layoutContent').removeClass('valid-email-is-show');
    },
    
    showConfirmPopup: function(){
        this.$('#сonfirmPopup').addClass('active');
    },
    
    closeConfirmPopup: function(){
        this.$('#сonfirmPopup').removeClass('active');
        this.closeEmailValidation();
    },
    
    resendConfirmMail: function(event) {
        var userEmail = app.appData().get("user").email,
            userId = app.appData().get("user").id,
            messageBlock = $('.email-validation-message'),
            jsonData = {};

        jsonData[app.csrfToken.name] = app.csrfToken.value;
        jsonData['RegistrationCompleteForm'] = {'resendEmail': userEmail},
        jsonData['id'] = userId;

        $.ajax({
            url      : '/user/resendConfirmMail',
            type     : 'post',
            data     : jsonData,
            dataType : 'json',
            success  : function (response) {
                if (response.status == 'success') {
                    $('.email-validation-nav').hide();
                    messageBlock.html(response.data.message);
                    messageBlock.removeClass('hidden');
                }
            }
        });
        return false;
    },

    showChangeEmailForm: function() {
        $('.email-validation-nav').hide();
        if($('#change-email-form').hasClass('hidden')){
            $('#change-email-form').removeClass('hidden');
        } else {
            $('#change-email-form').show();
        }
        return false;
    },

    changeEmail: function() {
        var newEmail = $('#newEmail').val(),
            userId = app.appData().get("user").id,
            messageBlock = $('.email-validation-message'),
            jsonData = {};

        jsonData[app.csrfToken.name] = app.csrfToken.value;
        jsonData['RegistrationCompleteForm'] = {'resendEmail': newEmail},
        jsonData['id'] = userId;

        $.ajax({
            url      : '/user/resendConfirmMail',
            type     : 'post',
            data     : jsonData,
            dataType : 'json',
            success  : function (response) {
                if (response.status == 'success') {
                    messageBlock.html(response.data.message);
                    $('#checkMailBox').attr('href', response.data.emailServiceInfo.url);
                    $('#change-email-form').addClass('hidden');
                    $('#change-email-form').hide();
                    $('.complete-email').html(response.data.changedEmail);
                } else if (response.status == 'error') {
                    messageBlock.html(response.meta.description.general[0]);
                }
                messageBlock.removeClass('hidden');
            }
        });
    },

    changeLogin: function(model) {
        $('#userNavPhoto').text(model.get('user').login);
    }

});
