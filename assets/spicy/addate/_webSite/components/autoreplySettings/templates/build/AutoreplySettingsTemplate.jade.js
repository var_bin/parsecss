this["JST"] = this["JST"] || {};

this["JST"]["AutoreplySettingsTemplate"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),autoreplySettings = locals_.autoreplySettings,invisibleMode = locals_.invisibleMode,_t = locals_._t,invisibleModeActivated = locals_.invisibleModeActivated;
if ( (!autoreplySettings.disableAutoreplySettings || invisibleMode === true))
{
buf.push("<div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.features")) ? "" : jade_interp)) + "</div><span class=\"save-status inactive\">" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "text.saved")) ? "" : jade_interp)) + "</span><div class=\"b-value\"><ul class=\"b-options-list\">");
if ( (invisibleMode === true))
{
buf.push("<li><label><input type=\"checkbox\" data-checkbox=\"1\" data-invisible-mode-activated=\"1\" name=\"name\"" + (jade.attr("checked", (invisibleModeActivated === true)? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.invisible_mode_activated")) ? "" : jade_interp)) + "</span></label></li>");
}
if ( (!autoreplySettings.disableAutoreplySettings))
{
// iterate autoreplySettings
;(function(){
  var $$obj = autoreplySettings;
  if ('number' == typeof $$obj.length) {

    for (var type = 0, $$l = $$obj.length; type < $$l; type++) {
      var data = $$obj[type];

buf.push("<li" + (jade.attr("autoreply-type", type, true, false)) + "><label><input type=\"checkbox\" data-checkbox=\"1\" data-change-autoreply-settings=\"1\"" + (jade.attr("data-autoreply-type", type, true, false)) + " data-key=\"site\"" + (jade.attr("checked", (data.state > 0), true, false)) + "/><span id=\"autoreplyType\">" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.autoreply_" + type)) ? "" : jade_interp)) + "</span></label><button" + (jade.attr("title", _t("autoreplySettings", "tooltip.change_autoreply"), true, false)) + " data-btn-edit=\"1\" class=\"btn-light btn-edit-bare\"></button><div class=\"features-form inactive\"><div><label><input type=\"radio\"" + (jade.attr("name", "list-select-" + type, true, false)) + " list-select=\"1\" value=\"\"" + (jade.attr("checked", (data.filledByUser == 0 && data.text.length > 1), true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.choose_autoreply")) ? "" : jade_interp)) + "</span></label></div><div><div data-ui-select=\"data-ui-select\" class=\"field select-output-dropdown dark\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", ((data.filledByUser == 0) ? data.text : ""), true, false)) + (jade.attr("name", "select-"+type, true, false)) + " data-control=\"data-control\"/><ul class=\"list\">");
if ( data.dropdownText)
{
// iterate data.dropdownText
;(function(){
  var $$obj = data.dropdownText;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var val = $$obj[$index];

buf.push("<li" + (jade.attr("data-item", val, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = val) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var val = $$obj[$index];

buf.push("<li" + (jade.attr("data-item", val, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = val) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

}
buf.push("</ul></div></div></div></div><div><label><input type=\"radio\"" + (jade.attr("name", "text-select-" + type, true, false)) + " text-select=\"1\" value=\"\"" + (jade.attr("checked", (data.filledByUser > 0 && data.text.length > 1), true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.enter_autoreply")) ? "" : jade_interp)) + "</span></label></div><div><div data-ui-textarea=\"data-ui-textarea\" class=\"field select-output-dropdown dark\"><div class=\"textarea-wrap\"><textarea autoreply-text=\"1\" data-resize=\"auto\" data-control=\"data-control\"" + (jade.attr("readonly", (data.filledByUser == 0), true, false)) + (jade.attr("name", "text-"+type, true, false)) + " maxlength=\"200\" class=\"message-input\">");
if ((data.filledByUser > 0 && data.text.length>1))
{
buf.push(jade.escape(null == (jade_interp = data.text) ? "" : jade_interp));
}
buf.push("</textarea></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div><div class=\"line\"><button data-change-autoreply=\"1\"" + (jade.attr("data-autoreply-type", type, true, false)) + " class=\"btn-light btn-text-green\">" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "button.save")) ? "" : jade_interp)) + "</button></div></div></li>");
    }

  } else {
    var $$l = 0;
    for (var type in $$obj) {
      $$l++;      var data = $$obj[type];

buf.push("<li" + (jade.attr("autoreply-type", type, true, false)) + "><label><input type=\"checkbox\" data-checkbox=\"1\" data-change-autoreply-settings=\"1\"" + (jade.attr("data-autoreply-type", type, true, false)) + " data-key=\"site\"" + (jade.attr("checked", (data.state > 0), true, false)) + "/><span id=\"autoreplyType\">" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.autoreply_" + type)) ? "" : jade_interp)) + "</span></label><button" + (jade.attr("title", _t("autoreplySettings", "tooltip.change_autoreply"), true, false)) + " data-btn-edit=\"1\" class=\"btn-light btn-edit-bare\"></button><div class=\"features-form inactive\"><div><label><input type=\"radio\"" + (jade.attr("name", "list-select-" + type, true, false)) + " list-select=\"1\" value=\"\"" + (jade.attr("checked", (data.filledByUser == 0 && data.text.length > 1), true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.choose_autoreply")) ? "" : jade_interp)) + "</span></label></div><div><div data-ui-select=\"data-ui-select\" class=\"field select-output-dropdown dark\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", ((data.filledByUser == 0) ? data.text : ""), true, false)) + (jade.attr("name", "select-"+type, true, false)) + " data-control=\"data-control\"/><ul class=\"list\">");
if ( data.dropdownText)
{
// iterate data.dropdownText
;(function(){
  var $$obj = data.dropdownText;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var val = $$obj[$index];

buf.push("<li" + (jade.attr("data-item", val, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = val) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var val = $$obj[$index];

buf.push("<li" + (jade.attr("data-item", val, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = val) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

}
buf.push("</ul></div></div></div></div><div><label><input type=\"radio\"" + (jade.attr("name", "text-select-" + type, true, false)) + " text-select=\"1\" value=\"\"" + (jade.attr("checked", (data.filledByUser > 0 && data.text.length > 1), true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "title.enter_autoreply")) ? "" : jade_interp)) + "</span></label></div><div><div data-ui-textarea=\"data-ui-textarea\" class=\"field select-output-dropdown dark\"><div class=\"textarea-wrap\"><textarea autoreply-text=\"1\" data-resize=\"auto\" data-control=\"data-control\"" + (jade.attr("readonly", (data.filledByUser == 0), true, false)) + (jade.attr("name", "text-"+type, true, false)) + " maxlength=\"200\" class=\"message-input\">");
if ((data.filledByUser > 0 && data.text.length>1))
{
buf.push(jade.escape(null == (jade_interp = data.text) ? "" : jade_interp));
}
buf.push("</textarea></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div><div class=\"line\"><button data-change-autoreply=\"1\"" + (jade.attr("data-autoreply-type", type, true, false)) + " class=\"btn-light btn-text-green\">" + (jade.escape(null == (jade_interp = _t("autoreplySettings", "button.save")) ? "" : jade_interp)) + "</button></div></div></li>");
    }

  }
}).call(this);

}
buf.push("</ul></div>");
};return buf.join("");
};