$(document).ready(function()
{
    //vars
    var formId = "CreditCardPaymentForm";
    var smsProcessing = false;
    var changeSourceTypeId = null;
    var cardInputs = $('.creditcard-filed-input.many input');
    var clockObject = $(".clock");
    var clockTime = parseInt(clockObject.attr("data-time-to-count"));
    var hashParams = getHashParams();
    var cardLastNumbers = $('#cardLastNum');
    var validators = {
        'isPartialCharge' : function(data){
            $.each(data, function(k, v){
                var value = v ? 'true' : 'false';
                $('#subscription input#package' + k).parents('li').removeAttr('partial').attr('partial', value);
                $('#subscription .package.active').click();
            })
        }
    };


    $(".unbind").unbind();
    //show package descriptions
    $(".packages-list li").click(function(){
        $(".packages-list li").removeClass("active");
        $(this).addClass("active");

        $(".packages-list li").find("input[name='CreditCardPaymentForm[package_id]']").removeAttr('checked');
        $(this).find("input[name='CreditCardPaymentForm[package_id]']").attr('checked', 'checked');

        if (typeof($(this).attr('lang')) != "undefined") {
            var details = $.parseJSON($(this).attr('lang'));

            if (typeof(details.package) != "undefined") {
                $("._package").hide();
                $("._package_" + details.package).show();
            }

            if (typeof(details.package_type) != "undefined") {
                $("._package_type").hide();
                $("._package_type_" + details.package_type).show();
            }
        }

        showDescription();

        if ($("#selectedPackegePrice") !== undefined) {
            var packageType = $(this).attr("id");
            var isPartial = $(this).attr("partial") == 'true';

            if (packageType == "package_3DAY1" && isPartial) {
                $("#selectedPackegePrice .def").hide();
                $("#selectedPackegePrice .3d").show();
            } else {
                $("#selectedPackegePrice .def").show();
                $("#selectedPackegePrice .3d").hide();
            }
        }

        $("#selectedPackegePrice .price").html($(this).find(".packageChargePrice").val());
        $("#selectedPackegePrice .name").html($(this).find(".packageFullName").val());

        if ($('.repeat_' + packageType) !== undefined) {
            $('.footer-repeat').hide();
            $('.repeat_' + packageType).show();
        }
    });

    if ($("[data-tabs=tab][data-type=card]").hasClass('active')) {
        $('#smsTabHeaderText').addClass('inactive');
        $('#tabHeaderText').removeClass('inactive');
        clockObject.removeClass('inactive');
    }

    if ($("[data-tabs=tab][data-type=sms]").hasClass('active')) {
        $('#smsTabHeaderText').removeClass('inactive');
    }

    $('#ccashAdditionalPackage').unbind().click(function(){
        var hiddenBlock = $('#CreditCardPaymentForm_ccashAdditionalPackage');
        var value = (hiddenBlock.val() == 1) ? 0 : 1;

        hiddenBlock.val(value);
    });

    //change tabs content
    $("[data-tabs=tab]").click(function(event){
        event.preventDefault();
        $(this).parent().find("[data-tabs=tab]").removeClass("active current");
        $(this).parent().next().find("[data-tabs=content]").removeClass("active current");
        $(this).addClass("active current");
        $(this).parent().next().find("[data-tabs=content]").eq($(this).index()).addClass("active current");

        if ($('#smsTabHeaderText').length) {
            var isSmsTabActive = ('sms' == $(this).data('type'));

            $('#smsTabHeaderText').toggleClass('inactive', !isSmsTabActive);
            $('#tabHeaderText').toggleClass('inactive', isSmsTabActive);
            clockObject.toggleClass('inactive', isSmsTabActive);
        }

        window.location.hash = "pay_via_" + $(this).find("[type=radio]").val();
    });

    //if set anchor sms - make active this tab
    if (window.location.hash != '' && window.location.hash === '#pay_via_sms') {
        $("[type=radio][value=sms]").click();
    }

    //after decline set active package
    if (hashParams['packageId'] !== undefined) {
        $('input#package' + hashParams['packageId']).closest('.packages-list li').click();
    }

    //show description for active package
    showDescription();

    //sms form submit
    $("#smsPaymentForm").submit(function(){
        if (smsProcessing === true) return false;

        smsProcessing = true;

        var form        = $(this);
        $.ajax({
            url:        "/pay/sms/",
            type:       "post",
            data:       form.serialize(),
            dataType:   "json",
            error:      function(){},
            success:    function( response ){
                if( response.status == true ){
                    form.find(".sms_form_content").hide();
                    form.find(".sms_form_response iframe").attr("src", response.url).parent().show();
                }
            },
            complete:   function(){
                smsProcessing = false;
            }
        });

        return false;
    });

    //Custom validators
    $("#subscription input[name='CreditCardPaymentForm[card_number]']").change(function(){
        var formData = {
            'ajax':             'subscription',
            'validatorActions': 'isPartialCharge'
        };

        $('#subscription').find('input[type="radio"], input[type="text"], input[type="hidden"], input[type="number"], select').each(function(){
            formData[$(this).attr('name')] = $(this).val();
        });

        $.ajax({
            url:        "/pay/pay",
            type:       "post",
            data:       formData,
            dataType:   "json",
            error:      function(){},
            success:    function(response){
                $.each(response, function(key, data){
                    if (typeof(validators[key]) == 'function'){
                        validators[key](data);
                    }
                })
            },
            complete:   function(){}
        });

        return;
    });

    //disable pay button to prevent double click
    $("#subscription").live("submit", function (){
        $("#btn-pay").attr("disabled", "disabled");
    });

    //show hidden form
    $("#change_payment_details").click(function () {
        $(".creditcard-pay").toggleClass("inactive");
        if ($("#payment_details").attr("data-form-scenario") == "LONG") {
            $("#payment_details").toggleClass("inactive");
        }
        $("#" + formId + "_source_type").val(changeSourceTypeId);
        var hidePaymentForm = $("#"+formId+"_hidePaymentForm");
        if (hidePaymentForm.val() == true) {
            hidePaymentForm.val(0);
        } else {
            hidePaymentForm.val(1);
        }

        showDescription();
    });

    //set card number if full on page load
    if ($('#CreditCardPaymentForm_card_number').val().length == 16) {
        var cardNumber = $('#CreditCardPaymentForm_card_number').val();

        //fill card number digits
        cardInputs.eq(0).val(cardNumber.substr(0, 4));
        cardInputs.eq(1).val(cardNumber.substr(4, 4));
        cardInputs.eq(2).val(cardNumber.substr(8, 4));
        cardInputs.eq(3).val(cardNumber.substr(12, 4));
        //fill last card numbers near cvv code
        cardLastNumbers.html(cardInputs.eq(3).val());
    }

    //detect card bin if first field filled
    if (cardInputs.eq(0).val().length == 4) {
        detectCardBin(cardInputs.eq(0).val());
    }

    //switch cvv block on click for card number field
    $('.creditcard-pay').on('click', function() {
        //method used on bad card validation
        jQuery.openCreditCard();
    });

    /**
     * Allow only numbers
     * @param event
     */
    function allowNumbersOnly(event)
    {
        var keyCode = event.keyCode;

        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (keyCode >= 35 && keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((event.shiftKey || (keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
            event.preventDefault();
        }
    }

    /**
     * Make input active and set cursor to end
     * @param $el
     */
    function setCursorToEndOnFocus($el)
    {
        $el.val($el.val());
        $el.focus().val($el.val());
    }

    $('#CreditCardPaymentForm_security_number').on('keydown', allowNumbersOnly);

    //on key down
    $('.creditcard-filed-input.many input')
    .on('keydown',allowNumbersOnly)
    .on('keyup',function(event) {
        var keyCode   = event.keyCode;
        var self      = $(this);
        var value     = self.val();
        var isFirst   = self.hasClass('first');
        var isLast    = self.hasClass('last');
        var maxLength = self.attr('maxlength');

        if (keyCode == 8 && !value.length && !isFirst) {
            setCursorToEndOnFocus(self.prev('input'));
            event.preventDefault();

            return true;
        }

        if (isFirst) {
            detectCardBin(value);
        }

        if (value.length == maxLength) {
            if (isLast) {
                cardLastNumbers.html(cardInputs.eq(3).val());

                return true;
            } else if ((keyCode >= 48 && keyCode <= 57 || keyCode >= 96 && keyCode <= 103)) {
                var next = self.next('input');

                if (next.val().length && next.val().length < maxLength) {
                    next.val(next.val() + String.fromCharCode(keyCode));
                }

                setCursorToEndOnFocus(next);
                event.preventDefault();

                return true;
            }
        } else if (isLast) {
            cardLastNumbers.html('xxxx');
        }
    })
    .on('blur',function(event) {
        markFilled($(this));
        collectCardNumber();
    });

    $('div.select select').each(function(e){
        if($(this).val() != '')
        {
            $(this).parent().children('span.value').text($(this).val());
        }
    });
    $('div.select select').change(function(e){

        $(this).parent().children('span.value').text($(this).val());
    });

    // Change sms package amount by operator
    $('#psmsOperatorSelect').change(function(e){
        var selectedOperator = $(this).val();

        if (smsOperatorsList[selectedOperator]) {
            var curTab = $(this).closest('.b-tab-content');
            curTab.find('.b-packege-mobile .amount').text(smsOperatorsList[selectedOperator].amount);

            $('#SmsPaymentForm_amount').val(smsOperatorsList[selectedOperator].amount);
            $('.weekly-psms-price').html(smsOperatorsList[selectedOperator].amount);
        }
    });
    $('#psmsOperatorSelect').change();


    //event blur to mark filled inputs
    $('.creditcard-pay input[type=text]').live('blur', function() {
        markFilled($(this));
    });

    //flip clock
    if (clockTime > 0) {
        FlipClock.Lang.Current = {
            'hours'   : clockObject.attr('data-hours'),
            'minutes' : clockObject.attr('data-minutes'),
            'seconds' : clockObject.attr('data-seconds')
        };

        var clock = clockObject.FlipClock(clockTime, {
            countdown: true,
            language: 'Current'
        });
    }

    /**
     * Mark field as filled
     * @param $el
     */
    function markFilled($el)
    {
        if ($el.val().length > 0) {
            $el.addClass('filled');
        } else {
            $el.removeClass('filled');
        }
    }

    /**
     * Detect card bin
     * @param cardBin
     */
    function detectCardBin(cardBin)
    {
        if (cardBin.length) {
            $('.card-type').removeClass('visa').removeClass('master')
            switch (cardBin[0]) {
                case '4': cardType = 'visa';break;
                case '5': cardType = 'master';break;
                default: cardType = false;
            }
            //switch payment system icon
            if (cardType != false) {
                $('.card-type').addClass(cardType).removeClass('hidden');
            } else {
                $('.card-type').addClass('hidden');
            }
        } else {
            $('.card-type').addClass('hidden');
        }
    }

    /**
     * Collect card number to hidden checkable field
     *
     * @returns {array}
     */
    function collectCardNumber()
    {
        //check card number digits
        cardNumber = new Array();
        cardNumber.push(cardInputs.eq(0).val());
        cardNumber.push(cardInputs.eq(1).val());
        cardNumber.push(cardInputs.eq(2).val());
        cardNumber.push(cardInputs.eq(3).val());

        var creditCardNumberInput = $('#CreditCardPaymentForm_card_number');
        creditCardNumberInput.val(cardNumber.join(''));
        if (creditCardNumberInput.val().length == 16) {
            creditCardNumberInput.trigger('change');
        }

        return cardNumber;
    }

    /**
     * Show package description
     */
    function showDescription()
    {
        showCardDescription();
        showEftDescription();
    }

    /**
     * Show card description
     */
    function showCardDescription()
    {
        $("#3dayPackageDescription").hide();
        $("#10dayPackageDescription").hide();
        $("#3dayPackageDescriptionPartial").hide();
        var elem = $(".b-tab-content-bycard .package").find("input:checked");
        var package3day = elem.parents("li #package_3DAY1");
        var package10day = elem.parents("li #package_10DAY1");
        if (package3day.length > 0) {
            $("#3dayPackageDescription").show();
        } else if (package10day.length > 0) {
            $("#10dayPackageDescription").show();
        }

        if ($(".package.active").filter("li[partial=true]").length && $("#"+formId+"_hidePaymentForm").val()!=true) {
            $("#3dayPackageDescriptionPartial").show();
        }
    }

    /**
     * Show EFT Description
     */
    function showEftDescription()
    {
        $("#3dayEftPackageDescription").hide();
        $("#10dayEftPackageDescription").hide();
        var elem = $(".b-tab-content-eft .package").find("input:checked");
        var package3day = elem.parents("li #eft_package_3DAY1");
        var package10day = elem.parents("li #eft_package_10DAY1");
        if (package3day.length > 0) {
            $("#3dayEftPackageDescription").show();
        } else if (package10day.length > 0) {
            $("#10dayEftPackageDescription").show();
        }
    }

    /**
     * Parse hash params
     * @returns {{}}
     */
    function getHashParams()
    {
        var hashParams = {};
        var e,
            a = /\+/g,  // Regex for replacing addition symbol with a space
            r = /([^&;=]+)=?([^&;]*)/g,
            d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
            q = window.location.hash.substring(1);

        while (e = r.exec(q))
            hashParams[d(e[1])] = d(e[2]);

        return hashParams;
    }
});
