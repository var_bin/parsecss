var RemoveAccountView = Backbone.View.extend({
    isRenderFetch: false,
    events: {
        'click #check-password': 'checkPassword',
        'click #check-code': 'checkRemoveCode',
        'click #check-email': 'checkEmail',
        'click #close-popup' : function () {
            app.regions.popup.close();
            return false;
        },
        'click [data-notificationSubscription-checkbox]': function(event) {
            var elm = $(event.currentTarget);
            var name = elm.data('name');
            var key = elm.data('key');
            this.model.get('notificationSubscription').settings[name][key] = elm.prop('checked');
        },
        'click [data-notificationSettings-checkbox]': function(event) {
            var elm = $(event.currentTarget);
            var name = elm.data('name');
            var key = elm.data('key');
            this.model.get('notificationSettings').settings[name][key] = elm.prop('checked');
        },
        'click #confirm': function() {
            var coregCheckbox = $("#coreg-placement-remove-account-stage-stop input[type='checkbox']");
            if (typeof coregCheckbox != 'undefined' && coregCheckbox.attr('checked') == 'checked') {
                Backbone.trigger('Coregistration.coregProfile', {
                    'id': "remove-account-stage-stop",
                    'container' : "#coreg-placement-remove-account-stage-stop"
                });
            }

            this.model.save({'stage' : 'stop'}, {type: 'POST'});
            return false;
        },
        'click #remind-password': function() {
            this.passwordRecovery = new PasswordRecovery();
            var self = this;
            this.listenTo(this.passwordRecovery, 'change', function() {
                self.$('#password-error').html($.t('removeAccount', 'password has been sent') + this.passwordRecovery.escape('email'));
            });
            this.passwordRecovery.fetch();
        },
        'click #status-membership' : function() {
            var notificationSubscription = new NotificationSubscriptionModel(this.model.get('notificationSubscription'));
            notificationSubscription.save(null, {type: 'POST'});

            var notificationSettings = new NotificationSettingsModel(this.model.get('notificationSettings'));
            notificationSettings.save(null, {type: 'POST'});

            var coregCheckbox = $("#coreg-placement-remove-account-stage-statusMembership input[type='checkbox']");
            if (typeof coregCheckbox != 'undefined' && coregCheckbox.attr('checked') == 'checked') {
                Backbone.trigger('Coregistration.coregProfile', {
                    'id': "remove-account-stage-statusMembership",
                    'container' : "#coreg-placement-remove-account-stage-statusMembership"
                });
            }

            var statusMembershipAnswer = $("#status-membership-user-answer input[type='radio']:checked").val();
            this.model.save({'statusMembershipAnswer': statusMembershipAnswer}, {type: 'POST'});
        },
        "change #user-reason-answer input[type='radio']" : function() {
            var userReasonAnswer = $("#user-reason-answer input[type='radio']:checked");
            var reasonAction = userReasonAnswer.attr('data-action');
            var reasonHasSub = userReasonAnswer.parent().parent().attr('data-has-sub');
            var imChildren = userReasonAnswer.parent().parent().attr('data-sub');

            if(reasonHasSub == '1') {
                $('#user-reason-answer [data-sub]').addClass('inactive');
                userReasonAnswer.parent().parent().children('[data-sub]').removeClass('inactive');
            }
            else {
                if(imChildren == '1') {
                    $('#user-reason-answer [data-sub]').addClass('inactive');
                    userReasonAnswer.parent().parent().removeClass('inactive');
                }
                else {
                    if(reasonAction == 'other') {
                        $('#user-reason-answer [data-sub]').addClass('inactive');
                        userReasonAnswer.parent().parent().children('[data-additional]').removeClass('inactive');
                    }
                    else {
                        $('#user-reason-answer [data-sub]').addClass('inactive');
                    }
                }
            }
        },
        'click #user-reason' : function() {
            var userReasonAnswer = $("#user-reason-answer input[type='radio']:checked");
            var reasonValue = userReasonAnswer.val();
            var reasonAction = userReasonAnswer.attr('data-action');
            var answerCustom = '';
            if(reasonAction == 'other') {
                answerCustom = userReasonAnswer.parent().parent().children('div[data-additional]').children('div').children('textarea[data-reason]').val();
            }

            //coregistration
            var coregCheckbox = $("#coreg-placement-remove-account-stage-userReason input[type='checkbox']");
            if ( typeof coregCheckbox != "undefined" && coregCheckbox.attr("checked") == "checked" ) {
                Backbone.trigger('Coregistration.coregProfile', {
                    'id': "remove-account-stage-userReason",
                    'container' : "#coreg-placement-remove-account-stage-userReason"
                });
            }

            this.model.save({'userReasonAnswer': reasonValue,'answerCustom' : answerCustom}, {type: 'POST'});
        },
        'click #user-story' : function() {
            var userStory = $('#user-story-text').val();
            this.model.save({'userStory': userStory}, {type: 'POST'});
        },
        'click #are-you-sure' : function() {
            var areYouSureAnswer = $("#are-you-sure-answer input[type='radio']:checked").val();
            this.model.save({'areYouSureAnswer': areYouSureAnswer}, {type: 'POST'});
        }
    },

    initialize: function() {
        this.listenTo(this.model, 'change', this.render);
        this.listenTo(this.model, 'change:needToReload', this.reload);
        this.listenTo(this.model, 'sync', this.processCoregs);
        Resources.load(['coregistration']);
    },

    render: function() {
        if (this.isRenderFetch) {
            this.$el.html(tpl.render('PopupStopAction', this.model.toJSON()));
            return this;
        } else {
            var self = this;
            self.isRenderFetch = true;
            self.model.fetch({
                success: function() {
                    self.model.set({error: null});
                    self.$el.html(tpl.render('PopupStopAction', self.model.toJSON()));
                }
            });
            return self;
        }
    },

    checkPassword: function() {
        var self = this;

        self.model.once('invalid', function(model, error) {
            if(_.isObject(error)) {
                for(var curFieldName in error) {
                    var errorString = '';
                    if(_.isArray(error[curFieldName])) {
                        for(var i in error[curFieldName]) {
                            errorString += error[curFieldName][i] + ' ';
                        }
                    } else if (_.isString(error[curFieldName])) {
                        errorString = error[curFieldName];
                    }
                    if(curFieldName == 'password') {
                        self.model.set({'error' : errorString});
                    }
                }
            } else {
                self.model.set({'error' : ''});
            }
        });

        self.model.save({
                password: self.$el.find('#password').val(),
                error: ''
            }, {
                type: 'POST',
                wait: true,
                view: self.$el
            }
        );
        return false;
    },

    checkRemoveCode: function() {
        var self = this;
        self.model.save({code: self.$el.find("#code").val()}, {
            type: "POST",
            wait: true,
            view: self.$el
        });
    },

    checkEmail: function() {
        var self = this;
        self.model.save({email: self.$el.find("#email").val()}, {
            type: "POST",
            wait: true,
            view: self.$el
        });
    },

    reload: function() {
        if(this.model.attributes.needToReload === true) {
            app.regions.popup.close();
            if(this.model.attributes.statusMembershipAnswer == 'off-mailing' ||
                this.model.attributes.statusMembershipAnswer == 'hide-off' ||
                this.model.attributes.areYouSureAnswer == 'no') {
                Backbone.trigger('Account.Reload');
            }
        }
    },
    processCoregs: function() {
        var stage = this.model.get("stage");
        if ($.inArray(stage, ['statusMembership', 'userReason', 'stop']) !== -1) {
            Backbone.trigger('Coregistration.showNext', {
                'id': "remove-account-stage-"+stage,
                'container' : "#coreg-placement-remove-account-stage-"+stage
            });
        }
    }
});
