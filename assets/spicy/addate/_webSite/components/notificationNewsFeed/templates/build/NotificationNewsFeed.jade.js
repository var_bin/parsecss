this["JST"] = this["JST"] || {};

this["JST"]["NotificationNewsFeed"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,settings = locals_.settings;
buf.push("<div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title.newsfeed")) ? "" : jade_interp)) + "</div><span class=\"save-status inactive\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "text.saved")) ? "" : jade_interp)) + "</span><div class=\"b-value\"><ul class=\"b-options-list\">");
// iterate settings
;(function(){
  var $$obj = settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

buf.push("<li>");
// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + " data-change-newsfeed-subscription=\"1\"" + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + name)) ? "" : jade_interp)) + "</span></label>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + " data-change-newsfeed-subscription=\"1\"" + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + name)) ? "" : jade_interp)) + "</span></label>");
    }

  }
}).call(this);

buf.push("</li>");
    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

buf.push("<li>");
// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + " data-change-newsfeed-subscription=\"1\"" + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + name)) ? "" : jade_interp)) + "</span></label>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<label><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + " data-change-newsfeed-subscription=\"1\"" + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + name)) ? "" : jade_interp)) + "</span></label>");
    }

  }
}).call(this);

buf.push("</li>");
    }

  }
}).call(this);

buf.push("</ul></div>");;return buf.join("");
};