this["JST"] = this["JST"] || {};

this["JST"]["User"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user,_t = locals_._t,userPhotoCount = locals_.userPhotoCount,profileStatus = locals_.profileStatus,upgrade = locals_.upgrade,naughtyMode = locals_.naughtyMode,userPrimaryPhoto = locals_.userPrimaryPhoto,userPhotos = locals_.userPhotos,buttons = locals_.buttons,smsChatData = locals_.smsChatData,talksEnabled = locals_.talksEnabled,lookingFor = locals_.lookingFor;
jade_mixins["userInfo"] = function(title, key){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if(user[key])
{
buf.push("<div class=\"data-single\"><span class=\"title\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</span>");
if (isNaN(user[key]))
{
buf.push("<span class=\"info\">" + (jade.escape(null == (jade_interp = user[key].ucfirst()) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span class=\"info\">" + (jade.escape(null == (jade_interp = user[key]) ? "" : jade_interp)) + "</span>");
}
buf.push("</div>");
}
};
jade_mixins["photoStatus"] = function(){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if(user.statuses)
{
if(user.statuses.newStatus)
{
buf.push("<span class=\"b-new\">" + (jade.escape(null == (jade_interp = _t("user", "title.new")) ? "" : jade_interp)) + "</span>");
}
}
if(userPhotoCount > 1)
{
buf.push("<span class=\"b-photos-count\">" + (jade.escape(null == (jade_interp = userPhotoCount) ? "" : jade_interp)) + "</span>");
}
};
buf.push("<div id=\"usertop-banner\"><div id=\"openx-banner-placement-usertop\" class=\"usertop-banner-openx\"></div></div><div id=\"openx-banner-placement-userright\" class=\"banner-zone-right openx-banner\"></div><div class=\"w-user-profile\">");
if(profileStatus === "active")
{
buf.push("<div class=\"b-left-column\"><div" + (jade.cls(['b-user-profile',(upgrade && upgrade.photo_big) ? "free" : ""], [null,true])) + ">");
if(upgrade && upgrade.photo_big)
{
buf.push("<a" + (jade.attr("href", upgrade.photo_big, true, false)) + (jade.cls(['b-user-photo',(naughtyMode.naughtyClass)?naughtyMode.naughtyClass:""], [null,true])) + ">");
if(user.statuses)
{
if(user.statuses.onlineStatus)
{
buf.push("<span class=\"online-status online\"></span>");
}
if(user.statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"online-status\"></span>");
}
}
if(user.hotUser && user.hotUser.isHot)
{
buf.push("<span" + (jade.attr("title", _t("user", "title.hot_user"), true, false)) + " class=\"hot-status\"></span>");
}
buf.push("<div class=\"image-holder\"><img" + (jade.attr("src", (userPrimaryPhoto && userPrimaryPhoto.avatar) ? userPrimaryPhoto.avatar : user.photoUrl, true, false)) + (jade.attr("alt", user.login, true, false)) + "/>");
if ((naughtyMode.naughtyLevel == 1))
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("user", "title.naughty_photo")) ? "" : jade_interp)) + "</span></div>");
}
else if ((naughtyMode.naughtyLevel == 2))
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("user", "title.explicit_Nudity")) ? "" : jade_interp)) + "</span></div>");
}
buf.push("</div>");
if ((naughtyMode.naughtyMode))
{
buf.push("<div class=\"naughty-menu\"><div class=\"naughty-menu-wrap\">");
if ((naughtyMode.naughtyLevel == 1))
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_sexy")) ? "" : jade_interp)) + "</div><button data-nm-trigger=\"2\" href=\"#\" class=\"btn-switch\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.sexy")) ? "" : jade_interp)) + "</button>");
}
if ((naughtyMode.naughtyLevel == 2))
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_no_limits")) ? "" : jade_interp)) + "</div><button data-nm-trigger=\"3\" href=\"#\" class=\"btn-switch\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.no_limits")) ? "" : jade_interp)) + "</button>");
}
buf.push("</div></div>");
}
jade_mixins["photoStatus"]();
buf.push("</a>");
}
else
{
if ( (userPhotos.length))
{
buf.push("<div id=\"userPhotos\"></div>");
}
else
{
buf.push("<div class=\"image-holder\"><img" + (jade.attr("src", (userPrimaryPhoto && userPrimaryPhoto.avatar) ? userPrimaryPhoto.avatar : user.photoUrl, true, false)) + (jade.attr("alt", user.login, true, false)) + "/>");
if ((naughtyMode.naughtyLevel == 1))
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("user", "title.naughty_photo")) ? "" : jade_interp)) + "</span></div>");
}
else if ((naughtyMode.naughtyLevel == 2))
{
buf.push("<div class=\"b-blur-message\"><span>" + (jade.escape(null == (jade_interp = _t("user", "title.explicit_Nudity")) ? "" : jade_interp)) + "</span></div>");
}
buf.push("</div>");
if ((naughtyMode.naughtyMode))
{
buf.push("<div class=\"naughty-menu\"><div class=\"naughty-menu-wrap\">");
if ((naughtyMode.naughtyLevel == 1))
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_sexy")) ? "" : jade_interp)) + "</div><button data-nm-trigger=\"2\" href=\"#\" class=\"btn-switch\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.sexy")) ? "" : jade_interp)) + "</button>");
}
if ((naughtyMode.naughtyLevel == 2))
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_no_limits")) ? "" : jade_interp)) + "</div><button data-nm-trigger=\"3\" href=\"#\" class=\"btn-switch\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.no_limits")) ? "" : jade_interp)) + "</button>");
}
buf.push("</div></div>");
}
}
buf.push("<div class=\"user-statuses\">");
if(user.statuses)
{
if(user.statuses.onlineStatus)
{
buf.push("<span class=\"online-status online\"></span>");
}
if(user.statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"online-status\"></span>");
}
}
if(user.hotUser && user.hotUser.isHot)
{
buf.push("<span" + (jade.attr("title", _t("user", "title.hot_user"), true, false)) + " class=\"hot-status\"></span>");
}
buf.push("</div>");
}
buf.push("<div class=\"b-user-main-data\"><h1 class=\"b-screenname\"><span" + (jade.cls([(user.invisibleMode) ? "invisible" : ""], [true])) + ">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span></h1>");
if (!user.isUserAdmin)
{
buf.push("<div class=\"b-user-data\"><div class=\"b-btn-activity\">");
if(buttons && (buttons.block || buttons.reportProfile))
{
buf.push("<div class=\"b-btn-activity-grid\"><span class=\"btn-block\"><div class=\"blocker-select\"><ul class=\"list\">");
if(buttons.block && buttons.block.allowForUser)
{
buf.push("<li class=\"item\"><a" + (jade.attr("data-blockbutton", user.id, true, false)) + (jade.cls(['block-btn',(buttons.block.activated)?"activated":""], [null,true])) + "><span" + (jade.attr("title", _t("blocked", "tooltip.block_" + user.data_gender), true, false)) + " class=\"default\">" + (jade.escape(null == (jade_interp = _t("blocked", "button.block")) ? "" : jade_interp)) + "</span><span" + (jade.attr("title", _t("blocked", "tooltip.unblock"), true, false)) + " class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("blocked", "button.unblock")) ? "" : jade_interp)) + "</span></a></li>");
}
if(buttons.reportProfile && buttons.reportProfile.allowForUser)
{
buf.push("<li class=\"item\"><a" + (jade.attr("data-reportbutton", user.id, true, false)) + (jade.cls(['report-btn',(buttons.reportProfile.activated)?"activated":""], [null,true])) + "><span" + (jade.attr("title", _t("reportProfile", "tooltip.report"), true, false)) + " class=\"default\">" + (jade.escape(null == (jade_interp = _t("reportProfile", "button.report")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("reportProfile", "button.unreport")) ? "" : jade_interp)) + "</span></a></li>");
}
buf.push("</ul></div></span></div>");
}
buf.push("<div class=\"b-btn-activity-grid\"><button type=\"button\"" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.attr("title", _t("user", "button.friend"), true, false)) + (jade.cls(['btn-activity','btn-favorite','btn-text-def',(buttons.favorite.activated)?"activated":""], [null,null,null,true])) + "></button></div><div class=\"b-btn-activity-grid\"><button type=\"button\"" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.attr("title", _t("user", "button.wink"), true, false)) + (jade.cls(['btn-activity','btn-wink','btn-text-def',(buttons.wink.activated)?"activated":""], [null,null,null,true])) + "></button></div>");
if ( smsChatData.isSmsChatAvailable)
{
buf.push("<div class=\"b-btn-activity-grid\"><button href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('SmsChat.Start', 'addMsisdnFromProfile', '"+user.id+"')", true, false)) + " title=\"SMS chat\" class=\"btn-activity btn-sms btn-text-def\"></button></div>");
}
if ( talksEnabled)
{
buf.push("<div class=\"b-btn-activity-grid\"><button type=\"button\"" + (jade.attr("data-messagebutton", user.id, true, false)) + " href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('Talks.Start', '" + user.id + "')", true, false)) + (jade.attr("title", _t('user', 'button.message'), true, false)) + (jade.cls(['btn-activity','btn-message','btn-text-def',(buttons.favorite.activated)?"activated":""], [null,null,null,true])) + "></button></div>");
}
buf.push("</div></div>");
if(upgrade && upgrade.photo_more)
{
buf.push("<div class=\"upgrade-banner\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_a_full_membership")) ? "" : jade_interp)) + "</span><a" + (jade.attr("href", upgrade.photo_more, true, false)) + " class=\"btn-upgrade btn-green\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_it_now")) ? "" : jade_interp)) + "</a></div>");
}
}
buf.push("</div></div>");
if (!user.isUserAdmin)
{
buf.push("<div class=\"w-user-details\"><h2 class=\"profile-section-title\">" + (jade.escape(null == (jade_interp = _t("user", "About Her", {"{n}":user.data_gender})) ? "" : jade_interp)) + "</h2><div class=\"profile-bordered-section\"><div class=\"profile-data-group\"><h3 class=\"profile-data-group__title mind\">" + (jade.escape(null == (jade_interp = _t("user", "title.whats_on_gender_mind", {"{n}": user.data_gender})) ? "" : jade_interp)) + "</h3><div class=\"profile-data-group__info\">" + (jade.escape(null == (jade_interp = user.chat_up_line) ? "" : jade_interp)) + "</div></div><div class=\"profile-data-group\"><h3 class=\"profile-data-group__title own-words\">" + (jade.escape(null == (jade_interp = _t("user", "title.in_gender_own_words", {"{n}": user.data_gender})) ? "" : jade_interp)) + "</h3><div class=\"profile-data-group__info\">" + (jade.escape(null == (jade_interp = user.description) ? "" : jade_interp)) + "</div></div><div class=\"profile-data-group\"><h3 class=\"profile-data-group__title personal-info\">" + (jade.escape(null == (jade_interp = _t("user", "title.personal_info")) ? "" : jade_interp)) + "</h3>");
jade_mixins["userInfo"](_t("user", "title.age"), "age");
if(user["geo"])
buf.push("<div class=\"data-single\"><span class=\"title\">City</span><span class=\"info\">" + (jade.escape(null == (jade_interp = user["geo"]["city"].ucfirst()) ? "" : jade_interp)) + "</span></div><div class=\"data-single\"><span class=\"title\">Away from you</span><span class=\"info\">" + (jade.escape(null == (jade_interp = _t("user", "title.away-number-" + user.distanceUnits, {"{n}": user["geo"]["distanceToMe"]})) ? "" : jade_interp)) + "</span></div></div><div class=\"profile-data-group\"><h3 class=\"profile-data-group__title appearance\">" + (jade.escape(null == (jade_interp = _t("user", "title.appearance")) ? "" : jade_interp)) + "</h3>");
jade_mixins["userInfo"](_t("user", "title.height"), "height");
jade_mixins["userInfo"](_t("user", "title.weight"), "weight");
jade_mixins["userInfo"](_t("user", "title.body_type"), "build");
jade_mixins["userInfo"](_t("user", "title.hair_color"), "hair_color");
jade_mixins["userInfo"](_t("user", "title.eyes_colour"), "eye_color");
jade_mixins["userInfo"](_t("user", "title.pircing"), "pierced");
jade_mixins["userInfo"](_t("user", "title.tattoo"), "tattoo");
buf.push("</div><div class=\"profile-data-group\"><h3 class=\"profile-data-group__title lifestyle\">" + (jade.escape(null == (jade_interp = _t("user", "text.lifestyle")) ? "" : jade_interp)) + "</h3>");
jade_mixins["userInfo"](_t("user", "title.smoke"), "smoke");
jade_mixins["userInfo"](_t("user", "title.marital_status"), "marital_status");
jade_mixins["userInfo"](_t("user", "title.drink"), "drink");
jade_mixins["userInfo"](_t("user", "title.orientation"), "sexual_orientation");
jade_mixins["userInfo"](_t("user", "title.ethnic_origin"), "race");
jade_mixins["userInfo"](_t("user", "title.living"), "living");
jade_mixins["userInfo"](_t("user", "title.education"), "education");
jade_mixins["userInfo"](_t("user", "title.income"), "income");
jade_mixins["userInfo"](_t("user", "title.religion"), "religion");
jade_mixins["userInfo"](_t("user", "title.children"), "children");
buf.push("</div><div class=\"profile-data-group\"><h3 class=\"profile-data-group__title looking-for\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.looking_for")) ? "" : jade_interp)) + "</h3>");
if ( lookingFor.show === "looking-for-block")
{
buf.push("<div class=\"looking-for-info\">");
if ( lookingFor.gender || lookingFor.age)
{
if ( lookingFor.gender)
{
buf.push("<div class=\"data-single\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t("user", "title.looking_for_gender")) ? "" : jade_interp)) + "</span><span class=\"info\">" + (jade.escape(null == (jade_interp = _t("user", lookingFor.gender)) ? "" : jade_interp)) + "</span></div>");
}
if ( lookingFor.age)
{
buf.push("<div class=\"data-single\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t("user", "title.lookin_for_age")) ? "" : jade_interp)) + "</span><span class=\"info\">" + (jade.escape(null == (jade_interp = lookingFor.age.from + "-" + lookingFor.age.to) ? "" : jade_interp)) + "</span></div>");
}
}
if ( (lookingFor.religion && lookingFor.religion.length) || (lookingFor.race && lookingFor.race.length))
{
if ( lookingFor.religion && lookingFor.religion.length)
{
buf.push("<div class=\"data-single\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t("user", "title.lookin_for_religion")) ? "" : jade_interp)) + "</span><span class=\"info\">" + (jade.escape(null == (jade_interp = lookingFor.religion.join(", ").ucfirst()) ? "" : jade_interp)) + "</span></div>");
}
if ( lookingFor.race && lookingFor.race.length)
{
buf.push("<div class=\"data-single\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t("user", "title.lookin_for_ethnic_origin")) ? "" : jade_interp)) + "</span><span class=\"info\">" + (jade.escape(null == (jade_interp = lookingFor.race.join(", ").ucfirst()) ? "" : jade_interp)) + "</span></div>");
}
}
buf.push("</div>");
}
else
{
buf.push("<div class=\"looking-for-info member\"><div class=\"member-content\"><span class=\"profile-data-group__info\">" + (jade.escape(null == (jade_interp = _t("user", "text.full_member_for_view", { "{n}": user.data_gender })) ? "" : jade_interp)) + "</span><a" + (jade.attr("href", lookingFor.upgrade, true, false)) + " class=\"btn-upgrade btn-green\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_in_now")) ? "" : jade_interp)) + "</a></div></div>");
}
buf.push("</div></div></div>");
}
buf.push("</div><div class=\"b-right-column\"><div id=\"messengerContainer\"></div>");
if (!user.isUserAdmin)
{
buf.push("<div id=\"openx-banner-placement-user\" class=\"banner-zone-center openx-banner\"></div><div id=\"activityHistory\"></div>");
}
buf.push("</div>");
}
else if(profileStatus === "hidden")
{
buf.push("<div class=\"profile-hidden\">" + (jade.escape(null == (jade_interp = _t("user", "text.profile_was_hidden")) ? "" : jade_interp)) + "</div>");
}
else if(profileStatus === "removed")
{
buf.push("<div class=\"profile-hidden\">" + (jade.escape(null == (jade_interp = _t("user", "text.profile_was_removed")) ? "" : jade_interp)) + "</div>");
}
buf.push("</div><div id=\"openx-banner-placement-userbottom\" class=\"banner-zone-bottom openx-banner\"></div>");;return buf.join("");
};