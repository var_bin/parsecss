var UserView = Backbone.View.extend({
    regions: {
        userPhotos: '#userPhotos',
        messenger: '#messengerContainer'
    },

    events: {
        "click [data-tab]": "tab",

        "click .btn-block" : function(event){
            var $dropdownElement = $(event.currentTarget);

            if ($dropdownElement.hasClass("activated")) {
                $dropdownElement.removeClass("activated")
            } else {
                $dropdownElement.addClass("activated")
            }
        },

        "click [data-blockbutton]" : function(event) {
            var $buttonElement = $(event.currentTarget);

            var uid = $buttonElement.data("blockbutton");
            var blockStatus = $buttonElement.hasClass("activated");

            var model = new BlockStatusModel({
                uid: uid,
                status: blockStatus
            });

            if ($buttonElement.hasClass("loading")) {
                return;
            }
            $buttonElement.addClass("loading");

            model.saveAjax({
                error: function() {
                    $buttonElement.removeClass("loading");
                }
            });
        },

        "click [data-reportbutton]" : function(event) {
            var $buttonElement = $(event.currentTarget);
            var uid = $buttonElement.data("reportbutton");

            var reportProfile = new ReportProfileView({
                model: new ReportProfileModel,
                uid: uid
            });

            reportProfile.activate(function(){
              $buttonElement.removeClass("loading");
            });
        },

        "click [data-nm-trigger]": function(event) {
            event.preventDefault();
            var $el = $(event.currentTarget);
            Backbone.trigger("NaughtyMode.Change", $el.data("nm-trigger"));
        }
    },

    initialize: function(options) {
        this.model.getUser(options.params.id);
        this.activity = new UserActivityBlockView({model: this.model.activities});
        this.listenTo(this.model, "sync", this.renderUser);
        this.listenTo(this.model, "sync", this.processBanners);

        this.listenTo(Backbone, "Blocked.changeStatus", this.updateBlockStatus)
        this.listenTo(Backbone, "ReportProfile.changeStatus", this.updateReportStatus)
    },

    updateReportStatus: function(options) {
        var uid = this.model.get("user").id;

        if (uid == options.uid) {
            var buttons = _.clone(this.model.get("buttons"));
            buttons = _.extend({}, buttons, {
                reportProfile: {
                    activated: options.status,
                    allowForUser: true
                }
            });

            this.model.set("buttons", buttons);
            this.renderUser();
        }
    },

    updateBlockStatus: function(options) {
        var uid = this.model.get("user").id;

        if (uid == options.uid) {
            var buttons = _.clone(this.model.get("buttons"));
            buttons = _.extend({}, buttons, {
                block: {
                    activated: options.status,
                    allowForUser: true
                }
            });

            this.model.set("buttons", buttons);
            this.renderUser();
        }
    },

    renderUser: function() {


        var modelData = this.model.toJSON();
        var talksMessengerConfig = app.appData().get('talksMessengerConfig');
            talksEnabled = talksMessengerConfig && talksMessengerConfig.enabled,
            naughtyModeData = {};

        if(modelData.userPrimaryPhoto && modelData.userPrimaryPhoto.attributes){
            naughtyModeData = app.appData().photoHolderInfo(modelData.userPrimaryPhoto.attributes.level);
        }

        modelData = _.extend(modelData, {
            talksEnabled: talksEnabled,
            naughtyMode : naughtyModeData
        });

        this.$el.html(tpl.render("User", modelData));

        this.regions.userPhotos.render(UserPhotoListView, {
            collection: new UserPhotoListCollection(modelData.userPhotos),
            userId: modelData.user && modelData.user.id,
            params: {access: modelData.photosAccessFreeUser, upgrade: modelData.upgrade}
        });

        if(app.appData().get("user").id !== modelData.user.id && !talksEnabled) {
            this.regions.messenger.render(MessengerView, {
                model: new MessengerModel(modelData.messages, {
                    user: modelData.user
                })
            });
        }

        if(!this.model.get("isOwnProfile")) {
            this.activity.render("#activityHistory");
        }

        this.region.ready();

        Backbone.trigger('userViewRendered');
    },

    tab: function(event) {
        var $el = $(event.currentTarget);
        this.$('[data-tab-content]').removeClass('active');
        this.$('[data-tab]').parent().removeClass('active');
        $el.parent().addClass('active');
        this.$('[data-tab-content="'+$el.attr('data-tab')+'"]').addClass('active');
    },

    processBanners: function() {
        Backbone.trigger('BannerOpenx.process', {
            'id' : 'usertop,user,userright,userbottom',
            'container': '#openx-banner-placement-',
            'asyncLoading': false,
            'success': function(data) {
                $('.'+data.div_id).parents('.openx-banner').addClass('active');
            }
        });
    }
});
