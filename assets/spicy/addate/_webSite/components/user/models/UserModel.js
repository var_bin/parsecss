var UserModel = Backbone.Model.extend({
    url: function() {
        return "/user/view/id/"+this.id;
    },

    initialize: function(options) {
        this.activities = new UserActivityBlock();
        this.on('change', this.ready, this);
    },

    ready: function() {
        this.activities.set({
            "data": this.get("activities"),
            "user": {
                "id": this.get("user").id,
                "login": this.get("user").login,
                "gender": this.get("user").gender
            }
        }, {
            "silent": true
        });
    },

    getUser: function(id) {
        if(!id) throw new Error("No id detected in user model");
        this.id = id;
        this.fetch();
    }
});
