this["JST"] = this["JST"] || {};

this["JST"]["UserPhotoGallery"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),data = locals_.data,_t = locals_._t;
var currentPhoto = data.photos[data.open] ? data.photos[data.open].normal : '';
buf.push("<div class=\"b-frame-photo-viewer active\"><div class=\"b-photo-nav-bar\"><a href=\"javascript:void(0)\" data-close=\"data-close\" class=\"btn-close\"></a><a href=\"#\" id=\"userPhotoGalleryPrev\" class=\"btn-prev-wrap\"><span class=\"btn-prev\"></span></a><a href=\"#\" id=\"userPhotoGalleryNext\" class=\"btn-next-wrap\"><span class=\"btn-next\"></span></a></div><div id=\"userPhotoGalleryFullWrap\" class=\"b-full-photo\"><span class=\"photo-wrap\"><img id=\"userPhotoGalleryFull\"" + (jade.attr("src", currentPhoto, true, false)) + "/></span></div><div id=\"userPhotoGalleryList\" class=\"b-photos-list\"><a id=\"userPhotoGalleryToggle\" href=\"#\" data-content=\"photos-list-switch\" class=\"b-photos-list-switch\">" + (jade.escape(null == (jade_interp = _t("userPhotoGallery", "button.all_photos")) ? "" : jade_interp)) + "</a><ul>");
// iterate data.photos
;(function(){
  var $$obj = data.photos;
  if ('number' == typeof $$obj.length) {

    for (var index = 0, $$l = $$obj.length; index < $$l; index++) {
      var photo = $$obj[index];

buf.push("<li" + (jade.attr("data-gallery-photos", '1'+index, true, false)) + (jade.cls([(index == data.open ? 'active' : '')], [true])) + "><a href=\"javascript:void(0)\"" + (jade.attr("data-photo", photo.normal, true, false)) + (jade.attr("data-level", photo.attributes.level, true, false)) + "><span class=\"photo-list\"><img" + (jade.attr("src", photo.avatar, true, false)) + " class=\"photo-small\"/></span></a></li>");
    }

  } else {
    var $$l = 0;
    for (var index in $$obj) {
      $$l++;      var photo = $$obj[index];

buf.push("<li" + (jade.attr("data-gallery-photos", '1'+index, true, false)) + (jade.cls([(index == data.open ? 'active' : '')], [true])) + "><a href=\"javascript:void(0)\"" + (jade.attr("data-photo", photo.normal, true, false)) + (jade.attr("data-level", photo.attributes.level, true, false)) + "><span class=\"photo-list\"><img" + (jade.attr("src", photo.avatar, true, false)) + " class=\"photo-small\"/></span></a></li>");
    }

  }
}).call(this);

buf.push("</ul></div></div>");;return buf.join("");
};