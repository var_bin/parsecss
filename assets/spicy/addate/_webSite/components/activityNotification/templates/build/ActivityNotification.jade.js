this["JST"] = this["JST"] || {};

this["JST"]["ActivityNotification"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),type = locals_.type,params = locals_.params,txtId = locals_.txtId,gender = locals_.gender,_t = locals_._t,login = locals_.login,promocode = locals_.promocode,userLink = locals_.userLink,fromId = locals_.fromId,naughtyMode = locals_.naughtyMode,photo_url = locals_.photo_url,age = locals_.age,geo = locals_.geo,messageWrite = locals_.messageWrite,talksEnabled = locals_.talksEnabled,buttons = locals_.buttons;
var notifyKey = ['notification', type];
if (params && params.subType) notifyKey.push(params.subType);
if (txtId) notifyKey.push(txtId);
notifyKey.push(gender);
buf.push("<span class=\"header\">");
if ( (type === 'webcam'))
{
buf.push("<span class=\"title\"><span class=\"livecams-ico\"><span class=\"livecams-ico-inner\"><span class=\"livecams-ico-handler\"></span><span class=\"livecams-ico-lines\"></span></span><span class=\"livecams-ico-arrow\"></span></span><span class=\"title-info\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "Video chat request")) ? "" : jade_interp)) + "</span></span>");
}
else
{
buf.push("<span class=\"title\">" + (jade.escape(null == (jade_interp = _t("activityNotification", notifyKey.join('.'))) ? "" : jade_interp)) + "</span>");
}
buf.push("<button type=\"button\" data-close=\"\" class=\"close-button\"></button></span><span class=\"b-user-popup\"><a" + (jade.attr("href", ( type === 'webcam' ? '/livecam/view/id/'+login+'/s/1/promocode/'+ promocode : userLink(fromId) ), true, false)) + (jade.attr("target", ( type === 'webcam' ? '_blank' : '_self' ), true, false)) + " class=\"link\"><span class=\"b-photo\"><span class=\"b-photo-link\"><span class=\"overlay\">&nbsp;</span><div" + (jade.cls(['image-holder',(naughtyMode.naughtyClass)?naughtyMode.naughtyClass:""], [null,true])) + "><img" + (jade.attr("src", photo_url, true, false)) + (jade.attr("alt", login, true, false)) + " class=\"photo\"/></div></span></span><span class=\"b-user-info\"><span class=\"b-screenname\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = login) ? "" : jade_interp)) + "</span></span><span class=\"b-age\">" + (jade.escape(null == (jade_interp = age + ' ' + _t("activityNotification", "text.years")) ? "" : jade_interp)) + "</span>");
if ( (type !== 'webcam'))
{
buf.push("<span class=\"b-miles-away\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "text.miles_from_you", {"{n}": geo.distanceToMe})) ? "" : jade_interp)) + "</span>");
}
else if ( (type === 'webcam'))
{
buf.push("<div class=\"b-live-cam-natification-sponsor\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.this_is_cam_profile")) ? "" : jade_interp)) + "</div>");
}
buf.push("</span></a><span class=\"divider\"></span><span class=\"b-btn-activity-holder\"><span class=\"b-btn-activity\">");
if ( (type === 'webcam'))
{
buf.push("<a" + (jade.attr("href", '/livecam/view/id/'+login+'/s/1/promocode/'+ promocode, true, false)) + (jade.attr("target", ( type === 'webcam' ? '_blank' : '_self' ), true, false)) + " class=\"btn-cams btn-text-green\"><span class=\"livecams-ico\"><span class=\"livecams-ico-inner\"><span class=\"livecams-ico-handler\"></span><span class=\"livecams-ico-lines\"></span></span><span class=\"livecams-ico-arrow\"></span></span><span class=\"btn-title\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.on_cam_now")) ? "" : jade_interp)) + "</span></a>");
}
if ( (params && params.subType && params.subType == 'onSmsChat'))
{
buf.push("<span class=\"b-btn-activity-grid\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('SmsChat.Start', 'addMsisdnFromNotification', '" + fromId + "')", true, false)) + " data-close=\"\" class=\"btn-activity btn-sms btn-text-def\"><span class=\"btn-title\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "Sms Chat")) ? "" : jade_interp)) + "</span></a></span>");
}
else if ( (params && params.subType && params.subType == 'onSmsChatFeed' && type !== 'webcam'))
{
buf.push("<span class=\"b-btn-activity-grid\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('SmsChat.Start', 'addMsisdnFromNotificationFeed', '" + fromId + "')", true, false)) + " data-close=\"\" class=\"btn-activity btn-sms btn-text-def\"><span class=\"btn-title\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "Sms Chat")) ? "" : jade_interp)) + "</span></a></span>");
}
else if ( ( type !== 'webcam'))
{
buf.push("<span class=\"b-btn-activity-grid\">");
if(messageWrite && !messageWrite.next && messageWrite.url)
{
buf.push("<a" + (jade.attr("href", messageWrite.url + '/chat/1', true, false)) + " target=\"_blank\" class=\"btn-activity btn-message btn-text-def\"><span class=\"btn-title\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.message")) ? "" : jade_interp)) + "</span></a>");
}
else
{
if ( talksEnabled)
{
buf.push("<a href=\"javascript:void(0)\"" + (jade.attr("onclick", "Backbone.trigger('Talks.Start', '" + fromId + "')", true, false)) + " data-close=\"\" class=\"btn-activity btn-message btn-text-def\"><span class=\"btn-title\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.message")) ? "" : jade_interp)) + "</span></a>");
}
else
{
buf.push("<a" + (jade.attr("href", userLink(fromId), true, false)) + " data-close=\"\" class=\"btn-activity btn-message btn-text-def\"><span class=\"btn-title\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.message")) ? "" : jade_interp)) + "</span></a>");
}
}
buf.push("</span><span class=\"b-btn-activity-grid\"><button" + (jade.attr("data-winkbutton", fromId, true, false)) + (jade.cls(['btn-activity','btn-wink','btn-text-def',(buttons.wink.activated)?"activated":""], [null,null,null,true])) + "><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.wink")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.wink_sent")) ? "" : jade_interp)) + "</span></button></span>");
}
buf.push("</span></span><a href=\"/#account\" class=\"link\"><span class=\"pop-up-settings\">" + (jade.escape(null == (jade_interp = _t("activityNotification", "button.popup_settings")) ? "" : jade_interp)) + "</span></a></span>");;return buf.join("");
};