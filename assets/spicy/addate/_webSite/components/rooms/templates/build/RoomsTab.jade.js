this["JST"] = this["JST"] || {};

this["JST"]["RoomsTab"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),content = locals_.content;
// iterate content
;(function(){
  var $$obj = content;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<a href=\"#\"" + (jade.attr("data-content-id", item.id, true, false)) + (jade.cls(['link','rooms-tab',item.active ? "active" : ""], [null,null,true])) + ">" + (jade.escape(null == (jade_interp = item.title) ? "" : jade_interp)));
if ( item.closeable)
{
buf.push("<i class=\"close rooms-leave\"></i>");
}
buf.push("</a>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<a href=\"#\"" + (jade.attr("data-content-id", item.id, true, false)) + (jade.cls(['link','rooms-tab',item.active ? "active" : ""], [null,null,true])) + ">" + (jade.escape(null == (jade_interp = item.title) ? "" : jade_interp)));
if ( item.closeable)
{
buf.push("<i class=\"close rooms-leave\"></i>");
}
buf.push("</a>");
    }

  }
}).call(this);
;return buf.join("");
};