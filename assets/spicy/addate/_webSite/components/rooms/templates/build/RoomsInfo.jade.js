this["JST"] = this["JST"] || {};

this["JST"]["RoomsInfo"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),content = locals_.content,_t = locals_._t;
buf.push("<div class=\"wrap\"><div><div" + (jade.cls(['photo-holder',(content.naughtyMode.naughtyClass)?content.naughtyMode.naughtyClass:""], [null,true])) + "><img" + (jade.attr("src", content.photos.url, true, false)) + " alt=\"\"/>");
if ( content.marks && content.marks.hotUser)
{
buf.push("<span class=\"hot\"></span>");
}
buf.push("</div><div class=\"info-holder\"><a" + (jade.attr("href", "/user/view/id/" + content.id, true, false)) + " target=\"_blank\" class=\"title\">" + (jade.escape(null == (jade_interp = content.login) ? "" : jade_interp)) + "</a><div class=\"years\">" + (jade.escape(null == (jade_interp = _t("rooms", "user_info.years", {"{n}": content.age})) ? "" : jade_interp)) + "</div>");
if ( content.geo)
{
buf.push("<div class=\"place\">");
if ( content.geo.distanceToMe)
{
buf.push((jade.escape(null == (jade_interp = content.geo.distanceToMe + " " +  _t("rooms", "user_info.miles_from_you")) ? "" : jade_interp)) + "<br/>");
}
buf.push("<span>" + (jade.escape(null == (jade_interp = content.geo.location) ? "" : jade_interp)) + "</span></div>");
}
buf.push("<div class=\"button-holder\"><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a href=\"#\"" + (jade.attr("data-contact-id", content.id, true, false)) + " class=\"message-btn btn-activity btn-message private\"></a></div><div class=\"b-btn-activity-grid\">");
if ( (content.talksEnabled))
{
buf.push("<a" + (jade.attr("href", "/user/view/id/" + content.id, true, false)) + " target=\"_blank\"" + (jade.attr("onclick", "Backbone.trigger('Talks.Start', '" + content.id + "'); return false;", true, false)) + " class=\"message-btn btn-activity btn-private-message\"></a>");
}
else
{
buf.push("<a" + (jade.attr("href", "/user/view/id/" + content.id, true, false)) + " target=\"_blank\" class=\"message-btn btn-activity btn-private-message\"></a>");
}
buf.push("</div><div class=\"b-btn-activity-grid\"><a href=\"#\"" + (jade.attr("data-favoritebutton", content.id, true, false)) + (jade.cls(['btn-activity','btn-favorite',content.buttons.favorite.activated ? "active" : ""], [null,null,true])) + "></a></div></div></div></div></div>");
if ( content.about)
{
buf.push("<div class=\"about\">" + (jade.escape(null == (jade_interp = content.about) ? "" : jade_interp)) + "</div>");
}
if ( (content.buttons && (content.buttons.block || content.buttons.reportProfile)))
{
buf.push("<div class=\"wrap-block-btns\">");
if ( (content.buttons.block && content.buttons.block.allowForUser))
{
buf.push("<a" + (jade.attr("data-blockbutton", content.id, true, false)) + (jade.cls(['block-btn',(content.buttons.block.activated)?"activated":""], [null,true])) + "><span" + (jade.attr("title", _t("blocked", "tooltip.block_" + content.gender), true, false)) + " class=\"default\">" + (jade.escape(null == (jade_interp = _t("blocked", "button.block")) ? "" : jade_interp)) + "</span><span" + (jade.attr("title", _t("blocked", "tooltip.unblock"), true, false)) + " class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("blocked", "button.unblock")) ? "" : jade_interp)) + "</span></a>");
}
if ( (content.buttons.reportProfile && content.buttons.reportProfile.allowForUser))
{
buf.push("<a" + (jade.attr("data-reportbutton", content.id, true, false)) + (jade.cls(['report-btn',(content.buttons.reportProfile.activated)?"activated":""], [null,true])) + "><span" + (jade.attr("title", _t("reportProfile", "tooltip.report"), true, false)) + " class=\"default\">" + (jade.escape(null == (jade_interp = _t("reportProfile", "button.report")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("reportProfile", "button.unreport")) ? "" : jade_interp)) + "</span></a>");
}
buf.push("</div>");
}
buf.push("</div>");;return buf.join("");
};