this["JST"] = this["JST"] || {};

this["JST"]["RoomsContent"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),contactGroups = locals_.contactGroups,content = locals_.content,former = locals_.former;
buf.push("<div class=\"chat-holder\"><div class=\"chat-contact-list\"><div class=\"accordion-holder\"><div class=\"accordion\">");
// iterate contactGroups
;(function(){
  var $$obj = contactGroups;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var group = $$obj[$index];

var items = content.contacts[group] || []
buf.push("<div" + (jade.attr("style", !items.length ? "display: none;" : "", true, false)) + (jade.cls(['rooms-group',group], [null,true])) + "><h4 class=\"active\">" + (jade.escape(null == (jade_interp = group + "s | ") ? "" : jade_interp)) + "<label>" + (jade.escape(null == (jade_interp = items.length) ? "" : jade_interp)) + "</label><span></span></h4><ul>");
// iterate items
;(function(){
  var $$obj = items;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  }
}).call(this);

buf.push("</ul></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var group = $$obj[$index];

var items = content.contacts[group] || []
buf.push("<div" + (jade.attr("style", !items.length ? "display: none;" : "", true, false)) + (jade.cls(['rooms-group',group], [null,true])) + "><h4 class=\"active\">" + (jade.escape(null == (jade_interp = group + "s | ") ? "" : jade_interp)) + "<label>" + (jade.escape(null == (jade_interp = items.length) ? "" : jade_interp)) + "</label><span></span></h4><ul>");
// iterate items
;(function(){
  var $$obj = items;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  }
}).call(this);

buf.push("</ul></div>");
    }

  }
}).call(this);

buf.push("</div></div><a style=\"display:none\" class=\"btn-green turn-on-button\">Turn cam on</a></div><div class=\"chat-list\"><div class=\"chat-list-holder\"><div class=\"chat-list-wrapper rooms-log scrollend\">");
// iterate content.messages || []
;(function(){
  var $$obj = content.messages || [];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push(null == (jade_interp = former.message(item)) ? "" : jade_interp);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push(null == (jade_interp = former.message(item)) ? "" : jade_interp);
    }

  }
}).call(this);

buf.push("</div><div class=\"rooms-banner\">");
if ( (content.restriction))
{
buf.push(null == (jade_interp = former[content.restriction.tpl](content.restriction.body)) ? "" : jade_interp);
}
buf.push("</div></div><div class=\"message-enter-wrap\"><div class=\"message-enter\"><div class=\"input\"><div class=\"input-wrap\"><div tabindex=\"0\" contenteditable=\"true\"" + (jade.attr("data-room-id", content.id, true, false)) + " class=\"message-input rooms-input autofocus\"></div></div></div><div class=\"submit\"><a class=\"btn btn-send rooms-submit\">Sent</a></div></div></div></div></div>");;return buf.join("");
};