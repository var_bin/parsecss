this["JST"] = this["JST"] || {};

this["JST"]["RoomsList"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),client = locals_.client,content = locals_.content,_t = locals_._t;
// iterate client.orderedRooms
;(function(){
  var $$obj = client.orderedRooms;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var group = $$obj[$index];

var items = content.list[group] || []
if ( items.length)
{
buf.push("<div class=\"b-room-list\">");
if ( "local" === group)
{
buf.push("<h3>" + (jade.escape(null == (jade_interp = client.country) ? "" : jade_interp)) + "</h3>");
}
else
{
buf.push("<h3>" + (jade.escape(null == (jade_interp = _t("rooms", "title.international_rooms")) ? "" : jade_interp)) + "</h3>");
}
// iterate items
;(function(){
  var $$obj = items;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<div class=\"b-item\"><a href=\"#\"" + (jade.attr("data-room-id", item.rid, true, false)) + " class=\"title rooms-join\">" + (jade.escape(null == (jade_interp = item.title) ? "" : jade_interp)) + "</a>");
if ( item.count)
{
buf.push("<p class=\"number\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.n_participants", {"{n}": item.count})) ? "" : jade_interp)) + "</p>");
}
else
{
buf.push("<p class=\"number empty-room\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.count_participants_unoccupied")) ? "" : jade_interp)) + "</p>");
}
buf.push("<p>" + (jade.escape(null == (jade_interp = item.description) ? "" : jade_interp)) + "</p></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<div class=\"b-item\"><a href=\"#\"" + (jade.attr("data-room-id", item.rid, true, false)) + " class=\"title rooms-join\">" + (jade.escape(null == (jade_interp = item.title) ? "" : jade_interp)) + "</a>");
if ( item.count)
{
buf.push("<p class=\"number\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.n_participants", {"{n}": item.count})) ? "" : jade_interp)) + "</p>");
}
else
{
buf.push("<p class=\"number empty-room\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.count_participants_unoccupied")) ? "" : jade_interp)) + "</p>");
}
buf.push("<p>" + (jade.escape(null == (jade_interp = item.description) ? "" : jade_interp)) + "</p></div>");
    }

  }
}).call(this);

buf.push("</div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var group = $$obj[$index];

var items = content.list[group] || []
if ( items.length)
{
buf.push("<div class=\"b-room-list\">");
if ( "local" === group)
{
buf.push("<h3>" + (jade.escape(null == (jade_interp = client.country) ? "" : jade_interp)) + "</h3>");
}
else
{
buf.push("<h3>" + (jade.escape(null == (jade_interp = _t("rooms", "title.international_rooms")) ? "" : jade_interp)) + "</h3>");
}
// iterate items
;(function(){
  var $$obj = items;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push("<div class=\"b-item\"><a href=\"#\"" + (jade.attr("data-room-id", item.rid, true, false)) + " class=\"title rooms-join\">" + (jade.escape(null == (jade_interp = item.title) ? "" : jade_interp)) + "</a>");
if ( item.count)
{
buf.push("<p class=\"number\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.n_participants", {"{n}": item.count})) ? "" : jade_interp)) + "</p>");
}
else
{
buf.push("<p class=\"number empty-room\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.count_participants_unoccupied")) ? "" : jade_interp)) + "</p>");
}
buf.push("<p>" + (jade.escape(null == (jade_interp = item.description) ? "" : jade_interp)) + "</p></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push("<div class=\"b-item\"><a href=\"#\"" + (jade.attr("data-room-id", item.rid, true, false)) + " class=\"title rooms-join\">" + (jade.escape(null == (jade_interp = item.title) ? "" : jade_interp)) + "</a>");
if ( item.count)
{
buf.push("<p class=\"number\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.n_participants", {"{n}": item.count})) ? "" : jade_interp)) + "</p>");
}
else
{
buf.push("<p class=\"number empty-room\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.count_participants_unoccupied")) ? "" : jade_interp)) + "</p>");
}
buf.push("<p>" + (jade.escape(null == (jade_interp = item.description) ? "" : jade_interp)) + "</p></div>");
    }

  }
}).call(this);

buf.push("</div>");
}
    }

  }
}).call(this);
;return buf.join("");
};