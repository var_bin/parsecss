this["JST"] = this["JST"] || {};

this["JST"]["RoomsMessage"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),content = locals_.content,client = locals_.client;
buf.push("<div" + (jade.cls(['b-chat-list',content.body.indexOf('@' + client.card.login + ' ') === 0 ? "personal" : ""], [null,true])) + "><div" + (jade.attr("data-contact-id", content.uid, true, false)) + " data-contact-direction=\"left\"" + (jade.cls(['b-name','rooms-contact',content.card.gender], [null,null,true])) + ">" + (jade.escape(null == (jade_interp = content.card.login) ? "" : jade_interp)) + "</div><div class=\"message\">" + (jade.escape(null == (jade_interp = content.body) ? "" : jade_interp)) + "</div></div>");;return buf.join("");
};