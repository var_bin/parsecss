var RoomsContentModel = Backbone.Model.extend({
    defaults: function() {
        return {
            rooms: {
                id: "rooms", title: "Chatrooms", tpl: "list", active: false, closeable: false
            },
            moderator: {
                id: "moderator", title: "Moderator", tpl: "moderator", active: false, closeable: false
            }
        };
    }
});
