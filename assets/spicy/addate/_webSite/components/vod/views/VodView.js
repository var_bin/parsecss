/*
 * VOD controller
 */

var VodView = Backbone.View.extend({

    filters: [],
    words: '',
    countClicksShowMore: 0,
    freqRenderBanner: 3,
    countCategoriesColumns: 4,
    videoId: null,

    events: {
        'click .VODCat': 'getCategory',
        'click .vodLoadNext': 'loadNext',
        'click .vod-scene': 'showScene',
        'click .ffilter': 'setFilter',
        'click #ccname': 'toggleCategoryWindow',
        'keyup #words': 'setWords',
        'change #filtersPeriod': 'setSubfilters',
        'click #searchFormSubmit': 'setWords'
    },

    initialize: function (options) {
        jQuery.fn.isLoaded = function () {
            return this
                .filter("img")
                .filter(function () {
                    return this.complete;
                }).length > 0;
        };

        if (this.options.params.id) {
            this.videoId = this.options.params.id;
        }

        this.listenTo(this.model, "change:categories", this.renderTemplate);
        this.listenTo(this.model, "change:videos", this.renderVideo);
        this.model.fetch()

        this.slidey = null;
        this.slidePlayer = null;
        this.glide = null;
        this.$el.append($('<div/>').addClass('b-wrap-content b-filter b-filter--vod'));
        this.$el.append($('<div/>').addClass('b-wrap-content b-left-content__vod-b'));
        this.$filter = this.$el.find('.b-filter');
        this.$content = this.$el.find('.b-left-content__vod-b');

        this.$filter.append($('<div/>').attr({'id': 'vodTopMenu-place'}));

        this.$content.append($('<div/>').addClass('vod-b__title').addClass('title').text('Premium Adult Videos'));
        this.$content.append(
            $('<div/>').addClass('vod-b__wrapper-vod-b')
        );
        this.$videosContainer = $('.vod-b__wrapper-vod-b');
    },

    toggleCategoryWindow: function (event) {
        event.preventDefault();
        this.$filter.find('.b-filter--vod form').stop(false, true);
        var obj = $(event.currentTarget);
        var icon = obj.siblings('span');
        if (icon.hasClass('icon-arrow-down')) {
            icon.removeClass('icon-arrow-down').addClass('icon-arrow-up');
            this.$filter.find('.b-filter--vod form').slideDown();
        } else if (icon.hasClass('icon-arrow-up')) {
            icon.removeClass('icon-arrow-up').addClass('icon-arrow-down');
            this.$filter.find('.b-filter--vod form').slideUp();
        }
    },

    send: function (callback) {
        this.model.attributes.videos = {};
        this.model.sync('update', this.model, callback);
    },

    setWords: function(event) {
        this.videoId = null;
        var obj = $('#words'),
            words = obj.val(),
            self = this;
        if (words.length < 3 && words.length != 0) {
            return false;
        }
        this.words = words;
        this.model.attrs.words = this.words;
        this.send({success: function (data) {
            if (!$.isEmptyObject(data)) {
                self.model.set(
                    {
                        'videos': $.map(data.videos, function (value, index) {
                            return value;
                        })
                    }
                );
            }
        }});
    },

    setFilter: function (event) {
        this.videoId = null;
        event.preventDefault();
        this.filters = [];
        var self = this,
            $obj = $(event.currentTarget),
            $subFilters = $('.vod-subfilters'),
            filtersPeriod = '';

        if ($obj.hasClass('item-link--current')) {
            $obj.removeClass('item-link--current');
            $subFilters.hide();
        } else {
            $('.item-link--current').removeClass('item-link--current');
            $obj.addClass('item-link--current');
            $subFilters.find('.vod-subfilters__title').text($obj.text() + ' Videos');
            $subFilters.show();
            this.filters.push($obj.attr('type'));
            filtersPeriod = $subFilters.find('#filtersPeriod').attr('value');
        }
        this.model.attrs.filters = self.filters;
        this.model.attrs.filtersPeriod = filtersPeriod != '' ? filtersPeriod : 'month';

        this.send({success: function (data) {
            if (!$.isEmptyObject(data)) {
                self.model.set(
                    {
                        'videos': $.map(data.videos, function (value, index) {
                            return value;
                        })
                    }
                );
            }
        }});
    },

    setSubfilters: function (event) {
        this.videoId = null;
        var self = this,
            filtersPeriod = '';
        if (this.model.attrs.filters.length) {
            filtersPeriod = $('#filtersPeriod').attr('value');
            this.model.attrs.filtersPeriod = filtersPeriod != '' ? filtersPeriod : 'month';
            this.send({success: function (data) {
                if (!$.isEmptyObject(data)) {
                    self.model.set({
                        'videos': $.map(data.videos, function (value, index) {
                            return value;
                        })
                    });
                }
            }});
        }
    },

    renderTemplate: function () {
        var self = this,
            categories = this.model.get("categories"),
            catLen = categories.length,
            catPerColumn = 0,
            filters = this.model.get('filters');
        if (catLen % this.countCategoriesColumns == 0) {
            catPerColumn = catLen / this.countCategoriesColumns;
        } else {
            catPerColumn = parseInt(catLen / this.countCategoriesColumns) + 1;
        }
        this.$filter.find('#vodTopMenu-place').html(tpl.render("vod-nav", {
            categories: categories,
            selected: this.model.get('catId'),
            catPerColumn: catPerColumn,
            countCategoriesColumn: this.countCategoriesColumns,
            words: this.model.attrs.words || ''
        }));
        $("#categoryList").mouseleave(function() {
            $('#categoryList').hide();
            $('.icon-arrow-up').removeClass('icon-arrow-up').addClass('icon-arrow-down');
        });

        if (filters && filters.length) {
            $.each(filters, function (i, val) {
                self.$filter.find('.ffilter[type=' + val + ']')
                    .addClass('item-link--current');
            });
        }

        var ccname = this.$filter.find('a[cid=' + this.model.attrs.catId + ']').text();
        this.$filter.find('#ccname').text(ccname);

        this.region.ready();

        var timeout_id = null;

        var bckp = null;
        
        if(!('ontouchstart' in window || 'onmsgesturechange' in window)){
            $(document).on('mouseenter', '.wrapper-vod-b-item__video',function (e) {
                e.preventDefault();
                bckp = $(this).find('.slider').html();

                while ($(this).find('.slider img[src-data]').length > 0) {
                    obj = $(this).find('.slider img[src-data]').first();
                    obj.attr('src', obj.attr('src-data'));
                    obj.removeAttr('src-data');
                }

                var obj = $(this);

                timeout_id = setTimeout(function () {
                    var $slider = obj.find('.slider'),
                        glide = $slider.glide({
                            autoplay: 1300,
                            hoverpause: false,
                            animationDuration: 0,
                            arrows: false,
                            navigation: false,
                            keyboard: false
                        })
                        .data('api_glide');
                    glide.reinit();

                    obj
                        .find('.js-vod-item')
                        .addClass('vod-item--hidden');
                    glide.play();
                }, 400);

            }).on('mouseleave', '.wrapper-vod-b-item__video', function (e) {
                e.preventDefault();
                clearTimeout(timeout_id);
                $(this).find('.slider ul').remove();
                $(this)
                    .find('.slider')
                    .append(bckp);
                $(this)
                    .find('.js-vod-item')
                    .removeClass('vod-item--hidden');
            });
        } else {
            $(document).on('mouseenter', '.wrapper-vod-b-item__video',function () {
                $(this).find('a').click();
            });
        }

        return this;
    },

    renderVideo: function () {
        if (this.videoId) {
            this.showVOD(this.videoId);
            return;
        }
        var videos = this.model.get("videos");
        var vodPaymentUrl = this.model.get('vodPaymentUrl');
        if (videos.length > 0) {
            if (this.countClicksShowMore % this.freqRenderBanner == 0 ) {
                videos.splice(videos.length - 3, 0, {banner:true, id: "bannerOpenx" + this.countClicksShowMore + 'vod'} );
                this.processBanners();
            }
            this.$content.find('.vodLoadNext').remove();
            if (this.model.attrs.page === 1) {
                this.$videosContainer.html(tpl.render("vod-content", {
                    video: videos,
                    vodPaymentUrl: vodPaymentUrl
                }));
            } else {
                this.$videosContainer.append(tpl.render("vod-content", {
                    video: videos,
                    vodPaymentUrl: vodPaymentUrl
                }));
            }
        } else {
            this.$videosContainer.html('');
        }
    },

    getCategory: function (event) {
        this.videoId = null;
        event.preventDefault();
        var $obj = $(event.currentTarget),
            id = $obj.attr('cid'),
            self = this;
        id = id ? id : 0;
        this.model.attrs.catId = id;
        this.model.attrs.page = 1;
        this.model.attrs.type = 'list';
        if (id > 0) {
            this.model.url = '/vod/category';
        } else {
            this.model.url = '/vod';
        }
        this.send({
            success: function (data) {
                if (!$.isEmptyObject(data)) {
                    self.model.set({'videos': $.map(data.videos, function (value, index) {
                        return value;
                    })});
                }
            }
        });
        $('form.adv-filter').hide();
        $('.icon-arrow-up').removeClass('icon-arrow-up').addClass('icon-arrow-down');
        $('#ccname').text($obj.text());
    },

    loadNext: function () {
        var self = this;
        this.countClicksShowMore++;
        this.region.loading();
        this.model.attrs.page += 1;
        if (this.model.attrs.page > 2 && this.model.attributes.upgradeUrl.length > 0) {
            $('#layoutContent').addClass('loading');
            document.location.href = this.model.attributes.upgradeUrl;
            return;
        }
        this.send({
            success: function (data) {
                self.model.set({videos: data.videos});
                self.region.ready();
            }
        });
    },

    initVideo: function(video){
       flowplayer(function(api, root){
            api.bind('resume', function(){
                root.css('background-image', '');
            }).bind('load', function () {
                $('a[href="http://flowplayer.org"]').css({'top': '-9999px'});
                setTimeout(function(){
                    var account_status = app.appData().get('user').account_status;
                    if(navigator.userAgent.match(/iPhone|iPad/i) && account_status.type != "paid"){
                        var videoEl = root[0].getElementsByTagName('video')[0],
                            $coverImg = $('<img src="'+video.cover+'" />').css({position: 'absolute', top: '0px', left: '0px', height: '100%', overflow: 'hidden'});

                        videoEl.webkitExitFullScreen();
                        $(videoEl).attr({src: "", display: "none"}).parents('.flowplayer').prepend($coverImg);
                        $(root).find('.fp-message').empty();
                    }

                    $('.endscreen--free').show();
                }, 5000);
            }).bind('fullscreen', function () {
                $('.endscreen--free').css({background: 'rgba(0, 0, 0, 0.85)'});
            }).bind('fullscreen-exit', function () {
                $('.endscreen--free').css({background: 'rgba(0, 0, 0, 0.65)'});
            });
        });
        this.$el.find('.flowplayer').flowplayer({
            ratio: 9 / 16,
            key: "67d7242788b71facccd8e",
            rtmp: 'upfornew.api.membercms.com',
            embed: false,
            engine: ($.browser.msie ? 'flash' : 'html5'),
            volume: 0.2,
            poster: video.cover
        });
    },

    getRelatedVideos: function(currentId) {
        var videos = this.model.get("videos"),
            relatedVideos = [],
            count = 0,
            min = 0,
            max = videos.length -1;
        top:
        do {
            var i = Math.floor(Math.random() * (max - min) + min);
            if (!videos[i].banner && videos[i].id != currentId) {
                for (key in relatedVideos) {
                     if (relatedVideos[key].id == currentId) {
                         continue top;
                     }
                }
                relatedVideos.push(videos[i]);
                ++count;
            }
        } while (count < 3)
        return relatedVideos;
    },

    showScene: function (e) {
        if (!this.model.get('vodPaymentUrl')) {
            e.preventDefault();
            var self = this,
                sceneId = $(e.currentTarget).attr('scene');
            this.showVOD(sceneId);
        }
    },

    showVOD: function (sceneId) {
        var self = this;
        if (32 != sceneId.length) {
            return false;
        }
        $.ajax({
            url      : '/vod/scene',
            type     : 'post',
            data     : app.csrfToken.name + '=' + app.csrfToken.value + '&id=' + sceneId,
            dataType : 'json',
            success  : function (response) {
                if (response.status == 'success') {
                    self.$el.find('.vod-b__wrapper-vod-b').html(
                        tpl.render('VodVideoTemplate', {
                            video : response.data,
                            relatedVideos: self.getRelatedVideos(response.data.id),
                            userAccountStatus: app.appData().get('user').account_status
                        })
                    );
                    self.initVideo(response.data);
                    app.router.navigate("/vod/index/id/" + response.data.id);

                    self.comments = new UserCommentView({
                        parent: this,
                        el: self.$el.find('#comments'), // Место куда хочешь поместить комент
                        objectType: 3, // Для VOD objectType = 3
                        objectId: response.data.id, // VOD ID
                    });
                    $('#layoutContent').removeClass('loading');
                }
            }
        });
    },

    processBanners: function() {
        Backbone.trigger('BannerOpenx.process', {
            'id' : 'vod',
            'container': '#bannerOpenx'+this.countClicksShowMore,
            'asyncLoading': false,
            'success': function(response) {
                if(response && response.div_id)
                    $("."+response.div_id).parents('li.banner-vod').show();
            }
        });
    }
});
