var VodModel = Backbone.Model.extend({

    url: "/vod",

    /*
     * Атрибуты для запроса
     * @param type string Параметр содержащий тип
     */
    attrs : {
        type : 'list',
        catId : 0,
        page : 1,
        filters : [],
        words : '',
        filtersPeriod : 'month'
    },

    data: function() {
        return this.attrs;
    }

});
