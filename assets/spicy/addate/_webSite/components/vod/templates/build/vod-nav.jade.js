this["JST"] = this["JST"] || {};

this["JST"]["vod-nav"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,countCategoriesColumn = locals_.countCategoriesColumn,catPerColumn = locals_.catPerColumn,categories = locals_.categories;
buf.push("<div class=\"b-wrap-content b-filter b-filter--vod vod-filters\"><ul class=\"b-filter-content\"><li class=\"b-filter-content__item\"><span class=\"icon-arrow-down b-filter-content__item-link\"></span><a id=\"ccname\" href=\"#\" class=\"b-filter-content__item-link dropdown\">All categories</a></li><li class=\"b-filter-content__item\"><a href=\"#\" type=\"rate\" class=\"b-filter-content__item-link ffilter\">" + (jade.escape(null == (jade_interp = _t('vod', 'title.nav-filter-top-rated')) ? "" : jade_interp)) + "</a></li><li class=\"b-filter-content__item\"><a href=\"#\" type=\"view\" class=\"b-filter-content__item-link ffilter\">" + (jade.escape(null == (jade_interp = _t('vod', 'title.nav-filter-top-viewed')) ? "" : jade_interp)) + "</a></li></ul><form id=\"categoryList\" style=\"display:none;\" class=\"adv-filter\"><div class=\"adv-filter__columns\">");
for(var i = 0; i < countCategoriesColumn; i++)
{
buf.push("<div class=\"adv-filter__column adv-filter__column--vod\"><ul class=\"adv-filter__column__options\">");
for(var j = 0; j < catPerColumn; j++)
{
buf.push("<li class=\"adv-filter__column__option\">");
if (!categories[j+i*catPerColumn])
{
continue
}
if (categories[j+i*catPerColumn].isTop)
{
buf.push("<a href=\"#\"" + (jade.attr("cid", categories[j+i*catPerColumn].id, true, false)) + " class=\"VODCat topcat\">" + (jade.escape(null == (jade_interp = _t('vod', categories[j+i*catPerColumn].name)) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("<a href=\"#\"" + (jade.attr("cid", categories[j+i*catPerColumn].id, true, false)) + " class=\"VODCat\">" + (jade.escape(null == (jade_interp = _t('vod', categories[j+i*catPerColumn].name)) ? "" : jade_interp)) + "</a>");
}
buf.push("</li>");
}
buf.push("</ul></div>");
}
buf.push("</div></form><div class=\"b-vod-search-wrap\"><div class=\"field select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"><input id=\"words\" name=\"words\" type=\"text\" value=\"\" autocomplete=\"off\" data-control=\"data-control\" class=\"input-value\"/></div></div></div><button type=\"button\" id=\"searchFormSubmit\" class=\"btn-find\"></button></div></div><div style=\"display:none;\" class=\"vod-subfilters\"><span class=\"vod-subfilters__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'title.nav-subfilter-top-rated')) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field subfilter select-input-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.subfilter-this-month')) ? "" : jade_interp)) + "</div><div class=\"select\"><input id=\"filtersPeriod\" type=\"hidden\" value=\"\" name=\"type\" data-control=\"data-control\"/><ul class=\"list\"><li data-item=\"month\" class=\"item\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.subfilter-this-month')) ? "" : jade_interp)) + "</li><li data-item=\"year\" class=\"item\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.subfilter-this-year')) ? "" : jade_interp)) + "</li><li data-item=\"all\" class=\"item\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.subfilter-all-time')) ? "" : jade_interp)) + "</li></ul></div></div></div></div>");;return buf.join("");
};