this["JST"] = this["JST"] || {};

this["JST"]["VodVideoTemplate"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),video = locals_.video,userAccountStatus = locals_.userAccountStatus,_t = locals_._t,relatedVideos = locals_.relatedVideos,secureReproxy = locals_.secureReproxy;
jade_mixins["videoLength"] = function(hours, minutes, seconds){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if (seconds < 10)
{
seconds = "0" + seconds
}
if ( (hours && hours > 0))
{
buf.push("<span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = hours + ":" + minutes + ":" + seconds) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = minutes + ":" + seconds) ? "" : jade_interp)) + "</span>");
}
};
buf.push("<div class=\"player_wrapper\"><div class=\"single-vod\"><div class=\"single-vod__banners\"><!-- сюда фигачим баннеры--><div class=\"single-vod__banners__item\"></div></div><div class=\"single-vod__main\"><div class=\"single-vod__video\"><div class=\"flowplayer\"><video class=\"fp-engine\">");
// iterate video.formats
;(function(){
  var $$obj = video.formats;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var v = $$obj[$index];

buf.push("<source" + (jade.attr("src", v.url, true, false)) + " type=\"video/mp4\"/>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var v = $$obj[$index];

buf.push("<source" + (jade.attr("src", v.url, true, false)) + " type=\"video/mp4\"/>");
    }

  }
}).call(this);

buf.push("</video>");
if (userAccountStatus.type != 'paid')
{
buf.push("<div class=\"endscreen endscreen--free\"><div class=\"endscreen-wrap\"><div class=\"endscreen-inner\"><h2 class=\"endscreen-title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.buy_membership_to_watch_full_videos')) ? "" : jade_interp)) + "</h2><a" + (jade.attr("href", video.membershipUrl, true, false)) + " class=\"btn btn--membership\">" + (jade.escape(null == (jade_interp = _t('vod','Buy Membership')) ? "" : jade_interp)) + "</a><div class=\"trial-note\">" + (jade.escape(null == (jade_interp = _t('vod','This is trial version for free users')) ? "" : jade_interp)) + "</div></div></div></div>");
}
buf.push("</div><div class=\"video-title-wrap\"><div class=\"video-rating\"><div" + (jade.attr("style", "width:" + (video.rating/5 *100) + "%", true, false)) + " class=\"video-rating__count\"></div></div><div class=\"video-title\">" + (jade.escape(null == (jade_interp = video.title) ? "" : jade_interp)) + "</div></div></div><div class=\"vod-info-b__video-features\"><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.video-length')) ? "" : jade_interp)) + "</span>&nbsp;");
jade_mixins["videoLength"](video.duration_hr, video.duration_min, video.duration_sec);
buf.push("</div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.count-video-views')) ? "" : jade_interp)) + "</span>&nbsp;<span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = video.views) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.video-rating')) ? "" : jade_interp)) + "</span>&nbsp;<span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = video.rating) ? "" : jade_interp)) + "</span></div></div><div id=\"comments\" class=\"single-vod-comments\"></div></div></div><div class=\"related-videos\"><h3 class=\"related-videos__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'title.related-videos')) ? "" : jade_interp)) + "</h3><ul>");
// iterate relatedVideos
;(function(){
  var $$obj = relatedVideos;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var v = $$obj[$index];

buf.push("<li class=\"wrapper-vod-b-item\"><div class=\"wrapper-vod-b-item__video\"><a href=\"#\"" + (jade.attr("scene", v.id, true, false)) + " class=\"vod-scene\"><img id=\"cover\"" + (jade.attr("alt", v.title, true, false)) + (jade.attr("src", secureReproxy(v.cover), true, false)) + " width=\"304\" height=\"228\" class=\"wrapper-vod-b-item__video-item js-vod-item\"/><div class=\"b-profile-feed-middle-content__btn-play-small js-vod-item\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon-small icon-play\"></i></div></div><div class=\"slider\"><ul class=\"slides\">");
if ( v.thumbnails)
{
// iterate v.thumbnails
;(function(){
  var $$obj = v.thumbnails;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var th = $$obj[$index];

buf.push("<li class=\"slide\"><img src=\"\"" + (jade.attr("src-data", secureReproxy(th), true, false)) + " style=\"width:100%\" width=\"304\" height=\"228\"/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var th = $$obj[$index];

buf.push("<li class=\"slide\"><img src=\"\"" + (jade.attr("src-data", secureReproxy(th), true, false)) + " style=\"width:100%\" width=\"304\" height=\"228\"/></li>");
    }

  }
}).call(this);

}
buf.push("</ul></div></a></div><div class=\"wrapper-vod-b-item__vod-info-b\"><a href=\"#\"" + (jade.attr("scene", v.id, true, false)) + " class=\"vod-info-b__title vod-scene\">" + (jade.escape(null == (jade_interp = v.title) ? "" : jade_interp)) + "</a><div class=\"vod-info-b__video-features\"><div class=\"vod-info-b__video-features__item\">");
jade_mixins["videoLength"](v.duration_hr, v.duration_min, v.duration_sec);
buf.push("<span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-length')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.views) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-count-views')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.rating) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-rating')) ? "" : jade_interp)) + "</span></div></div></div></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var v = $$obj[$index];

buf.push("<li class=\"wrapper-vod-b-item\"><div class=\"wrapper-vod-b-item__video\"><a href=\"#\"" + (jade.attr("scene", v.id, true, false)) + " class=\"vod-scene\"><img id=\"cover\"" + (jade.attr("alt", v.title, true, false)) + (jade.attr("src", secureReproxy(v.cover), true, false)) + " width=\"304\" height=\"228\" class=\"wrapper-vod-b-item__video-item js-vod-item\"/><div class=\"b-profile-feed-middle-content__btn-play-small js-vod-item\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon-small icon-play\"></i></div></div><div class=\"slider\"><ul class=\"slides\">");
if ( v.thumbnails)
{
// iterate v.thumbnails
;(function(){
  var $$obj = v.thumbnails;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var th = $$obj[$index];

buf.push("<li class=\"slide\"><img src=\"\"" + (jade.attr("src-data", secureReproxy(th), true, false)) + " style=\"width:100%\" width=\"304\" height=\"228\"/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var th = $$obj[$index];

buf.push("<li class=\"slide\"><img src=\"\"" + (jade.attr("src-data", secureReproxy(th), true, false)) + " style=\"width:100%\" width=\"304\" height=\"228\"/></li>");
    }

  }
}).call(this);

}
buf.push("</ul></div></a></div><div class=\"wrapper-vod-b-item__vod-info-b\"><a href=\"#\"" + (jade.attr("scene", v.id, true, false)) + " class=\"vod-info-b__title vod-scene\">" + (jade.escape(null == (jade_interp = v.title) ? "" : jade_interp)) + "</a><div class=\"vod-info-b__video-features\"><div class=\"vod-info-b__video-features__item\">");
jade_mixins["videoLength"](v.duration_hr, v.duration_min, v.duration_sec);
buf.push("<span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-length')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.views) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-count-views')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.rating) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-rating')) ? "" : jade_interp)) + "</span></div></div></div></li>");
    }

  }
}).call(this);

buf.push("</ul></div></div>");;return buf.join("");
};