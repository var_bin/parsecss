this["JST"] = this["JST"] || {};

this["JST"]["vod-content"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),secureReproxy = locals_.secureReproxy,video = locals_.video,vodPaymentUrl = locals_.vodPaymentUrl,undefined = locals_.undefined,_t = locals_._t;
jade_mixins["videoLength"] = function(hours, minutes, seconds){
var block = (this && this.block), attributes = (this && this.attributes) || {};
seconds = parseInt(seconds)
if (seconds < 10)
{
seconds = "0" + seconds
}
if ( (hours && hours > 0))
{
buf.push("<span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = hours + ":" + minutes + ":" + seconds) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = minutes + ":" + seconds) ? "" : jade_interp)) + "</span>");
}
};
jade_mixins["coverImg"] = function(v){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if (v.type && v.type === 'webCam')
{
buf.push("<img id=\"cover\"" + (jade.attr("alt", v.title, true, false)) + (jade.attr("src", v.cover, true, false)) + " width=\"317\" height=\"211\" class=\"wrapper-vod-b-item__video-item js-vod-item\"/>");
}
else
{
buf.push("<img id=\"cover\"" + (jade.attr("alt", v.title, true, false)) + (jade.attr("src", secureReproxy(v.cover), true, false)) + " width=\"317\" height=\"211\" class=\"wrapper-vod-b-item__video-item js-vod-item\"/>");
}
};
jade_mixins["sliderImgs"] = function(v){
var block = (this && this.block), attributes = (this && this.attributes) || {};
// iterate v.thumbnails
;(function(){
  var $$obj = v.thumbnails;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var th = $$obj[$index];

buf.push("<li class=\"slide\">");
if (v.type && v.type === 'webCam')
{
buf.push("<img" + (jade.attr("src", th, true, false)) + (jade.attr("src-data", th, true, false)) + " style=\"width:100%\" width=\"317\" height=\"211\"/>");
}
else
{
buf.push("<img" + (jade.attr("src", secureReproxy(th), true, false)) + (jade.attr("src-data", secureReproxy(th), true, false)) + " style=\"width:100%\" width=\"317\" height=\"211\"/>");
}
buf.push("</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var th = $$obj[$index];

buf.push("<li class=\"slide\">");
if (v.type && v.type === 'webCam')
{
buf.push("<img" + (jade.attr("src", th, true, false)) + (jade.attr("src-data", th, true, false)) + " style=\"width:100%\" width=\"317\" height=\"211\"/>");
}
else
{
buf.push("<img" + (jade.attr("src", secureReproxy(th), true, false)) + (jade.attr("src-data", secureReproxy(th), true, false)) + " style=\"width:100%\" width=\"317\" height=\"211\"/>");
}
buf.push("</li>");
    }

  }
}).call(this);

};
if ( video)
{
buf.push("<ul class=\"wrapper-vod-b__table\">");
var vodSceneUrl = '/vod/index/id/'
if (vodPaymentUrl) vodSceneUrl = vodPaymentUrl + '&vid='
// iterate video
;(function(){
  var $$obj = video;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var v = $$obj[$index];

if(v.banner == undefined)
{
var itemUrl = vodSceneUrl + v.id
if (v.type && v.type === 'webCam')
{
itemUrl = v.url
}
buf.push("<li class=\"wrapper-vod-b-item\"><div" + (jade.cls(['wrapper-vod-b-item__video',(v.type && v.type === 'webCam')? "webcamItem" : null], [null,true])) + "><a" + (jade.attr("href", itemUrl, true, false)) + (jade.attr("scene", v.id, true, false)) + (jade.attr("target", (v.type && v.type === 'webCam') ? "_blank" : null, true, false)) + (jade.cls([(v.type && v.type === 'webCam')? "webcamLink" : "vod-scene"], [true])) + ">");
jade_mixins["coverImg"](v);
buf.push("<div class=\"b-profile-feed-middle-content__btn-play-small js-vod-item\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon-small icon-play\"></i></div></div><div class=\"slider\"><ul class=\"slides\">");
if ( v.thumbnails)
{
jade_mixins["sliderImgs"](v);
}
buf.push("</ul></div></a></div><div class=\"wrapper-vod-b-item__vod-info-b\"><a" + (jade.attr("href", itemUrl, true, false)) + (jade.attr("scene", v.id, true, false)) + (jade.attr("target", (v.type && v.type === 'webCam') ? "_blank" : null, true, false)) + (jade.cls(['vod-info-b__title',(v.type && v.type === 'webCam')? "webcamLink" : "vod-scene"], [null,true])) + ">" + (jade.escape(null == (jade_interp = v.title) ? "" : jade_interp)) + "</a><div class=\"vod-info-b__video-features\"><div class=\"vod-info-b__video-features__item\">");
jade_mixins["videoLength"](v.duration_hr, v.duration_min, v.duration_sec);
buf.push("<span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-length')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.views) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-count-views')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.rating) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-rating')) ? "" : jade_interp)) + "</span></div></div></div></li>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var v = $$obj[$index];

if(v.banner == undefined)
{
var itemUrl = vodSceneUrl + v.id
if (v.type && v.type === 'webCam')
{
itemUrl = v.url
}
buf.push("<li class=\"wrapper-vod-b-item\"><div" + (jade.cls(['wrapper-vod-b-item__video',(v.type && v.type === 'webCam')? "webcamItem" : null], [null,true])) + "><a" + (jade.attr("href", itemUrl, true, false)) + (jade.attr("scene", v.id, true, false)) + (jade.attr("target", (v.type && v.type === 'webCam') ? "_blank" : null, true, false)) + (jade.cls([(v.type && v.type === 'webCam')? "webcamLink" : "vod-scene"], [true])) + ">");
jade_mixins["coverImg"](v);
buf.push("<div class=\"b-profile-feed-middle-content__btn-play-small js-vod-item\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon-small icon-play\"></i></div></div><div class=\"slider\"><ul class=\"slides\">");
if ( v.thumbnails)
{
jade_mixins["sliderImgs"](v);
}
buf.push("</ul></div></a></div><div class=\"wrapper-vod-b-item__vod-info-b\"><a" + (jade.attr("href", itemUrl, true, false)) + (jade.attr("scene", v.id, true, false)) + (jade.attr("target", (v.type && v.type === 'webCam') ? "_blank" : null, true, false)) + (jade.cls(['vod-info-b__title',(v.type && v.type === 'webCam')? "webcamLink" : "vod-scene"], [null,true])) + ">" + (jade.escape(null == (jade_interp = v.title) ? "" : jade_interp)) + "</a><div class=\"vod-info-b__video-features\"><div class=\"vod-info-b__video-features__item\">");
jade_mixins["videoLength"](v.duration_hr, v.duration_min, v.duration_sec);
buf.push("<span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-length')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.views) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-count-views')) ? "" : jade_interp)) + "</span></div><div class=\"vod-info-b__video-features__item\"><span class=\"vod-info-b__video-features__item__number\">" + (jade.escape(null == (jade_interp = v.rating) ? "" : jade_interp)) + "</span><span class=\"vod-info-b__video-features__item__title\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.v-item-rating')) ? "" : jade_interp)) + "</span></div></div></div></li>");
}
    }

  }
}).call(this);

buf.push("</ul><div class=\"vod-loadmore-wrap\"><button style=\"'clear': 'both'\" class=\"vodLoadNext button-b-btn btn btn-text-green\">" + (jade.escape(null == (jade_interp = _t('vod', 'text.btn-show-more')) ? "" : jade_interp)) + "</button></div>");
};return buf.join("");
};