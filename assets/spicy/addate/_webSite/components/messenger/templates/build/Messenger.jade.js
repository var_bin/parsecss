this["JST"] = this["JST"] || {};

this["JST"]["Messenger"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),prev_exist = locals_.prev_exist,_t = locals_._t,gender = locals_.gender,upgrade_communication_show = locals_.upgrade_communication_show,upgrade_communication = locals_.upgrade_communication,userId = locals_.userId,smileList = locals_.smileList,smileBlank = locals_.smileBlank;
buf.push("<div class=\"w-messenger\"><div class=\"messenger-history\"><div id=\"messageHistory\" class=\"messages-wrapper\"><button id=\"messengerMore\"" + (jade.attr("style", (!prev_exist) ? "display: none;" : "", true, false)) + " class=\"previous-messages-holder\"><span class=\"text-in-button\">" + (jade.escape(null == (jade_interp = _t("messenger", "button.previous")) ? "" : jade_interp)) + "</span></button></div><div id=\"messengerRead\" class=\"msg-read-status\"><span class=\"read-text\">" + (jade.escape(null == (jade_interp = _t("messenger", "text.has_read." + gender)) ? "" : jade_interp)) + "</span><span class=\"n-read-text\">" + (jade.escape(null == (jade_interp = _t("messenger", "text.has_not_read." + gender)) ? "" : jade_interp)) + "</span></div>");
if ( upgrade_communication_show && upgrade_communication)
{
buf.push("<a" + (jade.attr("href", upgrade_communication+userId, true, false)) + " class=\"contact-free\">" + (jade.escape(null == (jade_interp = _t("messenger", "button.upgrade_communication")) ? "" : jade_interp)) + "</a>");
}
buf.push("</div><div class=\"messenger-input\"><div class=\"input\"><div class=\"input-wrap\"><div id=\"messengerInput\" contenteditable=\"true\"" + (jade.attr("placeholder", _t("messenger", "placeholder.enter_message"), true, false)) + " class=\"message-input\"></div><div id=\"messengerSmileList\" class=\"smiles\"><div id=\"messengerSmileListBtn\" data-toggle=\"data-toggle\" class=\"btn-smiles\"></div><div class=\"smiles-tooltip b-smiles-enter\"><ul>");
// iterate smileList || []
;(function(){
  var $$obj = smileList || [];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var smile = $$obj[$index];

buf.push("<li class=\"smile-item\"><img" + (jade.attr("data-smile", smile, true, false)) + (jade.attr("src", smileBlank, true, false)) + " class=\"smile-emoji\"/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var smile = $$obj[$index];

buf.push("<li class=\"smile-item\"><img" + (jade.attr("data-smile", smile, true, false)) + (jade.attr("src", smileBlank, true, false)) + " class=\"smile-emoji\"/></li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div></div><div class=\"btn-send-wrap\"><button id=\"messengerSend\" class=\"btn-send btn-text-green\">" + (jade.escape(null == (jade_interp = _t("messenger", "button.send")) ? "" : jade_interp)) + "</button></div></div></div>");;return buf.join("");
};