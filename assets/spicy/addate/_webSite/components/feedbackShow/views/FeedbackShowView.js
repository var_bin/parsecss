var FeedbackShowView = Backbone.View.extend({
    events: {
        "click .btn-side-feedback" : "showPopup",
        "mouseleave .btn-side-feedback" : "mouseLeave"
    },

    initialize: function() {
        this.listenTo(this.model, "change:feedbackEnabled", this.restore);
        this.model.fetch();
    },

    showPopup: function () {
        var self = this;
        self.model.fetch();
        Resources.load(["feedback"], function() {
            app.regions.popup.render(FeedbackView, {
                model: new FeedbackModel({
                    limitReached  : self.model.get("limitReached"),
                    siteLogo      : self.model.get("siteLogoBlack"),
                    selected      : self.model.get("selected"),
                    selectedText  : self.model.get("selectedText"),
                    catId         : self.model.get("catId")
                })
            });
            self.model.set({
                selected      : false,
                selectedText  : ""
            });
        });
    },

    onMouseUp: function() {
        var selectedText = '';
        if (window.getSelection) {
            selectedText = window.getSelection();
        } else if (document.getSelection) {
            selectedText = document.getSelection();
        } else if (document.selection) {
            selectedText = document.selection.createRange().text;
        }
        var self = this;
        if (selectedText.toString() !== "") {
            self.$(".b-tooltip.bug").show();
            self.$(".b-tooltip.bug").css({"opacity": "1", "visibility": "visible"});
            setTimeout(function () {
                self.$(".b-tooltip.bug").hide();
            }, 4000);
        }
    },

    mouseLeave: function() {
        this.$(".b-tooltip.bug").hide();
    },

    selectText: function(e) {
        var selectedText = '';
        e = arguments[0] || window.event;
        var code = e.keyCode ? e.keyCode : (e.which ? e.which : e.charCode);
        if (e.shiftKey && ((code === 13) || (code === 0xA) || (code === 0xD))) {
            if (window.getSelection) {
                selectedText = window.getSelection();
            } else if (document.getSelection) {
                selectedText = document.getSelection();
            } else if (document.selection) {
                selectedText = document.selection.createRange().text;
            }
            this.model.set({
                selected      : true,
                selectedText  : selectedText
            });
            this.showPopup();
        }
    },

    restore: function() {
        if(this.model.get("feedbackEnabled") === true) {
            $(document).on("mouseup", $.proxy(this.onMouseUp, this));
            $(document).on("keypress", $.proxy(this.selectText, this));
            this.$el.html(tpl.render("FeedbackShow"));
        }
        this.region.ready();
        return this;
    }
});
