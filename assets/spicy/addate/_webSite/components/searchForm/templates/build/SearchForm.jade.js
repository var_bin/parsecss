this["JST"] = this["JST"] || {};

this["JST"]["SearchForm"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),advancedForm = locals_.advancedForm,params = locals_.params,_t = locals_._t,livecamAnchor = locals_.livecamAnchor,livecamWocAnchor = locals_.livecamWocAnchor;
jade_mixins["_checkSection"] = function(sectionName){
var block = (this && this.block), attributes = (this && this.attributes) || {};
if(advancedForm[sectionName])
{
var section = advancedForm[sectionName];
buf.push("<section class=\"b-section\"><h5 class=\"title\">" + (jade.escape(null == (jade_interp = section.title.substr(0, 1).toUpperCase() + section.title.substr(1)) ? "" : jade_interp)) + "</h5><ul class=\"b-options-list\">");
// iterate section.values
;(function(){
  var $$obj = section.values;
  if ('number' == typeof $$obj.length) {

    for (var index = 0, $$l = $$obj.length; index < $$l; index++) {
      var val = $$obj[index];

var checked = false;
if (params[sectionName])
{
// iterate params[sectionName]
;(function(){
  var $$obj = params[sectionName];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var section = $$obj[$index];

if (index == section)
{
checked = true
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var section = $$obj[$index];

if (index == section)
{
checked = true
}
    }

  }
}).call(this);

}
buf.push("<li><label><input type=\"checkbox\"" + (jade.attr("name", sectionName+"[]", true, false)) + (jade.attr("value", index, true, false)) + (jade.attr("checked", checked, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = val.substr(0, 1).toUpperCase() + val.substr(1)) ? "" : jade_interp)) + "</span></label></li>");
    }

  } else {
    var $$l = 0;
    for (var index in $$obj) {
      $$l++;      var val = $$obj[index];

var checked = false;
if (params[sectionName])
{
// iterate params[sectionName]
;(function(){
  var $$obj = params[sectionName];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var section = $$obj[$index];

if (index == section)
{
checked = true
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var section = $$obj[$index];

if (index == section)
{
checked = true
}
    }

  }
}).call(this);

}
buf.push("<li><label><input type=\"checkbox\"" + (jade.attr("name", sectionName+"[]", true, false)) + (jade.attr("value", index, true, false)) + (jade.attr("checked", checked, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = val.substr(0, 1).toUpperCase() + val.substr(1)) ? "" : jade_interp)) + "</span></label></li>");
    }

  }
}).call(this);

buf.push("</ul></section>");
}
};
buf.push("<form id=\"searchForm\"><input type=\"hidden\" name=\"country\"" + (jade.attr("value", params.country, true, false)) + "/><div class=\"b-search-form-inner\"><div class=\"form-inner--left\"><div data-ui-select=\"data-ui-select\" class=\"field near select-input-dropdown\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("searchForm", "title.filter")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\">" + (jade.escape(null == (jade_interp = _t("searchForm", "value.people_near_you")) ? "" : jade_interp)) + "</div><div class=\"select\"><input id=\"searchType\" type=\"hidden\"" + (jade.attr("value", params.type, true, false)) + " name=\"type\" data-control=\"data-control\"/><ul class=\"list\"><li data-item=\"near_you\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("searchForm", "value.people_near_you")) ? "" : jade_interp)) + "</li><li data-item=\"online\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("searchForm", "value.online_now")) ? "" : jade_interp)) + "</li><li data-item=\"new_members\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("searchForm", "value.recently_joined")) ? "" : jade_interp)) + "</li>");
if ( (livecamAnchor))
{
buf.push("<a data-item=\"web_cams\" target=\"_blank\"" + (jade.attr("href", "/livecam/?promocode=" + (livecamWocAnchor) + "", true, false)) + " class=\"item\">Web Cams</a>");
}
buf.push("</ul></div></div></div></div><div class=\"form-inner--right\"><div id=\"search-form-select-gender\" data-ui-select=\"data-ui-select\" class=\"field sex select-output-dropdown\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("searchForm", "title.gender")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\">" + (jade.escape(null == (jade_interp = _t("searchForm", "value.female")) ? "" : jade_interp)) + "</div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", params.gender, true, false)) + " name=\"gender\" data-control=\"data-control\"/><ul class=\"list\"><li data-item=\"1\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("searchForm", "value.male")) ? "" : jade_interp)) + "</li><li data-item=\"2\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("searchForm", "value.female")) ? "" : jade_interp)) + "</li></ul></div></div></div><div class=\"field-age-holder\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("searchForm", "title.age")) ? "" : jade_interp)) + "</div><div id=\"search-form-select-age-from\" data-ui-select=\"data-ui-select\" data-select-max=\"searchFormAgeTo\" class=\"field age-from select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select age-from\"><input type=\"hidden\"" + (jade.attr("value", params.ageFrom, true, false)) + " name=\"ageFrom\" data-control=\"data-control\" id=\"searchFormAgeFrom\"/><ul class=\"list\">");
for(var i = 18; i < 79; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div><div class=\"dash\"></div><div id=\"search-form-select-age-to\" data-ui-select=\"data-ui-select\" data-select-min=\"searchFormAgeFrom\" class=\"field age-to select-output-dropdown\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select age-to\"><input type=\"hidden\"" + (jade.attr("value", params.ageTo, true, false)) + " name=\"ageTo\" data-control=\"data-control\" id=\"searchFormAgeTo\"/><ul class=\"list\">");
for(var i = 18; i < 79; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div></div><div data-ui-location=\"data-ui-location\" class=\"field location select-output-dropdown\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("searchForm", "title.location")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"><input name=\"location\" type=\"text\"" + (jade.attr("value", params.location, true, false)) + " autocomplete=\"off\" data-control=\"data-control\" class=\"input-value\"/></div><div data-list=\"data-list\" class=\"select location\"></div></div></div><div data-ui-select=\"data-ui-select\" class=\"field s-radius select-output-dropdown\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("searchForm", "title.search_radius")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", params.distance, true, false)) + " name=\"distance\" data-control=\"data-control\"/><ul class=\"list\"><li data-item='20' class=\"item\">20</li><li data-item='50' class=\"item\">50</li><li data-item='100' class=\"item\">100</li></ul></div></div></div><div class=\"buttons-row\"><button type=\"button\" id=\"sarchFormAdvancedOpen\" class=\"btn-text btn-more\">" + (jade.escape(null == (jade_interp = _t("searchForm", "button.more_settings")) ? "" : jade_interp)) + "</button><button type=\"button\" id=\"searchFormSubmit\" class=\"btn-find\"></button><div id=\"searchFormAdvanced\" class=\"adv-search-holder\"><div data-control=\"data-control\" class=\"adv-search\"><header><h3>" + (jade.escape(null == (jade_interp = _t("searchForm", "text.advanced_search_parameters")) ? "" : jade_interp)) + "</h3><button type=\"button\" data-close=\"data-close\" class=\"close-button\"></button></header><article class=\"content\"><ul class=\"item-list\"><li class=\"item col-l\"><ul class=\"item-list\"><li class=\"item col-1\">");
jade_mixins["_checkSection"]("photoLevel");
jade_mixins["_checkSection"]("race");
jade_mixins["_checkSection"]("income");
buf.push("</li><li class=\"item col-2\">");
jade_mixins["_checkSection"]("eye_color");
jade_mixins["_checkSection"]("build");
buf.push("</li><li class=\"item col-3\">");
jade_mixins["_checkSection"]("marital_status");
jade_mixins["_checkSection"]("education");
buf.push("</li></ul></li><li class=\"item col-r\"><ul class=\"item-list\"><li class=\"item col-4\">");
jade_mixins["_checkSection"]("drink");
jade_mixins["_checkSection"]("hair_color");
buf.push("</li><li class=\"item col-5\">");
jade_mixins["_checkSection"]("smoke");
jade_mixins["_checkSection"]("pierced");
jade_mixins["_checkSection"]("tattoo");
buf.push("</li><li class=\"item col-6\">");
jade_mixins["_checkSection"]("religion");
buf.push("</li></ul></li><li id=\"coreg-placement-advanced-search\" class=\"coreg-placement-on-advanced-search\"></li></ul><div class=\"buttons-row\"><button type=\"button\" id=\"searchFormAdvancedSubmit\" class=\"btn-send btn-text-green\">" + (jade.escape(null == (jade_interp = _t("searchForm", "button.find_matches")) ? "" : jade_interp)) + "</button></div></article></div></div></div></div></div></form>");;return buf.join("");
};