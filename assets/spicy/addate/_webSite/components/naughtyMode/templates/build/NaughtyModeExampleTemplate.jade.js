this["JST"] = this["JST"] || {};

this["JST"]["NaughtyModeExampleTemplate"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,enableNaughtyLevel = locals_.enableNaughtyLevel;
buf.push("<div class=\"right-side right-side-widget\"><div class=\"drop-menu\"><h4>" + (jade.escape(null == (jade_interp = _t("naughtyMode", "title.naughty_mode")) ? "" : jade_interp)) + "</h4><span>" + (jade.escape(null == (jade_interp = _t("naughtyMode", "value.normal")) ? "" : jade_interp)) + "</span></div><ul class=\"switch-list\">");
// iterate enableNaughtyLevel
;(function(){
  var $$obj = enableNaughtyLevel;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<li" + (jade.cls([(key == 0) ? "hovered" : ""], [true])) + ">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "value." + val)) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<li" + (jade.cls([(key == 0) ? "hovered" : ""], [true])) + ">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "value." + val)) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div>");;return buf.join("");
};