this["JST"] = this["JST"] || {};

this["JST"]["ProfileLookingFor"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,profile = locals_.profile,formParams = locals_.formParams;
buf.push("<div class=\"data-column-wrap\"><ul class=\"columns-wrap\"><li class=\"col\"><div data-ui-select=\"data-ui-select\" class=\"field select-output-dropdown dark\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.gender")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\">" + (jade.escape(null == (jade_interp = profile.lookingFor.gender) ? "" : jade_interp)) + "</div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.gender, true, false)) + " name=\"lookingForGender\" data-control=\"data-control\" id=\"lookingForGender\"/><ul class=\"list\">");
// iterate formParams.values.lookingFor.gender
;(function(){
  var $$obj = formParams.values.lookingFor.gender;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var gender = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = gender) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var gender = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = gender) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div><div data-ui-location=\"data-ui-location\" class=\"field location select-output-dropdown dark\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.location")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"><input name=\"lookingForCountryCode\" type=\"hidden\"" + (jade.attr("value", profile.lookingFor.country, true, false)) + "/><input name=\"lookingForLocation\" type=\"text\"" + (jade.attr("value", profile.lookingFor.location, true, false)) + " autocomplete=\"off\" data-control=\"data-control\" class=\"input-value\"/></div><div data-list=\"data-list\" class=\"select location\"></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div><div class=\"age-wrap\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.age")) ? "" : jade_interp)) + "</div><div data-ui-select=\"data-ui-select\" data-select-max=\"lookingForAgeTo\" class=\"field age-from select-output-dropdown dark\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select age-from\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.age.from, true, false)) + " name=\"lookingForAge[from]\" data-control=\"data-control\" id=\"lookingForAgeFrom\"/><ul data-items-list=\"data-items-list\" class=\"list\">");
for(var i = formParams.values.lookingFor.age.min; i <= formParams.values.lookingFor.age.max; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div><span class=\"separator\">-</span><div data-ui-select=\"data-ui-select\" data-select-min=\"lookingForAgeFrom\" class=\"field age-to select-output-dropdown dark\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select age-to\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.age.to, true, false)) + " name=\"lookingForAge[to]\" data-control=\"data-control\" id=\"lookingForAgeTo\"/><ul data-items-list=\"data-items-list\" class=\"list\">");
for(var i = formParams.values.lookingFor.age.min; i <= formParams.values.lookingFor.age.max; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div><div data-ui-select=\"data-ui-select\" class=\"field select-output-dropdown dark\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.search_radius")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.distance, true, false)) + " name=\"lookingForDistance\" data-control=\"data-control\" id=\"lookingForDistance\"/><ul data-items-list=\"data-items-list\" class=\"list\">");
// iterate formParams.values.lookingFor.distance
;(function(){
  var $$obj = formParams.values.lookingFor.distance;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var distance = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = distance) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var distance = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = distance) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div><div id=\"coregPlacementLookingFor\" class=\"field coreg-placement-lookigFor\"></div><div class=\"btn-row\"><div id=\"saveStatus\" style=\"display:none;\" class=\"info-save-status\"></div><button data-btn-undo=\"1\" class=\"btn-cancel btn-text-def\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "button.cancel")) ? "" : jade_interp)) + "</button><button data-btn-save=\"1\" class=\"btn-save btn-text-green\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "button.save")) ? "" : jade_interp)) + "</button></div></li><li id=\"edit-religion\" class=\"col\">");
if ( formParams.lookingForItems.indexOf('religion') !== -1)
{
buf.push("<span class=\"save-status inactive\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "text.saved")) ? "" : jade_interp)) + "</span><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.looking_for_religion")) ? "" : jade_interp)) + "</div><ul class=\"b-options-list\">");
// iterate formParams.values.lookingFor.religion
;(function(){
  var $$obj = formParams.values.lookingFor.religion;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var religion = $$obj[key];

buf.push("<li><label><input type=\"checkbox\" data-checkbox=\"1\" name=\"lookingForReligion[]\"" + (jade.attr("value", key, true, false)) + (jade.attr("checked", (profile.lookingFor.religion.indexOf(key) !== -1) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = religion) ? "" : jade_interp)) + "</span></label></li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var religion = $$obj[key];

buf.push("<li><label><input type=\"checkbox\" data-checkbox=\"1\" name=\"lookingForReligion[]\"" + (jade.attr("value", key, true, false)) + (jade.attr("checked", (profile.lookingFor.religion.indexOf(key) !== -1) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = religion) ? "" : jade_interp)) + "</span></label></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
buf.push("</li><li id=\"edit-race\" class=\"col\">");
if ( formParams.lookingForItems.indexOf('race') !== -1)
{
buf.push("<span class=\"save-status inactive\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "text.saved")) ? "" : jade_interp)) + "</span><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.looking_for_race")) ? "" : jade_interp)) + "</div><ul class=\"b-options-list\">");
// iterate formParams.values.lookingFor.race
;(function(){
  var $$obj = formParams.values.lookingFor.race;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var race = $$obj[key];

buf.push("<li><label><input type=\"checkbox\" data-checkbox=\"1\" name=\"lookingForRace[]\"" + (jade.attr("value", key, true, false)) + (jade.attr("checked", (profile.lookingFor.race.indexOf(key) !== -1) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = race) ? "" : jade_interp)) + "</span></label></li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var race = $$obj[key];

buf.push("<li><label><input type=\"checkbox\" data-checkbox=\"1\" name=\"lookingForRace[]\"" + (jade.attr("value", key, true, false)) + (jade.attr("checked", (profile.lookingFor.race.indexOf(key) !== -1) ? true : false, true, false)) + "/><span>" + (jade.escape(null == (jade_interp = race) ? "" : jade_interp)) + "</span></label></li>");
    }

  }
}).call(this);

buf.push("</ul>");
}
buf.push("</li></ul></div>");;return buf.join("");
};