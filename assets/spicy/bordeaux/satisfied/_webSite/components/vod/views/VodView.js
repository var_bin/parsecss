/*
 * VOD controller
 */

var VodView = Backbone.View.extend({

    filters: [],
    words: '',
    countClicksShowMore: 0,
    freqRenderBanner: 3,

    events: {
        'click .VODCat': 'getCategory',
        'click .vodLoadNext': 'loadNext',
        'click .vod-scene': 'showVOD',
        'click .ffilter': 'setFilter',
        'click #ccname': 'toggleCategoryWindow',
        'keyup #words': 'setWords'
    },

    initialize: function (options) {
        jQuery.fn.isLoaded = function () {
            return this
                .filter("img")
                .filter(function () {
                    return this.complete;
                }).length > 0;
        };
        this.model.on("change", this.renderVideo, this);
        this.model.on("change:categories", this.renderTemplate, this);
        this.model.fetch();
        this.slidey = null;
        this.slidePlayer = null;
        this.glide = null;
        this.$el.append($('<div/>').addClass('b-wrap-content b-filter b-filter--vod'));
        this.$el.append($('<div/>').addClass('b-wrap-content b-left-content__vod-b'));
        this.$filter = this.$el.find('.b-filter');
        this.$content = this.$el.find('.b-left-content__vod-b');

        this.$filter.append($('<div/>').attr({'id': 'vodTopMenu-place'}));

        this.$content.append($('<div/>').addClass('vod-b__title title').text('Premium Adult Videos'));
        this.$content.append(
            $('<div/>').addClass('vod-b__wrapper-vod-b').append(
                $('<ul/>').addClass('wrapper-vod-b__table')
            )
        );

        if (this.options.params.id) {
            app.vodPopup(this.options.params.id);
        }
    },

    toggleCategoryWindow: function (event) {
        event.preventDefault();
        this.$filter.find('.b-filter--vod form').stop(false, true);
        var obj = $(event.currentTarget);
        var icon = obj.siblings('span');
        if (icon.hasClass('icon-arrow-down')) {
            icon.removeClass('icon-arrow-down').addClass('icon-arrow-up');
            this.$filter.find('.b-filter--vod form').slideDown();
        } else if (icon.hasClass('icon-arrow-up')) {
            icon.removeClass('icon-arrow-up').addClass('icon-arrow-down');
            this.$filter.find('.b-filter--vod form').slideUp();
        }
    },

    send: function (callback) {
        this.model.attributes.videos = {};
        this.model.sync('update', this.model, callback);
    },

    setWords: function(event) {
        event.preventDefault();

        var self = this;
        var obj = $(event.currentTarget);
        this.words = obj.val();

        if (this.words == '') {
            this.$content.find('.vod-b__title').text('Premium Adult Videos');
        } else {
            this.$content.find('.vod-b__title').text('Search: ' + this.words);
        }

        this.model.attrs.words = this.words;
        this.send({success: function (data) {
            if (!$.isEmptyObject(data)) {
                self.model.set(
                    {
                        'videos': $.map(data.videos, function (value, index) {
                            return value;
                        })
                    }
                );
            }
        }});
    },

    setFilter: function (event) {
        event.preventDefault();
        var obj = $(event.currentTarget);
        var self = this;
        if (obj.hasClass('item-link--current')) {
            obj.removeClass('item-link--current');
            this.filters.pop(obj.attr('type'));
        } else {
            obj.addClass('item-link--current');
        }

        this.$filter.find('.ffilter.item-link--current').each(function () {
            var obj = $(this);
            self.filters.push(obj.attr('type'));
        });

        this.model.attrs.filters = self.filters;
        this.send({success: function (data) {
            if (!$.isEmptyObject(data)) {
                self.model.set(
                    {
                        'videos': $.map(data.videos, function (value, index) {
                            return value;
                        })
                    }
                );
            }
        }});
    },

    renderTemplate: function () {
        var catLen = this.model.get("categories").length,
            catPerColumn = 0,
            self = this;
        for (var i = 1; i < 100; i++) {
            if (catLen % 4 == 0) {
                catPerColumn = catLen / 4;
                break;
            }
            catLen++;
        }
        this.$filter.find('#vodTopMenu-place').html(tpl.render("vod-nav", {
            categories: this.model.get("categories"),
            selected: this.model.attrs.catId,
            catPerColumn: catPerColumn,
            words: this.model.attrs.words || ''
        }));

        var categories = this.model.get("categories"),
            curPosition = 1,
            curColumn = 0;

        catLen = categories.length;
        for (var i = 0; i < catLen; i++) {
            if (curPosition == catPerColumn) {
                curPosition = 1;
                curColumn++;
            }
            curPosition++;
            this.$filter.find('.adv-filter__column--vod').eq(curColumn).find('ul')
                .append(
                    $('<li/>').addClass('adv-filter__column__option').append(
                        $('<a/>').attr({
                            href: '#',
                            cid: categories[i].id
                        })
                            .addClass('VODCat')
                            .text(categories[i].name)
                    )
                );
        }

        if (this.model.attrs.filters.length) {
            $.each(this.model.attrs.filters, function (i, val) {
                self.$filter.find('.ffilter[type=' + val + ']')
                    .addClass('item-link--current');
            });
        }

        var ccname = this.$filter.find('a[cid=' + this.model.attrs.catId + ']').text();
        this.$filter.find('#ccname').text(ccname);
        this.renderVideo();

        appUi.initElements(this.$el);

        this.region.ready();

        if(!('ontouchstart' in window || 'onmsgesturechange' in window)){
            $(document).on('mouseenter', '.wrapper-vod-b-item__video', function (e) {
                e.preventDefault();
                var obj = $(e.currentTarget),
                    $slider = obj.find('.rslides img');

                $slider.unveil(200, function() {
                    $(this).load(function() {
                        this.style.opacity = 1;
                    });
                });
                $slider.trigger('unveil');

                if (!obj.hasClass('js-slide_starded')) {
                    obj
                        .addClass('js-slide_starded')
                        .find('.rslides')
                        .responsiveSlides({
                            auto: true,
                            timeout: 1000,
                            before: function () {
                                obj.find('.slider').addClass('no-loader');
                            }
                        });
                }

                obj
                    .find('.js-vod-item')
                    .addClass('vod-item--hidden');

            }).on('mouseleave', '.wrapper-vod-b-item__video', function (e) {
                e.preventDefault();
                var obj = $(e.currentTarget);

                obj
                    .find('.js-vod-item')
                    .removeClass('vod-item--hidden');
            });
        } else {
            $(document).on('mouseenter', '.wrapper-vod-b-item__video', function () {
                $(this).find('a').click();
            });
        }

        return this;
    },

    renderVideo: function () {
        if (this.model.get("videos").length > 0) {
            var videos = [];
            videos = this.model.get('videos');
            if (this.countClicksShowMore % this.freqRenderBanner == 0 ) {
                videos.splice(videos.length - 3, 0, {banner:true, id: "bannerOpenx" + this.countClicksShowMore + 'vod'} );
                this.processBanners();
            }
            this.$content.find('.vodLoadNext').remove();
            if (this.model.attrs.page === 1) {
                this.$content.find('ul.wrapper-vod-b__table').html(tpl.render("vod-content", {
                    video: videos
                }));
            } else {
                this.$content.find('ul.wrapper-vod-b__table').append(tpl.render("vod-content", {
                    video: videos
                }));
            }
            this.$content.find('.vod-b__wrapper-vod-b').append(
                $('<button/>')
                    .attr('id', 'vod-load_btn')
                    .addClass('vodLoadNext button-b-btn btn')
                    .css({'clear': 'both'})
                    .text('Show more')
            );
        } else {
            this.$content.find('ul.wrapper-vod-b__table').html('');
        }
    },

    getCategory: function (event) {
        event.preventDefault();
        var obj = $(event.currentTarget);
        var id = obj.attr('cid');
        if (isNaN(id)) id = 0;
        var self = this;
        this.model.attrs.catId = id;
        this.model.attrs.page = 1;
        this.model.attrs.type = 'list';
        if (id > 0) {
            this.model.url = '/vod/category';
        } else {
            this.model.url = '/vod';
        }
        this.send({
            success: function (data) {
                if (!$.isEmptyObject(data)) {
                    self.model.set({'videos': $.map(data.videos, function (value, index) {
                        return value;
                    })});
                }
            }
        });

    },

    loadNext: function (event) {
        var self = this,
            el_id = (event.currentTarget.id).toString();

        this.countClicksShowMore++;
        this.model.attrs.page += 1;
        if (this.model.attrs.page > 2 && this.model.attributes.upgradeUrl.length > 0) {
            $.gotoUrl(this.model.attributes.upgradeUrl);
        }

        preloader.createPreloader(el_id);
        this.send({
            success: function (data) {
                self.model.set({videos: data.videos});
                self.region.ready();
                preloader.destroyPreloader(el_id);
            }
        });
    },

    showVOD: function (e) {
        e.preventDefault();
        var scene = $(e.currentTarget).attr('scene');
        app.vodPopup(scene);
    },
    processBanners: function() {
        Backbone.trigger('BannerOpenx.process', {
            'id' : 'vod',
            'container': '#bannerOpenx'+this.countClicksShowMore,
            'asyncLoading': false,
            'success': function(response) {
                if(response && response.div_id)
                    $("."+response.div_id).parents('li.banner-vod').show();
            }
        });
    }
});