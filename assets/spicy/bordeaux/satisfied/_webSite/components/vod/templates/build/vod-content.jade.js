this["JST"] = this["JST"] || {};

this["JST"]["vod-content"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),video = locals_.video,undefined = locals_.undefined,secureReproxy = locals_.secureReproxy;
if ( video)
{
// iterate video
;(function(){
  var $$obj = video;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var v = $$obj[$index];

if(v.banner == undefined)
{
buf.push("<li class=\"wrapper-vod-b-item\"><div class=\"wrapper-vod-b-item__video\"><a href=\"#\"" + (jade.attr("scene", v.id, true, false)) + " class=\"vod-scene\"><img id=\"cover\"" + (jade.attr("alt", v.title, true, false)) + (jade.attr("src", secureReproxy(v.cover), true, false)) + " class=\"wrapper-vod-b-item__video-item js-vod-item\"/><div class=\"b-profile-feed-middle-content__btn-play-small js-vod-item\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon-small icon-play\"></i></div></div><div class=\"wrapper-vod-b-item__video-viewed\">Viewed</div><div class=\"slider\"><ul class=\"rslides\">");
if ( v.thumbnails)
{
// iterate v.thumbnails
;(function(){
  var $$obj = v.thumbnails;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var th = $$obj[$index];

buf.push("<li><img" + (jade.attr("data-src", secureReproxy(th), true, false)) + " src=\"/assets/static/blank.gif\"/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var th = $$obj[$index];

buf.push("<li><img" + (jade.attr("data-src", secureReproxy(th), true, false)) + " src=\"/assets/static/blank.gif\"/></li>");
    }

  }
}).call(this);

}
buf.push("</ul></div></a></div><div class=\"wrapper-vod-b-item__vod-info-b\"><div class=\"vod-info-b__title\">" + (jade.escape(null == (jade_interp = v.title) ? "" : jade_interp)) + "</div><div style=\"display:none\" class=\"vod-info-b__short-info\"><span class=\"vod-info-b__short-info-comment\"><span class=\"short-info-comment__icon icon-Comment\"></span>30</span><span class=\"vod-info-b__short-info-star\"><span class=\"short-info-comment__icon icon-Comment\"></span>10</span></div></div></li>");
}
else
{
buf.push("<li" + (jade.attr("id", v.id, true, false)) + " class=\"banner-vod\"></li>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var v = $$obj[$index];

if(v.banner == undefined)
{
buf.push("<li class=\"wrapper-vod-b-item\"><div class=\"wrapper-vod-b-item__video\"><a href=\"#\"" + (jade.attr("scene", v.id, true, false)) + " class=\"vod-scene\"><img id=\"cover\"" + (jade.attr("alt", v.title, true, false)) + (jade.attr("src", secureReproxy(v.cover), true, false)) + " class=\"wrapper-vod-b-item__video-item js-vod-item\"/><div class=\"b-profile-feed-middle-content__btn-play-small js-vod-item\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon-small icon-play\"></i></div></div><div class=\"wrapper-vod-b-item__video-viewed\">Viewed</div><div class=\"slider\"><ul class=\"rslides\">");
if ( v.thumbnails)
{
// iterate v.thumbnails
;(function(){
  var $$obj = v.thumbnails;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var th = $$obj[$index];

buf.push("<li><img" + (jade.attr("data-src", secureReproxy(th), true, false)) + " src=\"/assets/static/blank.gif\"/></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var th = $$obj[$index];

buf.push("<li><img" + (jade.attr("data-src", secureReproxy(th), true, false)) + " src=\"/assets/static/blank.gif\"/></li>");
    }

  }
}).call(this);

}
buf.push("</ul></div></a></div><div class=\"wrapper-vod-b-item__vod-info-b\"><div class=\"vod-info-b__title\">" + (jade.escape(null == (jade_interp = v.title) ? "" : jade_interp)) + "</div><div style=\"display:none\" class=\"vod-info-b__short-info\"><span class=\"vod-info-b__short-info-comment\"><span class=\"short-info-comment__icon icon-Comment\"></span>30</span><span class=\"vod-info-b__short-info-star\"><span class=\"short-info-comment__icon icon-Comment\"></span>10</span></div></div></li>");
}
else
{
buf.push("<li" + (jade.attr("id", v.id, true, false)) + " class=\"banner-vod\"></li>");
}
    }

  }
}).call(this);

};return buf.join("");
};