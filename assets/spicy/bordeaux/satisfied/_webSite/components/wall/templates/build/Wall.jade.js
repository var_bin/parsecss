this["JST"] = this["JST"] || {};

this["JST"]["Wall"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),wallRecords = locals_.wallRecords,users = locals_.users,userLink = locals_.userLink,helpers = locals_.helpers;
var displayActivity = ["photo","chat_up_line","description"]
if ( wallRecords)
{
// iterate wallRecords
;(function(){
  var $$obj = wallRecords;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var wallRecord = $$obj[$index];

if ( wallRecord.type == 'photo')
{
buf.push("<div class=\"b-feed__content b-feed-content b-two-photo-content\"><div class=\"b-feed-content__profile-avatar\">");
if(users[wallRecord.user_id].statuses)
{
if(users[wallRecord.user_id].statuses.onlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status online\"></span>");
}
if(users[wallRecord.user_id].statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status\"></span>");
}
}
buf.push("<a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + "><img" + (jade.attr("data-src", '' + (users[wallRecord.user_id].photos.url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-feed-content__profile-avatar-item\"/></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + " class=\"b-profile-feed-head__content-nick\">" + (jade.escape((jade_interp = users[wallRecord.user_id].login) == null ? '' : jade_interp)) + "</a>- uploaded " + (jade.escape((jade_interp = wallRecord.photos.length) == null ? '' : jade_interp)) + " new photo</div></div><div class=\"b-profile-feed-middle-content\">");
if ( wallRecord.photos.length < 3)
{
buf.push("<div class=\"photogrid g-1-1\"><ul>");
var i = 0;
// iterate wallRecord.photos
;(function(){
  var $$obj = wallRecord.photos;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\"><img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/></a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\"><img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/></a></li>");
    }

  }
}).call(this);

buf.push("</ul></div>");
}
else
{
buf.push("<div class=\"photogrid g-1-4\"><ul>");
var i = 0;
// iterate wallRecord.photos
;(function(){
  var $$obj = wallRecord.photos;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\">");
if ( i < 2)
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/>");
}
else
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.avatar) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\"/>");
}
buf.push("</a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\">");
if ( i < 2)
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/>");
}
else
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.avatar) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\"/>");
}
buf.push("</a></li>");
    }

  }
}).call(this);

buf.push("</ul></div>");
}
buf.push("</div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><ul class=\"b-profile-feed-footer__post-nav-b\"><li class=\"post-nav-b__item\"><a href=\"#\"" + (jade.attr("data-record-id", "" + (wallRecord.parentId) + "", true, false)) + (jade.attr("data-entity-id", "" + (wallRecord.entityId) + "", true, false)) + " data-object-type=\"2\" class=\"post-nav-b__item-link\">Comment</a></li><li class=\"post-nav-b__item\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (users[wallRecord.user_id].id) + "')", true, false)) + " class=\"post-nav-b__item-link\">Start Chat</a></li></ul></div></div><div" + (jade.attr("id", "comment-" + (wallRecord.entityId) + "", true, false)) + " class=\"b-feed-content__comment hide\"></div></div></div>");
}
else if ( wallRecord.type == 'VOD')
{
buf.push("<div class=\"b-feed__content b-feed-content b-new-video-content\"><div class=\"b-feed-content__profile-avatar\"><a href=\"/#vod\" class=\"b-feed-content__profile-avatar-item-icon icon-Video\"></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a href=\"/#vod\" class=\"b-profile-feed-head__content-nick\">VOD Chanel </a>- upload new video</div></div><div class=\"b-profile-feed-middle-content\"><a href=\"#\"" + (jade.attr("vod-id", wallRecord.videoId, true, false)) + "><img" + (jade.attr("data-src", '' + (wallRecord.img) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-profile-feed-middle-content__item\"/><div class=\"b-profile-feed-middle-content__btn-play\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon icon-play\"></i></div></div><div class=\"b-profile-feed-middle-content__short-info-video\"><div class=\"b-left-short-info-video\">" + (jade.escape((jade_interp = wallRecord.title) == null ? '' : jade_interp)) + "</div>");
var durationUnits = {hours : parseInt(wallRecord.duration.hours), minutes : parseInt(wallRecord.duration.min), seconds : parseInt(wallRecord.duration.sec)}
buf.push("<div class=\"b-right-short-info-video-time\">" + (jade.escape((jade_interp = helpers.formatDuration(durationUnits)) == null ? '' : jade_interp)) + "</div></div></a></div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><div class=\"b-profile-feed-footer__post-nav-b\"><a href=\"#\"" + (jade.attr("data-record-id", "" + (wallRecord.videoId) + "", true, false)) + (jade.attr("data-entity-id", "" + (wallRecord.entityId) + "", true, false)) + " data-object-type=\"3\"" + (jade.attr("data-timestamp", "" + (wallRecord.time) + "", true, false)) + " class=\"post-nav-b__item-link\">Comment</a></div></div></div><div" + (jade.attr("id", "comment-" + (wallRecord.entityId) + "", true, false)) + " class=\"b-feed-content__comment hide\"></div></div></div>");
}
else if ( (wallRecord.type == 'chat_up_line' || wallRecord.type == 'description'))
{
buf.push("<div class=\"b-feed__content b-feed-content b-join-to-site\"><div class=\"b-feed-content__profile-avatar\">");
if(users[wallRecord.user_id].statuses)
{
if(users[wallRecord.user_id].statuses.onlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status online\"></span>");
}
if(users[wallRecord.user_id].statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status\"></span>");
}
}
buf.push("<a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + "><img" + (jade.attr("data-src", '' + (users[wallRecord.user_id].photos.url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-feed-content__profile-avatar-item\"/></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + " class=\"b-profile-feed-head__content-nick\">" + (jade.escape((jade_interp = users[wallRecord.user_id].login) == null ? '' : jade_interp)) + " </a>");
if ( wallRecord.type == 'chat_up_line')
{
buf.push("- has updated profile status");
}
else
{
buf.push("- has updated profile description");
}
buf.push("</div></div><div class=\"b-profile-feed-middle-content\"><div class=\"b-profile-short-info\"><ul class=\"b-profile-short-info__table\"><li class=\"b-profile-short-info__table-item\"><span class=\"b-profile-short-info__table-item-icon icon-Calendar\"></span>" + (jade.escape((jade_interp = users[wallRecord.user_id].age) == null ? '' : jade_interp)) + " years</li><li class=\"b-profile-short-info__table-item\"><span class=\"b-profile-short-info__table-item-icon icon-Location\"></span>");
var geo = [users[wallRecord.user_id].geo.city, users[wallRecord.user_id].geo.country]
buf.push("" + (jade.escape((jade_interp = geo.join(' ')) == null ? '' : jade_interp)) + "</li><li class=\"b-profile-short-info__table-item\"><span class=\"b-profile-short-info__table-item-icon icon-Photo\"></span>1 photos</li></ul></div><div class=\"b-profile-short-info\"><div class=\"b-profile-short-info__table-item\">" + (jade.escape(null == (jade_interp = wallRecord.description) ? "" : jade_interp)) + "</div></div></div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><ul class=\"b-profile-feed-footer__post-nav-b\"><li class=\"post-nav-b__item\"><a href=\"#\"" + (jade.attr("data-record-id", "" + (wallRecord.parentId) + "", true, false)) + (jade.attr("data-entity-id", "" + (wallRecord.entityId) + "", true, false)) + " data-object-type=\"2\"" + (jade.attr("data-timestamp", "" + (wallRecord.time) + "", true, false)) + " class=\"post-nav-b__item-link\">Comment</a></li><li class=\"post-nav-b__item\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (users[wallRecord.user_id].id) + "')", true, false)) + " class=\"post-nav-b__item-link\">Start Chat</a></li></ul></div></div><div" + (jade.attr("id", "comment-" + (wallRecord.entityId) + "", true, false)) + " class=\"b-feed-content__comment hide\"></div></div></div>");
}
else if ( wallRecord.type == 'livecam')
{
buf.push("<div class=\"b-feed__content b-feed-content b-liveshow-content\"><div class=\"b-feed-content__profile-avatar\"><a" + (jade.attr("href", '/#search/model/room/' + (wallRecord.id) + '/status/20', true, false)) + "><img" + (jade.attr("data-src", '' + (wallRecord.photo_url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-feed-content__profile-avatar-item\"/></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a" + (jade.attr("href", '/#search/model/room/' + (wallRecord.id) + '/status/20', true, false)) + " class=\"b-profile-feed-head__content-nick\">" + (jade.escape((jade_interp = wallRecord.screen_name) == null ? '' : jade_interp)) + " </a>- started liveshow</div></div><div class=\"b-profile-feed-middle-content\"><a" + (jade.attr("href", '/#search/model/room/' + (wallRecord.id) + '/status/20', true, false)) + "><img" + (jade.attr("data-src", '' + (wallRecord.photo_url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-profile-feed-middle-content__item\"/><div class=\"b-profile-feed-middle-content__btn\">Watch liveshow</div></a></div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><div class=\"b-profile-feed-footer__post-nav-b\"><a href=\"#\" class=\"post-nav-b__item-link\">Add to favorites</a></div></div></div></div></div>");
}
else if ( wallRecord.type == 'banner')
{
buf.push("<div class=\"b-feed__content b-feed-content b-banner-content\"><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-middle-content\">");
if ( wallRecord.banner_type == 'html')
{
buf.push("" + (((jade_interp = wallRecord.html) == null ? '' : jade_interp)) + "");
}
else if ( wallRecord.banner_type == 'swf')
{
buf.push("<embed" + (jade.attr("width", "" + (wallRecord.width) + "", true, false)) + (jade.attr("height", "" + (wallRecord.height) + "", true, false)) + (jade.attr("src", "/banner/resource/id/" + (wallRecord.key) + "." + (wallRecord.banner_type) + "", true, false)) + " type=\"application/x-shockwave-flash\" bgcolor=\"#ffffff\" wmode=\"opaque\" allowscriptaccess=\"always\"/>");
}
else
{
buf.push("<a" + (jade.attr("href", "" + (wallRecord.url) + "", true, false)) + "><img" + (jade.attr("src", "/banner/resource/id/" + (wallRecord.key) + "." + (wallRecord.banner_type) + "", true, false)) + (jade.attr("alt", "" + (wallRecord.alt) + "", true, false)) + (jade.attr("width", "" + (wallRecord.width) + "", true, false)) + (jade.attr("height", "" + (wallRecord.height) + "", true, false)) + "/></a>");
}
buf.push("</div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var wallRecord = $$obj[$index];

if ( wallRecord.type == 'photo')
{
buf.push("<div class=\"b-feed__content b-feed-content b-two-photo-content\"><div class=\"b-feed-content__profile-avatar\">");
if(users[wallRecord.user_id].statuses)
{
if(users[wallRecord.user_id].statuses.onlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status online\"></span>");
}
if(users[wallRecord.user_id].statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status\"></span>");
}
}
buf.push("<a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + "><img" + (jade.attr("data-src", '' + (users[wallRecord.user_id].photos.url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-feed-content__profile-avatar-item\"/></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + " class=\"b-profile-feed-head__content-nick\">" + (jade.escape((jade_interp = users[wallRecord.user_id].login) == null ? '' : jade_interp)) + "</a>- uploaded " + (jade.escape((jade_interp = wallRecord.photos.length) == null ? '' : jade_interp)) + " new photo</div></div><div class=\"b-profile-feed-middle-content\">");
if ( wallRecord.photos.length < 3)
{
buf.push("<div class=\"photogrid g-1-1\"><ul>");
var i = 0;
// iterate wallRecord.photos
;(function(){
  var $$obj = wallRecord.photos;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\"><img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/></a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\"><img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/></a></li>");
    }

  }
}).call(this);

buf.push("</ul></div>");
}
else
{
buf.push("<div class=\"photogrid g-1-4\"><ul>");
var i = 0;
// iterate wallRecord.photos
;(function(){
  var $$obj = wallRecord.photos;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\">");
if ( i < 2)
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/>");
}
else
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.avatar) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\"/>");
}
buf.push("</a></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var photo = $$obj[$index];

buf.push("<li><a" + (jade.attr("href", '/#photo/record/id/' + (wallRecord.user_id) + '/open/' + (i++) + '', true, false)) + " rel=\"photobox\">");
if ( i < 2)
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.normal) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\" data-image=\"data-image\"/>");
}
else
{
buf.push("<img" + (jade.attr("data-src", '' + (photo.avatar) + '', true, false)) + " src=\"/assets/static/blank.gif\" alt=\"\"/>");
}
buf.push("</a></li>");
    }

  }
}).call(this);

buf.push("</ul></div>");
}
buf.push("</div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><ul class=\"b-profile-feed-footer__post-nav-b\"><li class=\"post-nav-b__item\"><a href=\"#\"" + (jade.attr("data-record-id", "" + (wallRecord.parentId) + "", true, false)) + (jade.attr("data-entity-id", "" + (wallRecord.entityId) + "", true, false)) + " data-object-type=\"2\" class=\"post-nav-b__item-link\">Comment</a></li><li class=\"post-nav-b__item\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (users[wallRecord.user_id].id) + "')", true, false)) + " class=\"post-nav-b__item-link\">Start Chat</a></li></ul></div></div><div" + (jade.attr("id", "comment-" + (wallRecord.entityId) + "", true, false)) + " class=\"b-feed-content__comment hide\"></div></div></div>");
}
else if ( wallRecord.type == 'VOD')
{
buf.push("<div class=\"b-feed__content b-feed-content b-new-video-content\"><div class=\"b-feed-content__profile-avatar\"><a href=\"/#vod\" class=\"b-feed-content__profile-avatar-item-icon icon-Video\"></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a href=\"/#vod\" class=\"b-profile-feed-head__content-nick\">VOD Chanel </a>- upload new video</div></div><div class=\"b-profile-feed-middle-content\"><a href=\"#\"" + (jade.attr("vod-id", wallRecord.videoId, true, false)) + "><img" + (jade.attr("data-src", '' + (wallRecord.img) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-profile-feed-middle-content__item\"/><div class=\"b-profile-feed-middle-content__btn-play\"><div class=\"b-profile-feed-middle-content__b-play\"><i class=\"b-profile-feed-middle-content__btn-play-icon icon-play\"></i></div></div><div class=\"b-profile-feed-middle-content__short-info-video\"><div class=\"b-left-short-info-video\">" + (jade.escape((jade_interp = wallRecord.title) == null ? '' : jade_interp)) + "</div>");
var durationUnits = {hours : parseInt(wallRecord.duration.hours), minutes : parseInt(wallRecord.duration.min), seconds : parseInt(wallRecord.duration.sec)}
buf.push("<div class=\"b-right-short-info-video-time\">" + (jade.escape((jade_interp = helpers.formatDuration(durationUnits)) == null ? '' : jade_interp)) + "</div></div></a></div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><div class=\"b-profile-feed-footer__post-nav-b\"><a href=\"#\"" + (jade.attr("data-record-id", "" + (wallRecord.videoId) + "", true, false)) + (jade.attr("data-entity-id", "" + (wallRecord.entityId) + "", true, false)) + " data-object-type=\"3\"" + (jade.attr("data-timestamp", "" + (wallRecord.time) + "", true, false)) + " class=\"post-nav-b__item-link\">Comment</a></div></div></div><div" + (jade.attr("id", "comment-" + (wallRecord.entityId) + "", true, false)) + " class=\"b-feed-content__comment hide\"></div></div></div>");
}
else if ( (wallRecord.type == 'chat_up_line' || wallRecord.type == 'description'))
{
buf.push("<div class=\"b-feed__content b-feed-content b-join-to-site\"><div class=\"b-feed-content__profile-avatar\">");
if(users[wallRecord.user_id].statuses)
{
if(users[wallRecord.user_id].statuses.onlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status online\"></span>");
}
if(users[wallRecord.user_id].statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"b-feed-content__online-status\"></span>");
}
}
buf.push("<a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + "><img" + (jade.attr("data-src", '' + (users[wallRecord.user_id].photos.url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-feed-content__profile-avatar-item\"/></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a" + (jade.attr("href", '' + (userLink(users[wallRecord.user_id].id)) + '', true, false)) + " class=\"b-profile-feed-head__content-nick\">" + (jade.escape((jade_interp = users[wallRecord.user_id].login) == null ? '' : jade_interp)) + " </a>");
if ( wallRecord.type == 'chat_up_line')
{
buf.push("- has updated profile status");
}
else
{
buf.push("- has updated profile description");
}
buf.push("</div></div><div class=\"b-profile-feed-middle-content\"><div class=\"b-profile-short-info\"><ul class=\"b-profile-short-info__table\"><li class=\"b-profile-short-info__table-item\"><span class=\"b-profile-short-info__table-item-icon icon-Calendar\"></span>" + (jade.escape((jade_interp = users[wallRecord.user_id].age) == null ? '' : jade_interp)) + " years</li><li class=\"b-profile-short-info__table-item\"><span class=\"b-profile-short-info__table-item-icon icon-Location\"></span>");
var geo = [users[wallRecord.user_id].geo.city, users[wallRecord.user_id].geo.country]
buf.push("" + (jade.escape((jade_interp = geo.join(' ')) == null ? '' : jade_interp)) + "</li><li class=\"b-profile-short-info__table-item\"><span class=\"b-profile-short-info__table-item-icon icon-Photo\"></span>1 photos</li></ul></div><div class=\"b-profile-short-info\"><div class=\"b-profile-short-info__table-item\">" + (jade.escape(null == (jade_interp = wallRecord.description) ? "" : jade_interp)) + "</div></div></div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><ul class=\"b-profile-feed-footer__post-nav-b\"><li class=\"post-nav-b__item\"><a href=\"#\"" + (jade.attr("data-record-id", "" + (wallRecord.parentId) + "", true, false)) + (jade.attr("data-entity-id", "" + (wallRecord.entityId) + "", true, false)) + " data-object-type=\"2\"" + (jade.attr("data-timestamp", "" + (wallRecord.time) + "", true, false)) + " class=\"post-nav-b__item-link\">Comment</a></li><li class=\"post-nav-b__item\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (users[wallRecord.user_id].id) + "')", true, false)) + " class=\"post-nav-b__item-link\">Start Chat</a></li></ul></div></div><div" + (jade.attr("id", "comment-" + (wallRecord.entityId) + "", true, false)) + " class=\"b-feed-content__comment hide\"></div></div></div>");
}
else if ( wallRecord.type == 'livecam')
{
buf.push("<div class=\"b-feed__content b-feed-content b-liveshow-content\"><div class=\"b-feed-content__profile-avatar\"><a" + (jade.attr("href", '/#search/model/room/' + (wallRecord.id) + '/status/20', true, false)) + "><img" + (jade.attr("data-src", '' + (wallRecord.photo_url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-feed-content__profile-avatar-item\"/></a></div><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left\"><a" + (jade.attr("href", '/#search/model/room/' + (wallRecord.id) + '/status/20', true, false)) + " class=\"b-profile-feed-head__content-nick\">" + (jade.escape((jade_interp = wallRecord.screen_name) == null ? '' : jade_interp)) + " </a>- started liveshow</div></div><div class=\"b-profile-feed-middle-content\"><a" + (jade.attr("href", '/#search/model/room/' + (wallRecord.id) + '/status/20', true, false)) + "><img" + (jade.attr("data-src", '' + (wallRecord.photo_url) + '', true, false)) + " src=\"/assets/static/blank.gif\" class=\"b-profile-feed-middle-content__item\"/><div class=\"b-profile-feed-middle-content__btn\">Watch liveshow</div></a></div><div class=\"b-profile-feed-footer\"><div class=\"b-profile-feed-footer__post-time\">" + (jade.escape((jade_interp = helpers.timeAgo(wallRecord.time)) == null ? '' : jade_interp)) + "</div><div class=\"b-profile-feed-footer__post-nav\"><div class=\"b-profile-feed-footer__post-nav-b\"><a href=\"#\" class=\"post-nav-b__item-link\">Add to favorites</a></div></div></div></div></div>");
}
else if ( wallRecord.type == 'banner')
{
buf.push("<div class=\"b-feed__content b-feed-content b-banner-content\"><div class=\"b-wrap-content b-wrap-content--profile-feed\"><div class=\"b-profile-feed-middle-content\">");
if ( wallRecord.banner_type == 'html')
{
buf.push("" + (((jade_interp = wallRecord.html) == null ? '' : jade_interp)) + "");
}
else if ( wallRecord.banner_type == 'swf')
{
buf.push("<embed" + (jade.attr("width", "" + (wallRecord.width) + "", true, false)) + (jade.attr("height", "" + (wallRecord.height) + "", true, false)) + (jade.attr("src", "/banner/resource/id/" + (wallRecord.key) + "." + (wallRecord.banner_type) + "", true, false)) + " type=\"application/x-shockwave-flash\" bgcolor=\"#ffffff\" wmode=\"opaque\" allowscriptaccess=\"always\"/>");
}
else
{
buf.push("<a" + (jade.attr("href", "" + (wallRecord.url) + "", true, false)) + "><img" + (jade.attr("src", "/banner/resource/id/" + (wallRecord.key) + "." + (wallRecord.banner_type) + "", true, false)) + (jade.attr("alt", "" + (wallRecord.alt) + "", true, false)) + (jade.attr("width", "" + (wallRecord.width) + "", true, false)) + (jade.attr("height", "" + (wallRecord.height) + "", true, false)) + "/></a>");
}
buf.push("</div></div></div>");
}
    }

  }
}).call(this);

};return buf.join("");
};