this["JST"] = this["JST"] || {};

this["JST"]["WallHeader"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),activeFilter = locals_.activeFilter,showMore = locals_.showMore;
buf.push("<button style=\"display: none;\" class=\"to-top\">Top</button><div class=\"b-wrap-content b-filter\"><ul class=\"b-filter-content\"><li class=\"b-filter-content__item\"><a href=\"#\" wall-data-action=\"\"" + (jade.cls(['b-filter-content__item-link',activeFilter==false ? 'item-link--current' : ''], [null,true])) + ">All Updates</a></li><li class=\"b-filter-content__item\"><a href=\"#\" wall-data-action=\"VOD\"" + (jade.cls(['b-filter-content__item-link',activeFilter=="VOD" ? 'item-link--current' : ''], [null,true])) + ">Videos</a></li><li class=\"b-filter-content__item\"><a href=\"#\" wall-data-action=\"photo\"" + (jade.cls(['b-filter-content__item-link',activeFilter=="photo" ? 'item-link--current' : ''], [null,true])) + ">Photos</a></li><li class=\"b-filter-content__item\"><a href=\"#\" wall-data-action=\"livecam\"" + (jade.cls(['b-filter-content__item-link',activeFilter=="livecam" ? 'item-link--current' : ''], [null,true])) + ">Livecams</a></li></ul></div><div class=\"b-feed-wall\"></div>");
if ( showMore)
{
buf.push("<a id=\"wall-next-page\" class=\"button-b-btn btn\">Show more</a>");
}
else
{
buf.push("<section class=\"empty-category\"><span>There are no more events</span></section>");
};return buf.join("");
};