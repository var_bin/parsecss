this["JST"] = this["JST"] || {};

this["JST"]["PopupStopAction"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,dictionary = locals_.dictionary,cancelBillingPageBeforeRemove = locals_.cancelBillingPageBeforeRemove,stage = locals_.stage,error = locals_.error,password = locals_.password,paymentStatus = locals_.paymentStatus,siteInfo = locals_.siteInfo,notificationSettings = locals_.notificationSettings,notificationSubscription = locals_.notificationSubscription,profileStatus = locals_.profileStatus,reasons = locals_.reasons,viaPhoneCode = locals_.viaPhoneCode,email = locals_.email,hasBeenCancelled = locals_.hasBeenCancelled,userSubscriptionExpire = locals_.userSubscriptionExpire,viaCode = locals_.viaCode,code = locals_.code,hotLineCall = locals_.hotLineCall,publicId = locals_.publicId;
jade_mixins["popupTitle"] = function(title, replacement){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+"."+title, replacement)) ? "" : jade_interp)) + "</span>");
};
buf.push("<div id=\"remove-account-popup\" class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\"><div class=\"b-popup b-remove-profile cancel-billing\"><div class=\"meta-button-wrap\"><button id=\"close-popup\"" + (jade.attr("title", _t("popupStopAction", "button.close"), true, false)) + " class=\"btn-close btn-meta\"></button></div>");
if ( (dictionary == "remove-account" && cancelBillingPageBeforeRemove == true))
{
buf.push("<div class=\"b-popup-content cancel-billing\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.remove-account.cancel-repeat-billing")) ? "" : jade_interp)) + "</p></div></div></div></div>");
}
else if ( (typeof stage == "undefined" || stage == "checkPassword"))
{
buf.push("<div class=\"b-popup-content step-1\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\"><b>" + (null == (jade_interp = _t("popupStopAction", "title.stage") + " 1.&nbsp;") ? "" : jade_interp) + "</b>");
jade_mixins["popupTitle"]("check-password-stage-header");
buf.push("</h2><div class=\"remove-acc_pop_up__stage-description\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".check-password-main-text")) ? "" : jade_interp)) + "</div></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p id=\"password-error\">");
if ( error)
{
buf.push("<span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--error\"><span class=\"icon-status icon-Attention\"></span>" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</span>");
}
buf.push("</p><p id=\"password-message\" class=\"inactive\"><span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--save\"><span class=\"icon-status icon-Check\"></span><span id=\"password-message-text\"></span></span></p></div><div class=\"remove-acc_pop_up__grey-wrapper remove-acc_pop_up__grey-wrapper__password\"><div class=\"remove-acc_pop_up__remind-password\"><a id=\"remind-password\" href=\"#\" class=\"remove-acc_pop_up__remind-password__link\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.remind-password")) ? "" : jade_interp)) + "</a><span class=\"remove-acc_pop_up__remind-password__title\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.your-password")) ? "" : jade_interp)) + "</span><div class=\"remove-acc_pop_up__remind-password__textfield-wrap\"><input type=\"password\" id=\"password\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.password"), true, false)) + (jade.attr("value", password, true, false)) + " class=\"remove-acc_pop_up__textfield\"/></div></div></div></div><div class=\"remove-acc_pop_up__footer\"><button id=\"check-password\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div></div>");
}
else
{
if ((typeof stage != "undefined"))
{
switch (stage){
case "statusMembership":
buf.push("<div class=\"b-popup-content step-2\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\"><b>" + (null == (jade_interp = _t("popupStopAction", "title.stage") + " 2.&nbsp;") ? "" : jade_interp) + "</b>");
jade_mixins["popupTitle"]("status-membership-stage-header");
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__grey-wrapper\"><div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--big\"><div class=\"remove-acc_pop_up__data-row__title remove-acc_pop_up__data-row__title--big\">Membership Status:</div>");
if ( paymentStatus.type == "free")
{
buf.push("<a" + (jade.attr("title", _t("popupStopAction", "text.click-to-upgrade-your-account", {"{sitename}": siteInfo.siteName}), true, false)) + " href=\"#\" class=\"icon-Hint icon-Hint--remove-acc\"></a><a" + (jade.attr("href", paymentStatus.url, true, false)) + " class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right remove-acc_pop_up__button--small-font\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.get-full-membership")) ? "" : jade_interp)) + "</a><span class=\"remove-acc_pop_up__data-row__membership\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.free-membership")) ? "" : jade_interp)) + "</span>");
}
else if ( paymentStatus.type == "paid")
{
buf.push("<span class=\"remove-acc_pop_up__data-row__membership\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.full-membership")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div><div data-notification-settings=\"1\" class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--big\"><div class=\"remove-acc_pop_up__data-row__title remove-acc_pop_up__data-row__title--big\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.new_activity_notifications") + ":") ? "" : jade_interp)) + "</div><div class=\"activity-notifications-checks\">");
// iterate notificationSettings.settings
;(function(){
  var $$obj = notificationSettings.settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

buf.push("<div class=\"activity-notifications-checks__item\">");
// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div class=\"checkbox-wrap\"><input" + (jade.attr("id", name, true, false)) + " data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " type=\"checkbox\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", name, true, false)) + " class=\"checkbox-item radiobox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</div></label></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div class=\"checkbox-wrap\"><input" + (jade.attr("id", name, true, false)) + " data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " type=\"checkbox\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", name, true, false)) + " class=\"checkbox-item radiobox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</div></label></div>");
    }

  }
}).call(this);

buf.push("</div>");
    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

buf.push("<div class=\"activity-notifications-checks__item\">");
// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div class=\"checkbox-wrap\"><input" + (jade.attr("id", name, true, false)) + " data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " type=\"checkbox\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", name, true, false)) + " class=\"checkbox-item radiobox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</div></label></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div class=\"checkbox-wrap\"><input" + (jade.attr("id", name, true, false)) + " data-notificationSettings-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " type=\"checkbox\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", name, true, false)) + " class=\"checkbox-item radiobox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.new_activity_notifications_"+name)) ? "" : jade_interp)) + "</div></label></div>");
    }

  }
}).call(this);

buf.push("</div>");
    }

  }
}).call(this);

buf.push("</div></div></div><div data-notification-subscription=\"1\" class=\"remove-acc_pop_up__titled-group\"><div class=\"remove-acc_pop_up__titled-group__head\"><div class=\"remove-acc_pop_up__notifications\"><div class=\"remove-acc_pop_up__notifications__item\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification.email")) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__notifications__item\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification.sms")) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__notifications__item\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification.push")) ? "" : jade_interp)) + "</div></div><div class=\"remove-acc_pop_up__data-row__title remove-acc_pop_up__data-row__title--big\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_sms_notification") + ":") ? "" : jade_interp)) + "</div></div><div class=\"remove-acc_pop_up__grey-wrapper\">");
var i = 0;
// iterate notificationSubscription.settings
;(function(){
  var $$obj = notificationSubscription.settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

i++;
buf.push("<div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row remove-acc_pop_up__data-row--nopad\"><div class=\"remove-acc_pop_up__data-row__title remove-acc_pop_up__data-row__title\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification_"+name)) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__notifications\">");
// iterate ["email", "mobile", "push"]
;(function(){
  var $$obj = ["email", "mobile", "push"];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__notifications__item\"><div class=\"checkbox-wrap\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<input type=\"checkbox\"" + (jade.attr("name", name, true, false)) + (jade.attr("id", key+i, true, false)) + " data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " class=\"checkbox-item--hide\"/><label" + (jade.attr("for", key+i, true, false)) + " class=\"checkbox-item radiobox-item\"><span class=\"checkbox-item--show\"></span></label>");
}
buf.push("</div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__notifications__item\"><div class=\"checkbox-wrap\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<input type=\"checkbox\"" + (jade.attr("name", name, true, false)) + (jade.attr("id", key+i, true, false)) + " data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " class=\"checkbox-item--hide\"/><label" + (jade.attr("for", key+i, true, false)) + " class=\"checkbox-item radiobox-item\"><span class=\"checkbox-item--show\"></span></label>");
}
buf.push("</div></div>");
    }

  }
}).call(this);

buf.push("</div></div>");
    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

i++;
buf.push("<div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row remove-acc_pop_up__data-row--nopad\"><div class=\"remove-acc_pop_up__data-row__title remove-acc_pop_up__data-row__title\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.email_or_sms_notification_"+name)) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__notifications\">");
// iterate ["email", "mobile", "push"]
;(function(){
  var $$obj = ["email", "mobile", "push"];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__notifications__item\"><div class=\"checkbox-wrap\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<input type=\"checkbox\"" + (jade.attr("name", name, true, false)) + (jade.attr("id", key+i, true, false)) + " data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " class=\"checkbox-item--hide\"/><label" + (jade.attr("for", key+i, true, false)) + " class=\"checkbox-item radiobox-item\"><span class=\"checkbox-item--show\"></span></label>");
}
buf.push("</div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__notifications__item\"><div class=\"checkbox-wrap\">");
if ((typeof setting[key] !== "undefined"))
{
buf.push("<input type=\"checkbox\"" + (jade.attr("name", name, true, false)) + (jade.attr("id", key+i, true, false)) + " data-notificationSubscription-checkbox=\"1\"" + (jade.attr("data-name", name, true, false)) + (jade.attr("data-key", key, true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + " class=\"checkbox-item--hide\"/><label" + (jade.attr("for", key+i, true, false)) + " class=\"checkbox-item radiobox-item\"><span class=\"checkbox-item--show\"></span></label>");
}
buf.push("</div></div>");
    }

  }
}).call(this);

buf.push("</div></div>");
    }

  }
}).call(this);

buf.push("</div></div><div class=\"remove-acc_pop_up__titled-group\"><div class=\"remove-acc_pop_up__titled-group__head\"><div class=\"remove-acc_pop_up__data-row__title remove-acc_pop_up__data-row__title--big\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.choose-options-below") + ":") ? "" : jade_interp)) + "</div></div><div class=\"remove-acc_pop_up__stage-description\"><p id=\"error-container\">");
if ( error)
{
buf.push("<span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--error\"><span class=\"icon-status icon-Attention\"></span>" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</span>");
}
buf.push("</p></div><div id=\"status-membership-user-answer\" class=\"remove-acc_pop_up__grey-wrapper\"><div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input id=\"takeoff_mail\" type=\"radio\" name=\"name\" value=\"off-mailing\" class=\"checkbox-item--hide radiobox-item--hide\"/><label for=\"takeoff_mail\" class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-2")) ? "" : jade_interp)) + "</div></label></div></div><div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input id=\"hide_profile\" type=\"radio\" name=\"name\"" + (jade.attr("value", (profileStatus == "active") ? "hide" : "show", true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label for=\"hide_profile\" class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">");
if ( profileStatus == "active")
{
buf.push(jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-3-hide")) ? "" : jade_interp));
}
else
{
buf.push(jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-3-show")) ? "" : jade_interp));
}
buf.push("</div></label></div></div><div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input id=\"hide_take_off\" type=\"radio\" name=\"name\" value=\"hide-off\" class=\"checkbox-item--hide radiobox-item--hide\"/><label for=\"hide_take_off\" class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-4")) ? "" : jade_interp)) + "</div></label></div></div><div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input id=\"remove_profile\" type=\"radio\" name=\"name\"" + (jade.attr("value", dictionary, true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label for=\"remove_profile\" class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".status-membership-user-answer-option-1")) ? "" : jade_interp)) + "</div></label></div></div></div></div></div><div class=\"remove-acc_pop_up__footer\"><button id=\"close-popup\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--grey remove-acc_pop_up__button--left\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"status-membership\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div></div>");
  break;
case "userReason":
buf.push("<div class=\"b-popup-content step-3\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\"><b>" + (null == (jade_interp = _t("popupStopAction", "title.stage") + " 3.&nbsp;") ? "" : jade_interp) + "</b>");
jade_mixins["popupTitle"]("user-reason-stage-header");
buf.push("</h2><div class=\"remove-acc_pop_up__stage-description\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-main-text")) ? "" : jade_interp)) + "</div></div><div id=\"user-reason-answer\" class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p id=\"error-container\">");
if ( error)
{
buf.push("<span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--error\"><span class=\"icon-status icon-Attention\"></span>" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</span>");
}
buf.push("</p></div><div class=\"remove-acc_pop_up__grey-wrapper\">");
// iterate reasons
;(function(){
  var $$obj = reasons;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var reason = $$obj[$index];

buf.push("<div" + (jade.attr("data-has-sub", (reason.children.length > 0) ? "1" : "0", true, false)) + (jade.attr("data-reasone", reason.reasonId, true, false)) + " class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input" + (jade.attr("id", reason.reasonId, true, false)) + " type=\"radio\" name=\"name\"" + (jade.attr("value", reason.reasonId, true, false)) + (jade.attr("data-action", reason.action, true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label" + (jade.attr("for", reason.reasonId, true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+reason.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div></label></div>");
if ( reason.children.length > 0)
{
buf.push("<div data-sub=\"1\" class=\"inactive\">");
// iterate reason.children
;(function(){
  var $$obj = reason.children;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var child = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input" + (jade.attr("id", child.reasonId, true, false)) + " type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label" + (jade.attr("for", child.reasonId, true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title." + dictionary + ".user-reason-" + child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var child = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input" + (jade.attr("id", child.reasonId, true, false)) + " type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label" + (jade.attr("for", child.reasonId, true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title." + dictionary + ".user-reason-" + child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  }
}).call(this);

buf.push("</div>");
}
if ( reason.action == "other")
{
buf.push("<div data-sub=\"1\" data-additional=\"1\" class=\"remove-acc_pop_up__data-row--radio-list inactive\"><span class=\"remove-acc_pop_up__textfield__title\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.your-comment")) ? "" : jade_interp)) + "</span><textarea name=\"frm-messenger-text\" data-reasone-text=\"data-reasone-text\" data-resize=\"auto\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.enter-your-text-here"), true, false)) + " class=\"remove-acc_pop_up__textfield\"></textarea></div>");
}
buf.push("</div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var reason = $$obj[$index];

buf.push("<div" + (jade.attr("data-has-sub", (reason.children.length > 0) ? "1" : "0", true, false)) + (jade.attr("data-reasone", reason.reasonId, true, false)) + " class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input" + (jade.attr("id", reason.reasonId, true, false)) + " type=\"radio\" name=\"name\"" + (jade.attr("value", reason.reasonId, true, false)) + (jade.attr("data-action", reason.action, true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label" + (jade.attr("for", reason.reasonId, true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-reason-"+reason.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div></label></div>");
if ( reason.children.length > 0)
{
buf.push("<div data-sub=\"1\" class=\"inactive\">");
// iterate reason.children
;(function(){
  var $$obj = reason.children;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var child = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input" + (jade.attr("id", child.reasonId, true, false)) + " type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label" + (jade.attr("for", child.reasonId, true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title." + dictionary + ".user-reason-" + child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var child = $$obj[$index];

buf.push("<div class=\"remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input" + (jade.attr("id", child.reasonId, true, false)) + " type=\"radio\" name=\"name\"" + (jade.attr("value", child.reasonId, true, false)) + (jade.attr("data-action", child.action, true, false)) + " class=\"checkbox-item--hide radiobox-item--hide\"/><label" + (jade.attr("for", child.reasonId, true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title." + dictionary + ".user-reason-" + child.reasonText, {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  }
}).call(this);

buf.push("</div>");
}
if ( reason.action == "other")
{
buf.push("<div data-sub=\"1\" data-additional=\"1\" class=\"remove-acc_pop_up__data-row--radio-list inactive\"><span class=\"remove-acc_pop_up__textfield__title\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.your-comment")) ? "" : jade_interp)) + "</span><textarea name=\"frm-messenger-text\" data-reasone-text=\"data-reasone-text\" data-resize=\"auto\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.enter-your-text-here"), true, false)) + " class=\"remove-acc_pop_up__textfield\"></textarea></div>");
}
buf.push("</div>");
    }

  }
}).call(this);

buf.push("</div></div><div class=\"remove-acc_pop_up__footer\"><button id=\"close-popup\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--grey remove-acc_pop_up__button--left\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"user-reason\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div></div>");
  break;
case "userStory":
buf.push("<div class=\"b-popup-content step-story\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\">");
jade_mixins["popupTitle"].call({
block: function(){
buf.push("+ \"&nbsp;\"");
}
}, "user-story-stage-header");
buf.push("</h2><div class=\"remove-acc_pop_up__stage-description\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".user-story-main-text", {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p id=\"error-container\">");
if ( error)
{
buf.push("<span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--error\"><span class=\"icon-status icon-Attention\"></span>" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</span>");
}
buf.push("</p></div><div class=\"remove-acc_pop_up__grey-wrapper remove-acc_pop_up__grey-wrapper__password\"><div class=\"remove-acc_pop_up__remind-password\"><span class=\"remove-acc_pop_up__textfield__title\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.write-your-story")) ? "" : jade_interp)) + "</span><textarea name=\"frm-messenger-text\" id=\"user-story-text\" data-resize=\"auto\"" + (jade.attr("placeholder", _t("popupStopAction", "placeholder.enter-your-text-here"), true, false)) + " class=\"remove-acc_pop_up__textfield\"></textarea></div></div></div><div class=\"remove-acc_pop_up__footer\"><button id=\"close-popup\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--grey remove-acc_pop_up__button--left\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"user-story\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.save-and-continue")) ? "" : jade_interp)) + "</button></div></div></div>");
  break;
case "areYouSure":
buf.push("<div class=\"b-popup-content step-4\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\"><b>" + (null == (jade_interp = _t("popupStopAction", "title.stage") + " 4.&nbsp;") ? "" : jade_interp) + "</b>");
jade_mixins["popupTitle"]("are-you-sure-stage-header");
buf.push("</h2><div class=\"remove-acc_pop_up__stage-description\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-main-text")) ? "" : jade_interp)) + "</div></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up_divider\"></div><div class=\"remove-acc_pop_up__remove-aftermath\"><div class=\"remove-acc_pop_up__remove-aftermath__row\"><span class=\"icon-Check\"></span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-1")) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__remove-aftermath__row\"><span class=\"icon-Check\"></span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-2", {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__remove-aftermath__row\"><span class=\"icon-Check\"></span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-3")) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__remove-aftermath__row\"><span class=\"icon-Check\"></span>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-lost-point-4")) ? "" : jade_interp)) + "</div></div><div class=\"remove-acc_pop_up_divider\"></div><div class=\"remove-acc_pop_up__stage-description\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-main-text-part-3")) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__shure-stage-title\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-answer-option-header")) ? "" : jade_interp)) + "</div><div class=\"remove-acc_pop_up__stage-description\"><p id=\"error-container\">");
if ( error)
{
buf.push("<span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--error\"><span class=\"icon-status icon-Attention\"></span>" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</span>");
}
buf.push("</p></div><div id=\"are-you-sure-answer\" class=\"remove-acc_pop_up__grey-wrapper\"><div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input id=\"remove\" type=\"radio\" name=\"name\" value=\"yes\" class=\"checkbox-item--hide radiobox-item--hide\"/><label for=\"remove\" class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".are-you-sure-answer-option-1")) ? "" : jade_interp)) + "</div></label></div></div><div class=\"remove-acc_pop_up__data-row remove-acc_pop_up__data-row--radio-list\"><div class=\"checkbox-wrap\"><input id=\"hide\" type=\"radio\" name=\"name\" value=\"no\" class=\"checkbox-item--hide radiobox-item--hide\"/><label for=\"hide\" class=\"checkbox-item\"><span class=\"checkbox-item--show radiobox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t('popupStopAction', "title."+dictionary+".are-you-sure-answer-option-2")) ? "" : jade_interp)) + "</div></label></div></div></div></div><div class=\"remove-acc_pop_up__footer\"><button id=\"close-popup\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--grey remove-acc_pop_up__button--left\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"are-you-sure\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div></div>");
  break;
case "stop":
if ( (dictionary == "cancel-billing" && viaPhoneCode == true))
{
buf.push("<div class=\"b-popup-content via-phone-code\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\">");
jade_mixins["popupTitle"]("via-phone-code-header", {"{sitename}": siteInfo.siteName});
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopaction", "title.cancel-billing.via-phone-code-body-text-1")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopaction", "title.cancel-billing.via-phone-code-body-text-2", {"{email address}": email})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopaction", "title.cancel-billing.via-phone-code-body-text-3")) ? "" : jade_interp)) + "</p></div></div></div></div>");
}
else if ( (dictionary == "cancel-billing" && hasBeenCancelled == true))
{
buf.push("<div class=\"b-popup-content b-step has-been-cancelled\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\">");
jade_mixins["popupTitle"]("has-been-cancelled-header");
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.cancel-billing.has-been-cancelled-body-text-1", {"{mm/dd/yy}": userSubscriptionExpire})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title.cancel-billing.has-been-cancelled-body-text-2")) ? "" : jade_interp)) + "</p></div></div></div></div>");
}
else if ( ((dictionary == "cancel-billing" || dictionary == "remove-account") && viaCode == true && code == null))
{
buf.push("<div class=\"b-popup-content b-step via-code\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\">");
jade_mixins["popupTitle"]("via-code-header", {"{sitename}": siteInfo.siteName});
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".via-code-body-text-1")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".via-code-body-text-2", {"{email address}": email})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".via-code-body-text-3")) ? "" : jade_interp)) + "</p></div></div></div></div>");
}
else if ( (hotLineCall == true))
{
buf.push("<div class=\"b-popup-content step-5\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\"><b>" + (null == (jade_interp = _t("popupStopAction", "title.stage") + " 5.&nbsp;") ? "" : jade_interp) + "</b>");
jade_mixins["popupTitle"]("hot-line-call-header");
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><div class=\"unscribe-b-wrap\"><div class=\"unscribe-b-wrap__content\"><div class=\"unscribe-b-wrap__content-item\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-main-text-part-1")) ? "" : jade_interp)) + "</div>");
if ( siteInfo.contact)
{
buf.push("<div class=\"unscribe-b-wrap__content-item unscribe-b-wrap__content-item--id\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.local") + ": " + siteInfo.contact.localContactPhone) ? "" : jade_interp)) + "</div><div class=\"unscribe-b-wrap__content-item unscribe-b-wrap__content-item--id\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "text.intnl") + ": " + siteInfo.contact.intnlContactPhone) ? "" : jade_interp)) + "</div>");
}
else
{
buf.push("<div class=\"unscribe-b-wrap__content-item unscribe-b-wrap__content-item--id\">" + (jade.escape(null == (jade_interp = siteInfo.phone.code + " " + siteInfo.phone.phoneNumber) ? "" : jade_interp)) + "</div>");
}
buf.push("<div class=\"unscribe-b-wrap__content-item unscribe-b-wrap__content-item\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-main-text-part-3") + publicId) ? "" : jade_interp)) + "</div></div><div class=\"unscribe-b-wrap__content unscribe-b-wrap__content--center\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "or")) ? "" : jade_interp)) + "</div><div class=\"unscribe-b-wrap__content\"><div class=\"unscribe-b-wrap__content-item\">&nbsp;</div><div class=\"unscribe-b-wrap__content-item\"><a" + (jade.attr("href", "skype:" + siteInfo.phone.code + siteInfo.phone.phoneNumber, true, false)) + " class=\"unscribe-b-wrap__content-item-btn\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-confirm")) ? "" : jade_interp)) + "</a><div class=\"unscribe-note\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".hot-line-call-main-text-part-2")) ? "" : jade_interp)) + "</div></div></div></div></div></div><div class=\"remove-acc_pop_up__footer\"></div></div></div>");
}
else
{
buf.push("<div class=\"b-popup-content b-step step-5 active\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\"><b>" + (null == (jade_interp = _t("popupStopAction", "title.stage") + " 5.&nbsp;") ? "" : jade_interp) + "</b>");
jade_mixins["popupTitle"]("stop-stage-header");
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".stop-subscription-main-text-part-1")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".stop-subscription-main-text-part-2", {"{sitename}": siteInfo.siteName})) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".stop-subscription-custom-text-1")) ? "" : jade_interp)) + "</p></div></div><div class=\"remove-acc_pop_up__footer remove-acc_pop_up__footer--shure-stage\"><button id=\"close-popup\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--grey remove-acc_pop_up__button--left\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"confirm\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title." + dictionary + ".stop-subscription-confirm")) ? "" : jade_interp)) + "</button></div></div></div>");
}
  break;
case "emailSecurityCheck":
buf.push("<div class=\"b-popup-content email-verificate\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\">");
jade_mixins["popupTitle"]("email-security-check-header");
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".email-security-check-body-text-1-valid-email")) ? "" : jade_interp)) + "</p></div><div class=\"remove-acc_pop_up__stage-description\"><p id=\"email-error\">");
if ( error)
{
buf.push("<span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--error\"><span class=\"icon-status icon-Attention\"></span>" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</span>");
}
buf.push("</p></div><div class=\"remove-acc_pop_up__grey-wrapper remove-acc_pop_up__grey-wrapper__password\"><div class=\"remove-acc_pop_up__remind-password\"><div class=\"remove-acc_pop_up__remind-password__textfield-wrap\"><input type=\"text\" id=\"email\"" + (jade.attr("value", email, true, false)) + " class=\"remove-acc_pop_up__textfield\"/></div></div></div><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".email-security-check-body-text-2")) ? "" : jade_interp)) + "</p></div></div><div class=\"remove-acc_pop_up__footer\"><button id=\"close-popup\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--grey remove-acc_pop_up__button--left\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.cancel")) ? "" : jade_interp)) + "</button><button id=\"check-email\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div></div>");
  break;
case "enterCode":
buf.push("<div class=\"b-popup-content enter-code\"><div class=\"remove-acc_pop_up__container\"><div class=\"remove-acc_pop_up__head\"><h2 class=\"remove-acc_pop_up__title\">");
jade_mixins["popupTitle"]("enter-code-header");
buf.push("</h2></div><div class=\"remove-acc_pop_up__main-content\"><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".enter-code-body-text-1")) ? "" : jade_interp)) + "</p></div><div class=\"remove-acc_pop_up__stage-description\"><p id=\"code-error\">");
if ( error)
{
buf.push("<span class=\"remove-acc_pop_up__status remove-acc_pop_up__status--error\"><span class=\"icon-status icon-Attention\"></span>" + (jade.escape(null == (jade_interp = error ? _t("popupStopAction", error) : "") ? "" : jade_interp)) + "</span>");
}
buf.push("</p></div><div class=\"remove-acc_pop_up__grey-wrapper remove-acc_pop_up__grey-wrapper__password\"><div class=\"remove-acc_pop_up__remind-password\"><div class=\"remove-acc_pop_up__remind-password__textfield-wrap\"><input type=\"text\" id=\"code\" maxlength=\"8\" class=\"remove-acc_pop_up__textfield\"/></div></div></div><div class=\"remove-acc_pop_up__stage-description\"><p>" + (jade.escape(null == (jade_interp = _t("popupStopAction", "title."+dictionary+".enter-code-body-text-2", {"{email address}": email})) ? "" : jade_interp)) + "</p></div></div><div class=\"remove-acc_pop_up__footer\"><button id=\"check-code\" class=\"remove-acc_pop_up__button remove-acc_pop_up__button--red remove-acc_pop_up__button--right\">" + (jade.escape(null == (jade_interp = _t("popupStopAction", "button.continue")) ? "" : jade_interp)) + "</button></div></div></div>");
  break;
}
}
}
buf.push("</div></div></div></div>");;return buf.join("");
};