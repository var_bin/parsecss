this["JST"] = this["JST"] || {};

this["JST"]["NearbyFilter"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div class=\"b-feed__content b-feed__content--nearby b-nearby-content\"><div class=\"b-wrap-content\"><div class=\"b-nearby-content__title\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--nearby\"><span class=\"b-nearby-content__some-title\">Search, Browse, Find, and Get in Contact</span></div></div><div class=\"b-nearby-content__filter\"><form class=\"b-nearby-content__fiter-form\"><div class=\"fiter-form__form-field fiter-form__form-field-inline-b\"><span class=\"fiter-form__form-field-title\">Looking For</span><div class=\"form-field__input\"><input id=\"c4\" type=\"radio\" name=\"looking-for\" data-search-param='1' class=\"radiobox-item--hide\"/><label for=\"c4\" class=\"radiobox-item\"><span class=\"radiobox-item--show\"></span>Male</label></div><div class=\"form-field__input\"><div class=\"field-b__radiobox\"><input id=\"c5\" type=\"radio\" name=\"looking-for\" data-search-param='2' class=\"radiobox-item--hide\"/><label for=\"c5\" class=\"radiobox-item\"><span class=\"radiobox-item--show\"></span>Female</label></div></div></div><div class=\"fiter-form__form-field fiter-form__form-field-inline-b fiter-form__form-field-inline-b-forms\"><span class=\"fiter-form__form-field-title fixPosition\">Age</span><div class=\"form-field__input\"><div class=\"fiter-form__form-field-age-form\"><div id=\"searchAgeFrom\" class=\"custom-select\"><i class=\"icon-arrow-down custom-select__pointer\"></i><span class=\"custom-select__selected-item\">18</span><select id=\"SearchAgeFromSelect\">");
for(var i=18; i<=75; i++) {
{
buf.push("<option" + (jade.attr("value", i, true, false)) + ">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</option>");
}
}
buf.push("</select></div></div><span class=\"fiter-form__form-field-age-form-deli\">to</span><div class=\"fiter-form__form-field-age-form\"><div id=\"searchAgeTo\" class=\"custom-select\"><i class=\"icon-arrow-down custom-select__pointer\"></i><span class=\"custom-select__selected-item\">25</span><select id=\"SearchAgeToSelect\">");
for(var i=18; i<=75; i++) {
{
buf.push("<option" + (jade.attr("value", i, true, false)) + ">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</option>");
}
}
buf.push("</select></div></div></div></div><div class=\"fiter-form__form-field fiter-form__form-field-inline-b\"><span class=\"fiter-form__form-field-title\">Location</span><div class=\"form-field__input form-field__input--nearby-location\"><div class=\"relative\"><input id=\"location\" type=\"text\" name=\"location\" value=\"\" class=\"input-item input-item--icon-right\"/><div class=\"b-notificators__popup--search shadow\"><div class=\"popup--search-overflow\"></div><div class=\"b-notificators__popup-arrow--slide\"></div></div><div class=\"form-field__input-icon icon-Globus\"></div></div></div></div><div class=\"fiter-form__form-field fiter-form__form-field-inline-b fiter-form__form-field-inline-b-distance\"><span class=\"fiter-form__form-field-title\">Distance (mi)</span><div class=\"form-field__input form-field__input--radius\"><div id=\"slider\"></div></div><span id=\"radiusMi\" class=\"form-field__input--radius-count\"></span></div></form></div><div class=\"b-profile-feed-footer\"><a id=\"goSearch\" href=\"#\" class=\"b-profile-feed-footer-button\">Search</a></div><!-- -------------------------------------------End Form---------------------------------------------------------></div></div>");;return buf.join("");
};