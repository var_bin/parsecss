this["JST"] = this["JST"] || {};

this["JST"]["NearbyHeader"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),activeFiltersList = locals_.activeFiltersList,showMore = locals_.showMore;
var TYPE_ONLINE = 'online';
var TYPE_NEW = 'new_members';
var is_no_type = true,
is_TYPE_ONLINE = false,
is_TYPE_NEW = false;
if (activeFiltersList.indexOf(TYPE_ONLINE) != -1) {
is_no_type = false;
is_TYPE_ONLINE = true;
}
if (activeFiltersList.indexOf(TYPE_NEW) != -1) {
is_no_type = false;
is_TYPE_NEW = true;
}
buf.push("<button style=\"display: none;\" class=\"to-top\">Top</button><div class=\"b-wrap-content b-people\"><div class=\"b-people-head b-filter b-filter--nearby-people\"><div class=\"b-people-head-title title\">People</div><ul class=\"b-filter-content b-filter-content--nearby-people\"><li" + (jade.cls(['b-filter-content__item',is_no_type == true ? 'current' : null], [null,true])) + "><a href=\"#\" data-search-type=\"\"" + (jade.cls(['b-filter-content__item-link',is_no_type == true ? 'item-link--current' : null], [null,true])) + "><span class=\"b-filter-content__item-txt\">All people</span></a><div class=\"b-filter-content__item--active-triangle\"></div></li><li" + (jade.cls(['b-filter-content__item',is_TYPE_ONLINE == true ? 'current' : null], [null,true])) + "><a href=\"#\" data-search-type=\"online\"" + (jade.cls(['b-filter-content__item-link',is_TYPE_ONLINE == true ? 'item-link--current' : null], [null,true])) + "><span class=\"b-filter-content__item-txt\">Online Now</span></a><div class=\"b-filter-content__item--active-triangle\"></div></li><li" + (jade.cls(['b-filter-content__item',is_TYPE_NEW == true ? 'current' : null], [null,true])) + "><a href=\"#\" data-search-type=\"new_members\"" + (jade.cls(['b-filter-content__item-link',is_TYPE_NEW == true ? 'item-link--current' : null], [null,true])) + "><span class=\"b-filter-content__item-txt\">Have Updates</span></a><div class=\"b-filter-content__item--active-triangle\"></div></li></ul></div><div class=\"b-profile-feed-middle-content\"><div class=\"b-profile-peoples_grid\"></div></div></div>");
if ( showMore)
{
buf.push("<a id=\"people-nearby-next-page\" class=\"button-b-btn btn\">Show more</a>");
}
else
{
buf.push("<p>There are no more profiles</p>");
};return buf.join("");
};