this["JST"] = this["JST"] || {};

this["JST"]["NearbyContent"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),users = locals_.users,userLink = locals_.userLink;
if ( users)
{
// iterate users
;(function(){
  var $$obj = users;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var user = $$obj[$index];

buf.push("<div class=\"peoples_grid__field__item--big\"><div class=\"peoples_grid__field__item_ava\"><img" + (jade.attr("src", '' + (user.photos.url) + '', true, false)) + "/><!--.peoples_grid_photo_count//.peoples_grid_photo_count--item\n//  | " + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "\n//.peoples_grid_photo_count-txt\n//  | Photos--><div class=\"peoples_grid_username\"><span>" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + "</span></div><div class=\"peoples_grid--hover-menu\"><a" + (jade.attr("href", '' + (userLink(user.id)) + '', true, false)) + " class=\"peoples_grid--hover-menu--link\"></a><div class=\"peoples_grid-hover-menu-wrap\"><div class=\"peoples_grid-info\"><div class=\"peoples_grid-info__year\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + ", " + (jade.escape((jade_interp = user.age) == null ? '' : jade_interp)) + "</div><div class=\"peoples_grid-info__city\">" + (jade.escape((jade_interp = user.geo.city) == null ? '' : jade_interp)) + ", " + (jade.escape((jade_interp = user.geo.country) == null ? '' : jade_interp)) + "</div></div><div class=\"peoples_grid-nav-btn\"><a href=\"javascript:void(0)\" title=\"Wink\" class=\"peoples_grid-nav-btn__item\"><i" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.cls(['peoples_grid-nav-btn__item-icon','icon-Wink',(user.buttons.wink.activated) ? "activated" : ""], [null,null,true])) + "></i></a><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (user.id) + "')", true, false)) + " title=\"Start chat\" class=\"peoples_grid-nav-btn__item\"><i class=\"peoples_grid-nav-btn__item-icon icon-Chat_action\"></i></a><a href=\"javascript:void(0)\" title=\"Add To Favorites\" class=\"peoples_grid-nav-btn__item\"><i" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.cls(['peoples_grid-nav-btn__item-icon','icon-Favorite',(user.buttons.favorite.activated) ? "activated": ""], [null,null,true])) + "></i></a></div></div></div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var user = $$obj[$index];

buf.push("<div class=\"peoples_grid__field__item--big\"><div class=\"peoples_grid__field__item_ava\"><img" + (jade.attr("src", '' + (user.photos.url) + '', true, false)) + "/><!--.peoples_grid_photo_count//.peoples_grid_photo_count--item\n//  | " + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "\n//.peoples_grid_photo_count-txt\n//  | Photos--><div class=\"peoples_grid_username\"><span>" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + "</span></div><div class=\"peoples_grid--hover-menu\"><a" + (jade.attr("href", '' + (userLink(user.id)) + '', true, false)) + " class=\"peoples_grid--hover-menu--link\"></a><div class=\"peoples_grid-hover-menu-wrap\"><div class=\"peoples_grid-info\"><div class=\"peoples_grid-info__year\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + ", " + (jade.escape((jade_interp = user.age) == null ? '' : jade_interp)) + "</div><div class=\"peoples_grid-info__city\">" + (jade.escape((jade_interp = user.geo.city) == null ? '' : jade_interp)) + ", " + (jade.escape((jade_interp = user.geo.country) == null ? '' : jade_interp)) + "</div></div><div class=\"peoples_grid-nav-btn\"><a href=\"javascript:void(0)\" title=\"Wink\" class=\"peoples_grid-nav-btn__item\"><i" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.cls(['peoples_grid-nav-btn__item-icon','icon-Wink',(user.buttons.wink.activated) ? "activated" : ""], [null,null,true])) + "></i></a><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (user.id) + "')", true, false)) + " title=\"Start chat\" class=\"peoples_grid-nav-btn__item\"><i class=\"peoples_grid-nav-btn__item-icon icon-Chat_action\"></i></a><a href=\"javascript:void(0)\" title=\"Add To Favorites\" class=\"peoples_grid-nav-btn__item\"><i" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.cls(['peoples_grid-nav-btn__item-icon','icon-Favorite',(user.buttons.favorite.activated) ? "activated": ""], [null,null,true])) + "></i></a></div></div></div></div></div>");
    }

  }
}).call(this);

}
else
{
buf.push("Sorry, no matches were found according to your request");
};return buf.join("");
};