var NearbyModel = Backbone.Model.extend({

    urlRoot: "/peopleNearby",

    data: function() {
        return {
            "page" : this.get('page'),
            "activeFilter" : this.get('activeFilter'),
            "type" : this.get('type')
        }
    },

    defaults: function() {
        return {
            type : '',
            offset : 0,
            limit : 48,
            activeFilter: 0,
            page : 1,
            part: 15
        };
    }
});