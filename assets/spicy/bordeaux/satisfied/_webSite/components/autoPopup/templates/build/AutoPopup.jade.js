this["JST"] = this["JST"] || {};

this["JST"]["AutoPopup"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),popupType = locals_.popupType,data = locals_.data,_t = locals_._t;
buf.push("<!-- After Payment Message Pop Up--><div class=\"common_pop_up\">");
if ( popupType === "paySuccess")
{
buf.push("<div class=\"common_pop_up__container\"><div class=\"common_pop_up__header\"><h2 class=\"common_pop_up__title\">");
if ( data.features && (data.membership || data.offer || data.lifetimeOffer))
{
buf.push(null == (jade_interp = _t('autoPopup','text.pay-success-membership-and-features', {"{upgradepackage}":data.features, "{sitename}":data.siteName, "{br}":"<br/>"})) ? "" : jade_interp);
}
if ( data.features)
{
buf.push(null == (jade_interp = _t('autoPopup', 'text.pay-success-features-only',{"{upgradepackage}":data.features,"{sitename}":data.siteName,"{br}":"<br />"})) ? "" : jade_interp);
}
if ((data.membership || data.offer || data.lifetimeOffer))
{
buf.push(null == (jade_interp = _t('autoPopup', 'text.pay-success-membership-only',{"{sitename}":data.siteName,"{br}":"<br />"})) ? "" : jade_interp);
}
buf.push((null == (jade_interp = _t("autoPopup", "text.pay_success_information", {"{br}":"<br>"})) ? "" : jade_interp) + "</h2></div><div class=\"pop_up__close\"><i data-close=\"data-close\"" + (jade.attr("title", _t("autoPopup", "text.close"), true, false)) + " class=\"icon-cross\"></i></div><div class=\"common_pop_up__content\"><p>" + (jade.escape(null == (jade_interp = _t("autoPopup", "text.pay-success-description")) ? "" : jade_interp)) + "</p><div class=\"centered-wrap\"><a data-close=\"data-close\" href=\"#\" class=\"button-b-btn btn\">" + (jade.escape(null == (jade_interp = _t("autoPopup", "text.continue")) ? "" : jade_interp)) + "</a></div></div></div>");
}
buf.push("</div><!-- [END] After Payment Message Pop Up-->");;return buf.join("");
};