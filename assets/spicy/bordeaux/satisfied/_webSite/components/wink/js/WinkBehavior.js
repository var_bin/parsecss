$(document).on("click.winkClick", "[data-winkbutton]", function(eventObject) {
    var $elem = $(eventObject.currentTarget);
    if(!$elem.hasClass("loading") && !$elem.hasClass("activated")) {
        var curUserId = $elem.attr("data-winkbutton");

        $("[data-winkbutton='"+curUserId+"']").addClass("loading");

        var model = new WinkModel({"userId":curUserId});

        model.doWinkAction({
            "error":   function(){
                $("[data-winkbutton='"+curUserId+"']").removeClass("loading");
            },
            "success": function(){
                $("[data-winkbutton='"+curUserId+"']").addClass("activated").removeClass("loading");
                Backbone.trigger("TalksRecipient.Wink", curUserId);
            }
        });
    }
});