var UserView = Backbone.View.extend({
    regions: {
        profile: "[general-info-container]",
        profilePhotoList: "[photos-container]",
        profileVideoList: "[videos-container]",
        popup: "div#layoutPopup"
    },
    tabs: {
        general: {
            link: "a#general-info",
            block: "div.general_info_block"
        },
        photos: {
            link: "a#photos",
            block: "div.photos_block"
        },
        video: { 
            link: "a#video",
            block: "div.videos_block"
        }
    },
    userId: null,
    openChat: null,
    events: {
        "click a#general-info": function() {
            this.switchTab('general');
        },
        "click a#photos": function() {
            this.switchTab('photos');
        },
        "click a#video": function() {
            this.switchTab('video');
        },

        "click [data-chat]": function() {
            var model = this.model.toJSON();
            this.permission.message(model.user.id);
        }
    },
    switchTab: function(tab) {
        $(this.tabs[tab].link).addClass('active');
        $(this.tabs[tab].block).show();
        for (var i in this.tabs) {
            if (i != tab) {
                $(this.tabs[i].link).removeClass('active');
                $(this.tabs[i].block).hide();
            }
        }
        
    },
    initialize: function(options) {
        this.userId = options.params.id;
        this.openChat = options.params.openChat;
        this.model.getUser(this.userId);
        this.listenTo(this.model, 'sync', this.renderUser);

        if (this.openChat === '1') {
            talksCall(this.userId);
        }

        this.permission = new PermissionModel();
        this.listenTo(this.permission, "getPermission", this.permissionAction);
    },
    
    renderUser: function () {
        this.$el.html(tpl.render("User", this.model.toJSON()));
        
        this.renderPhotos();
        
        this.region.ready();

        return this;
    },
    
    renderPhotos: function() {
        var self = this;
        self.regions.profilePhotoList.render(UserPhotoListView, {
            collection: new UserPhotoListCollection(_.values(self.model.toJSON().userPhotos || {}), {}),
            userId: self.model.toJSON().user && self.model.toJSON().user.id,
            params: {access: self.model.toJSON().photosAccessFreeUser, upgrade: self.model.toJSON().upgrade}
        });
    }
});
