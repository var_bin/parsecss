this["JST"] = this["JST"] || {};

this["JST"]["Profile"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;

buf.push("<div data-block=\"profile-edit\"></div><div class=\"description_block_all\"><div class=\"description_block_all-row\"><div class=\"description_block_all-row__field\"><div class=\"description_block_all-row__field__item\"><div data-block=\"general\" class=\"b-wrap-content b-feed-content__profile-feed\"></div></div></div><div class=\"description_block_all-row--separator\"></div><div class=\"description_block_all-row__field\"><div class=\"description_block_all-row__field__item\"><div data-block=\"appearance\" class=\"b-wrap-content b-feed-content__profile-feed\"></div></div></div></div></div><div class=\"description_block_all-row\"><div class=\"description_block_all-row__field\"><div class=\"description_block_all-row__field__item\"><div data-block=\"background\" class=\"b-wrap-content b-feed-content__profile-feed\"></div></div></div><div class=\"description_block_all-row--separator\"></div><div class=\"description_block_all-row__field\"><div class=\"description_block_all-row__field__item\"><div data-block=\"lookingfor\" class=\"b-wrap-content b-feed-content__profile-feed\"></div></div></div></div>");;return buf.join("");
};