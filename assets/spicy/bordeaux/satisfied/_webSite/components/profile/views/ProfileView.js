var ProfileView = Backbone.View.extend({

    regions: {
        profilePhoto: '#profilePhoto',
        profilePhotoUploader: '#profilePhotoUploader',
        profilePhotoList: '#profilePhotoList',
        profileEditor: '[data-block="profile-edit"]',
        profileGeneral: '[data-block="general"]',
        profileAppearance: '[data-block="appearance"]',
        profileBackground: '[data-block="background"]',
        profileLookingFor: '[data-block="lookingfor"]',
        profileDetails: '#profileDetails'
    },

    events: {
        "keypress .input-text-attr": function(event) {
            $(event.target).parents('.b-wrap-content').find('.action-status-b').show();
        }
    },

    initialize: function() {
        this.listenTo(this.model, 'change', this.renderProfile);

        this.renderProfile();
    },

    renderProfile: function() {
        var self = this,
            modelData = this.model.toJSON(),
            primaryPhoto = (modelData.myPhotos && modelData.myPhotos.primary) ? modelData.myPhotos.primary : {},
            profile = modelData.profile,
            useFbPhotos = !!profile.networks,
            preloadedLocationList = modelData.userAttributesDictionaries.locations,
            dateFormat = modelData.userAttributesDictionaries.dateFormat,
            fieldsData = modelData.userAttributesDictionaries.fields;

        this.$el.html(tpl.render('Profile', modelData));

        _.defer(function() {
            self.regions.profileEditor.render(ProfileEditorView, {
                model: new ProfileEditorModel(_.extend(profile, {
                    preloadedLocationList: preloadedLocationList,
                    dateFormat: dateFormat,
                    fieldsData: fieldsData
                }))
            });
        });

        _.defer(function() {
            self.regions.profileGeneral.render(ProfileDetailsView, {
                blockId: 'general',
                model: new ProfileDetailsModel(_.extend(profile, {
                    fieldsData: fieldsData,
                }))
            });
        });

        _.defer(function() {
            self.regions.profileAppearance.render(ProfileDetailsView, {
                blockId: 'appearance',
                model: new ProfileDetailsModel(_.extend(profile, {
                    fieldsData: fieldsData,
                }))
            });
        });

        _.defer(function() {
            self.regions.profileBackground.render(ProfileDetailsView, {
                blockId: 'background',
                model: new ProfileDetailsModel(_.extend(profile, {
                    fieldsData: fieldsData,
                }))
            });
        });

        _.defer(function() {
            self.regions.profileLookingFor.render(ProfileLookingForView, {
                model: new ProfileLookingForModel(_.extend(profile, {
                    fieldsData: fieldsData
                }))
            });
        });

        return this;
    }
});
