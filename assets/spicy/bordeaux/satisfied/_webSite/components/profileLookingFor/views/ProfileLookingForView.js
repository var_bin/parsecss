var ProfileLookingForView = Backbone.View.extend({

    timerSettings: {
        timers: [],
        duration: 2000
    },

    events: {
        'click [data-btn="reset"]': 'resetForm',
        'click [data-btn="save"]': 'saveFormData',
        'change [data-control]': 'showButtons'
    },

    coregOptions: {
        'id': "edit-tastes",
        'container' : "#coregPlacementLookingFor"
    },

    fields: {},

    getFormParams: function() {
        return {
            values: {
                lookingFor: {
                    gender: this.model.getAvailableValues('gender'),
                    age: this.model.getAvailableLookingForAges(),
                    distance: this.model.getAvailableLookingForDistances(),
                    photoLevel: this.model.getAvailableValues('photoLevel'),
                    race: this.model.getAvailableLookingForRaces(),
                    religion: this.model.getAvailableLookingForReligions()
                }
            },
            lookingForItems: (this.model.getAvailableValues('photoLevel').length === 0) ? ['race', 'religion'] : ['photoLevel', 'race', 'religion']
        };
    },

    showButtons: function() {
        this.$el.find('.action-status-b').removeClass('inactive');
    },

    hideButtons: function() {
        this.$el.find('.action-status-b').addClass('inactive');
    },

    render: function() {
        this.$el.html(tpl.render('ProfileLookingFor', {
            profile: this.model.attributes,
            formParams: this.getFormParams()
        }));

        this.fields = {};
        var self = this;

        this.$('[data-ui-select]').each(function() {
            var fieldName = $(this).find('input[type=hidden]').attr('name');
            self.fields[fieldName] = $(this).uiSelect('init', {'silent': true});
        });
        this.$('[data-ui-location]').each(function() {
            var fieldName = $(this).find('input[type=text]').attr('name');
            self.fields[fieldName] = $(this).uiLocation();
        });
        Backbone.trigger('Coregistration.showNext', this.coregOptions);

        this.region.ready();

        return this;
    },

    resetForm: function() {
        this.model.set(this.model.previousAttributes(), {silent: true});
        this.render();
    },

    saveFormData: function() {
        this.region.loading();

        this.model.needValidateAttr = true;

        var self = this,
            updateData = {},
            uiField = null;
        for (var curFieldName in self.fields) {
            if (self.fields.hasOwnProperty(curFieldName)) {
                uiField = self.fields[curFieldName].getInstance();
                updateData[curFieldName] = uiField.getVal();
            }
        }

        this.model.once('invalid', function(model, error) {
            var errorsList = [];
            for (var curFieldName in self.fields) {
                if (curFieldName in error) {
                    if (_.isArray(error[curFieldName])) {
                        errorsList = error[curFieldName];
                    } else if (_.isString(error[curFieldName])) {
                        errorsList.push(error[curFieldName]);
                    }
                    self.fields[curFieldName].getInstance().markAsError(errorsList);
                } else {
                    self.fields[curFieldName].getInstance().markAsNotError();
                }
            }
            self.region.ready();
        });

        if (!$.isEmptyObject(app.csrfToken)) {
            updateData[app.csrfToken.name] = app.csrfToken.value;
        }

        this.model.save(updateData, {
            type: 'POST',
            data: updateData,
            success: function(model, resp) {
                for (var curFieldName in self.fields) {
                    if (self.fields.hasOwnProperty(curFieldName)) {
                        self.fields[curFieldName].getInstance().markAsNotError();
                    }
                }
                self.hideButtons();
                self.region.ready();
            },
            error: function(model, response) {
                var curFieldName;
                if (typeof response === 'object') {
                    for (curFieldName in self.fields) {
                        if (curFieldName in response) {
                            self.fields[curFieldName].getInstance().markAsError(response[curFieldName]);
                        } else {
                            self.fields[curFieldName].getInstance().markAsNotError();
                        }
                    }
                } else {
                    for (curFieldName in self.fields) {
                        if (self.fields.hasOwnProperty(curFieldName)) {
                            self.fields[curFieldName].getInstance().markAsError();
                        }
                    }
                }
                self.region.ready();
            }
        });
        Backbone.trigger('Coregistration.coregProfileIfChecked', this.coregOptions);
        Backbone.trigger('Coregistration.remove', this.coregOptions);
        Backbone.trigger('Coregistration.showNext', this.coregOptions);
    }
});
