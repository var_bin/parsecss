this["JST"] = this["JST"] || {};

this["JST"]["ProfileLookingFor"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,formParams = locals_.formParams,profile = locals_.profile,lookingForDistance = locals_.lookingForDistance;
buf.push("<div class=\"b-profile-feed-head description-head\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--1em head_table__div_table-cell\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.looking_for")) ? "" : jade_interp)) + "<span data-message=\"result\" class=\"changes-status changes-status--save inactive\"><span class=\"icon-status icon-Check\"></span><span data-message-text=\"result\"></span></span><span data-message=\"error\" class=\"changes-status changes-status--error inactive\"><span class=\"icon-status icon-Attention\"></span><span data-message-text=\"error\"></span></span></div><div class=\"rightside-content-b-header-right head_table__div_table-cell\"></div></div><div class=\"description_block--content\"><table class=\"description_block--content__table\"><tbody><tr data-ui-select=\"data-ui-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.gender")) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = formParams.values.lookingFor.gender[profile.lookingFor.gender]) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select right\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.gender, true, false)) + " name=\"lookingForGender\" data-control=\"data-control\" id=\"lookingForGender\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.values.lookingFor.gender
;(function(){
  var $$obj = formParams.values.lookingFor.gender;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var gender = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = gender) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var gender = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = gender) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div></td></tr><tr class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.age")) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><span>From</span><div data-ui-select=\"data-ui-select\" data-select-max=\"searchFormAgeFrom\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = profile.lookingFor.age.from) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.age.from, true, false)) + " name=\"lookingForAge[from]\" data-control=\"data-control\" id=\"lookingForAgeFrom\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
for(var i = formParams.values.lookingFor.age.min; i <= formParams.values.lookingFor.age.max; i++)
{
buf.push("<a" + (jade.attr("data-item", i, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div><span>&nbsp;to</span><div data-ui-select=\"data-ui-select\" data-select-max=\"searchFormAgeTo\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = profile.lookingFor.age.to) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.age.to, true, false)) + " name=\"lookingForAge[to]\" data-control=\"data-control\" id=\"lookingForAgeTo\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
for(var i = formParams.values.lookingFor.age.min; i <= formParams.values.lookingFor.age.max; i++)
{
buf.push("<a" + (jade.attr("data-item", i, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div></td></tr><tr data-ui-select=\"data-ui-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.search_radius")) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = formParams.values.lookingFor.distance[profile.lookingFor.distance]) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select right\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.distance, true, false)) + (jade.attr("name", lookingForDistance, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", lookingForDistance, true, false)) + "/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.values.lookingFor.distance
;(function(){
  var $$obj = formParams.values.lookingFor.distance;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var distance = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = distance) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var distance = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = distance) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div></td></tr><tr data-ui-select=\"data-ui-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">Type of photo</td><td class=\"description_block--content__table__value\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = formParams.values.lookingFor.photoLevel[(profile.lookingFor.photoLevel.length > 0 ? profile.lookingFor.photoLevel[0]: 0)]) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select right\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", (profile.lookingFor.photoLevel.length > 0 ? profile.lookingFor.photoLevel[0]: 0), true, false)) + " name=\"lookingForPhotoLevel[]\" data-control=\"data-control\" id=\"lookingForPhotoLevel\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.values.lookingFor.photoLevel
;(function(){
  var $$obj = formParams.values.lookingFor.photoLevel;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var level = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = level) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var level = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = level) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div></td></tr><tr data-ui-select=\"data-ui-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.looking_for_race")) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = formParams.values.lookingFor.race[(profile.lookingFor.race.length > 0 ? profile.lookingFor.race[0]: 0)]) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select right\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", (profile.lookingFor.race.length > 0 ? profile.lookingFor.race[0] : 0), true, false)) + " name=\"lookingForRace[]\" data-control=\"data-control\" id=\"lookingForRace\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.values.lookingFor.race
;(function(){
  var $$obj = formParams.values.lookingFor.race;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var race = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = race) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var race = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = race) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div></td></tr><tr data-ui-select=\"data-ui-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.looking_for_religion")) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = formParams.values.lookingFor.religion[(profile.lookingFor.religion.length > 0 ? profile.lookingFor.religion[0] : 0)]) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select right\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", (profile.lookingFor.religion.length > 0 ? profile.lookingFor.religion[0] : 0), true, false)) + " name=\"lookingForReligion[]\" data-control=\"data-control\" id=\"lookingForReligion\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.values.lookingFor.religion
;(function(){
  var $$obj = formParams.values.lookingFor.religion;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var religion = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = religion) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var religion = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = religion) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div></td></tr></tbody></table></div><div class=\"b-profile-feed-footer search-box-b--footer\"><div class=\"b-profile-feed-footer__add b-profile-feed-footer__add--status\"><div class=\"action-status-b inactive\"><a data-btn=\"reset\" class=\"b-profile-feed-footer__action-btn-status\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "button.cancel")) ? "" : jade_interp)) + "</a><a data-btn=\"save\" class=\"b-profile-feed-footer__action-btn-status save\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "button.save")) ? "" : jade_interp)) + "</a></div></div></div>");;return buf.join("");
};