var appUi = {
    initElements: function($el) {
        if(!$el || !$el instanceof $) {
            $el = $(document);
        }

        $el.find("[data-ui-select]").uiSelect();
        $el.find("[data-ui-location]").uiLocation();
        $el.find("[data-ui-textarea]").uiTextarea();
        $el.find("[data-ui-input]").uiInput();
    }
};

var UiElement = (function() {
    var construct = function($el, staticProps) {
        this.$el = $el;
        _.extend(this, staticProps || {});
    };

    _.extend(construct.prototype, {
        behavior: function(method, params) {
            if(typeof method !== "string") {
                params = method;
                method = "init";
            }

            params = params || {};

            if(params.event)
                this.$el = $(params.event.target);

            if(this[method])
                return this[method](params);
            else
                throw new Error("There is no '"+method+"' method in UiElement");
        }
    });

    return construct;
}());

var UiFunction = function(obj) {
    return function(method, params, returnFirstResult) {
        var results = [], elements;

        returnFirstResult = returnFirstResult || false;

        elements = this.each(function() {
            var $this = $(this),
                instance = $this.getInstance();

            if(!instance) {
                instance = new UiElement($this, obj);
                $this.data("ui-data", instance);
                if(method !== "init") {
                    instance.behavior("init");
                }
            }

            results.push(instance.behavior(method, params));
        });

        if(returnFirstResult)
            return results[0];

        return elements;
    };
};

(function() {
    $.fn.extend({
        getInstance: function() {
            var $this = $(this),
                data = $this.data("ui-data");

            return data || null;
        }
    });
}());

(function() {
    var Select = {
        directive: "[data-ui-select]",

        init: function(params) {
            var $parent = this.$el.closest(this.directive);

            _.extend(this, {
                $parent: $parent,
                $control: $parent.find("[data-control]"),
                $value: $parent.find("[data-value]"),
                $items: $parent.find("[data-item]"),
                $itemsList: $parent.find("[data-items-list]"),
                $errors: $parent.find("[data-ui-error-text]")
            });

            this.initDefaultItem(params);
        },

        getVal: function() {
            return this.$control.val();
        },

        markAsError: function(errorsList) {
            errorsList = errorsList || [];
            this.$parent.addClass("error");

            var errors = "";
            _.each(
                errorsList,
                function(value) {
                    if(!_.isEmpty(errors) ) {
                        errors += "<br>";
                    }
                    errors += value;
                }
            );
            this.$errors.html(errors);
        },

        markAsNotError: function() {
            this.$parent.removeClass("error");
            this.$errors.html("");
        },

        initDefaultItem: function(params) {
            var value = this.$control.val();
            this.setVal(value, params);
        },

        toggle: function() {
            var isActive = this.$parent.hasClass("active");

            clearMenus.call(this.$parent);

            if(!isActive)
                this.$parent.addClass("active");

            if (!_.isEmpty(this.$itemsList) && this.$itemsList.height()) {
                var controlOffset = this.$el.offset();
                var lowPosition = controlOffset.top - $(document).scrollTop() + this.$itemsList.height() + this.$el.height()*2;
                if (lowPosition > $(window).height()) {
                    this.$parent.addClass("top");
                }
            }

            return false;
        },

        item: function(params) {
            this.$items.removeClass("active");
            this.$el.addClass("active");
            this.$value.html(this.$el.html());
            this.$control.val(this.$el.data("item"));
            if (!params.silent) {
                this.$control.trigger("change");
            }
        },

        setLimits: function() {
            var id = this.$el.attr("id"),
                value = this.$el.val(),
                $elements = [];
            if(id)
                $elements = $("[data-select-min='"+id+"'], [data-select-max='"+id+"']");

            if($elements.length)
                $elements.uiSelect("setLimitsValue", value);

            return false;
        },

        setLimitsValue: function(value) {
            if(this.$parent.attr("data-select-min")) {
                this.$items.show().filter(function() {
                    return $(this).attr("data-item") < value;
                }).hide();
            }

            if(this.$parent.attr("data-select-max")) {
                this.$items.show().filter(function() {
                    return $(this).attr("data-item") > value;
                }).hide();
            }
        },

        setVal: function(value, params) {
            this.$el = this.$items.filter("[data-item='"+value+"']:eq(0)");

            if(!this.$el.length)
                this.$el = this.$items.first();

            this.item(params);
        },

        hideOption: function(value) {
            this.$items.filter("[data-item='"+value+"']").hide();
        },

        showOption: function(value) {
            this.$items.filter("[data-item='"+value+"']").show();
        }
    };

    $.fn.extend({
        uiSelect: UiFunction(Select)
    });

    function clearMenus() {
        $(Select.directive).removeClass("active top");
    }

    function handler(event) {
        var params = event.data || {},
            $this = $(this),
            $parent = $this.closest(Select.directive);

        return $parent.uiSelect(params.method, { event: event }, true);
    }

    $(document).on("click.ui.select", clearMenus)
        .on("click.ui.select", Select.directive + " [data-toggle]", { method: "toggle" }, handler)
        .on("click.ui.select", Select.directive + " [data-item]", { method: "item" }, handler)
        .on("change.ui.select", Select.directive + " [data-control]", { method: "setLimits" }, handler);
}());

(function() {
    var DateSelect = {
        directive: "[data-ui-date-select]",

        init: function() {
            var $parent = this.$el.closest(this.directive);

            _.extend(this, {
                $parent: $parent,
                $control: $parent.find("[data-date-select-value]"),
                $errors: $parent.find("[data-ui-error-text]")
            });

            this.dateElements = {};
            this.dateElements.day = $parent.find("[data-date-select-day]").uiSelect({silent: true});
            this.dateElements.month = $parent.find("[data-date-select-month]").uiSelect({silent: true});
            this.dateElements.year = $parent.find("[data-date-select-year]").uiSelect({silent: true});

            var self = this;
            this.dateElements.month.getInstance().$control.on('change', function() {
                self.filterDayListByMonth();
            });

            this.initDefaultItem();
        },

        getVal: function() {
            var date = {
                year:  this.dateElements.year.getInstance().getVal(),
                month: this.dateElements.month.getInstance().getVal(),
                day:   this.dateElements.day.getInstance().getVal()
            };
            if (parseInt(date.day) < 10) {
                date.day = "0"+parseInt(date.day);
            }
            if (parseInt(date.month) < 10) {
                date.month = "0"+parseInt(date.month);
            }

            var dateValue = date.year+"-"+date.month+"-"+date.day;
            this.$control.val(dateValue);
            return dateValue;
        },

        markAsError: function(errorsList) {
            errorsList = errorsList || [];
            this.$parent.addClass("error");

            var errors = "";
            _.each(
                errorsList,
                function(value) {
                    if(!_.isEmpty(errors) ) {
                        errors += "<br>";
                    }
                    errors += value;
                }
            );
            this.$errors.html(errors);
        },

        markAsNotError: function() {
            this.$parent.removeClass("error");
            this.$errors.html("");
        },

        initDefaultItem: function() {
            var value = this.getVal();
            this.setVal(value);
        },

        setVal: function(value) {
        },

        filterDayListByMonth: function() {
            var days = {"1": 31, "2": 29, "3": 31, "4": 30, "5": 31, "6": 30, "7": 31, "8": 31, "9": 30, "10": 31, "11": 30, "12": 31};
            var daysCount = days[this.dateElements.month.getInstance().getVal()];
            if (this.dateElements.day.getInstance().getVal() > daysCount) {
                this.dateElements.day.getInstance().setVal(daysCount);
            }
            for (var i = 1; i <= 31; i++) {
                if (i <= daysCount) {
                    this.dateElements.day.getInstance().showOption(i);
                } else {
                    this.dateElements.day.getInstance().hideOption(i);
                }
            }
        }
    };

    $.fn.extend({
        uiDateSelect: UiFunction(DateSelect)
    });

    function clearMenus() {
        $(DateSelect.directive).removeClass("active");
    }

    function handler(event) {
        var params = event.data || {},
            $this = $(this),
            $parent = $this.closest(DateSelect.directive);

        return $parent.uiDateSelect(params.method, { event: event }, true);
    }
}());

(function() {
    var Textarea = {
        directive: "[data-ui-textarea]",

        init: function() {
            var $parent = this.$el.closest(this.directive);

            _.extend(this, {
                $parent: $parent,
                $control: $parent.find('[data-control]'),
                $errors: $parent.find("[data-ui-error-text]")
            });
        },

        getVal: function() {
            return this.$control.val();
        },

        markAsError: function(errorsList) {
            errorsList = errorsList || [];
            this.$parent.addClass("error");

            var errors = "";
            _.each(
                errorsList,
                function(value) {
                    if(!_.isEmpty(errors) ) {
                        errors += "<br>";
                    }
                    errors += value;
                }
            );
            this.$errors.html(errors);
        },

        markAsNotError: function() {
            this.$parent.removeClass("error");
            this.$errors.html("");
        },

        setVal: function(value) {
            this.$control.val(value).trigger("change");
        }
    };

    $.fn.extend({
        uiTextarea: UiFunction(Textarea)
    });

    function handler(event) {
        var params = event.data || {},
            $this = $(this),
            $parent = $this.closest(Textarea.directive);

        return $parent.uiTextarea(params.method, { event: event }, true);
    }
}());

(function() {
    var UiInput = {
        directive: "[data-ui-input]",

        init: function() {
            var $parent = this.$el.closest(this.directive);

            _.extend(this, {
                $parent: $parent,
                $control: $parent.find('[data-control]'),
                $errors: $parent.find("[data-ui-error-text]")
            });
        },

        getVal: function() {
            return this.$control.val();
        },

        markAsError: function(errorsList) {
            errorsList = errorsList || [];
            this.$parent.addClass("error");

            var errors = "";
            _.each(
                errorsList,
                function(value) {
                    if(!_.isEmpty(errors) ) {
                        errors += "<br>";
                    }
                    errors += value;
                }
            );
            this.$errors.html(errors);
        },

        markAsNotError: function() {
            this.$parent.removeClass("error");
            this.$errors.html("");
        },

        setVal: function(value) {
            this.$control.val(value).trigger("change");
        }
    };

    $.fn.extend({
        uiInput: UiFunction(UiInput)
    });

    function handler(event) {
        var params = event.data || {},
            $this = $(this),
            $parent = $this.closest(UiInput.directive);

        return $parent.uiInput(params.method, { event: event }, true);
    }
}());

(function() {
    var SuggestLocation = {
        timeout: null,

        directive: "[data-ui-location]",

        model: null,

        anyCityItem: true,

        init: function(params) {
            this.model = new SuggestLocationModel();
            if (params.locations) {
                this.model.set({
                    locations: params.locations
                });
            }
            if (typeof params.anyCityItem !== "undefined") {
                this.anyCityItem = params.anyCityItem;
            }
            var $parent = this.$el.closest(this.directive);

            this.model.on("change", this.renderSuggestions, this);

            _.extend(this, {
                $parent: $parent,
                $control: $parent.find("[data-control]"),
                $list: $parent.find("[data-list]"),
                $errors: $parent.find("[data-ui-error-text]"),
                $items: function() {
                    return this.$parent.find("[data-item]");
                }
            });
        },

        getVal: function() {
            return this.$control.val();
        },

        toggle: function(params) {
            var isActive = this.$parent.hasClass("active");

            clearMenus.call(this.$parent);

            if(!isActive) {
                this.$parent.addClass("active");
                this.render();
            }

            return false;
        },

        setActive: function(params) {
            if(!this.$parent.hasClass("active")) {
                this.$parent.addClass("active");
                this.render();
            }
        },

        setCountry: function(country) {
            if (country) {
                this.model.set({
                    country: country
                });
            }
        },

        item: function() {
            var $el = this.$el;
            this.setVal($el.attr("data-item"));
            if(!$el.attr("data-term")) return false;
        },

        renderSuggestions: function() {
            var locations = this.model.get("locations");
            if(locations && locations.length) {
                this.render();
            }
        },

        type: function(params) {
            var self = this;
            clearTimeout(this.timeout);
            this.timeout = setTimeout(function() {
                self.request();
            }, 100);
        },

        request: function() {
            var value = this.$control.val();

            var allowedTermLength = 2;
            var containsJapanese = value.match(/[\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]/);
            if (containsJapanese) {
                allowedTermLength = 1;
            }

            if(value.length === 0 || value.length >= allowedTermLength) {
                this.model.requestLocations(value);
            }
        },

        render: function() {
            this.$list.html(tpl.render("SuggestLocationList",
                _.extend(this.model.toJSON(), {
                    term: this.model.term,
                    anyCityItem: this.anyCityItem
                })
            ));
        },

        markAsError: function(errorsList) {
            errorsList = errorsList || [];
            this.$parent.addClass("error");

            var errors = "";
            _.each(
                errorsList,
                function(value) {
                    if(!_.isEmpty(errors) ) {
                        errors += "<br>";
                    }
                    errors += value;
                }
            );
            this.$errors.html(errors);
        },

        markAsNotError: function() {
            this.$parent.removeClass("error");
            this.$errors.html("");
        },

        setVal: function(value) {
            this.$control.val(value).trigger("change");
        }
    };

    $.fn.extend({
        uiLocation: UiFunction(SuggestLocation)
    });

    function clearMenus() {
        $(SuggestLocation.directive).removeClass("active");
    }

    function handler(event) {
        var params = event.data || {},
            $this = $(this),
            $parent = $this.closest(SuggestLocation.directive);

        return $parent.uiLocation(params.method, { event: event }, true);
    }

    $(document).on("click.ui.location", clearMenus)
        .on("click.ui.location", SuggestLocation.directive + " [data-toggle]", { method: "toggle" }, handler)
        .on("click.ui.location", SuggestLocation.directive + " [data-item]", { method: "item" }, handler);

    $(document).on("keydown.ui.location", SuggestLocation.directive + " [data-toggle]", { method: "setActive" }, handler);
    $(document).on("keydown change", SuggestLocation.directive + " [data-control]", { method: "type" }, handler);
}());

(function() {
    var ActivateItem = {
        directive: "[data-ui-activate]"
    };

    function activateItem() {
        var selector = $(this).attr("data-ui-activate");

        $(selector).addClass("active");

        return false;
    }

    $(document).on("click.ui.activete", ActivateItem.directive, activateItem);
}());
