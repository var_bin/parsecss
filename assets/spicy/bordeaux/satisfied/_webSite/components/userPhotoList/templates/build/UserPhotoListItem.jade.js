this["JST"] = this["JST"] || {};

this["JST"]["UserPhotoListItem"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),photos = locals_.photos;
// iterate photos
;(function(){
  var $$obj = photos;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div class=\"middle-content__foto-grid__item\"><a" + (jade.attr("data-src", val.src, true, false)) + " data-image=\"data-image\"" + (jade.attr("href", (!val.userUpgrade) ? val.url : val.userUpgrade, true, false)) + " class=\"middle-content__foto-grid__item__foto\"><img" + (jade.attr("src", val.model.avatar, true, false)) + (jade.attr("data-id", val.model.id, true, false)) + " class=\"foto-grid__item__foto__img\"/></a><div class=\"middle-content__foto-grid__item__coments\"></div></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div class=\"middle-content__foto-grid__item\"><a" + (jade.attr("data-src", val.src, true, false)) + " data-image=\"data-image\"" + (jade.attr("href", (!val.userUpgrade) ? val.url : val.userUpgrade, true, false)) + " class=\"middle-content__foto-grid__item__foto\"><img" + (jade.attr("src", val.model.avatar, true, false)) + (jade.attr("data-id", val.model.id, true, false)) + " class=\"foto-grid__item__foto__img\"/></a><div class=\"middle-content__foto-grid__item__coments\"></div></div>");
    }

  }
}).call(this);
;return buf.join("");
};