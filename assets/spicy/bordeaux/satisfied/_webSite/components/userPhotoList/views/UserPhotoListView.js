var UserPhotoListView = Backbone.View.extend({
    initialize: function(options) {
        this.userId = options.userId;
        this.userUpgrade = options.params && options.params.upgrade;
        this.rowsIterator = 0;
    },

    render: function() {
        this.$el.empty().html(tpl.render('UserPhotoList', {
            photos: this.collection.toJSON()
        }));

        if (this.collection.length) {
            this.renderPhotos();
        } else {
            this.region.ready();
        }

        return this;
    },

    renderPhotos: function() {
        var self = this,
            $photos = this.$('#userPhotosHidden [data-photo]'),
            photoSort = this.collection.getPhotoSort(this.collection.length),
            photoSortStatus = [],
            photoIndexMap = [],
            photoPositionMap = [],
            i, j,
            photoIndexIterator = 0;

        for (i = 0; i < photoSort.length; i++) {
            photoSortStatus[i] = 0;
            photoPositionMap[i] = [];
            for (j = 0; j < photoSort[i]; j++) {
                photoIndexMap[photoIndexIterator] = i;
                photoPositionMap[i].push({
                    photo: $photos[photoIndexIterator],
                    model: this.collection.at(photoIndexIterator),
                    index: photoIndexIterator
                });
                photoIndexIterator++;
            }
        }

        this.rowsIterator = photoPositionMap.length;

        $photos.one('load error', function(event) {
            var el = this,
                $el = $(el),
                index = $el.index(),
                row = photoIndexMap[index];

            photoSortStatus[row]++;
            if(photoSortStatus[row] === photoSort[row]) {
                self.renderRow(photoPositionMap[row], row);
            }
        }).each(function() {
            if (this.complete) $(this).load();
        });
    },

    renderRow: function(photos, index) {
        var naughtyClass = "",
            naughtyLevel = 0,
            attr = "";
        var naughtyMode = app.appData().get("naughtyMode");
        var $el,
            $beforeEl = this.$('[data-index=' + (+index +  1) + ']'),
            aspect = 0,
            data = {
                photos: [],
                total: photos.length,
                aspectTotal: 0,
                index: index
            };

        for (var i = 0; i < photos.length; i++) {
            aspect = photos[i].photo.width / photos[i].photo.height;
            attr = photos[i].model.get("attributes");
            data.aspectTotal += aspect;
            if(naughtyMode) {
                naughtyLevel = parseInt(attr.level);
                if((naughtyMode.naughtyMode == 2 && naughtyLevel !== 2) || naughtyMode.naughtyMode == 3) {
                    naughtyLevel = 0;
                }
            }

            data.photos.push({
                model: photos[i].model.toJSON(),
                src: photos[i].photo.src,
                aspect: aspect,
                url: '/#photo/gallery/id/'+this.userId+'/open/'+photos[i].index,
                userUpgrade: '',
                naughty: {naughtyClass: naughtyClass, naughtyLevel: naughtyLevel, naughtyMode: naughtyMode.naughtyMode}
            });
        }

        $el = $(tpl.render('UserPhotoListItem', data));

        if ($beforeEl.size())
            $beforeEl.before($el);
        else
            this.$el.append($el);
        $el.show();

        --this.rowsIterator;
        if (!this.rowsIterator) this.region.ready();
    }
});
