this["JST"] = this["JST"] || {};

this["JST"]["Funnel"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,userAttributes = locals_.userAttributes,defaultSearchParams = locals_.defaultSearchParams;
buf.push("<div class=\"b-popup-overlay\"><div class=\"b-popup-table static\"><div class=\"b-popup-cell\"><div class=\"b-popup b-popup-funnel\"><div class=\"voronka_pop_up__container\"><h2 class=\"voronka-main-title\">" + (jade.escape(null == (jade_interp = _t("funnel", "title.give_more_info")) ? "" : jade_interp)) + "</h2><div class=\"voronka_pop_up__content\"><form id=\"FunnelForm\">");
if ( userAttributes)
{
buf.push("<div class=\"voronka_pop_up__anketa\"><div class=\"voronka_pop_up__anketa__col\"><div class=\"voronka_pop_up__anketa__row\"><div data-ui-input=\"data-ui-input\" class=\"field\"><label class=\"voronka_pop_up__anketa__label\">" + (jade.escape(null == (jade_interp = _t("funnel", "title.screenname") + ":") ? "" : jade_interp)) + "</label><div class=\"b-value\"><div id=\"screenname-error\" data-ui-error-text=\"data-ui-error-text\" class=\"b-text\"></div><input type=\"text\"" + (jade.attr("value", userAttributes.screenname, true, false)) + " name=\"userAttributes[screenname]\" id=\"funnelScreenname\" class=\"voronka_pop_up__anketa__textfield screenname\"/></div></div></div><div class=\"voronka_pop_up__anketa__row\"><label class=\"voronka_pop_up__anketa__label\">" + (jade.escape(null == (jade_interp = _t("funnel", "title.on_my_mind")) ? "" : jade_interp)) + "</label><textarea data-resize=\"auto\" name=\"userAttributes[chatupline]\" id=\"funnelChatupline\" class=\"voronka_pop_up__anketa__textfield\">" + (jade.escape((jade_interp = userAttributes.chatUpLine) == null ? '' : jade_interp)) + "</textarea></div></div><div class=\"voronka_pop_up__anketa__col\"><div class=\"voronka_pop_up__anketa__row\"><label class=\"voronka_pop_up__anketa__label\">" + (jade.escape(null == (jade_interp = _t("funnel", "title.looking_for")) ? "" : jade_interp)) + "</label><div class=\"custom-select custom-select--anketa\"><div id=\"gender-error\" data-ui-error-text=\"data-ui-error-text\" class=\"b-text\"></div><input type=\"hidden\"" + (jade.attr("value", defaultSearchParams.gender, true, false)) + " name=\"defaultSearchParams[gender]\" data-control=\"data-control\" id=\"funnelGender\"/><i class=\"icon-arrow-down custom-select__pointer\"></i><span class=\"custom-select__selected-item\">");
if ( defaultSearchParams.gender==1)
{
buf.push(jade.escape(null == (jade_interp = _t("funnel", "value.male")) ? "" : jade_interp));
}
else
{
buf.push(jade.escape(null == (jade_interp = _t("funnel", "value.female")) ? "" : jade_interp));
}
buf.push("</span><select><option data-item=\"1\"" + (jade.attr("selected", ((1==defaultSearchParams.gender) ? 'selected' : null), true, false)) + ">" + (jade.escape(null == (jade_interp = _t("funnel", "value.male")) ? "" : jade_interp)) + "</option><option data-item=\"2\"" + (jade.attr("selected", ((2==defaultSearchParams.gender) ? 'selected' : null), true, false)) + ">" + (jade.escape(null == (jade_interp = _t("funnel", "value.female")) ? "" : jade_interp)) + "</option></select></div></div><div class=\"voronka_pop_up__anketa__row\"><span class=\"voronka_pop_up__anketa__label\">Age between:</span><div class=\"custom-select custom-select--anketa custom-select--small\"><input type=\"hidden\"" + (jade.attr("value", defaultSearchParams.ageFrom, true, false)) + " name=\"defaultSearchParams[ageFrom]\" data-control=\"data-control\" id=\"funnelFormAgeFrom\"/><i class=\"icon-arrow-down custom-select__pointer\"></i><span class=\"custom-select__selected-item\">" + (jade.escape((jade_interp = defaultSearchParams.ageFrom) == null ? '' : jade_interp)) + "</span><select>");
for(var i = 18; i < 79; i++)
{
buf.push("<option" + (jade.attr("data-item", i, true, false)) + (jade.attr("selected", ((i==defaultSearchParams.ageFrom) ? 'selected' : null), true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</option>");
}
buf.push("</select></div>to<div class=\"custom-select custom-select--anketa custom-select--small\"><input type=\"hidden\"" + (jade.attr("value", defaultSearchParams.ageTo, true, false)) + " name=\"defaultSearchParams[ageTo]\" data-control=\"data-control\" id=\"funnelFormAgeTo\"/><i class=\"icon-arrow-down custom-select__pointer\"></i><span class=\"custom-select__selected-item\">" + (jade.escape((jade_interp = defaultSearchParams.ageTo) == null ? '' : jade_interp)) + "</span><select>");
for(var i = 18; i < 79; i++)
{
buf.push("<option" + (jade.attr("data-item", i, true, false)) + (jade.attr("selected", ((i==defaultSearchParams.ageTo) ? 'selected' : null), true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</option>");
}
buf.push("</select></div></div><div class=\"voronka_pop_up__anketa__row\"><span class=\"voronka_pop_up__anketa__label\">Where whould we search?</span><div data-ui-location=\"data-ui-location\" class=\"select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div class=\"value\"><input name=\"defaultSearchParams[locationInput]\" type=\"text\"" + (jade.attr("value", defaultSearchParams.locationInput, true, false)) + " autocomplete=\"off\" data-control=\"data-control\" id=\"funnelLocale\" class=\"voronka_pop_up__anketa__textfield input-value\"/></div><div class=\"select-location location\"><div data-list=\"data-list\" class=\"b-dropdown-menu location\"></div></div></div></div></div></div></div><div class=\"voronka_pop_up__footer\"><button id=\"funnelSaveBtn\" class=\"voronka_pop_up__voronka-button voronka_pop_up__voronka-button--small voronka_pop_up__voronka-button--blue voronka_pop_up__voronka-button--right\">" + (jade.escape(null == (jade_interp = _t("funnel", "button.save")) ? "" : jade_interp)) + "</button></div>");
}
buf.push("</form></div></div></div></div></div><div class=\"b-loader\"></div></div>");;return buf.join("");
};