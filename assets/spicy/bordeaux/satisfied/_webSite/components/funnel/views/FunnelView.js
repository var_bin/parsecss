var FunnelView = Backbone.View.extend({
    events: {
        'click #funnelSaveBtn': 'save',
        'change .custom-select select': 'changeSelect'
    },

    initialize: function(options) {
        document.documentElement.style.overflow='hidden';
        // Turn off notifications on this page.
        app.appData().messanger.setup({
            declineActive: true
        });

        var self = this;
        this.model.fetch({
            success: function(model) {
                self.render();
            }
        });
    },

    render: function() {
        var modelData = this.model.toJSON();
        this.$el.html(tpl.render(
            'Funnel',
            modelData
        ));
        appUi.initElements(this.$el);
        return this;
    },

    remove: function() {
        document.documentElement.style.overflow='';
        // Revert turning off notification on this page.
        app.appData().messanger.setup({
            declineActive: false
        });
    },

    save: function() {
        var self = this;
        var formData = {};
        var form = this.$el.find('#FunnelForm');

        formData = {
            'userAttributes' : {
                screenname: $(form).find('#funnelScreenname').val(),
                chatUpLine: $(form).find('#funnelChatupline').val()
            },
            'defaultSearchParams': {
                ageFrom      : $(form).find('#funnelFormAgeFrom').val(),
                ageTo        : $(form).find('#funnelFormAgeTo')  .val(),
                gender       : $(form).find('#funnelGender')     .val(),
                locationInput: $(form).find('#funnelLocale')     .val(),
            }
        };

        this.model.set(formData);

        if ( this.model.isValid() ) {
            var modelData = this.model.toJSON();
            modelData.funnelProperties.isCompleted = true;
            modelData.defaultSearchParams.race = [];
            modelData.defaultSearchParams.religion = [];
            this.model.set(modelData);
            this.model.save({}, {
                type : 'POST',
                success : function() {
                    app.appData().get('user').login = formData.userAttributes.screenname;
                    
                    app.appData().get('searchForm').params.gender   = formData.defaultSearchParams.gender;
                    app.appData().get('searchForm').params.location = formData.defaultSearchParams.locationInput;
                    app.appData().get('searchForm').params.ageFrom  = formData.defaultSearchParams.ageFrom;
                    app.appData().get('searchForm').params.ageTo    = formData.defaultSearchParams.ageTo;

                    Backbone.trigger("getFunnelSearchParams", {getFunnelSearchParams: app.appData().get('searchForm')});

                    $('#funnelScreenname') .uiInput( 'markAsNotError');
                    $('#funnelFormAgeFrom').uiSelect('markAsNotError');
                    $('#funnelFormAgeTo')  .uiSelect('markAsNotError');
                    $('#funnelGender')     .uiSelect('markAsNotError');

                    var continueUrl = self.model.get('displayPhotoUploadMotivation') ? '/photoUploadMotivation/index/source/funnel' : '/wall';
                    self.region.close();

                    app.router.navigate(
                        continueUrl,
                        {
                            trigger: true
                        }
                    );
                },
                error : function(model, saveErrors) {
                    self.showUiErrors(saveErrors);
                }
            });
        } else {
            this.showUiErrors(this.model.validationError);
        }
        
        return false;
    },

    showUiErrors: function(errors) {
        var isShowedUiErrors = false;
        if( errors.userAttributes && errors.userAttributes.screenname ) {
            $('#funnelScreenname').uiInput('markAsError', errors.userAttributes.screenname);
            isShowedUiErrors = true;
        }
        if( errors.defaultSearchParams) {
            if(errors.defaultSearchParams.ageFrom ) {
                $('#funnelFormAgeFrom').uiSelect('markAsError', errors.defaultSearchParams.ageFrom);
                isShowedUiErrors = true;
            }
            if(errors.defaultSearchParams.ageTo ) {
                $('#funnelFormAgeTo').uiSelect('markAsError', errors.defaultSearchParams.ageTo);
                isShowedUiErrors = true;
            }
            if(errors.defaultSearchParams.gender ) {
                $('#funnelGender').uiSelect('markAsError', errors.defaultSearchParams.gender);
                isShowedUiErrors = true;
            }
        }

        if(!isShowedUiErrors) {
            $('#funnelScreenname') .uiInput( 'markAsError', []);
            $('#funnelFormAgeFrom').uiSelect('markAsError', []);
            $('#funnelFormAgeTo')  .uiSelect('markAsError', []);
            $('#funnelGender')     .uiSelect('markAsError', []);
        }
    },

    changeSelect: function(event) {
        var target = $(event.currentTarget);
        target.parent().find('.custom-select__selected-item').text(target.val());
        target.parent().find('input:hidden[data-control="data-control"]').val(target.find(':selected').attr('data-item'));
        return this;
    }
});