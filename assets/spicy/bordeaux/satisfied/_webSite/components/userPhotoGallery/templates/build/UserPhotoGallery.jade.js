this["JST"] = this["JST"] || {};

this["JST"]["UserPhotoGallery"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),data = locals_.data,currentHeight = locals_.currentHeight;
var currentPhoto = data.photos[data.open] ? data.photos[data.open].normal : '';
buf.push("<div data-close=\"data-close\" class=\"photo-item-popup\"><div class=\"photo-item-popup-wrap\"><div class=\"photo-item-popup-title\">&nbsp;</div><div id=\"userPhotoGalleryFullHelperWrap\" class=\"photo_wrapper_helper relative\"><img id=\"userPhotoGalleryFullHelper\"" + (jade.attr("src", currentPhoto, true, false)) + " data-photo=\"data-photo\"/></div><div id=\"userPhotoGalleryFullWrap\" class=\"photo_wrapper relative\">");
if ( (currentHeight))
{
buf.push("<img id=\"userPhotoGalleryFull\"" + (jade.attr("src", currentPhoto, true, false)) + " data-photo=\"data-photo\"" + (jade.attr("style", "max-height: " + currentHeight + "px;", true, false)) + "/>");
}
else
{
buf.push("<img id=\"userPhotoGalleryFull\"" + (jade.attr("src", currentPhoto, true, false)) + " data-photo=\"data-photo\"/>");
}
buf.push("</div><div class=\"photo-block_comments\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--photo_text\"><span>Comments</span></div></div><div data-comments=\"data-comments\" class=\"b-feed-content__comment\"></div></div><div data-close=\"data-close\" class=\"photo-item-popup-close\"><i data-close=\"data-close\" class=\"comments-header-text__icon icon-cross\"></i></div></div></div>");;return buf.join("");
};