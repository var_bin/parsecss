var UserPhotoGalleryView = Backbone.View.extend({

    naughtyModeAvailable: false,
    usersNaughtyLevel: 0,
    currentPhoto: 0,
    comments: {},
    source: '',

    events: {
        'click [data-close]': 'close',
        'click [data-photo]': 'changePhoto'
    },

    initialize: function() {
        this.comments = {};

        if (this.options.source) {
            this.source = this.options.source;
        }
        var naughtyMode = app.appData().get("naughtyMode");
        if (naughtyMode) {
            naughtyModeAvailable =  naughtyMode.available;
            usersNaughtyLevel = naughtyMode.naughtyMode;
        } else {
            naughtyModeAvailable = false;
            usersNaughtyLevel = 0;
        }

        this.listenTo(this.model, 'sync', this.renderGallery);
        this.model.fetch();
    },

    renderGallery: function() {
        this.currentPhoto = this.model.get("open") || 0;

        this.$el.html(tpl.render('UserPhotoGallery', {
                data: this.model.toJSON(),
                naughtyModeAvailable: naughtyModeAvailable,
                naughtyLevel: usersNaughtyLevel,
                currentHeight: document.body.clientHeight - 180
            }));

        this.$dom = {
            full: this.$('#userPhotoGalleryFull'),
            fullWrap: this.$('#userPhotoGalleryFullWrap'),
            fullHelper: this.$('#userPhotoGalleryFullHelper'),
            fullHelperWrap: this.$('#userPhotoGalleryFullHelperWrap')
        };

        if (this.model.get('photos')[this.currentPhoto]) {
            this.initComments(this.model.get('photos')[this.currentPhoto]['id']);
        }

        this.region.ready();
    },

    close: function(event) {
        if ($(event.target).data('close')) {
            this.comments = {};

            if (this.source == 'gallery') {
                app.router.navigate(userLink(this.model.get('id')), {silent: true});
            } else {
                app.router.navigate('', {silent: true});
            }
            this.region.close();
        }
    },

    changePhoto: function(event) {
        var nextId = this.getNextId();

        if (nextId != this.currentPhoto) {
            this.currentPhoto = nextId;
            photoData = this.model.get('photos')[nextId];

            var self = this,
                src = photoData.normal;
                $el = $('<img />');

            this.$dom.fullWrap.fadeOut();

            $el.one('load error', function() {
                self.$dom.full.css({
                    'max-height': '' + document.body.clientHeight - 180 + 'px'
                })
                self.$dom.fullHelper.attr('src', src)
                    .css({
                        'max-height': $(window).height()
                    });
                self.$dom.fullWrap.stop(true, true)
                    .fadeOut('fast', function() {
                        self.$dom.full.attr('src', src);
                        self.$dom.fullWrap.fadeTo(300, true);
                    });
                $(this).remove();
            }).each(function() {
                var $this = $(this);
                $this.attr('src', src);
                if (this.complete) $this.load();
            });

            self.initComments(photoData.id);
        }
    },

    getNextId: function() {
        var nextId = this.currentPhoto;
        nextId++;
        if (nextId > this.model.get('photos').length - 1) {
            nextId = 0;
        }

        if (typeof this.model.get('photos')[nextId] === 'object') {
            return nextId
        }

        return this.currentPhoto;
    },

    initComments: function(dataId) {
            var ucomment = this.comments[dataId];

            if (!ucomment) {
                ucomment = new UserCommentView({
                    parent: this,
                    el: this.$el.find('[data-comments]'),
                    objectType: 4,
                    objectId: dataId,
                    reverse: true
                });
                if (ucomment) {
                    this.comments[dataId] = ucomment;
                }
            } else {
                ucomment.render();
            }
    }
});
