this["JST"] = this["JST"] || {};

this["JST"]["SuggestLocationList"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),locations = locals_.locations,term = locals_.term;
buf.push("<div class=\"popup--search-overflow\">");
if ( locations)
{
// iterate locations
;(function(){
  var $$obj = locations;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var location = $$obj[$index];

buf.push("<div" + (jade.attr("data-item", location, true, false)) + (jade.attr("data-term", term || "", true, false)) + " class=\"popup--search-city\">" + (jade.escape(null == (jade_interp = location) ? "" : jade_interp)) + "</div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var location = $$obj[$index];

buf.push("<div" + (jade.attr("data-item", location, true, false)) + (jade.attr("data-term", term || "", true, false)) + " class=\"popup--search-city\">" + (jade.escape(null == (jade_interp = location) ? "" : jade_interp)) + "</div>");
    }

  }
}).call(this);

}
buf.push("</div><div class=\"b-notificators__popup-arrow--slide\"></div>");;return buf.join("");
};