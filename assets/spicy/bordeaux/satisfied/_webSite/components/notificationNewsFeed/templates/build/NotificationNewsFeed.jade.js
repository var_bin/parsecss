this["JST"] = this["JST"] || {};

this["JST"]["NotificationNewsFeed"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,settings = locals_.settings;
buf.push("<div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--1em\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title.newsfeed")) ? "" : jade_interp)) + "<span class=\"changes-status changes-status--save save-status inactive\"><span class=\"icon-status icon-Check\"></span>" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "text.saved")) ? "" : jade_interp)) + "</span></div></div><div class=\"b-profile-feed-middle-content gray border-radius b-profile-feed-middle-content--activity_notification\"><form class=\"b-profile-feed-middle-content__form\">");
var listChunks = [];
var chunks = [];
// iterate settings
;(function(){
  var $$obj = settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

var chunk = {'name': name, 'setting': setting, 'key': key, 'val': val};
chunks.push(chunk);
if (chunks.length == 3) {
{
listChunks.push(chunks);
chunks = []
}
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

var chunk = {'name': name, 'setting': setting, 'key': key, 'val': val};
chunks.push(chunk);
if (chunks.length == 3) {
{
listChunks.push(chunks);
chunks = []
}
}
    }

  }
}).call(this);

    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

var chunk = {'name': name, 'setting': setting, 'key': key, 'val': val};
chunks.push(chunk);
if (chunks.length == 3) {
{
listChunks.push(chunks);
chunks = []
}
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

var chunk = {'name': name, 'setting': setting, 'key': key, 'val': val};
chunks.push(chunk);
if (chunks.length == 3) {
{
listChunks.push(chunks);
chunks = []
}
}
    }

  }
}).call(this);

    }

  }
}).call(this);

if (chunks.length) {
{
listChunks.push(chunks);
}
}
// iterate listChunks
;(function(){
  var $$obj = listChunks;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var chunks = $$obj[$index];

buf.push("<div class=\"b-profile-feed-middle-content__form_field b-profile-feed-middle-content__form_field--activity_form b-profile-feed-middle-content__form_field--notification\">");
// iterate chunks
;(function(){
  var $$obj = chunks;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var chunk = $$obj[$index];

buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", '' + (chunk.name) + '', true, false)) + (jade.attr("data-key", "" + (chunk.key) + "", true, false)) + (jade.attr("checked", (chunk.setting[chunk.key]) ? true : false, true, false)) + (jade.attr("id", "" + (chunk.name) + "", true, false)) + " data-change-newsfeed-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", '' + (chunk.name) + '', true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + chunk.name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var chunk = $$obj[$index];

buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", '' + (chunk.name) + '', true, false)) + (jade.attr("data-key", "" + (chunk.key) + "", true, false)) + (jade.attr("checked", (chunk.setting[chunk.key]) ? true : false, true, false)) + (jade.attr("id", "" + (chunk.name) + "", true, false)) + " data-change-newsfeed-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", '' + (chunk.name) + '', true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + chunk.name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  }
}).call(this);

buf.push("</div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var chunks = $$obj[$index];

buf.push("<div class=\"b-profile-feed-middle-content__form_field b-profile-feed-middle-content__form_field--activity_form b-profile-feed-middle-content__form_field--notification\">");
// iterate chunks
;(function(){
  var $$obj = chunks;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var chunk = $$obj[$index];

buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", '' + (chunk.name) + '', true, false)) + (jade.attr("data-key", "" + (chunk.key) + "", true, false)) + (jade.attr("checked", (chunk.setting[chunk.key]) ? true : false, true, false)) + (jade.attr("id", "" + (chunk.name) + "", true, false)) + " data-change-newsfeed-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", '' + (chunk.name) + '', true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + chunk.name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var chunk = $$obj[$index];

buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", '' + (chunk.name) + '', true, false)) + (jade.attr("data-key", "" + (chunk.key) + "", true, false)) + (jade.attr("checked", (chunk.setting[chunk.key]) ? true : false, true, false)) + (jade.attr("id", "" + (chunk.name) + "", true, false)) + " data-change-newsfeed-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", '' + (chunk.name) + '', true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title." + chunk.name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  }
}).call(this);

buf.push("</div>");
    }

  }
}).call(this);

buf.push("</form></div>");;return buf.join("");
};