var Router = Backbone.Router.extend({
    routes: {
        '': 'run',
        'search/model/room(/:room)/status(/:status)': 'room',
        'search(/*query)': 'search',
        'pay(/*action)' : 'pay',
        ':controller(/*action)': 'run'
    },

    run: function(controller, action) {
        $('#rightSide').show();
        $('#layoutContent').removeClass('widePage');

        if (controller && !action) {
            controller = controller.replace(/\#[a-z0-9\-\_]+/i, '');
        }

        var params = this.parseAction(action);

        controller = controller || 'wall';

        if(window[controller+'Controller']) {
            window[controller+'Controller'].run(params);
        } else {
            throw new Error('Controller "'+controller+'" wasn\'t found.');
        }
    },

    search: function(query) {
        var params = {
            action: this.parseAction(query).action,
            options: {
                query: query
            }
        };

        controller = 'search';

        if(window[controller+'Controller']) {
            window[controller+'Controller'].run(params);
        } else {
            throw new Error('Controller "'+controller+'" wasn\'t found.');
        }
    },

    pay: function(query) {
        var data = {}, queries, temp, i, l;
        var urlQuery = location.search.substr(1);
        queries = urlQuery.split("&");
        for ( i = 0, l = queries.length; i < l; i++ ) {
            temp = queries[i].split('=');
            data[temp[0]] = temp[1];
        }

        var params = {
            action: this.parseAction(query).action,
            options: data
        };

        controller = 'pay';

        if(window[controller+'Controller']) {
            window[controller+'Controller'].run(params);
        } else {
            throw new Error('Controller "'+controller+'" wasn\'t found.');
        }
    },

    room: function (room, status) {
        var params = {
            action: 'model',
            options: {
                room: room,
                status: status
            }
        };

        controller = 'search';

        if (window[controller + 'Controller']) {
            window[controller + 'Controller'].run(params);
        } else {
            throw new Error('Controller "' + controller + '" wasn\'t found.');
        }
    },

    parseAction: function(action) {
        var values = [],
            params = {
                action: 'index',
                options: {}
            };

        if(action) {
            values = action.split('/');
            params.action = values.shift();
            for(var i = 0; i < values.length; i++) {
                if(values[i]) {
                    params.options[values[i]] = values[++i];
                }
            }
        }

        return params;
    },

    navigate: function(fragment, options) {
        app.unshadowPopup();

        if(fragment.substr(0, 2) === "\/#") {
            fragment = "\/" + fragment.substr(2);
        }

        Backbone.history.navigate(fragment, options);

        return this;
    }
});
