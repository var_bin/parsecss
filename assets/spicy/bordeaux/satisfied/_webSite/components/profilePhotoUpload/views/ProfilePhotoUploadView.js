var ProfilePhotoUploadView = Backbone.View.extend({
    fbAppSettings: {},
    useFbPhotos: false,
    templateName: 'ProfilePhotoUpload',
    viewType: 'normal',
    fbListType: 'normal',
    avatarSize: 180,

    _totalFiles: 0,
    _currentFile: 0,

    regions: {
        fbPhotoUploaderBtn:  '#fbPhotoUploaderBtn',
        fbPhotoUploaderList: '#fbPhotoUploaderList'
    },

    events: {
        'dragleave [data-drag-drop]': 'dragLeaveEvent',
        'drop [data-drag-drop]':      'dropEvent',
        'dragenter [data-drag-drop]': 'ignoreDragEvent',
        'dragover [data-drag-drop]':  'ignoreDragEvent'
    },

    initialize: function(options) {
        this.fbAppSettings = options.fbAppSettings;
        if (options.type) {
            this.viewType      = options.type;
        }
        if (options.fbListType) {
            this.fbListType = options.fbListType;
        }
        if (this.viewType === 'photoUploadMotivation') {
            this.templateName = 'ProfilePhotoUploadMotivation';
            this.avatarSize = 145;
        }
        this.listenTo(this.model, 'change', this.render);
    },

    render: function() {
        this.$el.html(tpl.render(this.templateName, {
            model: this.model.toJSON()
        }));

        this.createLoader();

        if (this.useFbPhotos) {
            var profilePhotoFBUploaderModel = new ProfilePhotoFBUploaderModel({
                fbAppSettings: this.fbAppSettings
            });

            this.regions.fbPhotoUploaderBtn.render(ProfilePhotoFBUploaderBtnView, {
                model: profilePhotoFBUploaderModel,
                type:  this.viewType
            });
            if (this.fbListType === 'normal') {
                this.regions.fbPhotoUploaderList.render(ProfilePhotoFBUploaderListView, {
                    model: profilePhotoFBUploaderModel,
                    type:  this.viewType
                });
            } else if (this.fbListType === 'popup') {
                app.regions.popup.render(ProfilePhotoFBUploaderListView, {
                    model: profilePhotoFBUploaderModel,
                    type:  this.viewType
                });
            } else {
                throw new Error('Unsupported fbListType: "'+this.fbListType+'"');
            }
        }

        return this;
    },

    createLoader: function() {
        var authParams = {};

        if (app.csrfToken && app.csrfToken.name) {
            authParams[app.csrfToken.name] = app.csrfToken.value;
        }
        authParams['avatarSize'] = this.avatarSize;

        this.uploader = new qq.FineUploaderBasic({
            button: this.$el.find('#ProfilePhotoUploadForm')[0],
            element: this.$el,
            maxConnections: 1,
            dragAndDrop: {
                extraDropzones: this.$el.find('[data-drag-drop]')[0]
            },
            validation: {
                acceptFiles: ['jpeg', 'jpg', 'gif', 'png'],
                allowedExtensions: ['jpeg', 'jpg', 'gif', 'png']
            },
            request: {
                endpoint: '/photo/upload',
                inputName: 'PhotoUploadForm[file]',
                forceMultipart: true,
                params: authParams,
                paramsInBody: true
            },
            callbacks: {
                onSubmit:   $.proxy(this.submitEvent,   this),
                onUpload:   $.proxy(this.uploadEvent,   this),
                onComplete: $.proxy(this.completeEvent, this),
                onProgress: $.proxy(this.progressEvent, this)
            }
        });
    },

    ignoreDragEvent: function(event) {
        event.originalEvent.stopPropagation();
        event.originalEvent.preventDefault();
        event.preventDefault();
        $(event.currentTarget).addClass('active');
    },

    dropEvent: function(event) {
        event.originalEvent.stopPropagation();
        event.originalEvent.preventDefault();
        event.preventDefault();
        var dt = event.originalEvent.dataTransfer;
        var files = dt.files;
        this.uploader.addFiles(files);
        $(event.currentTarget).removeClass('active');
    },

    dragLeaveEvent: function(event) {
        $(event.currentTarget).removeClass('active');
    },

    submitEvent: function(id, fileName) {
        Backbone.trigger(
            'UploadProfilePhoto.start',
            {
                id: id
            }
        );

        this._totalFiles++;
        this.$el.find('[data-uploadform]').addClass('inactive');
        this.$el.find('[data-progress]').removeClass('inactive');
        this.$el.find('[data-total]').text(this._totalFiles);
    },

    uploadEvent: function(id, fileName, xhr) {
        this._currentFile++;
        this.$el.find('[data-current]').text(this._currentFile);
    },

    progressEvent: function(id, fileName, loaded, total) {
        if (loaded < total) {
            progress = Math.round(loaded / total * 100);
            Backbone.trigger(
                'UploadProfilePhoto.progress',
                {
                    id:       id,
                    progress: progress
                }
            );
        } else {
            progress = 100;
        }

        this.$el.find('[data-progress-value]').text(progress + '%');
        this.$el.find('[data-progress-bar]').css('width', progress + '%');
    },

    completeEvent: function(id, fileName, response) {
        if (!response || response.status === 'error') {
            Backbone.trigger(
                'UploadProfilePhoto.complete',
                {
                    id:     id,
                    status: 'error',
                    errors: (response.meta.description) ? response.meta.description : []
                }
            );
        } else {
            Backbone.trigger(
                'UploadProfilePhoto.complete',
                {
                    id:     id,
                    status: 'done',
                    data: response.data || {}
                }
            );
        }

        this._totalFiles--;
        if (this._totalFiles == 0) {
            this._currentFile = 0;
            this.$el.find('[data-uploadform]').removeClass('inactive');
            this.$el.find('[data-progress]').addClass('inactive');
            this.$el.find('[data-progress-value]').text('0%');
            this.$el.find('[data-progress-bar]').css('width', '0%');
        }
    }
});
