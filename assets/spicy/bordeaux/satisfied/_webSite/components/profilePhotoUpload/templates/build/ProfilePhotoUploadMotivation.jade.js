this["JST"] = this["JST"] || {};

this["JST"]["ProfilePhotoUploadMotivation"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<div data-drag-drop=\"data-drag-drop\" data-uploadform=\"data-uploadform\" class=\"voronka_pop_up__drug-drop\"><div class=\"voronka_pop_up__drug-drop__title\">" + (jade.escape(null == (jade_interp = _t('profilePhotoUpload', 'text.drag_and_drop')) ? "" : jade_interp)) + "</div><input type=\"file\" size=\"1\" class=\"b-profile-feed-middle-content_upload__input\"/><a id=\"ProfilePhotoUploadForm\" class=\"voronka_pop_up__voronka-button voronka_pop_up__voronka-button--red\">" + (jade.escape(null == (jade_interp = _t('profilePhotoUpload', 'text.upload_photos')) ? "" : jade_interp)) + "</a></div><div data-progress=\"data-progress\" class=\"voronka_pop_up__drug-drop inactive\"><div class=\"voronka_pop_up_uploading-progress-container\"><div class=\"voronka_pop_up_uploading-text-progress\">" + (null == (jade_interp = _t('profilePhotoUpload', 'text.Uploading') + '&nbsp;') ? "" : jade_interp) + "<strong data-current=\"data-current\">0</strong>" + (null == (jade_interp = '&nbsp;' + _t('profilePhotoUpload', 'text.from') + '&nbsp;') ? "" : jade_interp) + "<b data-total=\"data-total\">0</b></div><div class=\"voronka_pop_up_uploading-progress-bar\"><div class=\"voronka_pop_up_uploading-progress-bar__inner\"><div style=\"width: 0%;\" data-progress-bar=\"data-progress-bar\" class=\"voronka_pop_up_uploading-progress-bar__progress\"><div class=\"noTrespassingAnimationG\">");
for(var i = 0; i < 24; i++)
{
buf.push("<div class=\"noTrespassingBarLineG\"></div>");
}
buf.push("</div></div></div></div><div class=\"voronka_pop_up_uploading-text-progress\"><b data-progress-value=\"data-progress-value\">0%</b></div></div></div>");;return buf.join("");
};