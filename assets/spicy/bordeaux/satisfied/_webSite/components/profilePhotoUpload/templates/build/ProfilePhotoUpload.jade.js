this["JST"] = this["JST"] || {};

this["JST"]["ProfilePhotoUpload"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<div class=\"b-wrap-content b-feed-content__profile-feed\"></div><div class=\"b-profile-feed-middle-content upload_block\"><div data-progress=\"data-progress\" class=\"progressbar_block_inner inactive\"><div class=\"uploading-progress-container\"><div class=\"uploading-text-progress\">" + (null == (jade_interp = _t('profilePhotoUpload', 'text.Uploading') + '&nbsp;') ? "" : jade_interp) + "<strong data-current=\"data-current\">0</strong>" + (null == (jade_interp = '&nbsp;' + _t('profilePhotoUpload', 'text.from') + '&nbsp;') ? "" : jade_interp) + "<b data-total=\"data-total\">0</b></div><div class=\"uploading-progress-bar\"><div class=\"uploading-progress-bar__inner\"><div style=\"width: 0%;\" data-progress-bar=\"data-progress-bar\" class=\"uploading-progress-bar__progress\"><div class=\"noTrespassingAnimationG\">");
for(var i = 0; i < 24; i++)
{
buf.push("<div class=\"noTrespassingBarLineG\"></div>");
}
buf.push("</div></div></div></div><div class=\"uploading-text-progress\"><b data-progress-value=\"data-progress-value\">0%</b></div></div></div><div data-drag-drop=\"data-drag-drop\" data-uploadform=\"data-uploadform\" class=\"upload_block_inner\"><div class=\"b-profile-feed-middle-content_dragDrop\"><span class=\"b-profile-feed-middle-content_dragDrop_icon icon-Photo\"></span><span>" + (jade.escape(null == (jade_interp = _t('profilePhotoUpload', 'text.drag_and_drop')) ? "" : jade_interp)) + "</span></div><div class=\"b-profile-feed-middle-content_upload\"><div id=\"ProfilePhotoUploadForm\" class=\"b-profile-feed-middle-content_upload_inner\"><span>" + (jade.escape(null == (jade_interp = _t('profilePhotoUpload', 'text.upload_photos')) ? "" : jade_interp)) + "</span><input type=\"file\" size=\"1\" class=\"b-profile-feed-middle-content_upload__input\"/></div></div></div></div>");;return buf.join("");
};