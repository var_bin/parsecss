var RoomsView = Backbone.View.extend({
    className: "b-chatrooms",
    GENERAL_ROOM_ID: 'general_1',

    events: {
        "click .rooms-tab": function(event) {
            event.preventDefault();
            this.activate(
                $(event.target).data("content-id")
            );
        },
        "click .rooms-leave": function(event) {
            event.preventDefault();
            this.leave(
                $(event.target).parent().data("content-id")
            );
        },
        "click .rooms-join": function(event) {
            var self = this;
            event.preventDefault();

            this.generalChatStart(this.GENERAL_ROOM_ID);
        },

        "click .rooms-group": function(event) {
            $(event.target).toggleClass("active");
        },
        "keypress .message-input": function(event) {
            if (event.keyCode === 13) {
                this.sendMsg();
                return false;
            }
        },
        "click .message-submit": function(event) {
            event.preventDefault();
            this.sendMsg();
        },
        "click .chat-message__title": function(event) {
            var card = this.vCard.get(
                $(event.target).data("contact-id")
            );

            if (!card) {
                return;
            }

            var $input = $(".message-input", this.$el),
                body = $input.html();

            $input.empty();
            this.insertTo(
                body.replace(/^(@\S+\s)?/, "@" + card.login + "&nbsp;")
            );
        },
        "mouseover .rooms-contact": "handleInfoBlock",
        "click .chatroom__users-list__item a": "handleInfoBlock",

        "mouseout .rooms-contact": function(event) {
            this.info.hide();
        },
        "click .chatroom__sex-group__title": function (e) {
            var $parent = $(e.currentTarget).parents(".chatroom__sex-group");
            $parent.find(".chatroom__users").slideToggle(200);
            return false;
        },
        'click #cTalksSmiles': function(event) {
            this.$('#cTalksSmiles').toggleClass('active');
        },
        'click [data-smile]': function(event) {
            var $target = $(event.currentTarget),
                smile = $target.data('smile');

            this.pasteSmile(smile);
        }
    },

    initialize: function() {
        this.content = new RoomsContentModel();
        this.vCard = new VCardModel();

        this.model.on("change:tabs", _.bind(function() {
            $(".rooms-tabs", this.$el).html(
                this.former.tabs.call(this)
            );
        }, this));

        this.smiles = new RoomsSmileCollection();

        this.restore();
    },

    restore: function() {
        var callback;

        this.info = new this.ContactInfo(this);
        for (key in this.info) {
            this.info[key] = _.bind(this.info[key], this);
        }

        callback = _.bind(init, this);
        this.model.fetch({ success: callback });
        this.listenTo(Backbone, "reconnect", _.bind(function() {
            var current = this.getActiveContent();
            var tabs = this.model.get('tabs');
            for(var i = 0; i < tabs.length; ++i) {
                if(tabs[i] != 'rooms') {
                    this.join(tabs[i]);
                }
            }
            if (current && current.id != 'rooms') {
                this.content.once("change:" + current.id, _.bind(function() {
                    this.activate(current.id);
                }, this));
            }
        }, this));

        function init() {
            if (!interaction.isAvailable()) {
                Backbone.once("interactionConnect", callback, this);
                return;
            }

            var card = this.vCard.get(this.model.get("uid"));

            if (!card) {
                this.vCard.request(this.model.get("uid"), { success: callback });
                return;
            }
            this.rooms();
            this.interval = setInterval(_.bind(this.rooms, this), 30 * 1000);
            this.listenTo(Backbone, "interactionRooms", _.bind(function(event) {
                if (!this.api[event]) {
                    return;
                }
                this.api[event].apply(this, _.toArray(arguments).slice(1));
            }, this));
        }
    },

    rooms: function() {
        var content = this.content.get("rooms");

        interaction.request("roomList", _.bind(function(result, error) {
            if (error) {
                console.error("rooms", error);
                return;
            }

            if ("error" === result.status) {
                return;
            }

            var current = this.getActiveContent();

            this.content.set(
                content.id, _.extend(content, {
                    list: result.data
                })
            );
            if (!current) {
                this.activate(content.id);
            }
            else if (current.id === content.id) {
                this.renderAsync(content);
            }
        }, this));
    },

    join: function(rid) {
        this.region.loading();
        var content = this.content.get(rid);

        interaction.request("roomJoin", { rid: rid }, _.bind(function(result, error) {
            if (error) {
                console.error("join", error);
                return;
            }

            if ("error" === result.status) {
                if ("payment-required" === result.data.condition) {
                    $.gotoUrl(result.data.body || "/");
                }
                return;
            }

            var room = _.first(
                _.where(this.content.get("rooms").list, { rid: rid })
            );

            if (!room) {
                return;
            }

            this.model.set("tabs", _.union(
                this.model.get("tabs"), rid
            ), {
                silent: true
            });
            content = {
                id: rid,
                title: room.title,
                tpl: "room",
                active: false,
                closeable: true,
                userAvatar: this.model.get('userAvatar')
            };
            this.content.set(rid, content);
            for (var i = 0, l = result.data.length; i < l; i++) {
                this.addContact(content, result.data[i]);
            }
            this.history(rid);
        }, this));
    },

    leave: function(rid) {
        var content = this.content.get(rid);

        if (!content) {
            return;
        }
        interaction.request("roomLeave", { rid: rid }, _.bind(function(result, error) {
            if (error) {
                console.error("leave", error);
                return;
            }

            if ("error" === result.status) {
                return;
            }

            this.model.set("tabs", _.without(
                this.model.get("tabs"), rid
            ), {
                silent: true
            });
            this.content.unset(rid);
            if (content.active) {
                this.activate("rooms");
            }
            else {
                this.model.trigger("change:tabs");
            }
        }, this));
    },

    history: function(rid) {
        var content = this.content.get(rid);

        if (!content) {
            return;
        }
        interaction.request("roomHistory", { rid: rid }, _.bind(function(result, error) {
            if (error) {
                console.error("history", error);
                return;
            }

            if ("error" === result.status) {
                return;
            }

            for (var i = 0, l = result.data.length; i < l; i++) {
                this.addMessage(content, result.data[i].uid, result.data[i].data);
            }
        }, this));
    },

    handleInfoBlock: function(event) {
        var $target = $(event.target);

        this.info.show(
            $target.data("contact-id"), $target.data("contact-direction"), {
                offset: $target.offset(),
                width: $target.width()
            }
        );
    },

    message: function(rid, body) {
        body = $.trim(
            body
                .replace(/(&nbsp;)|(<br>)/g, ' ')
                .replace(/[ ]{2,}/g, ' ')
        );

        if (!body) {
            return;
        }

        var content = this.content.get(rid);

        if (!content) {
            return;
        }
        interaction.request("roomMessage", {
            rid: rid, message: {
                body: body
            }
        }, _.bind(function(result, error) {
                if (error) {
                    console.error("message", error);
                    return;
                }

                var restriction = {},
                    tpl;

                if ("error" === result.status) {
                    switch (result.data.condition) {
                        case "payment-required":
                            tpl = "upgrade";
                            break;
                        default:
                            return;
                    }
                    this.content.set(
                        content.id, _.extend(content, {
                            restriction: {
                                tpl: tpl,
                                body: result.data.body
                            }
                        })
                    );
                    this.addBanner(content, this.former[tpl].call(this, result.data));
                }
            }, this)
        );
    },

    activate: function(id) {
        var content = this.content.get(id);

        if (!content || content.active) {
            return;
        }

        var current = this.getActiveContent();

        if (current) {
            this.content.set(
                current.id, _.extend(current, {
                    active: false
                })
            );
        }
        this.content.set(
            content.id, _.extend(content, {
                active: true
            })
        );
        this.renderAsync(content);
    },

    renderAsync: function(content) {
        this.$el.html(
            this.former.client.call(this)
        );
        $(".rooms-content", this.$el).html(
            this.former[content.tpl].call(this, content)
        );

        if (content.tpl === "list") {
            $(".b-chatrooms", this.$el).addClass("rooms-list");
        }
        else {
            $(".b-chatrooms", this.$el).removeClass("rooms-list");
        }

        this.region.ready();

        this.generalChatStart(this.GENERAL_ROOM_ID);

        $(".autofocus", this.$el).focus();
    },

    remove: function() {
        if (this.interval) {
            clearInterval(this.interval);
        }
        interaction.request("roomQuit", {});
    },

    former: {
        client: function() {
            var client = this.model.toJSON();

            return tpl.render("Rooms", {
                client: client
            });
        },

        tabs: function() {
            var client = this.model.toJSON(),
                content = this.content.toJSON();

            return tpl.render("RoomsTab", {
                client: client,
                content: _.pick(content, client.tabs)
            });
        },

        list: function(content) {
            var client = this.model.toJSON();
            content = _.clone(content);

            client.card = this.vCard.get(client.uid);
            content.list = _.groupBy(content.list, "type");
        },

        room: function(content) {
            var client = this.model.toJSON();
            content = _.clone(content);

            content.contacts = _.groupBy(
                _.map(content.contacts, this.vCard.get, this.vCard),
                "gender"
            );

            return tpl.render("RoomsContent", {
                client: client,
                content: content,
                former: {
                    contact: _.bind(this.former.contact, this),
                    message: _.bind(this.former.message, this),
                    upgrade: _.bind(this.former.upgrade, this)
                },
                smiles: this.smiles.toJSON()
            });
        },

        contact: function(content) {
            content = _.clone(content);

            return tpl.render("RoomsContact", {
                content: content
            });
        },

        message: function(content) {
            var client = this.model.toJSON();
            content = _.clone(content);

            client.card = this.vCard.get(client.uid);
            content.card = this.vCard.get(content.uid);
            return tpl.render("RoomsMessage", {
                client: client,
                content: content
            });
        },

        info: function(content) {
            var content = _.clone(content),
                location = [];

            if (content.geo) {
                if (content.geo.city) {
                    location.push(content.geo.city);
                }
                if (content.geo.country) {
                    location.push(content.geo.country);
                }
                content.geo.location = location.join(" | ");
            }

            if(content.photos && content.photos.attributes.level){
                var naughtyMode = app.appData().get("naughtyMode");
                naughtyLevel = parseInt(content.photos.attributes.level);
                if((naughtyMode.naughtyMode == 2 && naughtyLevel !== 2) || naughtyMode.naughtyMode == 3) {
                    naughtyLevel = 0;
                }
                switch(naughtyLevel) {
                    case 1:
                        naughtyClass = " blurred";
                        break;
                    case 2:
                        naughtyClass = " blurred-heavy";
                        break;
                }

                content = _.extend(content, {
                    naughtyClass: naughtyClass,
                    naughtyLevel: naughtyLevel,
                    naughtyMode: parseInt(naughtyMode.naughtyMode),
                    showRp: naughtyMode.showRp
                });
            }

            return tpl.render("RoomsInfo", {
                content: content
            });
        },

        upgrade: function(content) {
            var content = _.clone(content);

            return tpl.render("RoomsUpgrade", {
                content: content
            });
        }
    },

    api: {
        replace: function(rid) {
            var content = this.content.get(rid);

            if (!content) {
                return;
            }
            this.model.set("tabs", _.without(
                this.model.get("tabs"), rid
            ), {
                silent: true
            });
            this.content.unset(rid);
            if (content.active) {
                this.activate("rooms");
            }
            else {
                this.model.trigger("change:tabs");
            }
        },

        join: function(rid, uid) {
            var content = this.content.get(rid);

            if (!content) {
                return;
            }
            this.addContact(content, uid);
        },

        leave: function(rid, uid) {
            var content = this.content.get(rid);

            if (!content) {
                return;
            }
            this.removeContact(content, uid);
        },

        message: function(rid, uid, payload) {
            var content = this.content.get(rid);

            if (!content || !payload || !payload.body) {
                return;
            }

            var date = new Date();
            var incomingTime = date.getTime();
            payload.time = incomingTime;

            this.addMessage(content, uid, payload);
        }
    },

    addContact: function(content, uid) {
        if (_.contains(content.contacts, uid)) {
            return;
        }

        var card = this.vCard.get(uid);

        if (!card) {
            this.vCard.request(uid, {
                success: _.bind(this.addContact, this, content, uid)
            });
            return;
        }

        var contacts = _.sortBy(
            _.union(
                _.map(content.contacts, this.vCard.get, this.vCard), card
            ),
            "login"
        );

        this.content.set(
            content.id, _.extend(content, {
                contacts: _.pluck(contacts, "id")
            })
        );
        if (content.active) {
            insert.call(this);
        }

        function insert() {
            var $group = $(".rooms-group." + card.gender, this.$el),
                $node, items;

            items = _.where(contacts, { gender: card.gender });
            $node = $group
                        .find("ul")
                            .children()
                                .eq(items.indexOf(card));
            if (!$node.length) {
                $group.find("ul").append(
                    this.former.contact.call(this, card)
                );
            }
            else {
                $node.before(
                    this.former.contact.call(this, card)
                );
            }
            $group
                .show()
                .find(".count")
                .html(items.length);
        }
    },

    removeContact: function(content, uid) {
        var card = this.vCard.get(uid);

        if (!card) {
            return;
        }

        var contacts = _.without(content.contacts, card.id);

        this.content.set(
            content.id, _.extend(content, {
                contacts: contacts
            })
        );
        if (content.active) {
            remove.call(this);
        }

        function remove() {
            var $group = $(".rooms-group." + card.gender, this.$el),
                $node;

            $node = $group
                        .find("ul")
                            .children()
                                .has("a[data-contact-id='" + card.id + "']");
            if ($node.length) {
                items = _.where(
                    _.map(contacts, this.vCard.get, this.vCard), { gender: card.gender }
                );

                $node.remove();
                $group
                    .find("label")
                        .html(items.length);
                if (!items.length) {
                    $group.hide();
                }
            }
        }
    },

    addMessage: function(content, uid, payload) {
        var card = this.vCard.get(uid),
            msg, messages;

        if (!card) {
            this.vCard.request(uid, {
                success: _.bind(this.addMessage, this, content, uid, payload)
            });
            return;
        }

        messages = _.sortBy(
            _.union(
                content.messages || [], msg = _.extend(payload, {
                    uid: card.id
                })
            ),
            "time"
        );

        this.content.set(
            content.id,
            _.extend(content, {
                messages: _.last(messages, this.model.get("historySize"))
            })
        );

        if (content.active) {
            rotateMessages.call(this);
            insert.call(this);
        }

        function insert() {
            var $log = $(".rooms-log", this.$el),
                $node,
                insert_str = this.former.message.call(this, msg);

            $node = $log
                .children()
                .eq(messages.indexOf(msg));

            if (!$node.length) {
                $log.append(insert_str);
            }
            else {
                $node.before(insert_str);
            }
            this.chattoBottom();
        }

        function rotateMessages() {
            var $log = $(".rooms-log", this.$el),
                count = $log.children().length;
            
            if (count > this.model.get("historySize")) {
                $log.children().first().remove();
            }
        }
    },

    chattoBottom: function() {
        $('.chatroom__chat-area-wrap').scrollTop(999999);
    },

    addBanner: function(content, banner) {
        if (content.active) {
            insert.call(this);
        }

        function insert() {
            var $banner = $(".rooms-banner", this.$el);

            $banner.html(banner);
        }
        this.chattoBottom();
    },

    getActiveContent: function() {
        return _.first(
            _.where(this.content.attributes, { active: true })
        );
    },

    generalChatStart: function (rid) {
        var self = this,
            content = this.content.get(rid);

        if(content) {
            this.activate(rid);
            return;
        }
        this.join(rid);
        this.content.once("change:" + rid, function() {
            self.activate(rid);
        });
    },

    sendMsg: function() {
        var $input = $(".message-input", this.$el);

        this.message(
            this.GENERAL_ROOM_ID, $input.html()
        );
        $input.html("").focus();
    },

    pasteSmile: function(smileCode) {
        var smileImg = this.smiles.getSmileImg(smileCode);
        this.pasteCode(smileImg);
        this.$('.message-input').focus();
    },

    pasteCode: function(code) {
        var $el = $('.message-input'),
            selection, node, $node, range, textRange;

        if (typeof document.body.createTextRange != "undefined") {
            textRange = document.body.createTextRange();
            textRange.moveToElementText($el[0]);
            textRange.collapse(false);
            textRange.select();
            if (textRange.pasteHTML) {
                textRange.pasteHTML(code);
            }
        } else if (typeof window.getSelection != "undefined") {
            selection = window.getSelection();
            node = selection.anchorNode;

            if(!node) {
                $el.focus();
                node = sel.anchorNode;
            }

            $node = $(node.nodeName == "#text" ? node.parentNode : node);

            if($node[0] != $el[0]) {
                $el.focus();
                range = document.createRange();
                range.selectNodeContents($el[0]);
                range.collapse(false);
                selection.removeAllRanges()
                selection.addRange(range);
            }

            document.execCommand('insertHTML', false, code);
        }
    },

    insertTo: function(value) {
        var $input = $(".message-input", this.$el);

        if (window.getSelection && !$.browser.msie) {
            var sel = window.getSelection(),
                node = sel.anchorNode,
                $node;

            if(!node) {
                $input.focus();
                node = sel.anchorNode;
            }
            $node = $(node.nodeName == "#text" ? node.parentNode : node);
            if ($node[0] != $input[0]) {
                var range = document.createRange();

                $input.focus();
                range.selectNodeContents($input[0]);
                range.collapse(false);
                sel.removeAllRanges();
                sel.addRange(range);
            }
            document.execCommand('insertHTML', false, value);
        }
        else if (document.body.createTextRange) {
            var textRange = document.body.createTextRange();

            textRange.moveToElementText($input[0]);
            textRange.collapse(false);
            textRange.select();
            if (textRange.pasteHTML) {
                textRange.pasteHTML(value);
            }
        }
    },

    ContactInfo: function(view) {
        var $el = $("<div/>", { "class": "profile-view rooms-info" }),
            delta = 10,
            timeout;

        $el
            .mouseout(hide)
            .mouseover(function() {
                clearTimeout(timeout);
            })
            .delegate(".private", "click", onPrivate)
            .delegate(".btn-favorite", "click", onFavorite);
        return {
            show: show,
            hide: hide
        };

        function show(uid, direction, prop) {
            var card = this.vCard.get(uid);

            if (!card || card.id === this.model.get("uid")) {
                return;
            }
            clearTimeout(timeout);
            timeout = setTimeout(_.bind(function() {
                this.$el.append(
                    $el.html(
                        this.former.info.call(this, card)
                    )
                );
                $el
                    .css({
                        top: prop.offset.top -
                                (($el.outerHeight() / 2) - delta),
                        left: prop.offset.left +
                                ("left" === direction
                                    ? prop.width + delta
                                    : ($el.outerWidth() + delta) * -1)
                    })
                    .removeClass("rigth left")
                    .addClass(direction);
            }, this), 500);
        }

        function hide() {
            clearTimeout(timeout);
            timeout = setTimeout(_.bind(function() {
                $el.detach();
            }, this), 200);
        }

        function onPrivate(event) {
            event.preventDefault();

            var card = view.vCard.get(
                $(event.target).data("contact-id")
            );

            if (!card) {
                return;
            }

            var $input = $(".message-input", view.$el),
                body = $input.html();

            $input.empty();
            view.insertTo(
                body.replace(/^(@\S+\s)?/, "@" + card.login + "&nbsp;")
            );
            hide();
        }

        function onFavorite(event) {
            event.preventDefault();
            var contactId = $(event.target).parent(".btn-favorite").data("favoritebutton");
            var card = view.vCard.get(contactId);
            if (!card) {
                return;
            }
            view.vCard.set(card.id, _.extend(card, {
                buttons: {
                    favorite: {
                        activated: true
                    }
                }
            }));
        }
    }
});
