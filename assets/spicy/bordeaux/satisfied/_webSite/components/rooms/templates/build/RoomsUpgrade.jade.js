this["JST"] = this["JST"] || {};

this["JST"]["RoomsUpgrade"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,content = locals_.content;
buf.push("<div class=\"upgrade\">" + (jade.escape(null == (jade_interp = _t("rooms", "text.readonly1")) ? "" : jade_interp)) + "<br/><a" + (jade.attr("href", content.body, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("rooms", "text.readonly2")) ? "" : jade_interp)) + "</a><br/>" + (jade.escape(null == (jade_interp = _t("rooms", "text.readonly3")) ? "" : jade_interp)) + "</div>");;return buf.join("");
};