this["JST"] = this["JST"] || {};

this["JST"]["RoomsInfo"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),userLink = locals_.userLink,content = locals_.content,_t = locals_._t;
buf.push("<div class=\"wrap\"><div><div class=\"photo-holder\"><a" + (jade.attr("href", userLink(content.id), true, false)) + " target=\"_blank\"><img" + (jade.attr("src", content.photos.url, true, false)) + " alt=\"\"/></a>");
if ( content.marks && content.marks.hotUser)
{
buf.push("<span class=\"hot\"></span>");
}
buf.push("</div><div class=\"info-holder\"><a" + (jade.attr("href", userLink(content.id), true, false)) + " target=\"_blank\" class=\"title\">" + (jade.escape(null == (jade_interp = content.login) ? "" : jade_interp)) + "</a><div class=\"years\">" + (jade.escape(null == (jade_interp = _t("rooms", "user_info.years", {"{n}": content.age})) ? "" : jade_interp)) + "</div>");
if ( content.geo)
{
buf.push("<div class=\"place\">");
if ( content.geo.distanceToMe)
{
buf.push((jade.escape(null == (jade_interp = content.geo.distanceToMe + " " +  _t("rooms", "user_info.miles_from_you")) ? "" : jade_interp)) + "<br/>");
}
buf.push("<span>" + (jade.escape(null == (jade_interp = content.geo.location) ? "" : jade_interp)) + "</span></div>");
}
buf.push("<div class=\"button-holder\"><div class=\"b-btn-activity\"><div class=\"b-btn-activity-grid\"><a href=\"#\"" + (jade.attr("data-contact-id", content.id, true, false)) + (jade.attr("title", _t('rooms', "button.message_in_chatroom"), true, false)) + " class=\"message-btn btn-activity btn-message private icon-Chat_action\"></a></div><div class=\"b-btn-activity-grid\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (content.id) + "')", true, false)) + (jade.attr("title", _t('rooms', "button.private_message"), true, false)) + " class=\"message-btn btn-activity btn-private-message icon-Mail\"></a></div><div class=\"b-btn-activity-grid\">");
var fav_status = (content.buttons.favorite.activated) ? "activated" : "";
buf.push("<a href=\"#\"" + (jade.attr("data-favoritebutton", content.id, true, false)) + (jade.attr("title", _t('rooms', "button.add_friend"), true, false)) + (jade.cls(['btn-activity','btn-favorite','icon-Favorite',fav_status], [null,null,null,true])) + "></a></div></div></div></div></div>");
if ( content.about)
{
buf.push("<div class=\"about\">" + (jade.escape(null == (jade_interp = content.about) ? "" : jade_interp)) + "</div>");
}
buf.push("</div>");;return buf.join("");
};