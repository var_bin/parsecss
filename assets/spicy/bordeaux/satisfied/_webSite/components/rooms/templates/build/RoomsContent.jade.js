this["JST"] = this["JST"] || {};

this["JST"]["RoomsContent"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),content = locals_.content,former = locals_.former;
buf.push("<h2 class=\"chatroom__title\">General chatroom</h2><div class=\"chat-contact-list chatroom__sidebar\">");
// iterate ["female", "male"]
;(function(){
  var $$obj = ["female", "male"];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var group = $$obj[$index];

var items = content.contacts[group] || []
buf.push("<div" + (jade.attr("style", !items.length ? "display: none;" : "", true, false)) + (jade.cls(['rooms-group',group,'chatroom__sex-group'], [null,true,null])) + "><a href=\"#\" class=\"active chatroom__sex-group__title\"><span>" + (jade.escape((jade_interp = group + "s ") == null ? '' : jade_interp)) + "</span><i class=\"count\">" + (jade.escape((jade_interp = items.length) == null ? '' : jade_interp)) + "</i></a><div class=\"chatroom__users\"><ul class=\"chatroom__users-list\">");
// iterate items
;(function(){
  var $$obj = items;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  }
}).call(this);

buf.push("</ul></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var group = $$obj[$index];

var items = content.contacts[group] || []
buf.push("<div" + (jade.attr("style", !items.length ? "display: none;" : "", true, false)) + (jade.cls(['rooms-group',group,'chatroom__sex-group'], [null,true,null])) + "><a href=\"#\" class=\"active chatroom__sex-group__title\"><span>" + (jade.escape((jade_interp = group + "s ") == null ? '' : jade_interp)) + "</span><i class=\"count\">" + (jade.escape((jade_interp = items.length) == null ? '' : jade_interp)) + "</i></a><div class=\"chatroom__users\"><ul class=\"chatroom__users-list\">");
// iterate items
;(function(){
  var $$obj = items;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push(null == (jade_interp = former.contact(item)) ? "" : jade_interp);
    }

  }
}).call(this);

buf.push("</ul></div></div>");
    }

  }
}).call(this);

buf.push("</div><div class=\"chat-list chatroom__mainside\"><div class=\"chatroom__chat-area-wrap\"><div class=\"chatroom__chat-area\"><div class=\"chatroom__chat-area__messages rooms-log scrollend\">");
// iterate content.messages || []
;(function(){
  var $$obj = content.messages || [];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var item = $$obj[$index];

buf.push(null == (jade_interp = former.message(item)) ? "" : jade_interp);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var item = $$obj[$index];

buf.push(null == (jade_interp = former.message(item)) ? "" : jade_interp);
    }

  }
}).call(this);

buf.push("</div><div class=\"rooms-banner\">");
if ( (content.restriction))
{
buf.push(null == (jade_interp = former[content.restriction.tpl](content.restriction.body)) ? "" : jade_interp);
}
buf.push("</div></div></div><div class=\"message-input-wrap\"><form><div class=\"message-input-thumb\"><img" + (jade.attr("src", content.userAvatar, true, false)) + " alt=\"avatar\"/></div><div class=\"message-input-container\"><div class=\"message-input-inner\"><div contenteditable=\"\" class=\"message-input\"></div><button type=\"submit\" class=\"message-submit\"><span class=\"icon-Icon_2\"></span></button></div></div></form></div></div>");;return buf.join("");
};