this["JST"] = this["JST"] || {};

this["JST"]["RoomsMessage"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),userLink = locals_.userLink,content = locals_.content,client = locals_.client;
buf.push("<div class=\"chat-message\"><a" + (jade.attr("href", userLink(content.uid), true, false)) + " class=\"chat-message__thumb\"><img" + (jade.attr("src", "" + (content.card.photos.url) + "", true, false)) + " alt=\"\"/></a><div" + (jade.cls(['chat-message__body',content.body.indexOf('@' + client.card.login + ' ') === 0 ? "personal" : ""], [null,true])) + "><a" + (jade.attr("data-contact-id", content.uid, true, false)) + " data-contact-direction=\"left\" class=\"chat-message__title\">" + (jade.escape((jade_interp = content.card.login) == null ? '' : jade_interp)) + "</a><div class=\"chat-message__text\">" + (((jade_interp = content.body) == null ? '' : jade_interp)) + "</div></div></div>");;return buf.join("");
};