var RoomsModel = Backbone.Model.extend({
    urlRoot: "/rooms",
    defaults: function() {
        return {
            uid: "",
            tabs: ["rooms"],
            historySize: 100,
            userAvatar: app.appData().get('user').photoUrl
        };
    }
});
