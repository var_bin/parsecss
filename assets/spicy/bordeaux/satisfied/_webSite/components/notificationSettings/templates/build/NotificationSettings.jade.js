this["JST"] = this["JST"] || {};

this["JST"]["NotificationSettings"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,settings = locals_.settings;
buf.push("<div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--1em\">" + (jade.escape(null == (jade_interp = _t("notificationSettings", "title.new_activity_notifications")) ? "" : jade_interp)) + "<span class=\"changes-status changes-status--save save-status inactive\"><span class=\"icon-status icon-Check\"></span>" + (jade.escape(null == (jade_interp = _t("notificationSettings", "text.saved")) ? "" : jade_interp)) + "</span></div></div><div class=\"b-profile-feed-middle-content\"><form class=\"b-profile-feed-middle-content__form\"><div class=\"b-profile-feed-middle-content__form_field b-profile-feed-middle-content__form_field--activity_form\">");
// iterate settings
;(function(){
  var $$obj = settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

var id = name.replace(/\W/g, '');
buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", '' + (id) + '', true, false)) + " data-change-settings=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (id) + "", true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationSettings", "title.new_activity_notifications_" + name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

var id = name.replace(/\W/g, '');
buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", '' + (id) + '', true, false)) + " data-change-settings=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (id) + "", true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationSettings", "title.new_activity_notifications_" + name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  }
}).call(this);

    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

// iterate setting
;(function(){
  var $$obj = setting;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

var id = name.replace(/\W/g, '');
buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", '' + (id) + '', true, false)) + " data-change-settings=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (id) + "", true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationSettings", "title.new_activity_notifications_" + name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

var id = name.replace(/\W/g, '');
buf.push("<div class=\"form_field__input form_field__input--activity_form\"><div class=\"form_field__input-b relative\"><input type=\"checkbox\" data-checkbox=\"1\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", '' + (id) + '', true, false)) + " data-change-settings=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (id) + "", true, false)) + " class=\"checkbox-item checkbox-item--activity\"><span class=\"checkbox-item--show\"></span><div class=\"checkbox-item__somelabel\">" + (jade.escape(null == (jade_interp = _t("notificationSettings", "title.new_activity_notifications_" + name)) ? "" : jade_interp)) + "</div></label></div></div>");
    }

  }
}).call(this);

    }

  }
}).call(this);

buf.push("</div></form></div>");;return buf.join("");
};