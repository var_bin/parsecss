$(document).on("click.favoriteClick", "[data-favoritebutton]", function(eventObject) {
    var $elem = $(eventObject.currentTarget);
    if(!$elem.hasClass("loading")) {
        var curUserId = $elem.attr("data-favoritebutton"),
            model;
        $elem.addClass("loading");
        if($elem.hasClass("activated")) {
            $("[data-favoritebutton='"+curUserId+"']").addClass("loading");

            model = new FavoriteModel({"userId":curUserId});
            model.doUnFavoriteAction({
                "error":   function(){
                    $("[data-favoritebutton='"+curUserId+"']").addClass("activated").removeClass("loading");
                },
                "success": function(){
                    $("[data-favoritebutton='"+curUserId+"']").removeClass("activated").removeClass("loading");
                }
            });
        } else {
            $("[data-favoritebutton='"+curUserId+"']").addClass("loading");

            model = new FavoriteModel();
            model.set({"userId":curUserId});
            model.doFavoriteAction({
                "error":   function(){
                    $("[data-favoritebutton='"+curUserId+"']").removeClass("activated").removeClass("loading");
                },
                "success": function(){
                    $("[data-favoritebutton='"+curUserId+"']").addClass("activated").removeClass("loading");
                    Backbone.trigger("TalksRecipient.Favorite", curUserId);
                }
            });
        }
    }
});
