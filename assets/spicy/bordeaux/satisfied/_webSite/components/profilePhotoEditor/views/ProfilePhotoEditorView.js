var ProfilePhotoEditorView = Backbone.View.extend({
    cropInit: 'true',

    events: {
        'click [data-close]': 'close',
        'click [data-save]': 'save'
    },

    regions: {
        photoGallery: '[data-gallery]'
    },

    initialize: function(options) {
        if(options && options.cropInit) {
            this.cropInit = options.cropInit;
        }

        this.listenTo(Backbone, 'ProfilePhotoPrimary.change', function(data) {
            this.region.ready();
            this.close();
        });

        this.listenTo(Backbone, 'ProfilePhotoPrimary.fail', function(data) {
            this.region.ready();
            this.$el.find('[data-error]').removeClass('inactive');
        });
    },

    render: function() {
        var modelData = this.model.toJSON();

        this.$el.empty();

        this.$el.html(tpl.render('ProfilePhotoEditor', modelData));

        if (modelData.normal && this.cropInit !== 'false') {
            this.initCrop(this.$('#profilePhotoImg'));
        }

        this.region.ready();

        return this;
    },

    initCrop: function($el) {
        var self = this,
            el = $el.get(0),
            handler = function() {
                $(this).Jcrop({
                    setSelect: self.model.getCoords(),
                    createHandles: [ 'nw', 'ne', 'se', 'sw' ],
                    aspectRatio: 1,
                    onSelect: $.proxy(self.setCoords, self), //$.proxy(self.model.setCoords, self.model),
                    onChange: $.proxy(self.showPreview, self),
                    trueSize: [ this.naturalWidth, this.naturalHeight ],
                    boxWidth: $el.width(),
                    boxHeight: $el.height(),
                });
                self.$el.find('.jcrop-holder').css({margin: 'auto'});
            };

        $el.one('load', handler).each(function() {
            if(this.complete) $(this).load();
        });
    },

    showPreview: function(coords) {
        if (parseInt(coords.w) > 0) {
            this.showThumb(coords, 150, '#profilePreviewProfile');
            this.showThumb(coords, 50, '#profilePreviewSearch');
        }
    },

    setCoords: function(coords) {
        this.showPreview(coords);
        this.model.setCoords(coords);
    },

    showThumb: function(coords, size, holder) {
        var rx = size / coords.w;
        var ry = size / coords.h;

        var imgWidth = this.$el.find('#profilePhotoImg').get(0).naturalWidth;
        var imgHeight = this.$el.find('#profilePhotoImg').get(0).naturalHeight;

        $(holder).css({
            width: Math.round(rx * imgWidth) + 'px',
            height: Math.round(ry * imgHeight) + 'px',
            marginLeft: '-' + Math.round(rx * coords.x) + 'px',
            marginTop: '-' + Math.round(ry * coords.y) + 'px'
        });
    },

    save: function() {
        this.region.loading();

        this.model.changePhoto();
    },

    close: function() {
        app.unshadowPopup();

        this.$el.empty();
    },
});
