this["JST"] = this["JST"] || {};

this["JST"]["NotificationSubscription"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,settings = locals_.settings;
buf.push("<div class=\"b-profile-feed-head head_table\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--1em head_table__div_table-cell\">" + (jade.escape(null == (jade_interp = _t("notificationSubscription", "title.email_sms_notification")) ? "" : jade_interp)) + "<span class=\"changes-status changes-status--save save-status inactive\"><span class=\"icon-status icon-Check\"></span>" + (jade.escape(null == (jade_interp = _t("notificationSubscription", "text.saved")) ? "" : jade_interp)) + "</span></div><div class=\"rightside-content-b-header-right head_table__div_table-cell\"><span class=\"rightside-content-b-header-right_opt\">" + (jade.escape(null == (jade_interp = _t("notificationSubscription", "title.email_or_sms_notification.email")) ? "" : jade_interp)) + "</span><span class=\"rightside-content-b-header-right_opt\">" + (jade.escape(null == (jade_interp = _t("notificationSubscription", "title.email_or_sms_notification.sms")) ? "" : jade_interp)) + "</span><span class=\"rightside-content-b-header-right_opt\">" + (jade.escape(null == (jade_interp = _t("notificationSubscription", "title.email_or_sms_notification.push")) ? "" : jade_interp)) + "</span></div></div><div class=\"b-profile-feed-middle-content setting_block__inner\"><ul class=\"setting_block__inner__profile-grid\">");
// iterate settings
;(function(){
  var $$obj = settings;
  if ('number' == typeof $$obj.length) {

    for (var name = 0, $$l = $$obj.length; name < $$l; name++) {
      var setting = $$obj[name];

buf.push("<li class=\"profile-grid__item profile-grid__item--notifications-tabel\"><div class=\"profile-grid__item__label profile-grid__item__label--choose_notification\">" + (jade.escape(null == (jade_interp = _t("notificationSubscription", "title.email_or_sms_notification_" + name)) ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__radio\">");
// iterate ["email", "mobile", "push"]
;(function(){
  var $$obj = ["email", "mobile", "push"];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

if (typeof setting[key] !== "undefined")
{
var id = name.replace(/\W/g, '');
buf.push("<div class=\"profile-grid__item__radio-block\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", "" + (key + '_' + id) + "", true, false)) + " data-change-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (key + '_' + id) + "", true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show\"></span></label></div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

if (typeof setting[key] !== "undefined")
{
var id = name.replace(/\W/g, '');
buf.push("<div class=\"profile-grid__item__radio-block\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", "" + (key + '_' + id) + "", true, false)) + " data-change-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (key + '_' + id) + "", true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show\"></span></label></div>");
}
    }

  }
}).call(this);

buf.push("</div></li>");
    }

  } else {
    var $$l = 0;
    for (var name in $$obj) {
      $$l++;      var setting = $$obj[name];

buf.push("<li class=\"profile-grid__item profile-grid__item--notifications-tabel\"><div class=\"profile-grid__item__label profile-grid__item__label--choose_notification\">" + (jade.escape(null == (jade_interp = _t("notificationSubscription", "title.email_or_sms_notification_" + name)) ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__radio\">");
// iterate ["email", "mobile", "push"]
;(function(){
  var $$obj = ["email", "mobile", "push"];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

if (typeof setting[key] !== "undefined")
{
var id = name.replace(/\W/g, '');
buf.push("<div class=\"profile-grid__item__radio-block\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", "" + (key + '_' + id) + "", true, false)) + " data-change-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (key + '_' + id) + "", true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show\"></span></label></div>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

if (typeof setting[key] !== "undefined")
{
var id = name.replace(/\W/g, '');
buf.push("<div class=\"profile-grid__item__radio-block\"><input data-checkbox=\"1\" type=\"checkbox\"" + (jade.attr("data-name", "" + (name) + "", true, false)) + (jade.attr("data-key", "" + (key) + "", true, false)) + (jade.attr("checked", (setting[key]) ? true : false, true, false)) + (jade.attr("id", "" + (key + '_' + id) + "", true, false)) + " data-change-subscription=\"1\" class=\"checkbox-item--hide\"/><label" + (jade.attr("for", "" + (key + '_' + id) + "", true, false)) + " class=\"checkbox-item\"><span class=\"checkbox-item--show\"></span></label></div>");
}
    }

  }
}).call(this);

buf.push("</div></li>");
    }

  }
}).call(this);

buf.push("</ul></div>");;return buf.join("");
};