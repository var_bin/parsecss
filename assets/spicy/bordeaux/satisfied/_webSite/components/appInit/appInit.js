window.app = new Backbone.App({
    regions: {
        content: "#layoutContent",
        activityNotification: "#activityNotification",
        leftMenu: "#leftMenu",
        header: "#header",
        footer: "#footer",
        userNav: "#userNav",
        naughtyMode: "#naughtyMode",
        popup: "#layoutPopup",
        popupShadow: "#popupShadow",
        searchForm: "#searchToolbar",
        ear: "#ear",
        talks: '#cTalks',
        rightside: '#rightSide'
    },

    showPopup: function(params) {
        this.regions.popup.render(PopupSimpleView, {
            model: new PopupSimpleModel({
                continueUrl : params.url
            }),
            template : params.template,
            data     : params
        });
    },

    shadowPopup: function() {
        this.regions.popupShadow.$el().removeClass('hidden');
    },

    unshadowPopup: function() {
        if (!this.regions.popupShadow.$el().hasClass('hidden')) {
            this.regions.popupShadow.$el().addClass('hidden');
        }
    },

    vodPopup: function(id) {
        var self = this;
        
        Resources.load('viewvod', function(){
            self.regions.popup.render(WallVideoView, {
                model: new VODScene(),
                id: id
            });
        });
    },

    ready: function() {
        var self = this;

        _.defer(function() {
            self.regions.userNav.render(UserNavView, {
                model: new UserNavModel()
            });
        });

        _.defer(function() {
            self.regions.activityNotification.render(ActivityNotificationView, {
                model: new ActivityNotificationModel()
            });
        }, 10);

        self.regions.header.render(HeaderView, {});
        self.regions.rightside.render(RightBlock, {});
        self.regions.leftMenu.render(LeftMenuView, {});
        self.regions.footer.render(FooterView, {
            data: app.appData().get("toolbar")
        });

        _.defer(function () {
            var talksMessengerConfig = app.appData().get('talksMessengerConfig');

            if (talksMessengerConfig && talksMessengerConfig.enabled) {
                self.regions.talks.render(TalksView, {
                    model: new TalksModel()
                });
            }
        });

        self.regions.rightside.render(RightBlock, {});

        self.regions.footer.render(FooterView, {   
            data: app.appData().get("toolbar")
        });
        
        self.regions.header.render(HeaderView, {   
        });
        
        self.regions.leftMenu.render(LeftMenuView, {   
        });
        
        Backbone.on("appPopup", function(data) {
            Resources.load("autoPopup", function() {
                self.regions.popup.render(PopupSimpleView, {
                    template: "AutoPopup",
                    data: data
                });
            });
        });
        new BannerOpenxView({
            model: new BannerOpenx({globalBanner: false})
        });
        new BannerOpenxView({
            model: new BannerOpenx({id: 'top,right', globalBanner: true}),
            container: '#bannerOpenx',
            success: function(resp){
                $(resp).show();
            }

        });
        new BannerPopunderView({
            model: new BannerPopunder,
            trackingModel: new BannerPopunderTracking
        });

        if (self.unreadActions) {
            self.unreadActions().fetch();
        }
    }
});

$(function() {
    app.initialize();

    $(document).on("click", "a[href^='/#']", function(event) {
        event.preventDefault();
        app.router.navigate($(this).attr("href"), {
            trigger: true
        });
    });
});
