var DEBUG = window.DEBUG || true;
var console = window.console || {};
console.log = (function() {
    var nativeLog = console.log || new Function();
    return function(msg, color) {
        if (!DEBUG || !window.console) return;
        if (typeof msg == 'object' || typeof msg == 'undefined') {
            nativeLog.apply(console, arguments);
            return;
        }
        var currentDate = new Date();
        var h = currentDate.getHours(),
            m = currentDate.getMinutes(),
            s = currentDate.getSeconds();
        if (h<10) h = '0'+h;
        if (m<10) m = '0'+m;
        if (s<10) s = '0'+s;
        var time = h + ":" + m + ":" + s;
        color = color || '#700';
        nativeLog.apply(
            console,
            ['%c[' + time + ']: %c' + msg.toString(), 'color: #777', 'font-weight: bold; color:' + color]
        );
    };
})();
var log = console.log; // short form

var searchController = new Backbone.Controller({
    name: "Search",

    initialize: function() {
        this.region = app.regions.content;

        $(window).scroll(function (){
            if ($(this).scrollTop() > 200){
                $(".to-top").fadeIn();
            } else{
                $(".to-top").fadeOut();
            }
        });
    },

    indexAction: function(params) {
        var self = this;
        params = params || {};
        params = _.extend(params, {
            type: "search"
        });

        Resources.load("user");

        this.region.render(Backbone.View.extend({
            regions: {
                userList: "#userListContainer",
                flirtcast: "#userListFlirtcast",
                footer: "#userListFooter"
            },

            initialize: function () {
                this.listenTo(Backbone, "Search.CurrentType", this.searchType);
            },

            events: {
                "click #userListSort [data-item]" : function (event) {
                    var elm = $(event.currentTarget);
                    var name = elm.data('item');
                    Backbone.trigger("Search.Type", {searchType: name, status: false});
                },

                "click .to-top" : "scroll"
            },

            render: function() {
                this.$el.html(tpl.render("UserListLayout", {type: self.name}));
                appUi.initElements(this.$el);
                this.$dom = {
                    item: this.$("[data-item]")
                };
                this.regions.userList.render(UserListView, {
                    loadingRegion: this.region,
                    parentRegions: this.regions,
                    model: new UserListModel({
                        query: params.query ? params.query : ""
                    }, {
                        collection: new UserListCollection()
                    }),
                    params: params
                });

                this.regions.flirtcast.render(FlirtcastView, {
                    model: new FlirtcastModel(),
                    params: params
                });

                return this;
            },

            searchType: function(type) {
                $el = this.$dom.item.filter("[data-item="+type+"]");
                $el.parent().addClass("active");
            },

            scroll: function() {
                $("body,html").animate({scrollTop:0}, 500);
                return false;
            }
        }));
    },

    modelAction: function (params) {
        $('#rightSide').hide();
        $('#layoutContent').addClass('widePage');

        var self = this;

        params = params || {};

        self.region.close();

        Resources.load(["modelSearch"], function () {
            if (params.room) {
                self.region.render(RoomView, {
                    model: new RoomModel(),
                    params: params
                });
            } else {
                self.region.render(GirlView, {
                    model: new GirlModel(),
                    modelFilter: new FilterModel(),
                    modelFilterMenu: new FilterMenuModel(),
                    params: params
                });
            }
        });
    }
});

var viewsController = new Backbone.Controller({
    name: "views",

    initialize: function () {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("activityTypes", function () {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: "#userListContainer"
                },

                render: function () {
                    this.$el.html(tpl.render("UserListLayout"));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: "views",
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var messengerController = new Backbone.Controller({
    name: "messenger",

    initialize: function () {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("activityTypes", function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: "#userListContainer",
                },

                render: function() {
                    this.$el.html(tpl.render("UserListLayout"));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: "messenger",
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var winksController = new Backbone.Controller({
    name: "winks",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("activityTypes", function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: "#userListContainer",
                },

                render: function() {
                    this.$el.html(tpl.render("UserListLayout"));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: "winks",
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var favoritesController = new Backbone.Controller({
    name: "favorites",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("activityTypes", function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: "#userListContainer",
                },

                render: function() {
                    this.$el.html(tpl.render("UserListLayout"));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: "favorites",
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var newsFeedController = new Backbone.Controller({
    name: "newsFeed",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("activityTypes", function() {
            self.region.render(Backbone.View.extend({
                regions: {
                    userList: "#userListContainer",
                },

                render: function() {
                    this.$el.html(tpl.render("UserListLayout"));

                    this.regions.userList.render(UserListView, {
                        loadingRegion: this.region,
                        model: new ActivityTypesModel({}, {
                            type: "newsFeed",
                            collection: new UserListCollection()
                        }),
                        params: params
                    });

                    return this;
                }
            }));
        });
    }
});

var billingHistoryController = new Backbone.Controller({
    name: "billingHistory",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("billingHistory", function() {
            self.region.render(BillingHistoryView, {
                model: new BillingHistoryModel(),
                params: params
            });
        });
    }
});

var myprofileController = new Backbone.Controller({
    name: "MyProfile",

    initialize: function() {
        this.region = app.regions.content;
    },

    renderTemplate: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load(["myprofile"], function() {
            self.region.render(MyProfileView, {
                params: params
            });
        });
    },

    indexAction: function(params) {
        this.renderTemplate(params);
    },

    openSettingsAction: function(params) {
        params = params || {};

        params.openSettings = true;
        this.renderTemplate(params);
    }
});

var userController = new Backbone.Controller({
    name: "user",

    initialize: function() {
        this.region = app.regions.content;
    },

    viewAction: function(params) {
        var self = this;

        this.region.close();

        Resources.load(["user", "profile", "chat", "popupBigPhoto"], function() {
            self.region.render(UserView, {
                model: new UserModel(),
                params: params
            });
        });
    },

    massBlockedAction: function(params) {
        var self = this;

        this.region.close();
        Resources.load("reCaptcha", function() {
            self.region.render(ReCaptchaView, {
                loadingRegion: self.region,
                model: new ReCaptchaModel(),
                params: params
            });
        });
    }
});

var siteController = new Backbone.Controller({
    name: "site",

    initialize: function() {
        this.region = app.regions.content;
    },

    contactUsAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("contactUs", function() {
            self.region.render(ContactUsView, {
                model: new ContactUsModel(),
                params: params
            });
        });
    }
});

var photoUploadMotivationController = new Backbone.Controller({
    name: "photoUploadMotivation",

    initialize: function() {
        this.region = app.regions.popup;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("popupPhotoUploadMotivation", function() {
            self.region.render(PopupPhotoUploadMotivationView, {
                model: new PopupPhotoUploadMotivationModel(),
                params: params
            });
        });
    }
});

var funnelController = new Backbone.Controller({
    name: "funnel",

    initialize: function() {
        this.region = app.regions.popup;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load("funnel", function() {
            //load wall controller
            wallController.run({action: "index"});
            self.region.render(FunnelView, {
                model: new FunnelModel(),
                params: params
            });
        });
    }
});

var photoController = new Backbone.Controller({
    name: 'photo',

    initialize: function() {
        this.region = app.regions.popup;
    },

    galleryAction: function(params) {
        var self = this;

        app.shadowPopup();
        params.open = params.open && parseInt(params.open) || 0;

        Resources.load('photoGallery', function() {
            self.region.render(UserPhotoGalleryView, {
                source: 'gallery',
                model: new UserPhotoGalleryModel({
                    id: params.id,
                    open: params.open
                })
            });

            if (params.id && !app.regions.content.isRendered()) {
                Resources.load('user', function() {
                    userController.run({
                        action: 'view',
                        options: { id: params.id }
                    });
                });
            }
        });
    },

    recordAction: function(params) {
        var self = this;

        app.shadowPopup();
        params.open = params.open && parseInt(params.open) || 0;

        Resources.load('photoGallery', function() {
            self.region.render(UserPhotoGalleryView, {
                source: 'wall',
                model: new UserPhotoGalleryModel({
                    id: params.id,
                    open: params.open
                })
            });

            if (!app.regions.content.isRendered()) {
                Resources.load('wall', function() {
                    wallController.run({
                        action: 'index',
                    });
                });
            }
        });
    }
});

var roomsController = new Backbone.Controller({
    name: "rooms",

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        this.region.close();
        Resources.load("rooms", _.bind(function() {
            this.region.render(RoomsView, {
                model: new RoomsModel(),
                params: params
            });
        }, this));
    }
});

var vodController = new Backbone.Controller({
    name: 'vod',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;
        params = params || {};
        this.region.close();
        Resources.load('vod', function() {
            self.region.render(VodView, {
                model: new VodModel(),
                params: params
            });
        });
    }
});

var wallController = new Backbone.Controller({
    name: 'wall',

    initialize: function() {
        this.region = app.regions.content;

        $(window).scroll(function (){
            if ($(this).scrollTop() > 200){
                $(".to-top").fadeIn();
            } else{
                $(".to-top").fadeOut();
            }
        });
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('wall', function() {
            self.region.render(WallView, {
                model: new WallModel(),
                params: params
            });
        });
    }
});

var payController = new Backbone.Controller({
    name: 'pay',

    initialize: function() {
        this.region = app.regions.content;
        $('#rightSide').hide();
        $('#layoutContent').addClass('widePage');
    },

    productAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('pay', function() {
            self.region.render(PayView, {
                model: new PayModel(),
                params: params
            });
        });
    }
});

var peopleNearbyController = new Backbone.Controller({
    name: 'peopleNearby',

    initialize: function() {
        this.region = app.regions.content;

        $(window).scroll(function (){
            if ($(this).scrollTop() > 200){
                $(".to-top").fadeIn();
            } else{
                $(".to-top").fadeOut();
            }
        });
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        this.region.close();

        Resources.load('peopleNearby', function() {
            var searchFormData = app.appData().get("searchForm");

            self.region.render(NearbyView, {
                model: new NearbyModel(),
                modelSearchForm : new SearchFormModel(searchFormData),
                params: params
            });
        });
    }

});

var declineController = new Backbone.Controller({
    name: 'decline',

    initialize: function() {
        this.region = app.regions.content;
    },

    indexAction: function(params) {
        var self = this;

        params = params || {};

        app.regions.content.close();
        this.region.close();
        this.region.render(declineView, {});

    }

});