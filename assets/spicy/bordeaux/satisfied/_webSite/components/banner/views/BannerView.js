var BannerView = Backbone.View.extend({
    templateId: "#banner-empty-template",
    templateType: "BannerImage",

    initialize: function() {
        this.model.on("change", this.render, this);
        this.model.fetch();
    },

    render: function() {
        switch (this.model.get('type')) {
            case 'gif':
            case 'png':
            case 'jpg':
            case 'jpeg':
                this.templateId = "#banner-image-template";
                this.templateType = "BannerImage";
        }

        this.setElement(this.options.container);
        //this.template = _.template($(this.templateId).html());
        this.template = tpl.render(this.templateType, bannerParams);

        this.$el.html(this.template());

        var self = this;
        return this;
    }

});

var BannerOpenxView = Backbone.View.extend({
    
    templateId: "#openx-banner-template",
    templateType: "Banner",
    customSuccessCallback: [],

    initialize: function() {

        window.appStarted = window.appStarted || {}
        // array of banners zones on page
        this.bannersArr = new Array();
        // banners context to prevent showing more than one banner of every advertiser on page simultaneously
        this.contextParts = new Array();
        // is async zones loading 
        if ( this.options.asyncLoading !== "undefined" )
            this.asyncLoading = this.options.asyncLoading;
        else
            this.asyncLoading = false;
        this.model.on("change", this.render, this);
        if(!this.model.get('globalBanner')){
            this.listenTo(Backbone, 'BannerOpenx.process', this.processEvent);
        }else{
            this.listenTo(app.router, 'route', this.processEvent);
            BannerOpenxView.prototype.oxBannersViewObject = this;
        }
        this.model.fetch();
    },
    render: function() {
        this.bannersArr = this.model.attributes.zonesInfo;
        if ( this.asyncLoading )
        {
            for (attr in this.bannersArr)
            {
                this.drawBanner(this.bannersArr[attr]);
            }
        }
        else
        {
            this.showNextBanner();
        }
        return this;
    },
    processIframeResponse: function(response) {
        if ( response['zone_id'] > 0 && typeof response['div_id'] !== "undefined" ) {
            // adjust banner size
            if ( response['width'] > 0){
                $("."+response['div_id']+" iframe").css("width", response['width']);
            }
            if ( response['height'] > 0){
                $("."+response['div_id']+" iframe").css("height", response['height']);
            }
            // show banner containers
            $("."+response['div_id']).show();
            var containerId = 'container-for-'+response['div_id'];
            containerElement = $("#"+containerId);
            if ( typeof containerElement !== "undefined"){
                containerElement.show();
                var ids = (this.options.model.id+'').split(',');
                for(key in ids){
                    if($('#'+ids[key]+'-banner-widget').length)
                        $('#'+ids[key]+'-banner-widget').show();
                }
            }
            // check banner context
            if ( typeof response['context'] !== "undefined" ) {
                this.addContext(response['context']);
            }
            //custom success callback
            for(i in this.customSuccessCallback) {
                if ( typeof this.customSuccessCallback[i]  === 'function' ) {
                    this.customSuccessCallback[i](response);
                }
            }
        }
        this.showNextBanner();
    },
    showNextBanner: function() {
        if ( this.bannersArr && this.bannersArr.length )
        {
            var nextBanner = this.bannersArr.shift();
            this.drawBanner(nextBanner);
        }
        else
        {
            this.contextParts = new Array();
        }
    },
    drawBanner: function(bannerParams) {
        this.setElement(this.options.container+bannerParams.placement);
        bannerParams.ifrSrc = bannerParams.ifrSrc + "&context="+this.contextParts.join(";");
        bannerParams.ifrSrc = bannerParams.ifrSrc + "#" + window.location.protocol+"//"+window.location.hostname+"|"+bannerParams.divId;
        this.template = tpl.render(this.templateType, bannerParams);

        this.$el.html(this.template);
        var containerWithBannerPlacement = $("#container-for-openx-banner-placement-"+bannerParams.placement);
        if ( typeof containerWithBannerPlacement !== "undefined")
        {
            $(containerWithBannerPlacement).attr('id',"container-for-"+bannerParams.divId)
        }
    },
    addContext: function(context) {
        var contextFound = false;
        for ( i in this.contextParts ){
            if ( this.contextParts[i] === context ) {
                contextFound = true;
                break;
            }
        }
        if ( !contextFound ) {
            this.contextParts.push(context);
        }
    },
    processEvent: function(params) {
	if(params != undefined && params.id && params.container) {
            params.asyncLoading = !!params.asyncLoading;
            if(params.tpl)
                this.templateType = params.tpl;
            this.model.set({ 'id' : params.id });
            this.options.container = params.container;
            this.asyncLoading = params.asyncLoading;
            if ( params.asyncLoading !== "undefined" )
                this.options.asyncLoading = params.asyncLoading
            else
                this.asyncLoading = false;
            if(params.success)
                this.customSuccessCallback.push(params.success);
        
            BannerOpenxView.prototype.oxBannersViewObject = this;
            // array of banners zones on page
            
        }
        this.bannersArr = new Array();
            // banners context to prevent showing more than one banner of every advertiser on page simultaneously
            this.contextParts = new Array();
            this.model.fetch();
    }
});

var BannerPopunderView = Backbone.View.extend({

    bannerUrl: false,

    initialize: function() {
        this.model.on("change", this.processPopunder, this);
        this.model.fetch();
    },

    processPopunder: function() {
        var bannerUrl = this.bannerUrl || this.model.get("bannerUrl"),
            tracking = this.options.trackingModel,
            self = this;
        if (bannerUrl) {
            $(document.body).one("click", function(event) {
                var ignoreTags = ['a'];
                if (event.target.tagName
                    && $.inArray(event.target.tagName.toLowerCase(), ignoreTags) !== -1) {
                        self.bannerUrl = bannerUrl;
                        self.processPopunder();
                        return;
                }
                hotpop.settings.link = bannerUrl;
                hotpop.init();
                tracking.save({}, {type: 'POST'});
            });
        }
    }
});
