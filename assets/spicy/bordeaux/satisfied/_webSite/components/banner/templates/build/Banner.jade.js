this["JST"] = this["JST"] || {};

this["JST"]["Banner"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),divId = locals_.divId,iframeId = locals_.iframeId,ifrSrc = locals_.ifrSrc,width = locals_.width,height = locals_.height;
buf.push("<div><div style=\"display: none;\"" + (jade.cls(["" + (divId) + ""], [true])) + "><iframe" + (jade.attr("id", (iframeId), true, false)) + " name=\"openx-iframe\"" + (jade.attr("src", (ifrSrc), true, false)) + " marginWidth=\"0\" marginHeight=\"0\" hspace=\"0\" vspace=\"0\" frameSpacing=\"0\" frameBorder=\"0\" scrolling=\"no\"" + (jade.attr("width", (width), true, false)) + (jade.attr("height", (height), true, false)) + (jade.cls(["openx-banner-iframe " + (iframeId) + ""], [true])) + "></iframe></div></div>");;return buf.join("");
};