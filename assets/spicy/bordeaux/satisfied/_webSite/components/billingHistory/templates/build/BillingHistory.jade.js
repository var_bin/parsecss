this["JST"] = this["JST"] || {};

this["JST"]["BillingHistory"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,paymentStatus = locals_.paymentStatus,subscriptions = locals_.subscriptions,simpleCancelStatus = locals_.simpleCancelStatus,paymentsHistory = locals_.paymentsHistory;
buf.push("<div class=\"b-wrap-content billing\"><div class=\"billing-b-wrap\"><div class=\"billing-b-wrap-title\">" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.billing_history")) ? "" : jade_interp)) + "</div><div class=\"billing-b-content\"><div class=\"billing-b-content-label\">" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.account_status") + ":") ? "" : jade_interp)) + "</div><div class=\"billing-b-content-status\">" + (jade.escape(null == (jade_interp = _t("billingHistory", "value.membership_status_" + paymentStatus.type)) ? "" : jade_interp)) + "</div></div>");
if ((paymentStatus.type == 'free'))
{
buf.push("<div class=\"billingPageTopButton\"><a" + (jade.attr("href", paymentStatus.url, true, false)) + " class=\"btn btn-text-orange\">" + (jade.escape(null == (jade_interp = _t("billingHistory", "button.upgrade_account")) ? "" : jade_interp)) + "</a></div>");
}
buf.push("</div>");
if ( subscriptions.length > 0)
{
buf.push("<div class=\"billing-b-wrap\"><div class=\"billing-b-wrap-title\">" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.subscriptions") + ":") ? "" : jade_interp)) + "</div><div class=\"billing-b-wrap-items\">");
// iterate subscriptions
;(function(){
  var $$obj = subscriptions;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var subscription = $$obj[$index];

buf.push("<div class=\"billing-b-content-subc\">");
var externalResourceName = subscription.externalResourceName ? " " + subscription.externalResourceName : ""
buf.push("<div class=\"billing-b-content-label-td\">" + (jade.escape(null == (jade_interp = subscription.title + externalResourceName) ? "" : jade_interp)) + "</div><div class=\"billing-b-content-label-td\">" + (jade.escape(null == (jade_interp = subscription.created_at) ? "" : jade_interp)) + "</div><div class=\"billing-b-content-label-td link\">");
var id = "deactivate-subscription"
if ((simpleCancelStatus))
{
id = "simple-deactivate-subscription"
}
buf.push("<a" + (jade.attr("id", id, true, false)) + (jade.attr("data-subscription-id", subscription.autoincrement_id, true, false)) + (jade.attr("data-order-id", subscription.parent_id, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("billingHistory", "button.deactivate_subscription")) ? "" : jade_interp)) + "</a></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var subscription = $$obj[$index];

buf.push("<div class=\"billing-b-content-subc\">");
var externalResourceName = subscription.externalResourceName ? " " + subscription.externalResourceName : ""
buf.push("<div class=\"billing-b-content-label-td\">" + (jade.escape(null == (jade_interp = subscription.title + externalResourceName) ? "" : jade_interp)) + "</div><div class=\"billing-b-content-label-td\">" + (jade.escape(null == (jade_interp = subscription.created_at) ? "" : jade_interp)) + "</div><div class=\"billing-b-content-label-td link\">");
var id = "deactivate-subscription"
if ((simpleCancelStatus))
{
id = "simple-deactivate-subscription"
}
buf.push("<a" + (jade.attr("id", id, true, false)) + (jade.attr("data-subscription-id", subscription.autoincrement_id, true, false)) + (jade.attr("data-order-id", subscription.parent_id, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("billingHistory", "button.deactivate_subscription")) ? "" : jade_interp)) + "</a></div></div>");
    }

  }
}).call(this);

buf.push("</div></div>");
}
if ( paymentsHistory.length)
{
buf.push("<div class=\"billing-b-wrap\"><div class=\"billing-b-wrap-title\"><b>" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.payment_history") + ":") ? "" : jade_interp)) + "</b></div><div class=\"billing-b-content-subc-overflow\"><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" class=\"billing-b-content-subc-table\"><tr class=\"billing-b-content-subc-table-title\"><td>" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.paid_for")) ? "" : jade_interp)) + "</td><td>" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.amount")) ? "" : jade_interp)) + "</td><td>" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.date")) ? "" : jade_interp)) + "</td><td>" + (jade.escape(null == (jade_interp = _t("billingHistory", "title.status")) ? "" : jade_interp)) + "</td></tr>");
// iterate paymentsHistory
;(function(){
  var $$obj = paymentsHistory;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var payment = $$obj[key];

buf.push("<tr class=\"billing-b-content-subc-table-item\">");
var externalResourceName = payment.externalResourceName ? " " + payment.externalResourceName : ""
buf.push("<td class=\"nowrap\">" + (jade.escape(null == (jade_interp = _t("billingHistory", "text." + payment.paid, {'number': payment.numPartialCharge}) + externalResourceName) ? "" : jade_interp)) + "</td><td>" + (null == (jade_interp = payment["amount"]) ? "" : jade_interp) + "</td><td>" + (jade.escape(null == (jade_interp = payment["add_date"]) ? "" : jade_interp)) + "</td><td>" + (jade.escape(null == (jade_interp = _t("billingHistory", "value." + payment.status)) ? "" : jade_interp)) + "</td></tr>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var payment = $$obj[key];

buf.push("<tr class=\"billing-b-content-subc-table-item\">");
var externalResourceName = payment.externalResourceName ? " " + payment.externalResourceName : ""
buf.push("<td class=\"nowrap\">" + (jade.escape(null == (jade_interp = _t("billingHistory", "text." + payment.paid, {'number': payment.numPartialCharge}) + externalResourceName) ? "" : jade_interp)) + "</td><td>" + (null == (jade_interp = payment["amount"]) ? "" : jade_interp) + "</td><td>" + (jade.escape(null == (jade_interp = payment["add_date"]) ? "" : jade_interp)) + "</td><td>" + (jade.escape(null == (jade_interp = _t("billingHistory", "value." + payment.status)) ? "" : jade_interp)) + "</td></tr>");
    }

  }
}).call(this);

buf.push("</table></div></div>");
}
buf.push("</div>");;return buf.join("");
};