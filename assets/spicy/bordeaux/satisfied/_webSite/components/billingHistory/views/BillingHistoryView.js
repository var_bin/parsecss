var BillingHistoryView = Backbone.View.extend({

    events: {
        'click #deactivate-subscription': function(event) {
            event.preventDefault();
            var orderId = $(event.currentTarget).attr('data-order-id');
            Resources.load(['stopSubscription'], function() {
                app.regions.popup.render(StopSubscriptionView, {
                    model: new StopSubscriptionModel(),
                    orderId: orderId
                });
            });
        }
    },

    initialize: function() {
        this.model.fetch();
        this.listenTo(this.model, 'change', this.renderBillingHistory);
        this.listenTo(Backbone, 'BillingHistory.SwitchStatus', this.statusSwitcher);
    },

    renderBillingHistory: function() {
        this.$el.html(tpl.render('BillingHistory', this.model.toJSON()));

        this.region.ready();

        return this;
    },

    statusSwitcher: function(params) {
        var elem = this.$('[data-subscription-table]').find('[data-order-id=' + params.orderId + ']');
        var parent = elem.parent();
        var subscriptionText = $('<span>').attr('data-order-id', params.orderId);
        if (params.status === true) {
            subscriptionText.text($.t('billingHistory', 'value.activated'));
        }
        else {
            subscriptionText.text($.t('billingHistory', 'value.deactivated'));
        }
        elem.remove();
        parent.html(subscriptionText);
    }
});
