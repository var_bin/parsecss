var PopupPhotoUploadMotivationView = Backbone.View.extend({
    params: {
        returnUrl             : '',
        needToSetPrimaryPhoto : false
    },

    regions: {
        dragDrop:         '#PhotoMotivationDragDrop',
        profilePhotoList: '#profilePhotoList'
    },

    events: {
        'click #PhotoMotivationClose': 'close',
        'click #PhotoMotivationSave':  'saveBtnAction',
        'click #PhotoMotivationBack':  'goBack'
    },

    initialize: function(options) {
        // Turn off notifications on this page.
        app.appData().messanger.setup({
            declineActive: true
        });

        if (options.params && options.params.source) {
            if (options.params.source === 'funnel') {
                this.params.returnUrl = '/funnel';
            }
        }
        var self = this;
        this.model.fetch({
            success: function(model) {
                self.renderPopup();
            }
        });

        this.changeModel = new ProfilePhotoChangeModel();

        Backbone.on('UploadProfilePhoto.start', function() {
            self.$('#photoMotivationSearchSamplesList').hide();
        });
        Backbone.on('UploadProfilePhoto.complete', function(data) {
            if (data.status && data.status === 'done') {
                if (self.params.needToSetPrimaryPhoto) {
                    app.appData().changeProfilePhoto({photoUrl: data.data.avatar});
                    self.params.needToSetPrimaryPhoto = false;
                }
            } else {
                self.$('#photoMotivationSearchSamplesList').show();
            }
        });
        Backbone.on('ProfilePhotoPrimary.change', function(data) {
            self.changeModel.set({
                file: data.id
            });
        });
    },

    remove: function() {
        // Revert turning off notification on this page.
        app.appData().messanger.setup({
            declineActive: false
        });
    },

    render: function() {
        return this;
    },

    renderPopup: function() {
        var that = this;
        var renderData = this.model.toJSON();
        var primary = (renderData.myPhotos && renderData.myPhotos.primary) ? renderData.myPhotos.primary : {};
        renderData.params = {
            returnUrl: this.params.returnUrl
        };
        this.$el.html(tpl.render(
            'PopupPhotoUploadMotivation',
            renderData
        ));

        this.regions.dragDrop.render(ProfilePhotoUploadView, {
            model: new ProfilePhotoUploadModel({
                myPhotos: renderData.myPhotos
            }),
            useFbPhotos: false,
            fbAppSettings: this.model.get('fbAppSettings'),
            type: 'photoUploadMotivation'
        });

        this.params.needToSetPrimaryPhoto = renderData.myPhotos.primary.id ? false : true;

        _.defer(function() {
            that.regions.profilePhotoList.render(ProfilePhotoListView, {
                collection: new ProfilePhotoListCollection(_.values(renderData.myPhotos && renderData.myPhotos.gallery || {}), {
                    primary: primary
                }),
                type: 'photoUploadMotivation'
            });
        });

        return this;
    },

    saveBtnAction: function() {
        Backbone.trigger(
            'PopupPhotoUploadMotivationView.saveBtnAction'
        );
        return this.close();
    },

    close: function() {
        this.region.close();
        this.params.returnUrl = '';
        app.router.navigate('/wall', {trigger: true});
        return false;
    },

    goBack: function() {
        if (this.params.returnUrl) {
            app.router.navigate(this.params.returnUrl, {trigger: true});
        }
        return false;
    }
});
