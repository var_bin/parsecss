this["JST"] = this["JST"] || {};

this["JST"]["PopupPhotoUploadMotivation"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,users = locals_.users,user = locals_.user,params = locals_.params;
buf.push("<div class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\"><div id=\"funnel-popup-item\" class=\"b-popup b-popup-funnel step-2\"><div class=\"b-popup-content\"><a id=\"PhotoMotivationClose\" href=\"#\"" + (jade.attr("title", _t("popupPhotoUploadMotivation", "button.close"), true, false)) + " class=\"voronka_pop_up__close\"><i class=\"icon-cross\"></i></a><div class=\"voronka_pop_up__container\"><h2 class=\"voronka-main-title\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "title.upload_photo")) ? "" : jade_interp)) + "</h2><div class=\"voronka_pop_up__content\"><div class=\"voronka_pop_up__finish\"><div class=\"voronka_pop_up__attention\"><div class=\"voronka_pop_up__attention__title\">Please pay attention:</div><ul class=\"voronka_pop_up__attention__list\"><li class=\"voronka_pop_up__attention__list__item\">Only <b>members with photos appear </b>in search results<i class=\"icon-Check\"></i></li><li class=\"voronka_pop_up__attention__list__item\"><b>Women </b>won’t answer your messages if they <b>don’t see your photo</b><i class=\"icon-Check\"></i></li></ul></div><div class=\"voronka_pop_up__drug-drop-wrap\"><div id=\"PhotoMotivationDragDrop\"></div></div></div><div id=\"fbPhotoUploaderList\"></div><div id=\"profilePhotoList\" class=\"voronka_pop_up__search-results\"></div><div class=\"voronka_pop_up__search-results\"><ul class=\"rightside__grid--voronka\">");
if ( users)
{
// iterate [0,1,2,3]
;(function(){
  var $$obj = [0,1,2,3];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var key = $$obj[$index];

if ( users[key])
{
user = users[key]
{
buf.push("<li class=\"rightside__grid-item--voronka\"><a href=\"#\" class=\"rightside__grid-item-link\"><img" + (jade.attr("src", user.photos.url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"rightside__grid-item-link-img\"/>");
if ( (user.photos.count))
{
buf.push("<div class=\"rightside__grid-item--photo-counter rightside__grid-item--photo-counter--landing\"><span class=\"rightside__grid-item--photo-counter-icon icon-Photo\"></span>" + (jade.escape(null == (jade_interp = user.photos.count) ? "" : jade_interp)) + "</div>");
}
buf.push("</a><div class=\"rightside__grid-item-profile-b-info\"><a href=\"#\" class=\"grid-item-profile-b-info__name\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</a><div class=\"grid-item-profile-b-info__age\"><i class=\"grid-item-profile-b-info__age-icon icon-Calendar\">" + (jade.escape(null == (jade_interp = user.age) ? "" : jade_interp)) + "</i>years      </div></div></li>");
}
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var key = $$obj[$index];

if ( users[key])
{
user = users[key]
{
buf.push("<li class=\"rightside__grid-item--voronka\"><a href=\"#\" class=\"rightside__grid-item-link\"><img" + (jade.attr("src", user.photos.url, true, false)) + (jade.attr("alt", user.login, true, false)) + " class=\"rightside__grid-item-link-img\"/>");
if ( (user.photos.count))
{
buf.push("<div class=\"rightside__grid-item--photo-counter rightside__grid-item--photo-counter--landing\"><span class=\"rightside__grid-item--photo-counter-icon icon-Photo\"></span>" + (jade.escape(null == (jade_interp = user.photos.count) ? "" : jade_interp)) + "</div>");
}
buf.push("</a><div class=\"rightside__grid-item-profile-b-info\"><a href=\"#\" class=\"grid-item-profile-b-info__name\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</a><div class=\"grid-item-profile-b-info__age\"><i class=\"grid-item-profile-b-info__age-icon icon-Calendar\">" + (jade.escape(null == (jade_interp = user.age) ? "" : jade_interp)) + "</i>years      </div></div></li>");
}
}
    }

  }
}).call(this);

}
buf.push("</ul></div><div class=\"voronka_pop_up__footer\"><a id=\"PhotoMotivationSave\" class=\"voronka_pop_up__voronka-button voronka_pop_up__voronka-button--small voronka_pop_up__voronka-button--blue voronka_pop_up__voronka-button--right\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "button.save")) ? "" : jade_interp)) + "</a>");
if ( params.returnUrl)
{
buf.push("<a id=\"PhotoMotivationBack\" class=\"voronka_pop_up__voronka-button voronka_pop_up__voronka-button--small voronka_pop_up__voronka-button--gray voronka_pop_up__voronka-button--left\">" + (jade.escape(null == (jade_interp = _t("popupPhotoUploadMotivation", "button.back")) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div></div></div></div></div><div class=\"b-loader\"></div></div>");;return buf.join("");
};