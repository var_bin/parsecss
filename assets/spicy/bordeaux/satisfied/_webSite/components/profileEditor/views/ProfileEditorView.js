var ProfileEditorView = Backbone.View.extend({

    events: {
        'keyup #status, #about': function(event) {
            this.model.pressButtonDetected = true;
        },
        'paste #status, #about': function(event) {
            this.model.copyPasteDetected = true;
        },
        'keyup .input-text-attr': function(event) {
            $(event.target).parents('.b-wrap-content').find('.action-status-b').show();
        },
        "click [data-reset-btn]": function(event) {
            event.preventDefault();
            this.resetField($(event.target).data('reset-btn'));
        },
        "click [data-save-btn]": function(event) {
            event.preventDefault();
            this.saveField($(event.target).data('save-btn'))
        }
    },

    fields: {},

    getFormParams: function() {
        return {
            dateFormat: this.model.get('dateFormat'),
            years: this.model.getAvailableYears()
        };
    },

    render: function() {
        this.$el.html(tpl.render('ProfileEditor', {
            profile: this.model.attributes,
            formParams: this.getFormParams()
        }));

        this.fields = {};
        var self = this;

        this.$('[data-ui-input]').each(function() {
            var fieldName = $(this).find('input').attr('name');
            self.fields[fieldName] = $(this).uiInput();
        });

        this.region.ready();

        return this;
    },

    resetForm: function() {
        this.model.set(this.model.previousAttributes(), {silent: true});
        this.render();
    },

    resetField: function(formField) {
        var uiField = this.fields[formField].getInstance();
        if (uiField) {
            uiField.markAsNotError();
            uiField.setVal(this.model.get(formField));
            uiField.$parent.find('.action-status-b').hide();
        }
    },

    saveFormData: function() {
        this.region.saving();

        this.model.needValidateAttr = true;

        var self = this;

        var updateData = {};
        for (var curFieldName in self.fields) {
            var uiField = self.fields[curFieldName].getInstance();
            updateData[curFieldName] = uiField.getVal();
        }

        if (this.model.copyPasteDetected) {
            updateData.copyPasteDetected = 1;
        } else {
            updateData.copyPasteDetected = 0;
        }

        if (this.model.pressButtonDetected) {
            updateData.pressButtonDetected = 1;
        } else {
            updateData.pressButtonDetected = 0;
        }

        this.model.once('invalid', function(model, error) {
            for (var curFieldName in self.fields) {
                if (curFieldName in error) {
                    var errorsList = [];
                    if (_.isArray(error[curFieldName])) {
                        errorsList = error[curFieldName];
                    } else if (_.isString(error[curFieldName])) {
                        errorsList.push(error[curFieldName]);
                    }
                    self.fields[curFieldName].getInstance().markAsError(errorsList);
                } else {
                    self.fields[curFieldName].getInstance().markAsNotError();
                }
            }
            self.region.ready();
        });

        if (updateData.description) {
            updateData.description = updateData.description.replace(/<(?:.|\n)*?>/gm, '');
            $('[name=description]').val(updateData.description);
        }

        if (updateData.chat_up_line) {
            updateData.chat_up_line = updateData.chat_up_line.replace(/<(?:.|\n)*?>/gm, '');
            $('[name=chat_up_line]').val(updateData.chat_up_line);
        }

        if (!$.isEmptyObject(app.csrfToken)) {
            updateData[app.csrfToken.name] = app.csrfToken.value;
        }

        this.model.save(updateData, {
            type: 'POST',
            data: updateData,
            success: function(model, response) {
                for (var curFieldName in self.fields) {
                    self.fields[curFieldName].getInstance().markAsNotError();
                }
                self.region.ready();
            },
            error: function(model, response) {
                var curFieldName;
                if (typeof response === 'object') {
                    for (curFieldName in self.fields) {
                        if (curFieldName in response) {
                            self.fields[curFieldName].getInstance().markAsError(response[curFieldName]);
                        } else {
                            self.fields[curFieldName].getInstance().markAsNotError();
                        }
                    }
                } else {
                    for (curFieldName in self.fields) {
                        self.fields[curFieldName].getInstance().markAsError();
                    }
                }
                self.region.ready();
            }
        });
    },

    saveField: function(formField) {
        this.region.saving();

        this.model.needValidateAttr = true;

        var self = this;

        var updateData = {};

        var uiField = self.fields[formField].getInstance();
        updateData[formField] = uiField.getVal();

        if (this.model.copyPasteDetected) {
            updateData.copyPasteDetected = 1;
        } else {
            updateData.copyPasteDetected = 0;
        }

        if (this.model.pressButtonDetected) {
            updateData.pressButtonDetected = 1;
        } else {
            updateData.pressButtonDetected = 0;
        }

        this.model.once('invalid', function(model, error) {
            if (formField in error) {
                var errorsList = [];
                if (_.isArray(error[formField])) {
                    errorsList = error[formField];
                } else if (_.isString(error[formField])) {
                    errorsList.push(error[formField]);
                }
                self.fields[formField].getInstance().markAsError(errorsList);
            } else {
                self.fields[formField].getInstance().markAsNotError();
            }
            self.region.ready();
        });

        if (updateData.description) {
            updateData.description = updateData.description.replace(/<(?:.|\n)*?>/gm, '');
            $('[name=description]').val(updateData.description);
        }

        if (updateData.chat_up_line) {
            updateData.chat_up_line = updateData.chat_up_line.replace(/<(?:.|\n)*?>/gm, '');
            $('[name=chat_up_line]').val(updateData.chat_up_line);
        }

        if (!$.isEmptyObject(app.csrfToken)) {
            updateData[app.csrfToken.name] = app.csrfToken.value;
        }

        this.model.save(updateData, {
            type: 'POST',
            data: updateData,
            success: function(model, response) {
                self.fields[formField].getInstance().markAsNotError();
                self.fields[formField].getInstance().$parent.find('.action-status-b').hide();
                Backbone.trigger('ProfileEditor.change', model.toJSON());
                self.region.ready();
            },
            error: function(model, response) {
                if (typeof response === 'object') {
                    if (formField in response) {
                        self.fields[curFieldName].getInstance().markAsError(response[curFieldName]);
                    } else {
                        self.fields[curFieldName].getInstance().markAsNotError();
                    }
                } else {
                    self.fields[formField].getInstance().markAsError();
                }
                self.region.ready();
            }
        });
    }
});
