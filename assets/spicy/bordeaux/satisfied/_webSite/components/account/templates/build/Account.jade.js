this["JST"] = this["JST"] || {};

this["JST"]["Account"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,paymentStatus = locals_.paymentStatus,upgradeButton = locals_.upgradeButton,userId = locals_.userId,email = locals_.email,l10n = locals_.l10n,hasBillingHistory = locals_.hasBillingHistory;
buf.push("<div class=\"b-feed__content b-feed-content b-upload-content\"><div class=\"b-wrap-content b-wrap-content--myprofile b-feed-content__profile-feed\"><div class=\"b-profile-feed-head\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--1em\">" + (jade.escape(null == (jade_interp = _t("account", "title.my_settings")) ? "" : jade_interp)) + "<span data-message=\"result\" class=\"changes-status changes-status--save inactive\"><span class=\"icon-status icon-Check\"></span><span data-message-text=\"result\"></span></span><span data-message=\"error\" class=\"changes-status changes-status--error inactive\"><span class=\"icon-status icon-Attention\"></span><span data-message-text=\"error\"></span></span></div></div><div class=\"b-profile-feed-middle-content setting_block__inner\"><div class=\"setting_block__inner__profile-grid\"><div class=\"profile-grid__item\"><div class=\"profile-grid__item__label\">" + (jade.escape(null == (jade_interp = _t("account", "title.membership_status") + ':') ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__status\">" + (jade.escape(null == (jade_interp = _t("account", "value.membership_status_" + paymentStatus.type)) ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__button\">");
if(upgradeButton.type == 'main' || upgradeButton.type == 'features')
{
buf.push("<a" + (jade.attr("href", "" + (upgradeButton.url) + "", true, false)) + " class=\"profile-grid__item__button__link\">" + (jade.escape(null == (jade_interp = _t("account", "button.upgrade_" + paymentStatus.type)) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("&nbsp;");
}
buf.push("</div></div><div class=\"profile-grid__item\"><div class=\"profile-grid__item__label\">" + (jade.escape(null == (jade_interp = _t("account", "title.user_id") + ':') ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__status\">" + (jade.escape((jade_interp = userId) == null ? '' : jade_interp)) + "</div><div class=\"profile-grid__item__button\">&nbsp;</div></div><div data-update=\"password\" class=\"profile-grid__item\"><div class=\"profile-grid__item--changed\"><div class=\"profile-grid__item__label\">" + (jade.escape(null == (jade_interp = _t("account", "title.change_password") + ':') ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__status\">***********</div><div class=\"profile-grid__item__edit\"><a data-change=\"password\" class=\"profile-grid__item__edit__link\">" + (jade.escape(null == (jade_interp = _t("account", "button.edit")) ? "" : jade_interp)) + "</a></div></div><div class=\"profile-grid__item--change inactive\"><div class=\"profile-grid__item-change-inputs\"><div class=\"item-change-inputs-wrap\"><input type=\"password\" data-password=\"old\"" + (jade.attr("placeholder", _t('account', 'title.old_password'), true, false)) + " class=\"item-change-inputs\"/><span data-show-password=\"1\" class=\"item-change-inputs--show-pass icon-Eyeclose\"></span></div><div class=\"item-change-inputs-wrap\"><input type=\"password\" data-password=\"new\"" + (jade.attr("placeholder", _t('account', 'title.new_password'), true, false)) + " class=\"item-change-inputs\"/><span data-show-password=\"1\" class=\"item-change-inputs--show-pass icon-Eyeclose\"></span></div></div><div class=\"profile-grid__item--btn-action\"><a data-save=\"password\" class=\"btn-action--save\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</a><a data-cancel=\"password\" class=\"btn-action--cancel\">" + (jade.escape(null == (jade_interp = _t("account", "button.cancel")) ? "" : jade_interp)) + "</a></div></div></div><div data-update=\"email\" class=\"profile-grid__item\"><div class=\"profile-grid__item--changed\"><div class=\"profile-grid__item__label\">" + (jade.escape(null == (jade_interp = _t("account", "title.change_email") + ':') ? "" : jade_interp)) + "</div><div data-email=\"current\" class=\"profile-grid__item__status\">" + (jade.escape((jade_interp = email) == null ? '' : jade_interp)) + "</div><div class=\"profile-grid__item__edit\"><a data-change=\"email\" class=\"profile-grid__item__edit__link\">" + (jade.escape(null == (jade_interp = _t("account", "button.edit")) ? "" : jade_interp)) + "</a></div></div><div class=\"profile-grid__item--change inactive\"><div class=\"profile-grid__item-change-inputs\"><div class=\"item-change-inputs-wrap\"><input" + (jade.attr("placeholder", _t('account', 'title.new_email_address'), true, false)) + " data-email=\"new\" class=\"item-change-inputs\"/></div><div class=\"item-change-inputs-wrap\"><input type=\"password\"" + (jade.attr("placeholder", _t('account', 'title.your_password'), true, false)) + " data-email=\"password\" class=\"item-change-inputs\"/><span data-show-password=\"1\" class=\"item-change-inputs--show-pass icon-Eyeclose\"></span></div></div><div class=\"profile-grid__item--btn-action\"><a data-save=\"email\" class=\"btn-action--save\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</a><a data-cancel=\"email\" class=\"btn-action--cancel\">" + (jade.escape(null == (jade_interp = _t("account", "button.cancel")) ? "" : jade_interp)) + "</a></div></div></div><div data-update=\"locale\" data-ui-select=\"data-ui-select\" class=\"profile-grid__item\"><div class=\"profile-grid__item__label\">" + (jade.escape(null == (jade_interp = _t("account", "title.language") + ':') ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__status\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\" class=\"profile-grid__item__edit__link\">");
if ( l10n.languagesList[l10n.locale])
{
buf.push(jade.escape(null == (jade_interp = l10n.languagesList[l10n.locale].ucfirst()) ? "" : jade_interp));
}
else
{
buf.push("<l10n class=\"locale\"></l10n>");
}
buf.push("</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", l10n.locale, true, false)) + " name=\"locale\" data-control=\"data-control\"/><span data-value=\"data-value\">" + (jade.escape(null == (jade_interp = l10n.locale) ? "" : jade_interp)) + "</span><div class=\"current-select-item-icon-arrow-up\"></div></div>");
if ( l10n.languagesList)
{
buf.push("<div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate l10n.languagesList
;(function(){
  var $$obj = l10n.languagesList;
  if ('number' == typeof $$obj.length) {

    for (var language = 0, $$l = $$obj.length; language < $$l; language++) {
      var title = $$obj[language];

if ( l10n.locale == language)
{
buf.push("<a" + (jade.attr("data-item", language, true, false)) + " class=\"option-select-item option-select-item current-option\">" + (jade.escape(null == (jade_interp = title.ucfirst()) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("<a" + (jade.attr("data-item", language, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = title.ucfirst()) ? "" : jade_interp)) + "</a>");
}
    }

  } else {
    var $$l = 0;
    for (var language in $$obj) {
      $$l++;      var title = $$obj[language];

if ( l10n.locale == language)
{
buf.push("<a" + (jade.attr("data-item", language, true, false)) + " class=\"option-select-item option-select-item current-option\">" + (jade.escape(null == (jade_interp = title.ucfirst()) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("<a" + (jade.attr("data-item", language, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = title.ucfirst()) ? "" : jade_interp)) + "</a>");
}
    }

  }
}).call(this);

buf.push("</div>");
}
buf.push("</div></div></div><div class=\"profile-grid__item__edit\">&nbsp;</div></div><div data-update=\"timezone\" data-ui-select=\"data-ui-select\" class=\"profile-grid__item\"><div class=\"profile-grid__item__label\">" + (jade.escape(null == (jade_interp = _t("account", "title.timezone") + ':') ? "" : jade_interp)) + "</div><div class=\"profile-grid__item__status\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\" class=\"profile-grid__item__edit__link\">");
if ( l10n.timezone)
{
buf.push(jade.escape(null == (jade_interp = l10n.timezone) ? "" : jade_interp));
}
else
{
buf.push(jade.escape(null == (jade_interp = l10n.timezone) ? "" : jade_interp));
}
buf.push("</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", l10n.timezone, true, false)) + " name=\"timezone\" data-control=\"data-control\"/><span data-value=\"data-value\">" + (jade.escape(null == (jade_interp = l10n.timezone) ? "" : jade_interp)) + "</span><div class=\"current-select-item-icon-arrow-up\"></div></div>");
if ( l10n.timezonesList)
{
buf.push("<div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate l10n.timezonesList
;(function(){
  var $$obj = l10n.timezonesList;
  if ('number' == typeof $$obj.length) {

    for (var timezoneTitle = 0, $$l = $$obj.length; timezoneTitle < $$l; timezoneTitle++) {
      var timeZone = $$obj[timezoneTitle];

buf.push("<span class=\"option-select-group\">" + (jade.escape(null == (jade_interp = timezoneTitle) ? "" : jade_interp)) + "</span>");
if ( timeZone)
{
// iterate timeZone
;(function(){
  var $$obj = timeZone;
  if ('number' == typeof $$obj.length) {

    for (var zone = 0, $$l = $$obj.length; zone < $$l; zone++) {
      var title = $$obj[zone];

if ( l10n.timezone == zone)
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item option-select-item current-option\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
    }

  } else {
    var $$l = 0;
    for (var zone in $$obj) {
      $$l++;      var title = $$obj[zone];

if ( l10n.timezone == zone)
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item option-select-item current-option\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var timezoneTitle in $$obj) {
      $$l++;      var timeZone = $$obj[timezoneTitle];

buf.push("<span class=\"option-select-group\">" + (jade.escape(null == (jade_interp = timezoneTitle) ? "" : jade_interp)) + "</span>");
if ( timeZone)
{
// iterate timeZone
;(function(){
  var $$obj = timeZone;
  if ('number' == typeof $$obj.length) {

    for (var zone = 0, $$l = $$obj.length; zone < $$l; zone++) {
      var title = $$obj[zone];

if ( l10n.timezone == zone)
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item option-select-item current-option\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
    }

  } else {
    var $$l = 0;
    for (var zone in $$obj) {
      $$l++;      var title = $$obj[zone];

if ( l10n.timezone == zone)
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item option-select-item current-option\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
else
{
buf.push("<a" + (jade.attr("data-item", zone, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</a>");
}
    }

  }
}).call(this);

}
    }

  }
}).call(this);

buf.push("</div>");
}
buf.push("</div></div></div><div class=\"profile-grid__item__edit\">&nbsp;</div></div></div></div></div></div><div class=\"b-feed__content b-feed-content b-upload-content\"><div class=\"b-wrap-content b-wrap-content--myprofile b-feed-content__profile-feed\"><div notification-settings-container=\"1\"></div><div notification-news-feed-container=\"1\"></div><div notification-subscription-container=\"1\"></div></div></div><div class=\"b-feed__content b-feed-content b-upload-content\"><ul class=\"b-popup-content__nav-footer\"><li class=\"b-popup-content__nav-footer__item\"><a data-remove-account=\"data-remove-account\">" + (jade.escape(null == (jade_interp = _t("account", "button.remove_account")) ? "" : jade_interp)) + "</a></li>");
if ((hasBillingHistory === true))
{
buf.push("<li class=\"b-popup-content__nav-footer__item\"><a href=\"/#billingHistory\">" + (jade.escape(null == (jade_interp = _t("account", "button.billing_history")) ? "" : jade_interp)) + "</a></li>");
}
buf.push("</ul></div>");;return buf.join("");
};