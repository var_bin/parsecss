var AccountView = Backbone.View.extend({

    regions: {
        notificationSettings: "[notification-settings-container]",
        notificationNewsFeed: "[notification-news-feed-container]",
        notificationSubscription: "[notification-subscription-container]"
    },

    events: {
        "click [data-change]": function(event) {
            event.preventDefault();
            this.clearMessages();
            var $changedBlock = this.$el.find("[data-update='" + $(event.currentTarget).data('change') + "']")
            $changedBlock.addClass('edit')
            $changedBlock.find('.profile-grid__item--changed').addClass('inactive');
            $changedBlock.find('.profile-grid__item--change').removeClass('inactive');

        },
        "click [data-cancel]": function(event) {
            event.preventDefault();
            this.clearMessages();
            var $changedBlock = this.$el.find("[data-update='" + $(event.currentTarget).data('cancel') + "']")
            $changedBlock.find('input').removeClass('error').val('');
            $changedBlock.removeClass('edit')
            $changedBlock.find('.profile-grid__item--changed').removeClass('inactive');
            $changedBlock.find('.profile-grid__item--change').addClass('inactive');
        },
        "click [data-show-password]": function(event) {
            var $element = $(event.target);
            if ($element.hasClass('show-pass')){
                $element.removeClass('show-pass');
                $element.parent().find('input').hidePassword();
                $element.parent().find('.item-change-inputs--show-pass').removeClass('icon-Browse');
                $element.parent().find('.item-change-inputs--show-pass').addClass('icon-Eyeclose');
            } else {
                $element.addClass('show-pass');
                $element.parent().find('input').showPassword();
                $element.parent().find('.item-change-inputs--show-pass').removeClass('icon-Eyeclose');
                $element.parent().find('.item-change-inputs--show-pass').addClass('icon-Browse');
            }
        },
        "click [data-save='password']": "updatePassword",
        "click [data-save='email']": "updateEmail",
        "change [data-control]": "updateL10n",

        "click [data-remove-account]": function() {
            Resources.load(["removeAccount"], function() {
                app.regions.popup.render(RemoveAccountView, {
                    model: new RemoveAccountModel()
                });
            });
        }
    },

    updatePassword: function() {
        var self = this;
        self.clearMessages();
        self.$dom.oldPassword.removeClass("error");
        self.$dom.newPassword.removeClass("error");

        self.accountPassword.once("invalid", function(model, error) {
            self.onPasswordError(model, error);
        });

        self.accountPassword.save({
                oldPassword: self.$('[data-password="old"]').val(),
                newPassword: self.$('[data-password="new"]').val()
            }, {
                success: function() {
                    self.$("[data-update='password'] [data-cancel]").click();

                    self.$dom.messageStatus.toggleClass('inactive', false);
                    self.$dom.messageStatusText.html($.t("account", "text.password_updated"));
                },
                error: function(model, response) {
                    self.onPasswordError(model, response);
                },
                type: "POST"
            }
        );

        return false;
    },

    updateEmail: function() {
        var self = this;
        self.clearMessages();
        self.$dom.emailUpdatePassword.removeClass("error");
        self.$dom.emailUpdateEmail.removeClass("error");

        self.accountEmail.once("invalid", function(model, error) {
            self.onEmailError(model, error);
        });

        self.accountEmail.save({
                password: self.$('[data-email="password"]').val(),
                email: self.$('[data-email="new"]').val()
            }, {
                success: function() {
                    self.$('[data-update="email"] [data-cancel]').click();

                    self.$dom.messageStatus.toggleClass('inactive', false);
                    self.$dom.messageStatusText.html($.t("account", "text.request_sent"));
                },
                error: function(model, response) {
                    self.onEmailError(model, response);
                },
                type: "POST"
            }
        );

        return false;
    },

    updateL10n: function() {
        this.clearMessages();
        this.l10n.set({
            timezone: this.fields['timezone'].getInstance().getVal(),
            locale: this.fields['locale'].getInstance().getVal()
        });
        if(!_.isEmpty(this.l10n.changed)) {
            this.region.loading();
            this.l10n.save({},{
                success: function() {
                    window.location.reload();
                },
                error: function() {
                    window.location.reload();
                },
                type: "POST"
            });
        }
    },

    passwordErrorHandler: function(errors) {
        var e = errors || {};

        if (e.oldPassword) {
            this.$dom.oldPassword.addClass("error");
            this.$dom.messageError.toggleClass('inactive', false);
            this.$dom.messageErrorText.html(e.oldPassword[0]);
        }

        if (e.newPassword) {
            this.$dom.newPassword.addClass("error");
            this.$dom.messageError.toggleClass('inactive', false);
            this.$dom.messageErrorText.html(e.newPassword[0]);
        }
    },

    emailErrorHandler: function(errors) {
        var e = errors || {};

        if (e.password) {
            this.$dom.emailUpdatePassword.addClass("error");
            this.$dom.messageError.toggleClass('inactive', false);
            this.$dom.messageErrorText.html(e.password[0]);
        }

        if (e.email) {
            this.$dom.emailUpdateEmail.addClass("error");
            this.$dom.messageError.toggleClass('inactive', false);
            this.$dom.messageErrorText.html(e.email[0]);
        }
    },

    _errorHandler : function(response,type) {
        switch(type) {
            case "email":
                this.emailErrorHandler(response);
                break;
            case "password":
                this.passwordErrorHandler(response);
                break;
        }
    },

    onPasswordError: function(model, response) {
        this._errorHandler(response, "password");
    },

    onEmailError: function(model, response) {
        this._errorHandler(response, "email");
    },

    invalidL10n: function(model) {
        var self = this;
        _.each(model.validationError, function(val, key) {
            if(val) {
                switch(key) {
                    case "timezone":
                        self.$dom.messageError.toggleClass('inactive', false);
                        self.$dom.messageErrorText.html($.t("account", "error.cant_change_timezone"));
                        break;
                    case "locale":
                        self.$dom.messageError.toggleClass('inactive', false);
                        self.$dom.messageErrorText.html($.t("account", "error.cant_change_language"));
                        break;
                    default:
                        break;
                }
            }
        });
        this.region.ready();
    },

    clearMessages: function() {
        this.$dom.messageError.toggleClass('inactive', true);
        this.$dom.messageStatus.toggleClass('inactive', true);
        this.$dom.messageErrorText.html('');
        this.$dom.messageStatusText.html('');
    },

    initialize: function() {
        this.accountPassword = new AccountPassword();

        this.accountEmail = new AccountEmail();

        this.l10n = new AccountL10n();

        this.listenTo(this.l10n, "invalid", this.invalidL10n);
        this.listenTo(this.model, "change", this.renderAccount);

        this.renderAccount();
    },

    renderAccount: function() {
        this.$el.html(tpl.render("Account", this.model.toJSON()));

        var model = this.model.toJSON();
        var self = this;

        this.l10n.validateData = model.l10n;

        this.l10n.set({
            timezone: model.l10n.timezone,
            locale: model.l10n.locale
        });

        this.fields = {};

        this.$("[data-ui-select]").each(function() {
            var fieldName = $(this).find("input[type=hidden]").attr("name");
            self.fields[fieldName] = $(this).uiSelect();
        });
        this.$("[data-ui-textarea]").each(function() {
            var fieldName = $(this).find("textarea").attr("name");
            self.fields[fieldName] = $(this).uiTextarea();
        });

        this.$dom = {
            messageError: this.$('[data-message="error"]'),
            messageStatus: this.$('[data-message="result"]'),
            messageErrorText: this.$('[data-message-text="error"]'),
            messageStatusText: this.$('[data-message-text="result"]'),
            oldPassword: this.$('[data-password="old"]'),
            newPassword: this.$('[data-password="new"]'),
            emailUpdateEmail: this.$('[data-email="new"]'),
            emailUpdatePassword: this.$('[data-email="password"]')
        };

        _.defer(function() {
            self.regions.notificationSubscription.render(NotificationSubscriptionView, {
                model: new NotificationSubscriptionModel(self.model.get("notificationSubscription"))
            });
        });

        _.defer(function() {
            self.regions.notificationSettings.render(NotificationSettingsView, {
                model: new NotificationSettingsModel(self.model.get("notificationSettings"))
            });
        });

        _.defer(function() {
            self.regions.notificationNewsFeed.render(NotificationNewsFeedSubscriptionView, {
                model: new NotificationNewsFeedSubscriptionModel(self.model.get("notificationNewsFeed"))
            });
        });

        return this;
    }
});
