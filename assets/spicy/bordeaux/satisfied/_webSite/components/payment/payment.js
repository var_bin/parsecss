$(document).ready(function() {

    var $countDownElement = jQuery('#countdown');
    if ($countDownElement.data('endtime')) {
        $countDownElement.countdown($countDownElement.data('endtime') * 1000, function (event) {
            var index;
            var totalHours = event.offset.totalDays * 24 + event.offset.hours;
            var hoursArray = processInt(totalHours, 3);
            var minArray = processInt(event.offset.minutes, 2);
            var secArray = processInt(event.offset.seconds, 2);
            for (index = 0; index < hoursArray.length; ++index) {
                $(this).find("#hours" + index).text(hoursArray[index]);
            }
            for (index = 0; index < minArray.length; ++index) {
                $(this).find("#minutes" + index).text(minArray[index]);
            }
            for (index = 0; index < secArray.length; ++index) {
                $(this).find("#seconds" + index).text(secArray[index]);
            }

        });
    }

    jQuery('.packages-list .package').on("click", function (event) {
        var $target = $(event.currentTarget);
        if (! $target.hasClass('active')) {
            $('.packages-list .package').removeClass('active');
            $('.packages-list').find(':radio').attr("checked", null);
            $target.addClass('active');
            $target.find(':radio').attr('checked', 'checked');

            if ($target.attr('lang')) {
                var details = $.parseJSON($target.attr('lang'));

                if (details.package) {
                    $("#info_" + details.package).addClass('active').removeClass('inactive');
                }
            }
        }
    });

    jQuery('.content-pp-new-packages-item').on("click", function (event) {
        var $target = $(event.currentTarget);
        if (! $target.hasClass('current-packages-item')) {
            $('.content-pp-new-packages-item').removeClass('current-packages-item');
            $('.content-pp-new-packages-wrap').find(':radio').attr("checked", null);
            $target.addClass('current-packages-item');
            $target.find(':radio').attr('checked', 'checked');

            $('.content-pp-new-packages-information-item').removeClass('active').addClass('inactive')
            if ($target.attr('lang')) {
                var details = $.parseJSON($target.attr('lang'));

                if (details.package) {
                    $("#info_" + details.package).addClass('active').removeClass('inactive');
                }
            }
        }
    });

    jQuery('.custom-select select').on("change", function (event) {
        var $el = $(event.target);
        $el.parent().find('.custom-select__selected-item').text($el.find(':selected').text());
    });

    jQuery('#change_payment_details').on("click", function (event) {
        event.preventDefault();
        jQuery('#payment_more').toggleClass('inactive');
        jQuery('#payment_details').toggleClass('inactive');

          jQuery("[id$=_source_type]").val(jQuery('[data-typeid]').attr('data-typeid'));
          var hidePaymentForm = jQuery("[id$=_hidePaymentForm]");
          if (hidePaymentForm.val() == true) {
            hidePaymentForm.val(0);
          } else {
            hidePaymentForm.val(1);
          }

    });
});

function processInt(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size).split('');
}