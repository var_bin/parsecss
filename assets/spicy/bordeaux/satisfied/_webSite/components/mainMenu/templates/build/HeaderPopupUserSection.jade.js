this["JST"] = this["JST"] || {};

this["JST"]["HeaderPopupUserSection"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),userLink = locals_.userLink,id = locals_.id,photo_url = locals_.photo_url,login = locals_.login,action = locals_.action,_t = locals_._t,type = locals_.type;
buf.push("<div class=\"b-notificators__popup-content\"><div class=\"b-notificators__popup-content-avatar inline\"><a" + (jade.attr("href", userLink(id), true, false)) + "><img" + (jade.attr("src", photo_url, true, false)) + " class=\"b-notificators__popup-content-avatar__item\"/></a></div><div class=\"b-notificators__popup-content-message inline\"><a" + (jade.attr("href", userLink(id), true, false)) + " class=\"popup-content-message__nick\">" + (jade.escape(null == (jade_interp = login) ? "" : jade_interp)) + "</a>");
if ( action == 'newsFeed')
{
buf.push("<div class=\"b-notificators__popup-message-text\">" + (jade.escape(null == (jade_interp = _t("notificationNewsFeed", "title.newsFeed::" + type)) ? "" : jade_interp)) + "</div>");
}
buf.push("</div><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (id) + "')", true, false)) + " class=\"b-notificators__popup-content-btn-link inline\">Start Chat</a></div>");;return buf.join("");
};