var HeaderView = Backbone.View.extend({
    events: {
        "click [data-nav-item] > a.b-main-menu-content__item-link": "clickItem",
        "click .profile-b__profile-info--show-popup": "toggleUserPopup",
        "click .b-notificators__link--show-popup": 'showSpecificPopup'
    },

    initialize: function(params) {
        var self = this;

        $(document).on('click.Toolbar', ':not(#toolbar [data-nav-item])', function() {
            self.$('[data-activity]').removeClass('active');
        });

        this.listenTo(Backbone, 'interactionMessagecounters', function(data){
            var counters = data.counters || {};
            var activities = data.activities || {};
            _.each(counters, this.counter);
            _.each(activities, this.activity);
        });

        this.hidePopup();
    },

    render: function() {
        this.$el.html(tpl.render(
            "Header",
            {
                accountStatus : app.appData().get('user').account_status.type,
                photoUrl : app.appData().get('user').photoUrl,
                credits : app.appData().get('userCredits').credits || 0
            }
        ));
        return this;
    },

    activeMenu: function(options) {
        var name = options.name || null;
        this.$('[data-nav-item].active').removeClass('active');
        if(name) this.$('[data-nav-item="'+name+'"]').addClass('active');
    },

    counter: function(value, name) {
        var $counter = this.$("[data-counter='"+name+"']");

        value = parseInt(value);
        $counter.text(value);

        if(value > 0) {
            $counter.parent('li:first').addClass("new-notif");
        } else {
            $counter.parent('li:first').removeClass("new-notif");
        }
    },
            
    activity: function(value, option) {
        $popup = this.$("[data-popup='"+option+"']");
        if (!_.isEmpty(value)) {
            var self = this;
            $popup.find('.b-notificators__popup-content-block').html('');
            $.each(value, function(k, v){
                var $el = $(tpl.render('HeaderPopupUserSection', v));
                $el.prependTo($popup.find('.b-notificators__popup-content-block'));
            });
        }
    },

    clickItem: function(event) {
        $el = $(event.currentTarget);

        if($el.find("[data-counter]").hasClass("active")) {
            event.preventDefault();
            event.stopPropagation();
            if($el.parent().find("[data-activity]").hasClass("active")){
                $el.parent().find("[data-activity]").removeClass("active");
            } else {
                $el.parent().find("[data-activity]").addClass("active");
            }
        }
    },

    toggleUserPopup: function(event) {
        $('.b-user-info__popup--show-info-about-me ').toggleClass('hide');
        var toogleIcon = $(event.currentTarget).find('.profile-b__icon--toogle');
        if ($(toogleIcon).hasClass('open icon-cross')) {
            $(toogleIcon).removeClass('open icon-cross').addClass('open icon-arrow-down');
        } else {
            $(toogleIcon).removeClass('open icon-arrow-down').addClass('open icon-cross');
        }
        event.preventDefault();
        event.stopPropagation();
    },
            
    showSpecificPopup: function(event) {
        $target = $(event.currentTarget);
        $el = $target.siblings('.b-notificators__popup');
        $('.b-notificators__popup').not($el).each(function(){
            $(this).addClass('hide').hide();
        });
        $el.toggleClass('hide').toggle();
    },

    hidePopup: function () {
        $(document).on('click', function (e) {
            if ($(e.target).hasClass('b-notificators__link--show-popup')) {
                $('.b-notificators__item').not($(e.target)).removeClass('b-notificators__item--active');
                $(e.target).closest('.b-notificators__item').addClass('b-notificators__item--active');

                return;
            }
            else {
                $('.b-notificators__popup').fadeOut("slow");
                $('.b-notificators__item').removeClass('b-notificators__item--active');
            }

            e.stopPropagation();
        });
    }

});
