this["JST"] = this["JST"] || {};

this["JST"]["ProfilePhotoListItem"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),photos = locals_.photos;
for(var i in photos)
{
buf.push("<div class=\"middle-content__foto-grid__item\"><div" + (jade.attr("data-src", '' + (photos[i].src) + '', true, false)) + " data-image=\"1\" class=\"middle-content__foto-grid__item__foto\"><img" + (jade.attr("src", photos[i].model.avatar, true, false)) + (jade.attr("data-id", '' + (photos[i].model.id) + '', true, false)) + " class=\"foto-grid__item__foto__img\"/><div class=\"foto-grid__item__foto-edit\"><span" + (jade.attr("data-edit", photos[i].model.id, true, false)) + " class=\"foto-grid__item__foto__edit icon-Edit\"></span></div><div class=\"foto-grid__item__foto-remove\"><span" + (jade.attr("data-remove", photos[i].model.id, true, false)) + " class=\"foto-grid__item__foto-remove-icon icon-cross\"></span></div></div><div class=\"middle-content__foto-grid__item__coments\"></div></div>");
};return buf.join("");
};