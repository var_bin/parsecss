this["JST"] = this["JST"] || {};

this["JST"]["ProfilePhotoListUploadError"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,errorText = locals_.errorText;
buf.push("<div class=\"b-popup\"><button" + (jade.attr("title", _t("profilePhotoList", "button.close"), true, false)) + " href=\"#\" id=\"popupClose\" class=\"btn-close grey btn-text-def\"></button>");
if ( errorText)
{
buf.push("<p>" + (null == (jade_interp = errorText) ? "" : jade_interp) + "</p>");
}
else
{
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("profilePhotoList", "text.upload_error")) ? "" : jade_interp)) + "</p>");
}
buf.push("<button id=\"popupContinue\" class=\"btn btn-text-green\">" + (jade.escape(null == (jade_interp = _t("profilePhotoList", "button.continue")) ? "" : jade_interp)) + "</button></div>");;return buf.join("");
};