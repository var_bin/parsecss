this["JST"] = this["JST"] || {};

this["JST"]["ProfilePhotoUploadMotivationListItem"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),index = locals_.index,photos = locals_.photos,primary = locals_.primary,_t = locals_._t;
buf.push("<ul" + (jade.attr("data-index", index, true, false)) + " class=\"rightside__grid rightside__grid--voronka\">");
for(var i in photos)
{
buf.push("<li class=\"rightside__grid-item--voronka\"><div class=\"rightside__grid-item-link\"><img" + (jade.attr("src", photos[i].model.avatar, true, false)) + " class=\"rightside__grid-item-link-img\"/>");
if ( (primary && primary.id === photos[i].model.id))
{
buf.push("<div class=\"voronka_rightside__grid-item--photo-primary rightside__grid-item--photo-counter--landing primary\"><div" + (jade.attr("data-primary", photos[i].model.id, true, false)) + (jade.attr("title", _t("profilePhotoList", "button.primary"), true, false)) + " class=\"voronka_rightside__grid-item--photo-primary-icon\"></div></div>");
}
else
{
buf.push("<div class=\"voronka_rightside__grid-item--photo-primary rightside__grid-item--photo-counter--landing\"><div" + (jade.attr("data-primary", photos[i].model.id, true, false)) + (jade.attr("title", _t("profilePhotoList", "button.primary"), true, false)) + " class=\"voronka_rightside__grid-item--photo-primary-icon\"></div></div>");
}
buf.push("<div class=\"voronka_rightside__grid-item--photo-remove rightside__grid-item--photo-counter--landing\"><span" + (jade.attr("data-remove", photos[i].model.id, true, false)) + " class=\"voronka_rightside__grid-item--photo-remove-icon icon-cross\"></span></div></div><div class=\"rightside__grid-item-profile-b-info\"></div></li>");
}
buf.push("</ul>");;return buf.join("");
};