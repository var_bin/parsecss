var ProfilePhotoListView = Backbone.View.extend({
    templates: {
        listItem: 'ProfilePhotoListItem',
    },
    events: {
        'click [data-primary]': 'changePrimary',
        'click [data-remove]': 'removePhoto',
        'click [data-edit]': 'editPhoto'
    },

    initialize: function(options) {
        if(options.type == 'photoUploadMotivation') {
            this.templates.listItem = 'ProfilePhotoUploadMotivationListItem';
        } else {
            this.templates.listItem = 'ProfilePhotoListItem';
        }

        this.listenTo(this.collection, 'add remove', this.render);
        this.listenTo(this.collection, 'addError', function() {
            this.region.ready();
        });
        this.listenTo(Backbone, 'UploadProfilePhoto.start', function() {
            this.region.loading();
        });
        this.listenTo(Backbone, 'UploadProfilePhoto.complete UploadProfilePhoto.uploadError', function(data) {
            if (data.status === 'error') {
                app.showPopup({
                    url       : '/myprofile',
                    template  : 'ProfilePhotoListUploadError',
                    errorText : (data.errors && data.errors.message) || false
                });
            }
        });
        this.listenTo(Backbone, 'ProfilePhotoPrimary.set', this.changeCoords);
    },

    render: function() {

        this.$el.empty().html(tpl.render('ProfilePhotoList', {
            photos: this.collection.toJSON()
        }));

        Backbone.trigger('ProfilePhotoCollection.change', this.collection.length);

        if (this.collection.length) {
            this.renderPhotos();
        } else {
            this.region.ready();
        }

        return this;
    },

    renderPhotos: function() {
        var self = this,
            $photos = this.$('#profilePhotosHidden [data-photo]'),
            photoSort = this.getPhotoSort(this.collection.length),
            photoSortStatus = [],
            photoIndexMap = [],
            photoPositionMap = [],
            i, j,
            photoIndexIterator = 0;

        for (i = 0; i < photoSort.length; i++) {
            photoSortStatus[i] = 0;
            photoPositionMap[i] = [];
            for (j = 0; j < photoSort[i]; j++) {
                photoIndexMap[photoIndexIterator] = i;
                photoPositionMap[i].push({
                    photo: $photos[photoIndexIterator],
                    model: this.collection.at(photoIndexIterator),
                    index: photoIndexIterator
                });
                photoIndexIterator++;
            }
        }

        this.rowsIterator = photoPositionMap.length;

        $photos.one('load error', function(event) {
            var el = this,
                $el = $(el),
                index = $el.index(),
                row = photoIndexMap[index];

            photoSortStatus[row]++;
            if(photoSortStatus[row] === photoSort[row]) {
                self.renderRow(photoPositionMap[row], row);
            }
        }).each(function() {
            if (this.complete) $(this).load();
        });
    },

    renderRow: function(photos, index) {
        var $el,
            $beforeEl = this.$('[data-index=' + (+index +  1) + ']'),
            aspect = 0,
            data = {
                photos: [],
                total: photos.length,
                aspectTotal: 0,
                index: index,
                primary: this.collection.primaryModel.toJSON()
            };

        for (var i = 0; i < photos.length; i++) {
            aspect = photos[i].photo.width / photos[i].photo.height;
            data.aspectTotal += aspect;
            data.photos.push({
                model: photos[i].model.toJSON(),
                src: photos[i].photo.src,
                aspect: aspect
            });
        }

        $el = $(tpl.render(this.templates.listItem, data));

        if ($beforeEl.size())
            $beforeEl.before($el);
        else
            this.$el.append($el);
        $el.show();

        --this.rowsIterator;
        if (!this.rowsIterator) this.region.ready();
    },

    changePrimary: function(event) {
        var $el = $(event.target),
            id = $el.data('primary');

        if(!$el.parent().hasClass('primary') && this.collection.changePrimary(id)) {
            this.$el.find('.primary').removeClass('primary');
            $el.parent().addClass('primary');
        }
    },

    removePhoto: function(event) {
        var $el = $(event.target),
            id = $el.data('remove');

        this.region.loading();

        this.collection.removePhoto(id);
    },

    editPhoto: function(event) {
        var $el = $(event.target),
            id = $el.data('edit');

        var model = this.collection.findWhere({ id: id });

        if (model) {
            app.shadowPopup();
            app.regions.popup.render(ProfilePhotoEditorView, {
                model: new ProfilePhotoEditorModel(model.toJSON())
            });
        }
    },

    changeCoords: function(data) {
        this.collection.changeCoords(data.file, data.coords);
    },

    getPhotoSort: function(length) {
        var total = length,
            step = 4,
            mod = total % step,
            totalRows = Math.ceil(total / step),
            filledRows = (mod) ? totalRows - 1 : totalRows,
            result = [],
            i = 0;

        switch (true) {
            case (total <= step):
                result.push(total);
                break;
            case (total === 5):
                result = [3, 2];
                break;
            default:
                for (i = 0; i < filledRows; i++) {
                    result.push(step);
                }
                if (mod) {
                    result.push(mod);
                }
        }

        return result;
    }
});
