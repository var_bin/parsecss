var ProfileDetailsModel = Backbone.Model.extend({
    url: '/profile/update',

    dateFormat: ['d', 'm', 'y'],
    preloadedLocationList: [],
    fieldsData: {},

    needValidateAttr: false,

    validate: function(attrs) {
        var errorsList = {};
        var addErrorToList = function(attrName, message) {
            if (!_.isArray(errorsList[attrName])) {
                errorsList[attrName] = [];
            }
            errorsList[attrName].push(message);
        };
        if (this.needValidateAttr) {
            var fieldsData = this.get('fieldsData');
            for (var name in attrs) {
                var value = attrs[name];
                if (value != this.previous(name)) {
                    switch (name) {
                        case 'location':
                            if (value.toString().length === 0) {
                                addErrorToList(
                                    name,
                                    $.t('profileEditor', 'error.profile_editor_not-valid')
                                );
                            }
                        break;
                        default:
                            if (name in fieldsData) {
                                if (!(value in fieldsData[name])) {
                                    addErrorToList(
                                        name,
                                        $.t('profileEditor', 'error.profile_editor_not-valid')
                                    );
                                }
                            }
                        break;
                    }
                }
            }
        }
        if (!_.isEmpty(errorsList)) {
            return errorsList;
        }
    },

    getAvailableValues: function(field) {
        var fieldsData = this.get('fieldsData');
        if (field in fieldsData) {
            return fieldsData[field];
        } else {
            return [];
        }
    },

    getAvailableYears: function() {
        var yearsList = {};
        var startYear = new Date().getFullYear() - 18;
        for (var i = 0; i < 61; i++) {
            yearsList[i] = startYear - i;
        }
        return yearsList;
    }
});
