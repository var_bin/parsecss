var ProfileDetailsView = Backbone.View.extend({

    events: {
        'click [data-btn="reset"]': 'resetForm',
        'click [data-btn="save"]': 'saveFormData',
        'change [data-control]': 'showButtons'
    },

    fields: {},

    blockId: 'default',

    getFormParams: function() {
        return {
            values: {
                sexual_orientation: this.model.getAvailableValues('sexual_orientation'),
                marital_status: this.model.getAvailableValues('marital_status'),
                children: this.model.getAvailableValues('children'),
                living: this.model.getAvailableValues('living'),
                race: this.model.getAvailableValues('race'),
                religion: this.model.getAvailableValues('religion'),
                height: this.model.getAvailableValues('height'),
                weight: this.model.getAvailableValues('weight'),
                build: this.model.getAvailableValues('build'),
                hair_color: this.model.getAvailableValues('hair_color'),
                eye_color: this.model.getAvailableValues('eye_color'),
                tattoo: this.model.getAvailableValues('tattoo'),
                pierced: this.model.getAvailableValues('pierced'),
                smoke: this.model.getAvailableValues('smoke'),
                drink: this.model.getAvailableValues('drink'),
                education: this.model.getAvailableValues('education'),
                income: this.model.getAvailableValues('income')
            },
            dateFormat: this.model.get('dateFormat'),
            years: this.model.getAvailableYears()
        };
    },

    render: function() {
        this.$el.html(tpl.render('ProfileDetails', {
            blockId: this.options.blockId,
            profile: this.model.attributes,
            formParams: this.getFormParams()
        }));

        this.fields = {};
        var self = this;

        this.$('[data-ui-select]').each(function() {
            var fieldName = $(this).find('input[type=hidden]').attr('name');
            self.fields[fieldName] = $(this).uiSelect('init', {'silent': true});
        });
        this.$('[data-ui-date-select]').each(function() {
            var fieldName = $(this).find('input[data-date-select-value]').attr('name');
            self.fields[fieldName] = $(this).uiDateSelect();
        });
        this.$('[data-ui-location]').each(function() {
            var fieldName = $(this).find('input[type=text]').attr('name');
            self.fields[fieldName] = $(this).uiLocation({
                anyCityItem: false,
                locations: self.model.get('preloadedLocationList')
            });
        });

        this.region.ready();

        return this;
    },

    showButtons: function() {
        this.$el.find('.action-status-b').removeClass('inactive');
    },

    hideButtons: function() {
        this.$el.find('.action-status-b').addClass('inactive');
    },

    resetForm: function() {
        this.model.set(this.model.previousAttributes(), {silent: true});
        this.render();
    },

    saveFormData: function() {
        this.region.loading();

        this.model.needValidateAttr = true;

        var self = this;

        var updateData = {};
        for (var curFieldName in self.fields) {
            var uiField = self.fields[curFieldName].getInstance();
            updateData[curFieldName] = uiField.getVal();
        }

        this.model.once('invalid', function(model, error) {
            for (var curFieldName in self.fields) {
                if (curFieldName in error) {
                    var errorsList = [];
                    if (_.isArray(error[curFieldName])) {
                        errorsList = error[curFieldName];
                    } else if (_.isString(error[curFieldName])) {
                        errorsList.push(error[curFieldName]);
                    }
                    self.fields[curFieldName].getInstance().markAsError(errorsList);
                } else {
                    self.fields[curFieldName].getInstance().markAsNotError();
                }
            }
            self.region.ready();
        });

        if (!$.isEmptyObject(app.csrfToken)) {
            updateData[app.csrfToken.name] = app.csrfToken.value;
        }

        this.model.save(updateData, {
            type: 'POST',
            data: updateData,
            success: function(model, resp) {
                for (var curFieldName in self.fields) {
                    self.fields[curFieldName].getInstance().markAsNotError();
                }
                self.hideButtons();
                self.region.ready();
            },
            error: function(model, response) {
                var curFieldName;
                if (typeof response === 'object') {
                    for (curFieldName in self.fields) {
                        if (curFieldName in response) {
                            self.fields[curFieldName].getInstance().markAsError(response[curFieldName]);
                        } else {
                            self.fields[curFieldName].getInstance().markAsNotError();
                        }
                    }
                } else {
                    for (curFieldName in self.fields) {
                        self.fields[curFieldName].getInstance().markAsError();
                    }
                }
                self.region.ready();
            }
        });
    }
});
