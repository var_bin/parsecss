this["JST"] = this["JST"] || {};

this["JST"]["ProfileDetails"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,blockId = locals_.blockId,formParams = locals_.formParams,profile = locals_.profile;
var blockFields = {'general': ["birthday", "location", "sexual_orientation", "height", "weight"], 'appearance': ["build", "hair_color", "eye_color", "race", "tattoo", "pierced"], 'background': ["marital_status", "children", "living", "religion", "smoke", "drink", "education", "income"]}
buf.push("<div class=\"b-profile-feed-head description-head\"><div class=\"b-profile-feed-head__content-left b-profile-feed-head__content-left--1em head_table__div_table-cell\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "title." + blockId)) ? "" : jade_interp)) + "<span data-message=\"result\" class=\"changes-status changes-status--save inactive\"><span class=\"icon-status icon-Check\"></span><span data-message-text=\"result\"></span></span><span data-message=\"error\" class=\"changes-status changes-status--error inactive\"><span class=\"icon-status icon-Attention\"></span><span data-message-text=\"error\"></span></span></div><div class=\"rightside-content-b-header-right head_table__div_table-cell\"></div></div><div class=\"description_block--content\">");
if ( (blockFields[blockId]))
{
buf.push("<table class=\"description_block--content__table\"><tbody>");
// iterate blockFields[blockId]
;(function(){
  var $$obj = blockFields[blockId];
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

if (field == 'birthday') {
{
buf.push("<tr data-ui-date-select=\"data-ui-date-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><input type=\"hidden\" name=\"birthday\" data-control=\"data-control\" data-date-select-value=\"1\" id=\"birthday\"/>");
// iterate formParams.dateFormat
;(function(){
  var $$obj = formParams.dateFormat;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.day)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.day), true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
for (var i = 1; i <= 31; i++)
{
buf.push("<a" + (jade.attr("data-item", i, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.month)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.month), true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\"><a data-item=\"1\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.january')) ? "" : jade_interp)) + "</a><a data-item=\"2\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.february')) ? "" : jade_interp)) + "</a><a data-item=\"3\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.march')) ? "" : jade_interp)) + "</a><a data-item=\"4\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.april')) ? "" : jade_interp)) + "</a><a data-item=\"5\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.may')) ? "" : jade_interp)) + "</a><a data-item=\"6\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.june')) ? "" : jade_interp)) + "</a><a data-item=\"7\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.july')) ? "" : jade_interp)) + "</a><a data-item=\"8\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.august')) ? "" : jade_interp)) + "</a><a data-item=\"9\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.september')) ? "" : jade_interp)) + "</a><a data-item=\"10\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.october')) ? "" : jade_interp)) + "</a><a data-item=\"11\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.november')) ? "" : jade_interp)) + "</a><a data-item=\"12\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.december')) ? "" : jade_interp)) + "</a></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = profile.birthdayDate.year) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.day)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.day), true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
for (var i = 1; i <= 31; i++)
{
buf.push("<a" + (jade.attr("data-item", i, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.month)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.month), true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\"><a data-item=\"1\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.january')) ? "" : jade_interp)) + "</a><a data-item=\"2\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.february')) ? "" : jade_interp)) + "</a><a data-item=\"3\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.march')) ? "" : jade_interp)) + "</a><a data-item=\"4\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.april')) ? "" : jade_interp)) + "</a><a data-item=\"5\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.may')) ? "" : jade_interp)) + "</a><a data-item=\"6\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.june')) ? "" : jade_interp)) + "</a><a data-item=\"7\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.july')) ? "" : jade_interp)) + "</a><a data-item=\"8\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.august')) ? "" : jade_interp)) + "</a><a data-item=\"9\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.september')) ? "" : jade_interp)) + "</a><a data-item=\"10\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.october')) ? "" : jade_interp)) + "</a><a data-item=\"11\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.november')) ? "" : jade_interp)) + "</a><a data-item=\"12\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.december')) ? "" : jade_interp)) + "</a></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = profile.birthdayDate.year) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div>");
}
    }

  }
}).call(this);

buf.push("</td></tr>");
}
} else if (field == 'location') {
{
buf.push("<tr data-ui-location=\"data-ui-location\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"form-field__input form-field__input--myprofile\"><div class=\"relative\"><input id=\"location\" type=\"text\" name=\"location\"" + (jade.attr("value", profile[field], true, false)) + " autocomplete=\"off\" data-control=\"data-control\" data-toggle=\"data-toggle\" class=\"input-item input-item--icon-right grey\"/><div data-list=\"data-list\" class=\"b-notificators__popup--search shadow\"></div></div></div></td></tr>");
}
} else {
{
buf.push("<tr data-ui-select=\"data-ui-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = formParams.values[field][profile[field]]) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select right\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div></td></tr>");
}
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

if (field == 'birthday') {
{
buf.push("<tr data-ui-date-select=\"data-ui-date-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><input type=\"hidden\" name=\"birthday\" data-control=\"data-control\" data-date-select-value=\"1\" id=\"birthday\"/>");
// iterate formParams.dateFormat
;(function(){
  var $$obj = formParams.dateFormat;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.day)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.day), true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
for (var i = 1; i <= 31; i++)
{
buf.push("<a" + (jade.attr("data-item", i, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.month)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.month), true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\"><a data-item=\"1\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.january')) ? "" : jade_interp)) + "</a><a data-item=\"2\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.february')) ? "" : jade_interp)) + "</a><a data-item=\"3\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.march')) ? "" : jade_interp)) + "</a><a data-item=\"4\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.april')) ? "" : jade_interp)) + "</a><a data-item=\"5\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.may')) ? "" : jade_interp)) + "</a><a data-item=\"6\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.june')) ? "" : jade_interp)) + "</a><a data-item=\"7\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.july')) ? "" : jade_interp)) + "</a><a data-item=\"8\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.august')) ? "" : jade_interp)) + "</a><a data-item=\"9\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.september')) ? "" : jade_interp)) + "</a><a data-item=\"10\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.october')) ? "" : jade_interp)) + "</a><a data-item=\"11\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.november')) ? "" : jade_interp)) + "</a><a data-item=\"12\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.december')) ? "" : jade_interp)) + "</a></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = profile.birthdayDate.year) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.day)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.day), true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
for (var i = 1; i <= 31; i++)
{
buf.push("<a" + (jade.attr("data-item", i, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = parseInt(profile.birthdayDate.month)) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", parseInt(profile.birthdayDate.month), true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\"><a data-item=\"1\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.january')) ? "" : jade_interp)) + "</a><a data-item=\"2\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.february')) ? "" : jade_interp)) + "</a><a data-item=\"3\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.march')) ? "" : jade_interp)) + "</a><a data-item=\"4\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.april')) ? "" : jade_interp)) + "</a><a data-item=\"5\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.may')) ? "" : jade_interp)) + "</a><a data-item=\"6\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.june')) ? "" : jade_interp)) + "</a><a data-item=\"7\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.july')) ? "" : jade_interp)) + "</a><a data-item=\"8\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.august')) ? "" : jade_interp)) + "</a><a data-item=\"9\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.september')) ? "" : jade_interp)) + "</a><a data-item=\"10\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.october')) ? "" : jade_interp)) + "</a><a data-item=\"11\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.november')) ? "" : jade_interp)) + "</a><a data-item=\"12\" class=\"option-select-item\">" + (jade.escape(null == (jade_interp = _t("profileDetails",'value.december')) ? "" : jade_interp)) + "</a></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"content-table-value-input\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = profile.birthdayDate.year) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<a" + (jade.attr("data-item", year, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div>");
}
    }

  }
}).call(this);

buf.push("</td></tr>");
}
} else if (field == 'location') {
{
buf.push("<tr data-ui-location=\"data-ui-location\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"form-field__input form-field__input--myprofile\"><div class=\"relative\"><input id=\"location\" type=\"text\" name=\"location\"" + (jade.attr("value", profile[field], true, false)) + " autocomplete=\"off\" data-control=\"data-control\" data-toggle=\"data-toggle\" class=\"input-item input-item--icon-right grey\"/><div data-list=\"data-list\" class=\"b-notificators__popup--search shadow\"></div></div></div></td></tr>");
}
} else {
{
buf.push("<tr data-ui-select=\"data-ui-select\" class=\"description_block--content__table--row\"><td class=\"description_block--content__table__nomination\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.'+field)) ? "" : jade_interp)) + "</td><td class=\"description_block--content__table__value\"><div class=\"relative\"><a data-toggle=\"data-toggle\" data-value=\"data-value\">" + (jade.escape(null == (jade_interp = formParams.values[field][profile[field]]) ? "" : jade_interp)) + "</a><div class=\"profile-grid-custom-select right\"><div class=\"current-select-item\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><span data-value=\"data-value\"></span><div class=\"current-select-item-icon-arrow-up\"></div></div><div data-items-list=\"data-items-list\" class=\"overflow-options\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</a>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<a" + (jade.attr("data-item", key, true, false)) + " class=\"option-select-item\">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</a>");
    }

  }
}).call(this);

buf.push("</div></div></div></td></tr>");
}
}
    }

  }
}).call(this);

buf.push("</tbody></table>");
}
buf.push("</div><div class=\"b-profile-feed-footer search-box-b--footer\"><div class=\"b-profile-feed-footer__add b-profile-feed-footer__add--status\"><div class=\"action-status-b inactive\"><a data-btn=\"reset\" class=\"b-profile-feed-footer__action-btn-status\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "button.cancel")) ? "" : jade_interp)) + "</a><a data-btn=\"save\" class=\"b-profile-feed-footer__action-btn-status save\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "button.save")) ? "" : jade_interp)) + "</a></div></div></div>");;return buf.join("");
};