this["JST"] = this["JST"] || {};

this["JST"]["Message"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),message = locals_.message,users = locals_.users,momentTime = locals_.momentTime;
buf.push("<div class=\"b-chat__item-message-wrap\"><div class=\"b-chat__item-message\"><div class=\"b-chat__item-avatar\">");
if (message.direction == 'out')
{
buf.push("<img" + (jade.attr("src", users[message.senderId].photoUrl, true, false)) + " class=\"b-chat__item-avatar-item border-radius chat-post__item-avatar-item\"/>");
}
else if (message.direction == 'in')
{
buf.push("<img" + (jade.attr("src", users[message.senderId].photoUrl, true, false)) + " class=\"b-chat__item-avatar-item border-radius chat-post__item-avatar-item\"/>");
}
buf.push("</div><div class=\"b-chat__item-info-massage\"><a href=\"#\" class=\"b-chat__item-info-massage-nick\">");
if (message.direction == 'out')
{
buf.push(jade.escape(null == (jade_interp = users[message.senderId].login) ? "" : jade_interp));
}
else if (message.direction == 'in')
{
buf.push(jade.escape(null == (jade_interp = users[message.senderId].login) ? "" : jade_interp));
}
buf.push("</a><div class=\"b-chat__item-info-massage-content\">" + (jade.escape(null == (jade_interp = message.text) ? "" : jade_interp)) + "</div></div><div class=\"b-chat__item-message-time\">" + (jade.escape(null == (jade_interp = momentTime) ? "" : jade_interp)) + "</div></div></div>");;return buf.join("");
};