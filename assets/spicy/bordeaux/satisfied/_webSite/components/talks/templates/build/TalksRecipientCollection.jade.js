this["JST"] = this["JST"] || {};

this["JST"]["TalksRecipientCollection"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),collection = locals_.collection,naughtyCollection = locals_.naughtyCollection,model = locals_.model,_t = locals_._t,userLink = locals_.userLink,collectionHistory = locals_.collectionHistory;
buf.push("<ul id=\"cTalksRecipientCollectionWrap\" class=\"b-list-wrap\">");
// iterate collection
;(function(){
  var $$obj = collection;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var recipient = $$obj[$index];

var naughtyRecipient = naughtyCollection && naughtyCollection[recipient.userId] || {};
if(recipient.user && ((model.state === 'small' && !recipient.history) ||model.state === 'big' ||recipient.active))
{
var statusText = '';
if ( (recipient.user.statuses && recipient.user.statuses.online))
{
if ( (recipient.user.isFriend))
{
statusText=_t("talks", "text.friend_online")
}
else
{
statusText= _t('talks', 'text.online')
}
}
else if ( (recipient.user.statuses && recipient.user.statuses.recentlyOnline))
{
statusText = _t('talks', 'text.recently_online')
}
buf.push("<li" + (jade.cls(['list-item',[recipient.active ? "active" : "", recipient.unreadMessageCount ? "not-read" : "", naughtyRecipient.naughtyClass]], [null,true])) + "><a" + (jade.attr("href", userLink(recipient.user.id), true, false)) + (jade.attr("data-userId", recipient.user.id, true, false)) + " class=\"list-item-wrap\"><span" + (jade.cls(['counter',(!recipient.unreadMessageCount)?"hidden":""], [null,true])) + ">" + (jade.escape(null == (jade_interp = recipient.unreadMessageCount) ? "" : jade_interp)) + "</span><span class=\"user-photo\"><img" + (jade.attr("src", recipient.user.photos.url, true, false)) + " class=\"photo\"/></span><div class=\"user-describe\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = recipient.user.login) ? "" : jade_interp)) + "</span><span" + (jade.cls(['status',recipient.user.statuses && recipient.user.statuses.online ? "online" : ""], [null,true])) + ">" + (jade.escape(null == (jade_interp = statusText) ? "" : jade_interp)) + "</span></div></a>");
if (naughtyRecipient.naughtyLevel > 0)
{
buf.push("<div class=\"dropdown-menu dropdown-naughty\"><div class=\"text\">");
switch (naughtyRecipient.naughtyLevel){
case 1:
buf.push("<div class=\"text-wrap\">" + (jade.escape(null == (jade_interp = _t("talks", "text.naughty_mode_switch_sexy")) ? "" : jade_interp)) + "</div>");
  break;
case 2:
buf.push("<div class=\"text-wrap\">" + (jade.escape(null == (jade_interp = _t("talks", "text.naughty_mode_switch_no_limits")) ? "" : jade_interp)) + "</div>");
  break;
}
buf.push("</div><div class=\"buttons-wrap\"><a href=\"#\" data-nm-trigger='1' class=\"btn-talks btn-naughty\">" + (jade.escape(null == (jade_interp = _t("talks", "value.naughty_mode_normal")) ? "" : jade_interp)) + "</a><a href=\"#\" data-nm-trigger='2' class=\"btn-talks btn-naughty on\">" + (jade.escape(null == (jade_interp = _t("talks", "value.naughty_mode_sexy")) ? "" : jade_interp)) + "</a><a href=\"#\" data-nm-trigger='3' class=\"btn-talks btn-naughty no-limits\">" + (jade.escape(null == (jade_interp = _t("talks", "value.naughty_mode_no_limits")) ? "" : jade_interp)) + "</a></div></div>");
}
buf.push("</li>");
}
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var recipient = $$obj[$index];

var naughtyRecipient = naughtyCollection && naughtyCollection[recipient.userId] || {};
if(recipient.user && ((model.state === 'small' && !recipient.history) ||model.state === 'big' ||recipient.active))
{
var statusText = '';
if ( (recipient.user.statuses && recipient.user.statuses.online))
{
if ( (recipient.user.isFriend))
{
statusText=_t("talks", "text.friend_online")
}
else
{
statusText= _t('talks', 'text.online')
}
}
else if ( (recipient.user.statuses && recipient.user.statuses.recentlyOnline))
{
statusText = _t('talks', 'text.recently_online')
}
buf.push("<li" + (jade.cls(['list-item',[recipient.active ? "active" : "", recipient.unreadMessageCount ? "not-read" : "", naughtyRecipient.naughtyClass]], [null,true])) + "><a" + (jade.attr("href", userLink(recipient.user.id), true, false)) + (jade.attr("data-userId", recipient.user.id, true, false)) + " class=\"list-item-wrap\"><span" + (jade.cls(['counter',(!recipient.unreadMessageCount)?"hidden":""], [null,true])) + ">" + (jade.escape(null == (jade_interp = recipient.unreadMessageCount) ? "" : jade_interp)) + "</span><span class=\"user-photo\"><img" + (jade.attr("src", recipient.user.photos.url, true, false)) + " class=\"photo\"/></span><div class=\"user-describe\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = recipient.user.login) ? "" : jade_interp)) + "</span><span" + (jade.cls(['status',recipient.user.statuses && recipient.user.statuses.online ? "online" : ""], [null,true])) + ">" + (jade.escape(null == (jade_interp = statusText) ? "" : jade_interp)) + "</span></div></a>");
if (naughtyRecipient.naughtyLevel > 0)
{
buf.push("<div class=\"dropdown-menu dropdown-naughty\"><div class=\"text\">");
switch (naughtyRecipient.naughtyLevel){
case 1:
buf.push("<div class=\"text-wrap\">" + (jade.escape(null == (jade_interp = _t("talks", "text.naughty_mode_switch_sexy")) ? "" : jade_interp)) + "</div>");
  break;
case 2:
buf.push("<div class=\"text-wrap\">" + (jade.escape(null == (jade_interp = _t("talks", "text.naughty_mode_switch_no_limits")) ? "" : jade_interp)) + "</div>");
  break;
}
buf.push("</div><div class=\"buttons-wrap\"><a href=\"#\" data-nm-trigger='1' class=\"btn-talks btn-naughty\">" + (jade.escape(null == (jade_interp = _t("talks", "value.naughty_mode_normal")) ? "" : jade_interp)) + "</a><a href=\"#\" data-nm-trigger='2' class=\"btn-talks btn-naughty on\">" + (jade.escape(null == (jade_interp = _t("talks", "value.naughty_mode_sexy")) ? "" : jade_interp)) + "</a><a href=\"#\" data-nm-trigger='3' class=\"btn-talks btn-naughty no-limits\">" + (jade.escape(null == (jade_interp = _t("talks", "value.naughty_mode_no_limits")) ? "" : jade_interp)) + "</a></div></div>");
}
buf.push("</li>");
}
    }

  }
}).call(this);

if(model.prevExist || model.state === 'small' && collectionHistory.length)
{
buf.push("<li><a href=\"javascript:void(0)\" data-history=\"data-history\" class=\"btn-talks btn-view\">" + (jade.escape(null == (jade_interp = _t("talks", "text.view_history")) ? "" : jade_interp)) + "</a></li>");
}
buf.push("</ul>");;return buf.join("");
};