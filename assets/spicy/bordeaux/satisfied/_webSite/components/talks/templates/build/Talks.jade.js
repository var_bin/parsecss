this["JST"] = this["JST"] || {};

this["JST"]["Talks"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),model = locals_.model;
var stateClass = "", overlayClass = "hidden";
switch (model.state){
case "closed":
stateClass = "hidden"
  break;
case "min":
stateClass = "c-talks-minimized"
  break;
case "small":
stateClass = "c-talks-small"
  break;
case "big":
overlayClass = ""
  break;
}
buf.push("<div id=\"cTalksOverlay\"" + (jade.cls(['c-talks-overlay',overlayClass], [null,true])) + "></div><div" + (jade.cls(['c-talks',stateClass], [null,true])) + "><div class=\"header\"><div id=\"cTalksHeader\" data-state-dblclick=\"fold\" class=\"header-wrap\"></div></div><div id=\"cTalksEmptyCollection\" style=\"display: none;\" class=\"emptyList\"><div class=\"empty-messenger\"></div></div><div class=\"head-msg\"><div id=\"cTalksRecipientHeader\" class=\"b-main b-info\"></div></div><div class=\"content-msg\"><div id=\"cTalksRecipientCollection\" class=\"b-aside b-list\"></div><div class=\"b-main b-talks\"><div id=\"cTalksBottomPosition\" class=\"messenger-history\"><div id=\"cTalksMessageCollection\" class=\"messages-wrapper\"></div></div><div id=\"cTalksInput\" class=\"messenger-input\"></div></div></div></div>");;return buf.join("");
};