this["JST"] = this["JST"] || {};

this["JST"]["TalksMessage"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),model = locals_.model,separateDate = locals_.separateDate,_t = locals_._t,cid = locals_.cid,canViewStatusIsRead = locals_.canViewStatusIsRead;
var messageClass = "new-message",translateKey = "regular";
switch (true){
case (model.flags.autoreplySubject || model.flags.autoreplyDescription):
messageClass = 'autoreply'
translateKey = 'autoreply'
  break;
case model.flags.userMatchAlert:
messageClass = 'activity-alert'
translateKey = 'activity_alert'
  break;
case model.flags.flirtcast:
messageClass = 'flirtcast'
translateKey = 'flirtcast'
  break;
}
if(separateDate)
{
var dateKey = model.dateKey;
if(dateKey === 'today' || dateKey === 'yesterday')
{
dateKey = _t('talks', 'text.' + dateKey)
}
buf.push("<div" + (jade.attr("data-separate", model.dateKey, true, false)) + " class=\"separate-msg\"><span class=\"title\">" + (jade.escape(null == (jade_interp = dateKey) ? "" : jade_interp)) + "</span></div>");
}
buf.push("<div" + (jade.attr("data-message", model.dateKey, true, false)) + (jade.attr("data-cid", cid, true, false)) + (jade.cls(['message',(model.direction)?"message-"+model.direction:"",(model.flags && model.flags.cutted)?"message-blur":""], [null,true,true])) + "><div" + (jade.cls(['info',(!model.time)?"loading":""], [null,true])) + ">");
if(model.direction === 'in')
{
buf.push("<span" + (jade.attr("title", _t('talks', 'text.type_' + translateKey), true, false)) + (jade.cls(['status',messageClass], [null,true])) + "></span>");
}
else
{
if(canViewStatusIsRead)
{
if(!model.isRead)
{
buf.push("<span" + (jade.attr("title", _t('talks', 'text.type_not_read'), true, false)) + " class=\"status reading\"></span>");
}
else
{
buf.push("<span" + (jade.attr("title", _t('talks', 'text.type_read'), true, false)) + " class=\"status read\"></span>");
}
}
}
buf.push("<span class=\"date\">" + (jade.escape(null == (jade_interp = model.time) ? "" : jade_interp)) + "</span></div><span class=\"username\">" + (jade.escape(null == (jade_interp = model.sender.login) ? "" : jade_interp)) + "</span><div class=\"message-text\"><span>" + (null == (jade_interp = model.textHtml) ? "" : jade_interp) + "</span></div></div>");;return buf.join("");
};