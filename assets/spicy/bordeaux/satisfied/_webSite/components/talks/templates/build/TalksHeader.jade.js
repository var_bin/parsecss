this["JST"] = this["JST"] || {};

this["JST"]["TalksHeader"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,model = locals_.model,activeModel = locals_.activeModel,unreadRecipients = locals_.unreadRecipients,userLink = locals_.userLink;
buf.push("<a href=\"#\" data-state=\"fold\"" + (jade.attr("title", _t("talks", "text.minimise"), true, false)) + " class=\"b-fold\"></a><a href=\"#\" data-state=\"fold\"" + (jade.attr("title", _t("talks", "text.maximize"), true, false)) + " class=\"b-expand\"></a><a href=\"#\" data-state=\"min\"" + (jade.attr("title", _t("talks", "text.pop_in"), true, false)) + " class=\"b-minimize\"></a><a href=\"#\" data-state=\"max\"" + (jade.attr("title", _t("talks", "text.pop_out"), true, false)) + " class=\"b-maxmimize\"></a><a href=\"#\" data-state=\"close\"" + (jade.attr("title", _t("talks", "text.close"), true, false)) + " class=\"b-close\"></a>");
if(model.state === 'big' || !activeModel || model.state === 'min' && unreadRecipients > 1)
{
buf.push("<h2 class=\"title icon-msg\"><span class=\"name\">" + (jade.escape(null == (jade_interp = _t("talks", "title.messenger")) ? "" : jade_interp)) + "</span>");
if (unreadRecipients)
{
buf.push("<span class=\"counter\">" + (jade.escape(null == (jade_interp = unreadRecipients) ? "" : jade_interp)) + "</span>");
}
buf.push("</h2>");
}
else if(activeModel && activeModel.user)
{
buf.push("<h2 class=\"title icon-msg\"><a" + (jade.attr("href", userLink(activeModel.userId), true, false)) + " class=\"name\">" + (jade.escape(null == (jade_interp = activeModel.user.login) ? "" : jade_interp)) + "</a>");
if(activeModel.unreadMessageCount)
{
buf.push("<span class=\"counter\">" + (jade.escape(null == (jade_interp = activeModel.unreadMessageCount) ? "" : jade_interp)) + "</span>");
}
buf.push("</h2>");
};return buf.join("");
};