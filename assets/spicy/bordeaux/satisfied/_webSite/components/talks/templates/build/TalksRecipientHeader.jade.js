this["JST"] = this["JST"] || {};

this["JST"]["TalksRecipientHeader"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user,_t = locals_._t,naughtyOptions = locals_.naughtyOptions,userLink = locals_.userLink,userId = locals_.userId;
if(user)
{
buf.push("<div class=\"b-info-wrap\"><div data-toggle=\"data-toggle\" class=\"b-info-types\"><div class=\"dropdown-menu dropdown-info-types\"><ul><li class=\"item-type new-message\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.type_regular')) ? "" : jade_interp)) + "</li><li class=\"item-type flirtcast\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.type_flirtcast')) ? "" : jade_interp)) + "</li><li class=\"item-type activity-alert\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.type_activity_alert')) ? "" : jade_interp)) + "</li><li class=\"item-type read\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.type_read')) ? "" : jade_interp)) + "</li><li class=\"item-type reading\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.type_not_read')) ? "" : jade_interp)) + "</li></ul></div></div><div" + (jade.cls(['user-info',naughtyOptions ? naughtyOptions.naughtyClass : ''], [null,true])) + "><a" + (jade.attr("href", userLink(userId), true, false)) + " class=\"user-photo\"><img" + (jade.attr("src", user.photos.url, true, false)) + " class=\"photo\"/></a><div class=\"user-describe\"><a" + (jade.attr("href", userLink(userId), true, false)) + " data-state=\"min\" class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</a><span class=\"describe-info\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.gender_'+user.gender)) ? "" : jade_interp)) + "<span class=\"b-old\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.years', user.age)) ? "" : jade_interp)) + "</span>");
if(user.geo && user.geo.city)
{
buf.push("<span class=\"location\">" + (jade.escape(null == (jade_interp = user.geo.city) ? "" : jade_interp)));
if(user.geo.country)
{
buf.push(jade.escape(null == (jade_interp = ', ' + user.geo.country) ? "" : jade_interp));
}
buf.push("</span>");
}
buf.push("</span></div></div>");
if(user.chatUpLine)
{
buf.push("<div class=\"user-details\">" + (jade.escape(null == (jade_interp = user.chatUpLine) ? "" : jade_interp)) + "</div>");
}
buf.push("<div class=\"communicate\"><a href=\"#\" data-button-friend=\"data-button-friend\"" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.cls(['btn-add-friend',(user.buttons && user.buttons.favorite && user.buttons.favorite.activated) ? "active" : ""], [null,true])) + "></a><div data-toggle=\"data-toggle\" class=\"btn-other\"><span class=\"name\">" + (jade.escape(null == (jade_interp = _t("talks", "text.or")) ? "" : jade_interp)) + "</span><div class=\"dropdown-menu dropdown-more\"><a" + (jade.attr("href", userLink(userId), true, false)) + " data-state=\"min\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("talks", "text.open_profile")) ? "" : jade_interp)) + "</a>");
if(user.buttons && user.buttons.wink && user.buttons.wink.activated)
{
buf.push("<span class=\"item\">" + (jade.escape(null == (jade_interp = _t("talks", "text.winked")) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<a href=\"#\" data-button-wink=\"data-button-wink\"" + (jade.attr("data-winkbutton", user.id, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = _t("talks", "text.wink")) ? "" : jade_interp)) + "</a>");
}
buf.push("</div></div></div></div>");
};return buf.join("");
};