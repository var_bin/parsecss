this["JST"] = this["JST"] || {};

this["JST"]["TalksMessageCollection"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t;
buf.push("<div id=\"cTalksMessageList\"><div id=\"cTalksPrevExist\" style=\"display: none;\" class=\"separate-msg\"><a href=\"#\" data-talks-more=\"1\" class=\"btn-talks btn-view-older\">" + (jade.escape(null == (jade_interp = _t("talks", "text.view_older_messages")) ? "" : jade_interp)) + "</a></div></div>");;return buf.join("");
};