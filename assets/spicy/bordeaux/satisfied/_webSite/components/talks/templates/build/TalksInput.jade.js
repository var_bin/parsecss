this["JST"] = this["JST"] || {};

this["JST"]["TalksInput"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),model = locals_.model,_t = locals_._t,smiles = locals_.smiles;
if(model.fetchedMessages && model.fetchedUser)
{
if(model.messagesData && model.messagesData.upgradeRead)
{
buf.push("<div class=\"message-upgrade\"><a href=\"#\" data-feature=\"upgradeRead\" class=\"btn-upgrade\">" + (jade.escape(null == (jade_interp = _t("talks", "text.upgrade_read")) ? "" : jade_interp)) + "</a></div>");
}
if(model.messagesData && model.messagesData.blockedByUser)
{
buf.push("<div data-feature=\"blocked\" class=\"message-blocked\"><p class=\"text\">" + (jade.escape(null == (jade_interp = _t("talks", "text.member_blocked_you")) ? "" : jade_interp)) + "</p></div>");
}
buf.push("<div class=\"btn-send-wrap\"><button id=\"cTalksInputSubmit\" class=\"btn-talks btn-send\">" + (jade.escape(null == (jade_interp = _t("talks", "text.send")) ? "" : jade_interp)) + "</button></div><div id=\"cTalksInputWrap\" class=\"input-wrap empty\"><div id=\"cTalksSmiles\" class=\"smiles icon-Wink\"><div class=\"smiles-tooltip b-smiles-enter\"><ul>");
// iterate smiles
;(function(){
  var $$obj = smiles;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var smile = $$obj[$index];

buf.push("<li class=\"smile-item\">" + (null == (jade_interp = smile.img) ? "" : jade_interp) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var smile = $$obj[$index];

buf.push("<li class=\"smile-item\">" + (null == (jade_interp = smile.img) ? "" : jade_interp) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div><div class=\"input-placeholder\">" + (jade.escape(null == (jade_interp = _t("talks", "text.write_your_message")) ? "" : jade_interp)) + "</div><div id=\"cTalksInputMessage\" contenteditable=\"true\" class=\"message-input\"></div></div>");
};return buf.join("");
};