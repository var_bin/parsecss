this["JST"] = this["JST"] || {};

this["JST"]["TalksEmptyCollection"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,countFreeMessages = locals_.countFreeMessages;
var searchLink = '<a href="/#peopleNearby" data-state="min">' + _t('talks', 'text.search') + '</a>';
var favoritesLink = '<a href="/#favorites" data-state="min">' + _t('talks', 'text.favorites') + '</a>';
buf.push("<div class=\"empty-wrap\"><div class=\"empty-messenger\">");
if(countFreeMessages.show)
{
buf.push("<p class=\"empty-text text1\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.emptyList.text4', {"{count}": countFreeMessages.countFreeMessages})  ) ? "" : jade_interp)) + "</p>");
}
buf.push("<p class=\"empty-text text1\">" + (jade.escape(null == (jade_interp = _t('talks', 'text.emptyList.text1')) ? "" : jade_interp)) + "</p><p class=\"empty-text text2\">" + (null == (jade_interp = _t('talks', 'text.emptyList.text2', {"{search}": searchLink})) ? "" : jade_interp) + "</p></div></div>");;return buf.join("");
};