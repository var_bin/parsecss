var TalksInputView = Backbone.View.extend({
    templateId: 'TalksInput',

    events: {
        'focus #cTalksInputMessage': 'placeholderFocus',
        'blur #cTalksInputMessage': 'placeholderBlur',
        'paste #cTalksInputMessage': 'placeholderPaste',
        'click #cTalksInputSubmit': 'submit',
        'drop #cTalksInputMessage': function(event) {
            event.preventDefault();
            return false;
        },
        'dragover #cTalksInputMessage': function(event) {
            event.preventDefault();
            return false;
        },
        'keypress #cTalksInputMessage': function(event) {
            var code = event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode);

            if (event.ctrlKey && ((code === 13) || (code === 0xA) || (code === 0xD))) {
                var html = this.$("#cTalksInputMessage").html();

                if (!/^[\s\S]*<br>$/.test(html)) {
                    if (_.contains(['firefox', 'chrome'], app.appData().get('userBrowser'))) {
                        this.$("#cTalksInputMessage").append($('<br />'));
                    }
                }

                this.saveSelection();
                this.pasteHtmlAtCursor('<br />&shy;');
            } else if (event.shiftKey && ((code === 13) || (code === 0xA) || (code === 0xD))) {
                return false;
            } else if ((code === 13) || (code === 0xD)) {
                this.submit(event);
                return false;
            }
        },
        'click #cTalksInputMessage': function(event) {
            this.saveSelection();
        },
        'keyup #cTalksInputMessage': function(event) {
            this.saveSelection();
        },
        'click [data-feature]': 'feature',
        'click #cTalksSmiles': function(event) {
            this.$('#cTalksSmiles').toggleClass('active');
        },
        'click [data-smile]': function(event) {
            var $target = $(event.currentTarget),
                smile = $target.data('smile');

            this.pasteSmile(smile);
        }
    },

    interactionLog: true,

    initialize: function() {
        this.SelectionProvider = {};

        var self = this;

        this.listenTo(this.model, 'change:messagesData', this.render);
        this.listenTo(Backbone, 'Talks.InputHeightRequest', this.triggerHeight);

        this.smiles = new TalksSmileCollection();

        $(document).on('click.Talks', function(event) {
            var $el = $(event.target);

            if ($el.filter(':not(#cTalksSmiles > *)').size() &&
                $el.filter(':not(#cTalksSmiles)').size()) {
                self.$('#cTalksSmiles').removeClass('active');
            }
        });
    },

    triggerHeight: function() {
        Backbone.trigger('Talks.InputHeight', this.$el.height());
    },

    render: function() {
        this.undelegateEvents();
        this.delegateEvents();

        this.$el.html(tpl.render(this.templateId, {
            model: this.model.toJSON(),
            smiles: this.smiles.toJSON()
        }));

        this.triggerHeight();

        this.$('#cTalksInputMessage').focus();

        return this;
    },

    remove: function() {
        $(document).off('click.Talks');

        this.$el.empty();
        this.stopListening();
        this.undelegateEvents();
    },

    submit: function(event) {
        event.preventDefault();

        var $message = this.$('#cTalksInputMessage'),
            message = $('<div>').append($('<div>').append($message.html())).html(),
            messagesData = this.model.get('messagesData');

        if (!interaction.isAvailable()) {
            if (this.interactionLog) {
                this.interactionLog = false;
                throw new Error('talks: can\'t send message(interaction is not available)');
            }

            return;
        }

        if (messagesData.upgradeType) {
            $.gotoUrl(messagesData.upgradeType);

            return;
        }

        if (($.trim($message.text()) || $message.find('img').size()) && message
            && !messagesData.blockedByUser) {
            $message.empty();
            this.model.submitMessage(message);
        }
    },

    feature: function(event) {
        event.preventDefault();
        var modelData = this.model.toJSON(),
            $target = $(event.currentTarget),
            feature = $target.data('feature'),
            url = feature && modelData.messagesData[feature];

        if (url) {
            if (url.substr(-13) === 'viaProfileId=') url += modelData.userId;
            $.gotoUrl(url);
        }
    },

    pasteSmile: function(smileCode) {
        var smileImg = this.smiles.getSmileImg(smileCode);

        this.pasteHtmlAtCursor(smileImg);
    },

    placeholderFocus: function() {
        this.$('#cTalksInputWrap').removeClass('empty');
    },

    placeholderBlur: function(event) {
        if (!this.$('#cTalksInputMessage').html() && !this.$('#cTalksInputMessage').text())
            this.$('#cTalksInputWrap').addClass('empty');
    },

    placeholderPaste: function() {
        this.model.set('copyPasteDetected', true);
    },

    saveSelection: function() {
        if (window.getSelection) {
            var sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                this.SelectionProvider.storedSelection = sel.getRangeAt(0);
            }
        } else if (document.selection && document.selection.createRange) {
            this.SelectionProvider.storedSelection = document.selection.createRange();
        } else {
            this.SelectionProvider.storedSelection = null;
        }
    },

    pasteHtmlAtCursor: function(html) {
        var sel, range = this.SelectionProvider.storedSelection;
        if (range) {
            if (window.getSelection) {
                sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);
            } else if (document.selection && range.select) {
                range.select();
            }
        }

        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.getRangeAt && sel.rangeCount) {
                range = sel.getRangeAt(0);
                range.deleteContents();

                var el = document.createElement("div");
                el.innerHTML = html;
                var frag = document.createDocumentFragment(), node, lastNode;
                while ((node = el.firstChild)) {
                    lastNode = frag.appendChild(node);
                }
                range.insertNode(frag);

                if (lastNode) {
                    range = range.cloneRange();
                    range.setStartAfter(lastNode);
                    range.collapse(true);
                    sel.removeAllRanges();
                    sel.addRange(range);
                }
            }
        } else if (document.selection && document.selection.type != "Control") {
            document.selection.createRange().pasteHTML(html);
        }
    }
});
