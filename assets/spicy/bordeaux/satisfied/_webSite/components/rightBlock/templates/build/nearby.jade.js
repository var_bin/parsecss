this["JST"] = this["JST"] || {};

this["JST"]["nearby"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),users = locals_.users,userLink = locals_.userLink;
buf.push("<div class=\"rightside__content-b-header\"><div class=\"rightside-content-b-header-title-left title\"><i class=\"rightside-content-b-header-title-icon icon-Search\"></i>People Nearby</div><div class=\"rightside-content-b-header-right\"><a href=\"/#peopleNearby\" class=\"rightside-content-b-header-right-link\">Show more</a></div></div><div class=\"rightside__content-b-middle jcarousel\"><ul class=\"rightside__grid\">");
if ( users)
{
// iterate users
;(function(){
  var $$obj = users;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var user = $$obj[$index];

buf.push("<li class=\"rightside__grid-item\"><a" + (jade.attr("href", userLink(user.id), true, false)) + " class=\"rightside__grid-item-link\"><img" + (jade.attr("src", '' + (user.photos.url) + '', true, false)) + " class=\"rightside__grid-item-link-img\"/>");
if ( user.photos.count > 0)
{
buf.push("<div class=\"rightside__grid-item--photo-counter\"><span class=\"rightside__grid-item--photo-counter-icon icon-Photo\"></span>" + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "</div>");
}
buf.push("</a><div class=\"rightside__grid-item-profile-b-info\"><a" + (jade.attr("href", userLink(user.id), true, false)) + " class=\"grid-item-profile-b-info__name\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + "</a><div class=\"grid-item-profile-b-info__age\"><i class=\"grid-item-profile-b-info__age-icon icon-Calendar\"></i>" + (jade.escape((jade_interp = user.age) == null ? '' : jade_interp)) + " years</div></div></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var user = $$obj[$index];

buf.push("<li class=\"rightside__grid-item\"><a" + (jade.attr("href", userLink(user.id), true, false)) + " class=\"rightside__grid-item-link\"><img" + (jade.attr("src", '' + (user.photos.url) + '', true, false)) + " class=\"rightside__grid-item-link-img\"/>");
if ( user.photos.count > 0)
{
buf.push("<div class=\"rightside__grid-item--photo-counter\"><span class=\"rightside__grid-item--photo-counter-icon icon-Photo\"></span>" + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "</div>");
}
buf.push("</a><div class=\"rightside__grid-item-profile-b-info\"><a" + (jade.attr("href", userLink(user.id), true, false)) + " class=\"grid-item-profile-b-info__name\">" + (jade.escape((jade_interp = user.login) == null ? '' : jade_interp)) + "</a><div class=\"grid-item-profile-b-info__age\"><i class=\"grid-item-profile-b-info__age-icon icon-Calendar\"></i>" + (jade.escape((jade_interp = user.age) == null ? '' : jade_interp)) + " years</div></div></li>");
    }

  }
}).call(this);

}
buf.push("</ul><div class=\"rightside__grid-gradient--hide-item rightside__grid-gradient--hide-item--prev\"></div><div class=\"rightside__grid-gradient--hide-item\"></div><a href=\"#\" class=\"rightside__item-arrow-link--show-more icon-arrow-left jcarousel-prev\"></a><a href=\"#\" class=\"rightside__item-arrow-link--show-more icon-arrow-right jcarousel-next\"></a></div>");;return buf.join("");
};