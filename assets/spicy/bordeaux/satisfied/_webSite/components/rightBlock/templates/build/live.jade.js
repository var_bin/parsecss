this["JST"] = this["JST"] || {};

this["JST"]["live"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),liveCams = locals_.liveCams;
buf.push("<div class=\"rightside__content-b-header\"><div class=\"rightside-content-b-header-title-left title\"><i class=\"rightside-content-b-header-title-icon icon-Webcam\"></i>Liveshows</div><div class=\"rightside-content-b-header-right\"><a href=\"/#search/model\" class=\"rightside-content-b-header-right-link\">Show more</a></div></div><div class=\"rightside__content-b-middle\"><ul class=\"rightside__grid\">");
if ( liveCams)
{
// iterate liveCams
;(function(){
  var $$obj = liveCams;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var liveCam = $$obj[$index];

buf.push("<li class=\"rightside__grid-item-video\"><a" + (jade.attr("href", '/#search/model/room/' + (liveCam.model_id) + '/status/20', true, false)) + " class=\"rightside__grid-item-link\"><img" + (jade.attr("src", liveCam.big_photo, true, false)) + " class=\"rightside__grid-item-link-img\"/></a><div class=\"rightside__grid-item-profile-b-info\"><a" + (jade.attr("href", '/#search/model/room/' + (liveCam.model_id) + '/status/20', true, false)) + " class=\"grid-item-profile-b-info__name-video\">" + (jade.escape((jade_interp = liveCam.screen_name) == null ? '' : jade_interp)) + "</a></div></li>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var liveCam = $$obj[$index];

buf.push("<li class=\"rightside__grid-item-video\"><a" + (jade.attr("href", '/#search/model/room/' + (liveCam.model_id) + '/status/20', true, false)) + " class=\"rightside__grid-item-link\"><img" + (jade.attr("src", liveCam.big_photo, true, false)) + " class=\"rightside__grid-item-link-img\"/></a><div class=\"rightside__grid-item-profile-b-info\"><a" + (jade.attr("href", '/#search/model/room/' + (liveCam.model_id) + '/status/20', true, false)) + " class=\"grid-item-profile-b-info__name-video\">" + (jade.escape((jade_interp = liveCam.screen_name) == null ? '' : jade_interp)) + "</a></div></li>");
    }

  }
}).call(this);

}
buf.push("</ul></div>");;return buf.join("");
};