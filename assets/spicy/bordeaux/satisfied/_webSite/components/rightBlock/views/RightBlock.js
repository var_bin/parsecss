var RightBlock = Backbone.View.extend({

    vodBlockModel : new vodPremiumBlockModel(),
    peopleNearbyBlockModel : new peopleNearbyBlockModel(),
    liveBlockModel : new liveBlockModel(),

    initialize : function(){
        this.region = app.regions.rightside;
        this.searchParams = app.appData().get("searchForm").params;
        this.searchParams.onlineNow = 'on'; // Online only

        var self = this;

        this.$el.html(tpl.render("skeleton", {}));
        this.peopleNearbyBlockModel.fetch({
            data : {
                offset: 0,
                limit: 50,
                SearchIndexForm: this.searchParams,
                usedAtBlock: true
            },
            success : function(model) {
                self.renderNearby();
            }
        });

        this.liveBlockModel.fetch({
            success : function(model) {
                self.renderLive();
            }
        });

        this.region.ready();

        this.listenTo(Backbone, "ControllerRun", this.activeMenu);
    },

    activeMenu : function() {
        var self = this;
        this.vodBlockModel.fetch({
            success : function(model) {
                self.renderVod();
            }
        });
    },

    renderNearby : function() {
        var users = this.peopleNearbyBlockModel.get('users'),
            nearby_bl = this.$el.find('.b-rightside__nearby');

        users = _.shuffle(users);

        if (users.length) {
            nearby_bl
                .html(tpl.render("nearby", {
                    users : users
                }));
            this.carouselInit();
        }
        else {
            nearby_bl.hide();
        }
    },
            
    renderVod : function() {
        this.$el
            .find('.b-rightside__vod')
            .html(tpl.render("vod", {
                videos : this.vodBlockModel.get('videos')
            }));
    },
            
    renderLive : function() {
        this.$el
            .find('.b-rightside__live')
            .html(tpl.render("live", {
                liveCams : this.liveBlockModel.get('results')
            }));
    },
    
    carouselInit: function () {
        'use strict';
        
        this.$el.find('.jcarousel').jcarousel({
            wrap: 'circular'
        });

        this.$el.find('.jcarousel-prev').jcarouselControl({
            target: '-=2'
        });

        this.$el.find('.jcarousel-next').jcarouselControl({
            target: '+=2'
        });
    }
});