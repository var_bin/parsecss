this["JST"] = this["JST"] || {};

this["JST"]["ActivityNotification"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),type = locals_.type,params = locals_.params,txtId = locals_.txtId,gender = locals_.gender,fromId = locals_.fromId,photo_url = locals_.photo_url,login = locals_.login,_t = locals_._t;
var availableTypes = new Array('wink','view','mail');
var notifyKey = ['notification', type];
if (params && params.subType) notifyKey.push(params.subType);
if (txtId) notifyKey.push(txtId);
notifyKey.push(gender);
var iconType = 'icon-Wink';
if (type == 'wink') iconType = 'icon-Wink';
if (type == 'view') iconType = 'icon-Browse';
if (type == 'mail') iconType = 'icon-Inbox';
if (availableTypes.indexOf(type) != -1) {
{
buf.push("<div class=\"notificators-footer-content\"><div class=\"notificators-footer-content-ava\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (fromId) + "')", true, false)) + " data-close=\"data-close\"><img" + (jade.attr("src", photo_url, true, false)) + (jade.attr("alt", login, true, false)) + (jade.attr("title", login, true, false)) + "/></a></div><div class=\"notificators-footer-content-message\"><div class=\"notificators-footer-content-message-nickname\"><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (fromId) + "')", true, false)) + " data-close=\"data-close\">" + (jade.escape(null == (jade_interp = login) ? "" : jade_interp)) + "</a></div><a href=\"javascript:void(0)\"" + (jade.attr("onclick", "talksCall('" + (fromId) + "')", true, false)) + " data-close=\"data-close\"><div class=\"notificators-footer-messagee-subject\"><span" + (jade.cls(['messagee-subject-icon','small-icon',iconType], [null,null,true])) + "></span>" + (jade.escape(null == (jade_interp = _t("activityNotification", notifyKey.join('.'))) ? "" : jade_interp)) + "</div></a></div><div data-close=\"\" class=\"notificators-footer-content-close-icon icon-cross\"></div></div>");
}
};return buf.join("");
};