this["JST"] = this["JST"] || {};

this["JST"]["User"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user,_t = locals_._t,upgrade = locals_.upgrade,profileStatus = locals_.profileStatus,messages = locals_.messages,forUser = locals_.forUser,trustedValidation = locals_.trustedValidation,currentUser = locals_.currentUser,naughtyMode = locals_.naughtyMode,userPrimaryPhoto = locals_.userPrimaryPhoto,url = locals_.url,forAdmin = locals_.forAdmin,talksEnabled = locals_.talksEnabled,friendsEnabled = locals_.friendsEnabled,buttons = locals_.buttons,userPhotoCount = locals_.userPhotoCount,lookingFor = locals_.lookingFor,showTrustedBanner = locals_.showTrustedBanner;
jade_mixins["userInfo"] = function(title, key){
var block = (this && this.block), attributes = (this && this.attributes) || {};
buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</span>");
if(user[key])
{
buf.push("<span>" + (jade.escape(null == (jade_interp = user[key].ucfirst()) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("user", "title.not_given")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div>");
};














buf.push("<div" + (jade.cls(['w-user-profile',(upgrade && upgrade.photo_big) ? "user-free" : ""], [null,true])) + "><div class=\"pattern-bg\"><div class=\"pattern-inner\"><div class=\"pattern-overlay\"></div></div></div><div id=\"content-top\" class=\"content-top\"><div class=\"content-settings\"></div></div>");
if(profileStatus === "active")
{
buf.push("<div class=\"content-main\">");
if(messages.upgrade_communication_show && messages.upgrade_communication && user.paymentStatus.type == "paid")
{
buf.push("<div class=\"sidebar\"><div class=\"sidebar-widget\"><span>" + (jade.escape(null == (jade_interp = _t("user", "text.to_let_any_non_paying_member")) ? "" : jade_interp)) + "</span><a" + (jade.attr("href", messages.upgrade_communication+user.id, true, false)) + " class=\"btn green\">" + (jade.escape(null == (jade_interp = _t("user", "text.upgrade_to_connect")) ? "" : jade_interp)) + "</a></div><div id=\"userright-banner-widget\" style=\"display: none;\" class=\"sidebar-widget\"><div id=\"openx-banner-placement-userright\" class=\"userright-banner-openx\"></div></div></div>");
}
else
{
buf.push("<div class=\"sidebar\"><div id=\"userright-banner-widget\" style=\"display: none;\" class=\"sidebar-widget\"><div id=\"openx-banner-placement-userright\" class=\"userright-banner-openx\"></div></div></div>");
}
buf.push("<div class=\"main-column\"><div id=\"user-profile\" class=\"b-user-profile\"><div class=\"usertop-banner\"><div id=\"openx-banner-placement-usertop\" class=\"usertop-banner-openx\"></div></div><div class=\"user-info-block\"><div class=\"b-screenname\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login) ? "" : jade_interp)) + "</span>");
if(user.statuses)
{
if(user.statuses.onlineStatus)
{
buf.push("<span class=\"status online\"></span>");
}
if(user.statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"status recently\"></span>");
}
}
if ( !user.isUserAdmin)
{
buf.push("<div class=\"b-features\">");
if(user.marks && user.marks.hotUser && user.marks.hotUser.upgradeUrl)
{
buf.push("<a" + (jade.attr("href", user.marks.hotUser.upgradeUrl, true, false)) + "><span" + (jade.attr("title", _t("user", "title.hot_user"), true, false)) + " class=\"item hot\">" + (jade.escape(null == (jade_interp = _t("user", "title.hot_user")) ? "" : jade_interp)) + "</span></a>");
}
if(user.invisibleMode)
{
if(forUser.invisibleMode)
{
buf.push("<span" + (jade.attr("title", _t("user", "title.invisible_mode"), true, false)) + " class=\"item invisible\">" + (jade.escape(null == (jade_interp = _t("user", "title.invisible_mode")) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<a" + (jade.attr("href", user.marks.invisible_mode.upgradeUrl, true, false)) + (jade.attr("title", _t("user", "title.invisible_mode"), true, false)) + " class=\"item invisible\">" + (null == (jade_interp = _t("user", "title.invisible_mode") + " &nbsp;") ? "" : jade_interp) + "</a>");
}
}
if(user.marks && user.marks.top_in_search && user.marks.top_in_search.upgradeUrl)
{
buf.push("<a" + (jade.attr("href", user.marks.top_in_search.upgradeUrl, true, false)) + "><button class=\"item vip\">" + (null == (jade_interp = _t("user", "title.top_in_search") + " &nbsp;") ? "" : jade_interp) + "</button></a>");
}
if(user.marks && user.marks.free_communication && user.marks.free_communication.show)
{
buf.push("<a" + (jade.attr("href", user.marks.free_communication.upgradeUrl, true, false)) + "><button class=\"item free\">" + (null == (jade_interp = _t("user", "title.free_communication") + " &nbsp;") ? "" : jade_interp) + "</button></a>");
}
if(user.statuses && user.statuses.newStatus)
{
buf.push("<button class=\"item new\">" + (jade.escape(null == (jade_interp = _t("user", "title.new_user")) ? "" : jade_interp)) + "</button>");
}
if(trustedValidation.trustedFunctionalEnabled && user.statuses && user.statuses.isTrusted)
{
buf.push("<span class=\"item trusted\">" + (jade.escape(null == (jade_interp = _t("user", "title.status.trusted")) ? "" : jade_interp)) + "<span class=\"b-tooltip\">" + (jade.escape(null == (jade_interp = _t("user", "tooltip.status.trusted")) ? "" : jade_interp)));
if(currentUser && !currentUser.isTrusted && trustedValidation.isAvailable)
{
buf.push("<a" + (jade.attr("href", trustedValidation.url, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("user", "tooltip.status.trusted.ppLink")) ? "" : jade_interp)) + "</a>");
}
buf.push("</span></span>");
}
buf.push("</div>");
}
buf.push("</div><div" + (jade.cls(['b-user-photo',(naughtyMode.naughtyClass)?naughtyMode.naughtyClass:""], [null,true])) + "><div class=\"image-holder\">");
if(naughtyMode.naughtyLevel == 1)
{
buf.push("<span class=\"b-blur-message\">" + (jade.escape(null == (jade_interp = _t("user", "title.naughty_photo")) ? "" : jade_interp)) + "</span>");
}
else if(naughtyMode.naughtyLevel == 2)
{
buf.push("<span class=\"b-blur-message\">" + (jade.escape(null == (jade_interp = _t("user", "title.explicit_Nudity")) ? "" : jade_interp)) + "</span>");
}
if(naughtyMode.naughtyMode)
{
buf.push("<div class=\"naughty-menu\">");
if ( naughtyMode.naughtyLevel === 2)
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("user", "title.switch_no_limits")) ? "" : jade_interp)) + "</div>");
}
else
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("user", "title.switch_sexy")) ? "" : jade_interp)) + "</div>");
}
buf.push("<div class=\"switcher extra-small\">");
if ( (naughtyMode.naughtyMode.enableNaughtyLevel))
{
// iterate naughtyMode.naughtyMode.enableNaughtyLevel
;(function(){
  var $$obj = naughtyMode.naughtyMode.enableNaughtyLevel;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode.naughtyMode.naughtyMode == key+1)?"active":""], [null,true])) + "><a" + (jade.attr("data-nm-trigger", key+1, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("user", "title." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode.naughtyMode.naughtyMode == key+1)?"active":""], [null,true])) + "><a" + (jade.attr("data-nm-trigger", key+1, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("user", "title." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  }
}).call(this);

}
buf.push("</div></div>");
}
if ( user.isUserAdmin)
{
buf.push("<a><img" + (jade.attr("src", (userPrimaryPhoto && userPrimaryPhoto.avatar) ? userPrimaryPhoto.avatar : user.photoUrl, true, false)) + " class=\"photo\"/></a>");
}
else
{
buf.push("<a" + (jade.attr("href", (upgrade.photo_big)?upgrade.photo_big:url, true, false)) + "><img" + (jade.attr("src", (userPrimaryPhoto && userPrimaryPhoto.avatar) ? userPrimaryPhoto.avatar : user.photoUrl, true, false)) + " class=\"photo\"/></a>");
}
buf.push("<div class=\"b-btn-activity\">");
if ( forAdmin)
{
if ( talksEnabled)
{
buf.push("<div class=\"b-btn-activity-grid\"><button" + (jade.attr("title", _t('user', 'button.message'), true, false)) + " class=\"btn-activity btn-message disactive\"></button></div>");
}
buf.push("<div class=\"b-btn-activity-grid\"><button type=\"button\"" + (jade.attr("title", _t("user", "button.add_friend"), true, false)) + " class=\"btn-activity btn-favorite\"><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("user", "button.add_friend")) ? "" : jade_interp)) + "</span></button></div><div class=\"b-btn-activity-grid\"><button type=\"button\"" + (jade.attr("title", _t("user", "button.wink"), true, false)) + " class=\"btn-activity btn-wink\"><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("user", "button.wink")) ? "" : jade_interp)) + "</span></button></div>");
}
else
{
if ( talksEnabled)
{
buf.push("<div class=\"b-btn-activity-grid\"><button data-chat=\"data-chat\"" + (jade.attr("title", _t('user', 'button.message'), true, false)) + " class=\"btn-activity btn-message\"></button></div>");
}
buf.push("<div class=\"b-btn-activity-grid\">");
if (friendsEnabled)
{
buf.push("<button type=\"button\"" + (jade.attr("data-friendsbutton", user.id, true, false)) + (jade.attr("title", _t("user", "tooltip.friendship." + ((buttons.friends.activated) ? "friend" : (buttons.friends.waiting) ? "requested" : "not_friend" )), true, false)) + (jade.cls(['btn-activity','btn-favorite',(buttons.friends.activated)?"active":(buttons.friends.waiting)?"waiting":""], [null,null,true])) + "><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("user", "button.add_friend")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("user", "button.friends")) ? "" : jade_interp)) + "</span><span class=\"remove\">" + (jade.escape(null == (jade_interp = _t("user", "button.remove")) ? "" : jade_interp)) + "</span><span class=\"waiting\">" + (jade.escape(null == (jade_interp = _t("user", "text.request_send")) ? "" : jade_interp)) + "</span></button>");
}
else
{
buf.push("<button type=\"button\"" + (jade.attr("data-favoritebutton", user.id, true, false)) + (jade.attr("title", (buttons.favorite.activated) ?  _t("user", "button.friends") : _t("user", "button.add_friend"), true, false)) + (jade.cls(['btn-activity','btn-favorite',(buttons.favorite.activated)?"active":""], [null,null,true])) + "><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("user", "button.add_friend")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("user", "button.friends")) ? "" : jade_interp)) + "</span></button>");
}
buf.push("</div><div class=\"b-btn-activity-grid\"><button type=\"button\"" + (jade.attr("data-winkbutton", user.id, true, false)) + (jade.attr("title", (buttons.wink.activated) ? _t("user", "button.winked") : _t("user", "button.wink"), true, false)) + (jade.cls(['btn-activity','btn-wink',(buttons.wink.activated)?"active":""], [null,null,true])) + "><span class=\"default\">" + (jade.escape(null == (jade_interp = _t("user", "button.wink")) ? "" : jade_interp)) + "</span><span class=\"alternate\">" + (jade.escape(null == (jade_interp = _t("user", "button.winked")) ? "" : jade_interp)) + "</span></button></div>");
}
buf.push("</div><div class=\"b-statuses\">");
if(userPhotoCount > 1)
{
buf.push("<span class=\"b-photos-count\">" + (jade.escape(null == (jade_interp = userPhotoCount) ? "" : jade_interp)) + "</span>");
}
buf.push("</div></div></div><div class=\"b-user-data\"><div class=\"data-wrap\"><div class=\"b-age\">" + (jade.escape(null == (jade_interp = _t("user", "text.n_years", {"{n}": user.age})) ? "" : jade_interp)) + "</div><div class=\"b-location\">");
if (user.geo.city != '')
{
buf.push("<span>" + (jade.escape((jade_interp = user.geo.city) == null ? '' : jade_interp)) + ",</span>");
}
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("searchForm", ' '+user.geo.country)) ? "" : jade_interp)) + "</span></div>");
if ( user.geo.distanceToMe)
{
buf.push("<div class=\"b-away\">" + (jade.escape(null == (jade_interp = _t("user", "title.away-number-" + user.distanceUnits, {"{n}": user.geo.distanceToMe})) ? "" : jade_interp)) + "</div>");
}
buf.push("</div><div data-view-more=\"status\" class=\"b-user-details minimized\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("user", "title.whats_on_gender_mind", { "{n}": user.data_gender })) ? "" : jade_interp)) + "</span>");
if(user.chat_up_line)
{
if(typeof user.chat_up_line_cut === 'string')
{
buf.push("<p class=\"cut\">" + (null == (jade_interp = user.chat_up_line_cut+'... ') ? "" : jade_interp) + "<a href=\"#\" data-view-more-link=\"more\">" + (jade.escape(null == (jade_interp = _t("user", "link.view_more")) ? "" : jade_interp)) + "</a></p><p class=\"large\">" + (null == (jade_interp = user.chat_up_line+' ') ? "" : jade_interp) + "<a href=\"#\" data-view-more-link=\"less\">" + (jade.escape(null == (jade_interp = _t("user", "link.view_less")) ? "" : jade_interp)) + "</a></p>");
}
else
{
buf.push("<p class=\"large\">" + (jade.escape(null == (jade_interp = user.chat_up_line) ? "" : jade_interp)) + "</p>");
}
}
else
{
buf.push("<p class=\"large\">" + (jade.escape(null == (jade_interp = _t("user", "description.no_info_shared")) ? "" : jade_interp)) + "</p>");
}
buf.push("</div></div></div><div class=\"user-info-block right-column\"><div data-view-more=\"description\" class=\"b-user-details minimized\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("user", "title.in_gender_own_words", { "{n}": user.data_gender })) ? "" : jade_interp)) + "</span>");
if(user.description)
{
if(typeof user.description_cut === 'string')
{
buf.push("<p class=\"cut\">" + (null == (jade_interp = user.description_cut+'... ') ? "" : jade_interp) + "<a href=\"#\" data-view-more-link=\"more\">" + (jade.escape(null == (jade_interp = _t("user", "link.view_more")) ? "" : jade_interp)) + "</a></p><p class=\"large\">" + (null == (jade_interp = user.description+' ') ? "" : jade_interp) + "<a href=\"#\" data-view-more-link=\"less\">" + (jade.escape(null == (jade_interp = _t("user", "link.view_less")) ? "" : jade_interp)) + "</a></p>");
}
else
{
buf.push("<p class=\"large\">" + (jade.escape(null == (jade_interp = user.description) ? "" : jade_interp)) + "</p>");
}
}
else
{
buf.push("<p>" + (jade.escape(null == (jade_interp = _t("user", "description.no_info_shared")) ? "" : jade_interp)) + "</p>");
}
buf.push("</div><div class=\"personal-info\"><div class=\"b-heading\">" + (jade.escape(null == (jade_interp = _t("user", "title.personal_info")) ? "" : jade_interp)) + "</div><div class=\"b-table\"><div" + (jade.cls(['b-column-wrap','body-info',user.data_gender], [null,null,true])) + ">");
jade_mixins["userInfo"](_t("user", "title.body_type"), "build");
jade_mixins["userInfo"](_t("user", "title.height"), "height");
jade_mixins["userInfo"](_t("user", "title.weight"), "weight");
jade_mixins["userInfo"](_t("user", "title.hair_color"), "hair_color");
jade_mixins["userInfo"](_t("user", "title.eyes_colour"), "eye_color");
jade_mixins["userInfo"](_t("user", "title.tattoo"), "tattoo");
jade_mixins["userInfo"](_t("user", "title.piercing"), "pierced");
buf.push("</div><div class=\"b-column-wrap social-info\">");
jade_mixins["userInfo"](_t("user", "title.marital_status"), "marital_status");
jade_mixins["userInfo"](_t("user", "title.orientation"), "sexual_orientation");
jade_mixins["userInfo"](_t("user", "title.living"), "living");
jade_mixins["userInfo"](_t("user", "title.income"), "income");
jade_mixins["userInfo"](_t("user", "title.children"), "children");
jade_mixins["userInfo"](_t("user", "title.smoke"), "smoke");
jade_mixins["userInfo"](_t("user", "title.drink"), "drink");
buf.push("</div><div class=\"b-column-wrap outlook-info\">");
jade_mixins["userInfo"](_t("user", "title.ethnic_origin"), "race");
jade_mixins["userInfo"](_t("user", "title.education"), "education");
jade_mixins["userInfo"](_t("user", "title.religion"), "religion");
buf.push("</div></div></div>");
if ( !user.isUserAdmin)
{
buf.push("<div class=\"looking-for\"></div>");
if ( lookingFor.show === "looking-for-block")
{
buf.push("<div class=\"b-heading\">" + (jade.escape(null == (jade_interp = _t("user", "title.looking_for_gender")) ? "" : jade_interp)) + "</div><div class=\"b-data-wrap\">");
if ( lookingFor.gender || lookingFor.age || (lookingFor.photoLevel && lookingFor.photoLevel.length))
{
buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("user", "title.gender")) ? "" : jade_interp)) + "</span>");
if ( lookingFor.gender)
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("user", lookingFor.gender)) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("user", "title.not_given")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div><div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("user", "title.age")) ? "" : jade_interp)) + "</span>");
if ( lookingFor.age)
{
buf.push("<span>" + (jade.escape(null == (jade_interp = lookingFor.age.from + "-" + lookingFor.age.to) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("user", "title.not_given")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div><div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("user", "title.looking_for_location")) ? "" : jade_interp)) + "</span>");
if ( lookingFor.location)
{
buf.push("<span>" + (jade.escape(null == (jade_interp = lookingFor.location) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("user", "title.not_given")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div>");
}
if ( (lookingFor.religion && lookingFor.religion.length) || (lookingFor.race && lookingFor.race.length))
{
buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("user", "title.lookin_for_religion")) ? "" : jade_interp)) + "</span>");
if ( lookingFor.religion && lookingFor.religion.length)
{
buf.push("<span>" + (jade.escape(null == (jade_interp = lookingFor.religion.join(", ").ucfirst()) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("user", "title.not_given")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div><div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("user", "title.lookin_for_ethnic_origin")) ? "" : jade_interp)) + "</span>");
if ( lookingFor.race && lookingFor.race.length)
{
buf.push("<span>" + (jade.escape(null == (jade_interp = lookingFor.race.join(", ").ucfirst()) ? "" : jade_interp)) + "</span>");
}
else
{
buf.push("<span>" + (jade.escape(null == (jade_interp = _t("user", "title.not_given")) ? "" : jade_interp)) + "</span>");
}
buf.push("</div>");
}
buf.push("</div>");
}
else
{
buf.push("<div class=\"upgrade-banner\"><div class=\"baner-holder\"><div class=\"title\">" + (jade.escape(null == (jade_interp = _t("user", "text.full_member_for_view", { "{n}": user.data_gender })) ? "" : jade_interp)) + "</div><a" + (jade.attr("href", lookingFor.upgrade, true, false)) + " class=\"btn green\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_in_now")) ? "" : jade_interp)) + "</a></div></div>");
}
}
buf.push("</div>");
if ( !user.isUserAdmin)
{
buf.push("<div class=\"user-info-block block_photos\"><div" + (jade.cls(['photo-wrap',(userPhotoCount == 1)?"one-photo":""], [null,true])) + ">");
if(naughtyMode.naughtyMode && !upgrade.photo_big)
{
buf.push("<div class=\"naughty-menu\"><div class=\"switcher extra-small\">");
if ( (naughtyMode.naughtyMode.enableNaughtyLevel))
{
// iterate naughtyMode.naughtyMode.enableNaughtyLevel
;(function(){
  var $$obj = naughtyMode.naughtyMode.enableNaughtyLevel;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode.naughtyMode.naughtyMode == key+1)?"active":""], [null,true])) + "><a" + (jade.attr("data-nm-trigger", key+1, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("user", "title." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode.naughtyMode.naughtyMode == key+1)?"active":""], [null,true])) + "><a" + (jade.attr("data-nm-trigger", key+1, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("user", "title." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  }
}).call(this);

}
buf.push("</div><div class=\"description\">" + (jade.escape(null == (jade_interp = _t("user", "title.switch_sexy_no_limits")) ? "" : jade_interp)) + "</div></div>");
}
if(upgrade && upgrade.photo_more)
{
buf.push("<div class=\"photos-upgrade-banner\"><span class=\"title\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_a_full_membership")) ? "" : jade_interp)) + "</span><a" + (jade.attr("href", upgrade.photo_more, true, false)) + "><button class=\"btn green small\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_it_now")) ? "" : jade_interp)) + "</button></a></div>");
}
buf.push("<div id=\"userPhotos\" class=\"photo-wrap photos\"></div>");
if ( (showTrustedBanner))
{
buf.push("<div class=\"trusted-banner\"><div class=\"banner-head\">" + (jade.escape(null == (jade_interp = _t("user", "text.obtain_trusted_status")) ? "" : jade_interp)) + "</div><div class=\"banner-body\"><ul class=\"benefit-list\">");
if ( (forUser.gender === "female"))
{
buf.push("<li>" + (jade.escape(null == (jade_interp = _t("user", "text.gain_your_matches")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("user", "text.get_many_times")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("user", "text.contact_any_man")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("user", "text.display_your_profile")) ? "" : jade_interp)) + "</li>");
}
else if ( (forUser.gender === "male"))
{
buf.push("<li>" + (jade.escape(null == (jade_interp = _t("user", "text.receive_messages_from")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("user", "text.get_many_times")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("user", "text.display_your_profile")) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul><div class=\"info\">" + (jade.escape(null == (jade_interp = _t("user", "text.more_likely_to_reply_" + forUser.gender)) ? "" : jade_interp)) + "</div><div class=\"wrap-btn\"><a" + (jade.attr("href", forUser.trustedValidation.urls.userProfile, true, false)) + " class=\"btn banner-linck\">" + (jade.escape(null == (jade_interp = _t("user", "text.get_trusted_status_" + forUser.gender)) ? "" : jade_interp)) + "</a></div></div></div>");
}
buf.push("</div></div>");
}
buf.push("</div><div id=\"openx-banner-placement-userbottom\" class=\"userbottom-banner-openx\"></div></div></div>");
}
else if(profileStatus === "hidden")
{
buf.push("<div class=\"profile-hidden\">" + (jade.escape(null == (jade_interp = _t("user", "text.profile_was_hidden")) ? "" : jade_interp)) + "</div>");
}
else if(profileStatus === "removed")
{
buf.push("<div class=\"profile-hidden\">" + (jade.escape(null == (jade_interp = _t("user", "text.profile_was_removed")) ? "" : jade_interp)) + "</div>");
}
buf.push("</div>");;return buf.join("");
};