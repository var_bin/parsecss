this["JST"] = this["JST"] || {};

this["JST"]["Profile"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),profile = locals_.profile,_t = locals_._t;
buf.push("<div class=\"w-my-profile\"><div class=\"pattern-bg\"><div class=\"pattern-inner\"><div class=\"pattern-overlay\"></div></div></div><div id=\"content-top\" class=\"content-top\"><div class=\"content-settings\">  </div></div><div class=\"content-main\"><div class=\"sidebar\"><div id=\"profileright-banner-widget\" style=\"display: none;\" class=\"sidebar-widget\"><div id=\"openx-banner-placement-profileright\" class=\"profileright-banner-openx\"></div></div></div><div class=\"main-column\"><div id=\"openx-banner-placement-profiletop\" class=\"profiletop-banner-openx\"></div><div id=\"my-profile\" class=\"b-my-profile\"><div class=\"profile-block\"><div class=\"b-screenname\"><div class=\"screenname\">" + (jade.escape(null == (jade_interp = profile.login) ? "" : jade_interp)) + "</div></div><div id=\"profilePhoto\" class=\"b-photo-primary\"></div><div class=\"b-features\"></div></div><div class=\"profile-block right-column photos-block\"><div class=\"upload-form\"><div id=\"profilePhotoUploader\"></div></div><div class=\"photo-upload-notice\">" + (jade.escape(null == (jade_interp = _t("profile", "text.photo_upload_notice")) ? "" : jade_interp)) + "</div><div class=\"photo-wrap\"><div id=\"profilePhotoList\"></div></div></div><div class=\"profile-block\"><div id=\"profileEditor\" class=\"b-right-column\"></div>");
if ((profile.trustedValidation.isAvailable))
{
buf.push("<div class=\"trusted-banner\"><div class=\"banner-head\">" + (jade.escape(null == (jade_interp = _t("profile", "text.obtain_trusted_status")) ? "" : jade_interp)) + "</div><div class=\"banner-body\"><ul class=\"benefit-list\">");
if ( (profile.genderKey === 2))
{
buf.push("<li>" + (jade.escape(null == (jade_interp = _t("profile", "text.gain_your_matches")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("profile", "text.get_many_times")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("profile", "text.contact_any_man")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("profile", "text.display_your_profile")) ? "" : jade_interp)) + "</li>");
}
else if ( (profile.genderKey === 1))
{
buf.push("<li>" + (jade.escape(null == (jade_interp = _t("profile", "text.receive_messages_from")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("profile", "text.get_many_times")) ? "" : jade_interp)) + "</li><li>" + (jade.escape(null == (jade_interp = _t("profile", "text.display_your_profile")) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul><div class=\"wrap-btn\"><a" + (jade.attr("href", profile.trustedValidation.urls.myProfile, true, false)) + " class=\"btn green banner-linck\">" + (jade.escape(null == (jade_interp = _t("profile", "text.get_trusted_status_" + (profile.genderKey === 2 ? "female" : "male"))) ? "" : jade_interp)) + "</a></div></div></div>");
}
buf.push("</div><div class=\"profile-block right-column\"><div id=\"profileDetails\" class=\"personal-info\"></div><div id=\"profileLookingFor\" class=\"looking-for\"></div></div></div><div id=\"openx-banner-placement-profilebottom\" class=\"profilebottom-banner-openx\"></div></div></div></div>");;return buf.join("");
};