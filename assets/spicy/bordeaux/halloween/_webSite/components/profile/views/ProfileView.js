var ProfileView = Backbone.View.extend({

    regions: {
        profilePhoto: '#profilePhoto',
        profilePhotoUploader: '#profilePhotoUploader',
        profilePhotoList: '#profilePhotoList',
        profileEditor: '#profileEditor',
        profileLookingFor: '#profileLookingFor',
        profileDetails: '#profileDetails'
    },

    events: {
        'click [data-tab-for]': function(event) {
            var self = this;
            $target = $(event.currentTarget);
            self.$el.find('[data-tab-for]').each(function() {
                var tabName = $(this).attr('data-tab-for');
                if (tabName) {
                    self.$el.find('#'+tabName).removeClass('active');
                }
                $(this).parent().removeClass('active');
            });
            $target.parent().addClass('active');
            var tabName = $target.attr('data-tab-for');
            if (tabName) {
                self.$el.find('#'+tabName).addClass('active');
            }
        }
    },

    initialize: function() {
        this.model.fetch();

        this.listenTo(this.model, 'change', this.renderProfile);
        this.listenTo(this.model, 'sync', this.processBanners);

        $('.footer').removeClass('no-sidebar');
    },

    renderProfile: function() {
        var self = this,
            modelData = this.model.toJSON(),
            primaryPhoto = (modelData.myPhotos && modelData.myPhotos.primary) ? modelData.myPhotos.primary : {},
            profile = modelData.profile,
            useFbPhotos = !!profile.networks,
            preloadedLocationList = modelData.userAttributesDictionaries.locations,
            dateFormat = modelData.userAttributesDictionaries.dateFormat,
            fieldsData = modelData.userAttributesDictionaries.fields;

        this.$el.html(tpl.render('Profile', modelData));

        _.defer(function() {
            self.regions.profilePhoto.render(ProfilePhotoView, {
                model: new ProfilePhotoModel(primaryPhoto)
            });
        });

        _.defer(function() {
            self.regions.profilePhotoList.render(ProfilePhotoListView, {
                collection: new ProfilePhotoListCollection(_.values(modelData.myPhotos && modelData.myPhotos.gallery || {}), {
                    primary: primaryPhoto
                })
            });
        });

        _.defer(function() {
            self.regions.profilePhotoUploader.render(ProfilePhotoUploadView, {
                model: new ProfilePhotoUploadModel({
                    myPhotos   : modelData.myPhotos,
                    validation : modelData.photoUploadValidation || {}
                }),
                fbAppSettings: modelData.fbAppSettings,
                fbListType: 'popup'
            });
        });

        _.defer(function() {
            self.regions.profileEditor.render(ProfileEditorView, {
                model: new ProfileEditorModel(_.extend(profile, {
                    preloadedLocationList: preloadedLocationList,
                    dateFormat: dateFormat,
                    fieldsData: fieldsData
                }))
            });
        });

        _.defer(function() {
            self.regions.profileLookingFor.render(ProfileLookingForView, {
                model: new ProfileLookingForModel(_.extend(profile, {
                    fieldsData: fieldsData
                }))
            });
        });

        _.defer(function() {
            self.regions.profileDetails.render(ProfileDetailsView, {
                model: new ProfileDetailsModel(_.extend(profile, {
                    fieldsData: fieldsData
                }))
            });
        });

        this.region.ready();

        return this;
    },

    processBanners: function() {
        Backbone.trigger('BannerOpenx.process', {
            'id' : 'profiletop,profile,profileright,profilebottom',
            'container': '#openx-banner-placement-',
            'asyncLoading': false,
            'success': function(data) {
                $('.'+data.div_id).parents('.openx-banner').addClass('active');
            }
        });
    }
});
