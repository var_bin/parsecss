this["JST"] = this["JST"] || {};

this["JST"]["ProfileDetails"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,profile = locals_.profile,formParams = locals_.formParams;
buf.push("<div class=\"b-heading\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "title.personal_info")) ? "" : jade_interp)) + "</div><div class=\"b-table\">");
var bodyList = ["height", "weight", "build", "hair_color", "eye_color", "tattoo", "pierced"];
var socialList = ["sexual_orientation", "marital_status", "children", "living", "smoke", "drink", "income"];
var outlookList = ["race", "education", "religion"];
{
buf.push("<div" + (jade.cls(['b-column-wrap','body-info',profile.gender], [null,null,true])) + ">");
// iterate bodyList
;(function(){
  var $$obj = bodyList;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.' + field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field small select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.' + field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field small select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  }
}).call(this);

buf.push("</div><div class=\"b-column-wrap social-info\">");
// iterate socialList
;(function(){
  var $$obj = socialList;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.' + field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field small select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.' + field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field small select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  }
}).call(this);

buf.push("</div><div class=\"b-column-wrap outlook-info\">");
// iterate outlookList
;(function(){
  var $$obj = outlookList;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.' + field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field small select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var field = $$obj[$index];

buf.push("<div class=\"data-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t('profileDetails', 'title.' + field)) ? "" : jade_interp)) + "</span><div data-ui-select=\"data-ui-select\" class=\"field small select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile[field], true, false)) + (jade.attr("name", field, true, false)) + " data-control=\"data-control\"" + (jade.attr("id", field, true, false)) + "/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.values[field]
;(function(){
  var $$obj = formParams.values[field];
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = value) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div>");
    }

  }
}).call(this);

buf.push("</div><div id=\"coregPlacementProfileDetails\" class=\"coreg-placement-on-profile-editor checkbox-info\"></div><div class=\"b-buttons\"><button data-btn-undo=\"1\" class=\"btn small\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "button.cancel")) ? "" : jade_interp)) + "</button><button data-btn-save=\"1\" class=\"btn orange small\">" + (jade.escape(null == (jade_interp = _t("profileDetails", "button.save")) ? "" : jade_interp)) + "</button></div>");
}
buf.push("</div>");;return buf.join("");
};