this["JST"] = this["JST"] || {};

this["JST"]["UserNav"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,photoUrl = locals_.photoUrl,login = locals_.login,trustedValidation = locals_.trustedValidation;
buf.push("<div class=\"b-user-nav\"><div id=\"userNavPhoto\"" + (jade.attr("title", _t("userNav", "title.message"), true, false)) + " class=\"photo-holder\"><div class=\"nav-icon\"></div><span class=\"overlay\"></span><div class=\"image-holder\"><img" + (jade.attr("src", photoUrl, true, false)) + (jade.attr("title", login, true, false)) + " class=\"photo\"/></div></div><div id=\"userNavDropdown\" class=\"b-dropdown-menu\"><ul class=\"nav\"><li class=\"item\"><a href=\"/#account\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.settings")) ? "" : jade_interp)) + "</a></li><li class=\"item\"><a href=\"/#profile\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.profile")) ? "" : jade_interp)) + "</a></li>");
if (trustedValidation.isAvailable)
{
buf.push("<li class=\"item trusted-item\"><a" + (jade.attr("href", trustedValidation.urls.menu, true, false)) + " class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.trusted_link")) ? "" : jade_interp)) + "</a></li>");
}
buf.push("<li class=\"item\"><a href=\"/#site/contactUs\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.contact_us")) ? "" : jade_interp)) + "</a></li><li class=\"item\"><a id=\"site-logout\" href=\"/site/logout\" class=\"link\">" + (jade.escape(null == (jade_interp = _t("userNav", "title.log_out")) ? "" : jade_interp)) + "</a></li></ul></div></div>");;return buf.join("");
};