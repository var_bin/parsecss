this["JST"] = this["JST"] || {};

this["JST"]["UserListWidget"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),user = locals_.user,naughtyMode = locals_.naughtyMode,currentUser = locals_.currentUser,userLink = locals_.userLink,_t = locals_._t,trustedValidation = locals_.trustedValidation;
buf.push("<li class=\"b-grid-item\">");
if(!user.upgradeBanner)
{
var userClass = [];
if(user.marks && user.marks.highlight_profile) userClass.push("highlight")
if(naughtyMode.naughtyClass) userClass.push(naughtyMode.naughtyClass)
if(!currentUser.photoAccess) userClass.push("free");
buf.push("<div" + (jade.attr("data-user-id", user.id, true, false)) + (jade.cls(['b-user',userClass], [null,true])) + "><div class=\"image-centerer\"><div class=\"image-holder\"><img" + (jade.attr("src", user.photos.url, true, false)) + " class=\"photo\"/></div></div>");
if(user.photos.count > 1 && user.photos.showCount)
{
buf.push("<span class=\"b-photos-count\">" + (jade.escape((jade_interp = user.photos.count) == null ? '' : jade_interp)) + "</span>");
}
buf.push("<a" + (jade.attr("href", userLink(user.id), true, false)) + " data-photo=\"\" class=\"overlay\"></a>");
if(naughtyMode.naughtyLevel == 1)
{
buf.push("<span class=\"b-blur-message\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.naughty_photo")) ? "" : jade_interp)) + "</span>");
}
else if(naughtyMode.naughtyLevel == 2)
{
buf.push("<span class=\"b-blur-message\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.explicit_nudity")) ? "" : jade_interp)) + "</span>");
}
if(naughtyMode.naughtyMode)
{
buf.push("<div id=\"naughtyMode\" class=\"naughty-menu\">");
if ( naughtyMode.naughtyLevel === 2)
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_no_limits")) ? "" : jade_interp)) + "</div>");
}
else
{
buf.push("<div class=\"description\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.switch_sexy")) ? "" : jade_interp)) + "</div>");
}
buf.push("<div class=\"switcher extra-small\">");
if ( (naughtyMode.naughtyMode.enableNaughtyLevel))
{
// iterate naughtyMode.naughtyMode.enableNaughtyLevel
;(function(){
  var $$obj = naughtyMode.naughtyMode.enableNaughtyLevel;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode.naughtyMode.naughtyMode == key+1)?"active":""], [null,true])) + "><a" + (jade.attr("data-nm-trigger", key+1, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode.naughtyMode.naughtyMode == key+1)?"active":""], [null,true])) + "><a" + (jade.attr("data-nm-trigger", key+1, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  }
}).call(this);

}
buf.push("</div></div>");
}
buf.push("<div class=\"b-user-info\"><h2 class=\"b-screenname\"><a" + (jade.attr("href", userLink(user.id), true, false)) + " data-screenname=\"\" class=\"link\"><span class=\"screenname\">" + (jade.escape(null == (jade_interp = user.login + " , " + user.age) ? "" : jade_interp)) + "</span>");
if(user.statuses)
{
if(user.statuses.onlineStatus)
{
buf.push("<span class=\"status online\"></span>");
}
else if(user.statuses.recentlyOnlineStatus)
{
buf.push("<span class=\"status r-online\"></span>");
}
}
buf.push("</a></h2>");
if((user.marks && ((user.marks.hotUser && user.marks.hotUser.upgradeUrl) || (user.marks.top_in_search && user.marks.top_in_search.upgradeUrl)
|| (user.marks.free_communication && user.marks.free_communication.show))) || user.marks.invisible_mode || (user.statuses && user.statuses.newStatus))
{
buf.push("<div class=\"b-features\">");
if(user.marks && user.marks.hotUser && user.marks.hotUser.upgradeUrl)
{
buf.push("<a" + (jade.attr("href", user.marks.hotUser.upgradeUrl, true, false)) + "><button class=\"item hot\">" + (null == (jade_interp = _t("userListWidget", "title.hot_user") + " &nbsp;") ? "" : jade_interp) + "</button></a>");
}
if(user.marks && user.marks.top_in_search && user.marks.top_in_search.upgradeUrl)
{
buf.push("<a" + (jade.attr("href", user.marks.top_in_search.upgradeUrl, true, false)) + "><button class=\"item top\">" + (null == (jade_interp = _t("userListWidget", "title.top_in_search") + " &nbsp;") ? "" : jade_interp) + "</button></a>");
}
if(user.marks && user.marks.free_communication && user.marks.free_communication.show)
{
buf.push("<a" + (jade.attr("href", user.marks.free_communication.upgradeUrl, true, false)) + "><button class=\"item free-m\">" + (null == (jade_interp = _t("userListWidget", "title.free_communication") + " &nbsp;") ? "" : jade_interp) + "</button></a>");
}
if(user.marks.invisible_mode && user.marks.invisible_mode.exist)
{
buf.push("<a" + (jade.attr("href", user.marks.invisible_mode.upgradeUrl, true, false)) + (jade.attr("title", _t("userListWidget", "title.invisible_mode"), true, false)) + "><button class=\"item invisible\">" + (null == (jade_interp = _t("userListWidget", "title.invisible_mode") + " &nbsp;") ? "" : jade_interp) + "</button></a>");
}
if(user.statuses && user.statuses.newStatus)
{
buf.push("<button class=\"item b-new\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.new")) ? "" : jade_interp)) + "</button>");
}
if(trustedValidation.trustedFunctionalEnabled && user.statuses && user.statuses.isTrusted)
{
buf.push("<span class=\"item trusted\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "title.status.trusted")) ? "" : jade_interp)) + "<span class=\"b-tooltip\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "tooltip.status.trusted")) ? "" : jade_interp)));
if(currentUser && !currentUser.isTrusted && trustedValidation.isAvailable)
{
buf.push("<a" + (jade.attr("href", trustedValidation.url, true, false)) + ">" + (jade.escape(null == (jade_interp = _t("userListWidget", "tooltip.status.trusted.ppLink")) ? "" : jade_interp)) + "</a>");
}
buf.push("</span></span>");
}
buf.push("</div>");
}
buf.push("</div><div data-btn-activity=\"1\" class=\"b-btn-activity\"></div></div>");
}
else
{
buf.push("<div class=\"upgrade-banner\"><div class=\"um-content square\"><div" + (jade.cls(['main',(currentUser.gender === "male")?"one-img-m":"one-img-w"], [null,true])) + "><div class=\"title\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "text.get_a_full_membership")) ? "" : jade_interp)) + "</div><div class=\"button-holder\"><a" + (jade.attr("href", user.upgradeBanner, true, false)) + " class=\"btn green\">" + (jade.escape(null == (jade_interp = _t("userListWidget", "button.get_it_now")) ? "" : jade_interp)) + "</a></div></div></div></div>");
}
buf.push("</li>");;return buf.join("");
};