this["JST"] = this["JST"] || {};

this["JST"]["ProfileEditor"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),profile = locals_.profile,_t = locals_._t,formParams = locals_.formParams;
if ( profile)
{
buf.push("<div class=\"profile-details-single\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.birthday")) ? "" : jade_interp)) + "</span><div class=\"b-details-value\"><div data-ui-date-select=\"data-ui-date-select\" class=\"field\"><input type=\"hidden\" name=\"birthday\" data-control=\"data-control\" data-date-select-value=\"1\" id=\"birthday\"/>");
// iterate formParams.dateFormat
;(function(){
  var $$obj = formParams.dateFormat;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"field select-widget year\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + ">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + ">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"field select-widget day\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.day, true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
for(var i = 1; i <= 31; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + ">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"field select-widget month\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.month, true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><div class=\"b-dropdown-menu\"><ul class=\"nav\"><li data-item=\"1\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.january")) ? "" : jade_interp)) + "</li><li data-item=\"2\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.february")) ? "" : jade_interp)) + "</li><li data-item=\"3\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.march")) ? "" : jade_interp)) + "</li><li data-item=\"4\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.april")) ? "" : jade_interp)) + "</li><li data-item=\"5\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.may")) ? "" : jade_interp)) + "</li><li data-item=\"6\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.june")) ? "" : jade_interp)) + "</li><li data-item=\"7\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.july")) ? "" : jade_interp)) + "</li><li data-item=\"8\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.august")) ? "" : jade_interp)) + "</li><li data-item=\"9\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.september")) ? "" : jade_interp)) + "</li><li data-item=\"10\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.october")) ? "" : jade_interp)) + "</li><li data-item=\"11\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.november")) ? "" : jade_interp)) + "</li><li data-item=\"12\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.december")) ? "" : jade_interp)) + "</li></ul></div></div></div></div>");
}
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var value = $$obj[key];

if ( value == "d")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-year=\"data-date-select-year\" class=\"field select-widget year\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.year, true, false)) + " name=\"birthday_year\" data-control=\"data-control\" id=\"birthday_year\"/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
// iterate formParams.years
;(function(){
  var $$obj = formParams.years;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + ">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var year = $$obj[key];

buf.push("<li" + (jade.attr("data-item", year, true, false)) + ">" + (jade.escape(null == (jade_interp = year) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div></div>");
}
else if ( value == "m")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-day=\"data-date-select-day\" class=\"field select-widget day\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.day, true, false)) + " name=\"birthday_day\" data-control=\"data-control\" id=\"birthday_day\"/><div class=\"b-dropdown-menu\"><ul class=\"nav\">");
for(var i = 1; i <= 31; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + ">" + (jade.escape(null == (jade_interp = (i < 10 ? "0" : "") + i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div></div>");
}
else if ( value == "y")
{
buf.push("<div data-ui-select=\"data-ui-select\" data-date-select-month=\"data-date-select-month\" class=\"field select-widget month\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", profile.birthdayDate.month, true, false)) + " name=\"birthday_month\" data-control=\"data-control\" id=\"birthday_month\"/><div class=\"b-dropdown-menu\"><ul class=\"nav\"><li data-item=\"1\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.january")) ? "" : jade_interp)) + "</li><li data-item=\"2\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.february")) ? "" : jade_interp)) + "</li><li data-item=\"3\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.march")) ? "" : jade_interp)) + "</li><li data-item=\"4\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.april")) ? "" : jade_interp)) + "</li><li data-item=\"5\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.may")) ? "" : jade_interp)) + "</li><li data-item=\"6\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.june")) ? "" : jade_interp)) + "</li><li data-item=\"7\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.july")) ? "" : jade_interp)) + "</li><li data-item=\"8\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.august")) ? "" : jade_interp)) + "</li><li data-item=\"9\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.september")) ? "" : jade_interp)) + "</li><li data-item=\"10\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.october")) ? "" : jade_interp)) + "</li><li data-item=\"11\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.november")) ? "" : jade_interp)) + "</li><li data-item=\"12\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "value.december")) ? "" : jade_interp)) + "</li></ul></div></div></div></div>");
}
    }

  }
}).call(this);

buf.push("<div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div><div class=\"profile-details-single location-block\"><span class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.location")) ? "" : jade_interp)) + "</span><div class=\"b-details-value\"><div data-ui-location=\"data-ui-location\" class=\"field select-widget location\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"data-value\"><input name=\"location\" type=\"text\"" + (jade.attr("value", profile.location, true, false)) + " autocomplete=\"off\" data-control=\"data-control\" class=\"input-value select-input value\"/></div><div class=\"b-dropdown-menu\"><div data-list=\"data-list\" class=\"select-location location\"></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div><div class=\"profile-details-single\"><div class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.what_mind")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><div data-ui-textarea=\"data-ui-textarea\" class=\"field\"><div class=\"b-details-value\"><div id=\"textarea\" class=\"textarea-wrap\"><textarea name=\"chat_up_line\" data-resize=\"auto\" data-control=\"data-control\" id=\"status\" class=\"textarea long\">" + (jade.escape(null == (jade_interp = profile.chat_up_line) ? "" : jade_interp)) + "</textarea></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div></div><div class=\"profile-details-single\"><div class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "title.in_my_words")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><div data-ui-textarea=\"data-ui-textarea\" class=\"field\"><div class=\"b-details-value\"><div id=\"textarea\" class=\"textarea-wrap\"><textarea name=\"description\" data-resize=\"auto\" data-control=\"data-control\" id=\"about\" class=\"textarea long\">" + (jade.escape(null == (jade_interp = profile.description) ? "" : jade_interp)) + "</textarea></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div></div><div id=\"coregPlacementEditAge\" class=\"coreg-placement-on-profile-editor checkbox-info\"></div><div class=\"b-buttons\"><button data-btn-undo=\"1\" class=\"btn small\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "button.cancel")) ? "" : jade_interp)) + "</button><button data-btn-save=\"1\" class=\"btn orange small\">" + (jade.escape(null == (jade_interp = _t("profileEditor", "button.save")) ? "" : jade_interp)) + "</button></div>");
};return buf.join("");
};