this["JST"] = this["JST"] || {};

this["JST"]["CoregistrationPopupTemplate"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),naughtyMode = locals_.naughtyMode,_t = locals_._t,label_translation = locals_.label_translation,termsString = locals_.termsString;
buf.push("<div class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\">");
if ( naughtyMode == 3)
{
buf.push("<div id=\"coreg-popup-item\" class=\"coreg-naughty b-popup b-coreg-popup\"><a id=\"popup-close\" title=\"Close\" class=\"btn-close\"></a><div class=\"b-popup-content\"><h1>" + (jade.escape(null == (jade_interp = _t("coregistration", "coreg.title")) ? "" : jade_interp)) + "</h1><span class=\"coreg-popup_mesage\">" + (jade.escape(null == (jade_interp = label_translation) ? "" : jade_interp)) + "</span><form><div class=\"b-wrapp-btn\"><input type=\"submit\"" + (jade.attr("value", _t("coregistration", "coreg.ok"), true, false)) + " id=\"popup-continue\" class=\"btn green\"/></div></form><p class=\"text-corg-popup-terms\">" + (null == (jade_interp = termsString) ? "" : jade_interp) + "</p></div></div>");
}
else
{
buf.push("<div id=\"coreg-popup-item\" class=\"b-popup b-coreg-popup\"><a id=\"popup-close\" title=\"Close\" class=\"btn-close\"></a><div class=\"b-popup-content\"><h1>" + (jade.escape(null == (jade_interp = _t("coregistration", "coreg.title")) ? "" : jade_interp)) + "</h1><span class=\"coreg-popup_mesage\">" + (jade.escape(null == (jade_interp = label_translation) ? "" : jade_interp)) + "</span><form><div class=\"b-wrapp-btn\"><input type=\"submit\"" + (jade.attr("value", _t("coregistration", "coreg.ok"), true, false)) + " id=\"popup-continue\" class=\"btn green\"/></div></form><p class=\"text-corg-popup-terms\">" + (null == (jade_interp = termsString) ? "" : jade_interp) + "</p></div></div>");
}
buf.push("</div></div><div class=\"b-loader\"></div></div>");;return buf.join("");
};