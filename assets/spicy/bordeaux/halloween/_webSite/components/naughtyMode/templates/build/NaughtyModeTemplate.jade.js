this["JST"] = this["JST"] || {};

this["JST"]["NaughtyModeTemplate"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),naughtyMode = locals_.naughtyMode,_t = locals_._t,enableNaughtyLevel = locals_.enableNaughtyLevel,model = locals_.model,locale = locals_.locale;
if(naughtyMode)
{
buf.push("<div class=\"naughty-widget menu-widget l-toolbar-widgets\"><div class=\"widget-button\"><div class=\"title\">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "title.naughty_mode")) ? "" : jade_interp)) + "</div><div data-nm-value=\"1\" class=\"mode\"></div></div><div class=\"b-dropdown-menu\"><div class=\"switcher small\">");
if ( enableNaughtyLevel)
{
// iterate enableNaughtyLevel
;(function(){
  var $$obj = enableNaughtyLevel;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode == key+1)?"active":""], [null,true])) + "><a href=\"#\"" + (jade.attr("data-nm-option", key+1, true, false)) + " class=\"on\">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "value." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div" + (jade.cls(['switcher-cell',(naughtyMode == key+1)?"active":""], [null,true])) + "><a href=\"#\"" + (jade.attr("data-nm-option", key+1, true, false)) + " class=\"on\">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "value." + val)) ? "" : jade_interp)) + "</a></div>");
    }

  }
}).call(this);

}
buf.push("</div><div class=\"mode-description\">");
if ( enableNaughtyLevel)
{
// iterate enableNaughtyLevel
;(function(){
  var $$obj = enableNaughtyLevel;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var val = $$obj[key];

buf.push("<div" + (jade.attr("data-nm-description", key+1, true, false)) + (jade.cls(['description',val], [null,true])) + ">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "text.mode_description." + val)) ? "" : jade_interp)) + "</div>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var val = $$obj[key];

buf.push("<div" + (jade.attr("data-nm-description", key+1, true, false)) + (jade.cls(['description',val], [null,true])) + ">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "text.mode_description." + val)) ? "" : jade_interp)) + "</div>");
    }

  }
}).call(this);

}
buf.push("</div></div></div><div data-restriction-popup=\"1\" style=\"display:none\"><div class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\"><div data-popup-body=\"1\" class=\"b-popup b-restrictions\"><div data-content-main=\"1\" class=\"b-popup-content\"><a title=\"Close\" data-close-popup=\"1\" class=\"btn-close\"></a><h1 class=\"title\"><span class=\"you-sure\">" + (jade.escape(null == (jade_interp = _t("naughtyMode", "text.rp-title")) ? "" : jade_interp)) + "</span></h1><span class=\"line\"></span><ol class=\"list-help-restrictions\"><li><div class=\"right-side\"><ul class=\"user-list\"><li><span>" + (jade.escape(null == (jade_interp = _t("naughtyMode","Explicit nudity")) ? "" : jade_interp)) + "</span></li><li><span>" + (jade.escape(null == (jade_interp = _t("naughtyMode","Naughty photo")) ? "" : jade_interp)) + "</span></li></ul></div><div class=\"left-side\">" + (null == (jade_interp = _t('naughtyMode', 'text.you-need-agree-1', {'{restrictionLink}': '<a href="#" id="contentRestrictions">' + _t("naughtyMode", "text.restriction-link") + '</a>'})) ? "" : jade_interp) + "<p>" + (jade.escape(null == (jade_interp = _t('naughtyMode', 'text.you-need-agree-2')) ? "" : jade_interp)) + "<br/><label><input type=\"checkbox\" data-restriction-content=\"1\" value=\"0\"/><span>" + (jade.escape(null == (jade_interp = _t('naughtyMode', 'text.rp-checkbox', {'{sitename}':model.siteName})) ? "" : jade_interp)) + "</span></label></p><p>" + (jade.escape(null == (jade_interp = _t('naughtyMode', 'text.rp-if-you-agree')) ? "" : jade_interp)) + "</p></div></li><span class=\"line\"></span><li><div class=\"right-side\"><span" + (jade.cls(["switch-naughty-mode switch-naughty-mode-" + (locale) + ""], [true])) + "></span></div><div class=\"left-side\">" + (jade.escape(null == (jade_interp = _t('naughtyMode', 'text.rp-switch-naughty-mode')) ? "" : jade_interp)) + "</div></li><span class=\"line\"></span></ol><div class=\"b-wrapp-btn\"><input type=\"submit\" value=\"OK\" data-agree-restriction='1' class=\"btn btn-continue no-margin\"/></div></div><div data-content-inner=\"1\" style=\"display:none\" class=\"b-popup-content\"><a title=\"Close\" data-close-popup=\"1\" class=\"btn-close\"></a><h1 class=\"title\"><span class=\"you-sure\">" + (null == (jade_interp = _t('naughtyMode', 'title.rp-content-restriction')) ? "" : jade_interp) + "</span></h1><span class=\"line\"></span><div class=\"restrictions-scroll-block\">" + (null == (jade_interp = _t('naughtyMode', 'text.rp-restriction-part-1', {'{siteName}':model.siteName})) ? "" : jade_interp) + (null == (jade_interp = _t('naughtyMode', 'text.rp-restriction-part-2', {'{siteName}':model.siteName})) ? "" : jade_interp) + "</div><div class=\"b-wrapp-btn\"><a data-show-main='1' class=\"btn btn-continue no-margin\">" + (jade.escape(null == (jade_interp = _t('naughtyMode', 'text.go-back')) ? "" : jade_interp)) + "</a></div></div></div></div></div></div></div>");
};return buf.join("");
};