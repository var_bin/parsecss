this["JST"] = this["JST"] || {};

this["JST"]["ProfileLookingFor"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,profile = locals_.profile,formParams = locals_.formParams;
buf.push("<div class=\"b-heading\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.looking_for")) ? "" : jade_interp)) + "</div><div class=\"b-data-wrap\"><div class=\"data-single gender\"><div class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.gender")) ? "" : jade_interp)) + "</div><div data-ui-select=\"data-ui-select\" class=\"field select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "value.female")) ? "" : jade_interp)) + "</div><div class=\"select-gender\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.gender, true, false)) + " name=\"lookingForGender\" data-control=\"data-control\" id=\"lookingForGender\"/><div class=\"b-dropdown-menu drop-up\"><ul class=\"nav\">");
// iterate formParams.values.lookingFor.gender
;(function(){
  var $$obj = formParams.values.lookingFor.gender;
  if ('number' == typeof $$obj.length) {

    for (var key = 0, $$l = $$obj.length; key < $$l; key++) {
      var gender = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = gender) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var key in $$obj) {
      $$l++;      var gender = $$obj[key];

buf.push("<li" + (jade.attr("data-item", key, true, false)) + ">" + (jade.escape(null == (jade_interp = gender) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

buf.push("</ul></div></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div><div class=\"data-single age\"><div class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.age")) ? "" : jade_interp)) + "</div><div class=\"field select-widget select-range\"><div id=\"search-form-select-age-from\" data-ui-select=\"data-ui-select\" data-select-max=\"lookingForAgeTo\" class=\"range-start select-single\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"select-input value\"></div><div class=\"select-age age-from\"><div class=\"b-dropdown-menu drop-up age_from\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.age.from, true, false)) + " name=\"lookingForAge[from]\" data-control=\"data-control\" id=\"lookingForAgeFrom\"/><ul data-items-list=\"data-items-list\" class=\"nav\">");
for(var i = formParams.values.lookingFor.age.min; i <= formParams.values.lookingFor.age.max; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + ">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div><span class=\"separator\">-</span><div id=\"profile-age-to\" data-ui-select=\"data-ui-select\" data-select-min=\"lookingForAgeFrom\" class=\"range-end select-single\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"select-input value\"></div><div class=\"select-age age-to\"><div class=\"b-dropdown-menu drop-up age_to\"><input type=\"hidden\"" + (jade.attr("value", profile.lookingFor.age.to, true, false)) + " name=\"lookingForAge[to]\" data-control=\"data-control\" id=\"lookingForAgeTo\"/><ul data-items-list=\"data-items-list\" class=\"nav\">");
for(var i = formParams.values.lookingFor.age.min; i <= formParams.values.lookingFor.age.max; i++)
{
buf.push("<li" + (jade.attr("data-item", i, true, false)) + ">" + (jade.escape(null == (jade_interp = i) ? "" : jade_interp)) + "</li>");
}
buf.push("</ul></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div></div><div class=\"data-single location\"><div class=\"b-details-title\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "title.location")) ? "" : jade_interp)) + "</div><div data-ui-location=\"data-ui-location\" class=\"field select-widget\"><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"data-value\" class=\"data-value\"><input name=\"lookingForCountryCode\" type=\"hidden\"" + (jade.attr("value", profile.lookingFor.country, true, false)) + "/><input name=\"lookingForLocation\" type=\"text\"" + (jade.attr("value", profile.lookingFor.location, true, false)) + " autocomplete=\"off\" data-control=\"data-control\" class=\"input-value select-input value\"/></div><div class=\"b-dropdown-menu drop-up\"><div data-list=\"data-list\" class=\"select-location location\"></div></div></div><div data-ui-error-text=\"1\" class=\"b-error\"></div></div></div><div id=\"coregPlacementPersonalInfo\" class=\"coreg-placement-on-profile-editor checkbox-info\"></div><div class=\"b-buttons\"><button data-btn-undo=\"1\" class=\"btn small\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "button.cancel")) ? "" : jade_interp)) + "</button><button data-btn-save=\"1\" class=\"btn orange small\">" + (jade.escape(null == (jade_interp = _t("profileLookingFor", "button.save")) ? "" : jade_interp)) + "</button></div></div>");;return buf.join("");
};