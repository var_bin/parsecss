this["JST"] = this["JST"] || {};

this["JST"]["UserListLayout"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,type = locals_.type,availableTabs = locals_.availableTabs,searchType = locals_.searchType;
jade_mixins["searchType"] = function(name, searchType){
var block = (this && this.block), attributes = (this && this.attributes) || {};
var switcherClass = (name === searchType ? 'active' : '')
buf.push("<div" + (jade.cls(['switcher-cell',switcherClass], [null,true])) + ">");
switch (name){
case "near_you":
buf.push("<a data-item=\"near_you\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.all_members")) ? "" : jade_interp)) + "</a>");
  break;
case "all_members":
buf.push("<a data-item=\"all_members\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.tab.all_members")) ? "" : jade_interp)) + "</a>");
  break;
case "online":
buf.push("<a data-item=\"online\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.online_now")) ? "" : jade_interp)) + "</a>");
  break;
case "new_members":
buf.push("<a data-item=\"new_members\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.new_members")) ? "" : jade_interp)) + "</a>");
  break;
case "livecam":
buf.push("<a data-item=\"livecam\" class=\"item\">" + (jade.escape(null == (jade_interp = _t("userList", "title.livecam")) ? "" : jade_interp)) + "</a>");
  break;
}
buf.push("</div>");
};
buf.push("<button style=\"display: none;\" class=\"to-top\">" + (jade.escape(null == (jade_interp = _t("userList", "title.top")) ? "" : jade_interp)) + "</button><div class=\"w-search user-free\"><div class=\"pattern-bg\"><div class=\"pattern-inner\"><div class=\"pattern-overlay\"></div></div></div><div class=\"content-top\"><div class=\"sidebar-holder\"></div>");
if ( type === 'Search')
{
buf.push("<div class=\"content-settings\"><div id=\"userListSort\" class=\"switcher\">");
// iterate availableTabs
;(function(){
  var $$obj = availableTabs;
  if ('number' == typeof $$obj.length) {

    for (var $index = 0, $$l = $$obj.length; $index < $$l; $index++) {
      var view = $$obj[$index];

jade_mixins["searchType"](view, searchType);
    }

  } else {
    var $$l = 0;
    for (var $index in $$obj) {
      $$l++;      var view = $$obj[$index];

jade_mixins["searchType"](view, searchType);
    }

  }
}).call(this);

buf.push("</div><div id=\"sortTypes\" class=\"sort-by\"></div><div id=\"screennameSearchContainer\" class=\"screenname-search-container\"></div></div>");
}
buf.push("</div><div class=\"content-main\"><div class=\"sidebar\"><div id=\"userListFlirtcast\" style=\"display: none;\" class=\"item\"></div><div id=\"userlistright-banner-widget\" style=\"display: none;\" class=\"sidebar-widget\"><div id=\"openx-banner-placement-userlistright\" class=\"item userlistright-banner-openx\"></div></div></div><div class=\"main-column\"><div id=\"search-results\" class=\"b-search-results activity\"><div id=\"openx-banner-placement-userlisttop\" class=\"userlisttop-banner-openx\"></div><ul id=\"userListContainer\" class=\"b-search-grid\"><li id=\"userListTopInSearchPromotion\" style=\"display: none;\" class=\"b-grid-item\"></li></ul></div></div><div id=\"openx-banner-placement-userlistbottom\" class=\"userlistbottom-banner-openx\"></div></div></div>");;return buf.join("");
};