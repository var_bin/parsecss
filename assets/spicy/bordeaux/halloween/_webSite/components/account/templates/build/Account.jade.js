this["JST"] = this["JST"] || {};

this["JST"]["Account"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),_t = locals_._t,userId = locals_.userId,paymentStatus = locals_.paymentStatus,upgradeButton = locals_.upgradeButton,siteName = locals_.siteName,email = locals_.email,l10n = locals_.l10n,hasBillingHistory = locals_.hasBillingHistory;
buf.push("<div class=\"b-page b-page-profile-settings\"><div class=\"pattern-bg\"><div class=\"pattern-inner\"><div class=\"pattern-overlay\"></div></div></div><div class=\"settings-content\"><div id=\"openx-banner-placement-accounttop\" class=\"accounttop-banner-openx\"></div><div id=\"openx-banner-placement-accountleft\" class=\"accountleft-banner-openx\"></div><div id=\"openx-banner-placement-accountright\" class=\"accountright-banner-openx\"></div><h1 class=\"title\"><span class=\"b-title\">" + (jade.escape(null == (jade_interp = _t("account", "title.my_settings")) ? "" : jade_interp)) + "</span><div class=\"b-settings-row b-id\"><div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("account", "title.user_id")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><span class=\"value\">" + (jade.escape(null == (jade_interp = userId) ? "" : jade_interp)) + "</span><span" + (jade.attr("title", _t("account", "tooltip.user_id"), true, false)) + " class=\"btn-light btn-text-confirm btn-info\">?</span></div></div></h1><div class=\"left-block\"><div class=\"b-settings-row member-status\"><div class=\"settings-holder\"><div class=\"b-setting\">" + (jade.escape(null == (jade_interp = _t("account", "title.membership_status")) ? "" : jade_interp)) + "</div><div class=\"b-value\"><span class=\"value\">" + (jade.escape(null == (jade_interp = _t("account", "value.membership_status_" + paymentStatus.type)) ? "" : jade_interp)));
if(upgradeButton.type == 'main' || upgradeButton.type == 'features')
{
buf.push("<span class=\"info\">" + (jade.escape(null == (jade_interp = " " + _t("account", "tooltip.upgrade_account_" + paymentStatus.type, {"{sitename}": siteName})) ? "" : jade_interp)) + "</span>");
}
buf.push("</span></div></div>");
if(upgradeButton.type == 'main' || upgradeButton.type == 'features')
{
buf.push("<a" + (jade.attr("href", upgradeButton.url, true, false)) + " class=\"btn green\">" + (jade.escape(null == (jade_interp = _t("account", "button.upgrade_" + paymentStatus.type)) ? "" : jade_interp)) + "</a>");
}
buf.push("</div><div data-update-password=\"1\" class=\"b-settings-row b-settings-password\"><div data-value=\"value\" class=\"b-value\"><div class=\"line\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.change_password")) ? "" : jade_interp)) + "</div><input type=\"password\" data-change=\"change\" value=\"*********\" class=\"password-old\"/><div data-password-update-result=\"1\" class=\"b-success\"></div></div></div><div data-value=\"value\" class=\"b-value inactive\"><div data-old-password-line=\"1\" class=\"line\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.old_password")) ? "" : jade_interp)) + "</div><input data-old-password=\"1\" type=\"password\" class=\"password-old\"/><button value=\"1\" data-show-old-password=\"1\" class=\"show-pass-btn active\"></button><div data-old-password-error=\"1\" class=\"b-error\"></div></div><div data-old-password-line=\"1\" class=\"line inactive\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.old_password")) ? "" : jade_interp)) + "</div><input data-old-password=\"1\" type=\"text\" class=\"password-old\"/><button value=\"1\" data-show-old-password=\"1\" class=\"show-pass-btn active\"></button><div data-old-password-error=\"1\" class=\"b-error\"></div></div><div data-new-password-line=\"1\" class=\"line\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.new_password")) ? "" : jade_interp)) + "</div><input data-new-password=\"1\" type=\"password\" class=\"password-new\"/><button value=\"1\" data-show-new-password=\"1\" class=\"show-pass-btn active\"></button><div data-new-password-error=\"1\" class=\"b-error\"></div></div><div data-new-password-line=\"1\" class=\"line inactive\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.new_password")) ? "" : jade_interp)) + "</div><input data-new-password=\"1\" type=\"text\" class=\"password-new\"/><button value=\"1\" data-show-new-password=\"1\" class=\"show-pass-btn active\"></button><div data-new-password-error=\"1\" class=\"b-error\"></div></div><div class=\"line\"><button data-change-password=\"1\" class=\"btn green\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</button><button data-change=\"change\" class=\"btn grey\">" + (jade.escape(null == (jade_interp = _t("account", "button.cancel")) ? "" : jade_interp)) + "</button></div></div></div><div data-update-email=\"1\" class=\"b-settings-row b-settings-email\"><div data-value=\"value\" class=\"b-value\"><div class=\"line\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.change_email")) ? "" : jade_interp)) + "</div><input type=\"text\" data-change=\"change\" data-current-email=\"1\"" + (jade.attr("value", email, true, false)) + " class=\"password\"/><div data-emailupdate-result=\"1\" class=\"b-success\"></div></div></div><div data-value=\"value\" class=\"b-value inactive\"><span data-current-email=\"1\" class=\"value\">" + (jade.escape(null == (jade_interp = email) ? "" : jade_interp)) + "</span><div data-emailupdate-password-line=\"1\" class=\"line\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.your_password")) ? "" : jade_interp)) + "</div><input data-emailupdate-password=\"1\" type=\"text\" class=\"password\"/><div data-emailupdate-password-error=\"1\" class=\"b-error\"></div></div><div data-emailupdate-email-line=\"1\" class=\"line\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.new_email_address")) ? "" : jade_interp)) + "</div><input data-emailupdate-email=\"1\" type=\"text\" class=\"email\"/><div data-emailupdate-email-error=\"1\" class=\"b-error\"></div></div><div class=\"line\"><button data-change-email=\"1\" class=\"btn green\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</button><button data-change=\"change\" class=\"btn grey\">" + (jade.escape(null == (jade_interp = _t("account", "button.cancel")) ? "" : jade_interp)) + "</button></div></div></div><div data-update-l10n=\"1\" class=\"b-settings-row b-settings-l10n\"><div data-value=\"value\" class=\"b-value\"><div data-l10n-locale=\"1\" class=\"line\"><div data-ui-select=\"data-ui-select\" class=\"field\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.language")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", l10n.locale, true, false)) + " name=\"profileLanguageSelect\" data-control=\"data-control\" id=\"profileLanguageSelect\"/><ul class=\"list\">");
if ( l10n.languagesList)
{
// iterate l10n.languagesList
;(function(){
  var $$obj = l10n.languagesList;
  if ('number' == typeof $$obj.length) {

    for (var language = 0, $$l = $$obj.length; language < $$l; language++) {
      var title = $$obj[language];

buf.push("<li" + (jade.attr("data-item", language, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = _t("account", "locale." + title)) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var language in $$obj) {
      $$l++;      var title = $$obj[language];

buf.push("<li" + (jade.attr("data-item", language, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = _t("account", "locale." + title)) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

}
buf.push("</ul></div></div></div><div data-error=\"1\" class=\"b-error\">" + (jade.escape(null == (jade_interp = _t("account", "error.cant_change_language")) ? "" : jade_interp)) + "</div></div><div data-l10n-timezone=\"1\" class=\"line\"><div data-ui-select=\"data-ui-select\" class=\"field\"><div class=\"b-name\">" + (jade.escape(null == (jade_interp = _t("account", "title.timezone")) ? "" : jade_interp)) + "</div><div data-toggle=\"data-toggle\" class=\"input-holder\"><div data-value=\"\" class=\"value\"></div><div class=\"select\"><input type=\"hidden\"" + (jade.attr("value", l10n.timezone, true, false)) + " name=\"profileTimezoneSelect\" data-control=\"data-control\" id=\"profileTimezoneSelect\"/><ul class=\"list\">");
if ( l10n.timezonesList)
{
// iterate l10n.timezonesList
;(function(){
  var $$obj = l10n.timezonesList;
  if ('number' == typeof $$obj.length) {

    for (var timezoneTitle = 0, $$l = $$obj.length; timezoneTitle < $$l; timezoneTitle++) {
      var timeZone = $$obj[timezoneTitle];

buf.push("<li class=\"items-group-name\">" + (jade.escape(null == (jade_interp = timezoneTitle) ? "" : jade_interp)) + "</li>");
if ( timeZone)
{
// iterate timeZone
;(function(){
  var $$obj = timeZone;
  if ('number' == typeof $$obj.length) {

    for (var zone = 0, $$l = $$obj.length; zone < $$l; zone++) {
      var title = $$obj[zone];

buf.push("<li" + (jade.attr("data-item", zone, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var zone in $$obj) {
      $$l++;      var title = $$obj[zone];

buf.push("<li" + (jade.attr("data-item", zone, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

}
    }

  } else {
    var $$l = 0;
    for (var timezoneTitle in $$obj) {
      $$l++;      var timeZone = $$obj[timezoneTitle];

buf.push("<li class=\"items-group-name\">" + (jade.escape(null == (jade_interp = timezoneTitle) ? "" : jade_interp)) + "</li>");
if ( timeZone)
{
// iterate timeZone
;(function(){
  var $$obj = timeZone;
  if ('number' == typeof $$obj.length) {

    for (var zone = 0, $$l = $$obj.length; zone < $$l; zone++) {
      var title = $$obj[zone];

buf.push("<li" + (jade.attr("data-item", zone, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</li>");
    }

  } else {
    var $$l = 0;
    for (var zone in $$obj) {
      $$l++;      var title = $$obj[zone];

buf.push("<li" + (jade.attr("data-item", zone, true, false)) + " class=\"item\">" + (jade.escape(null == (jade_interp = title) ? "" : jade_interp)) + "</li>");
    }

  }
}).call(this);

}
    }

  }
}).call(this);

}
buf.push("</ul></div></div></div><div data-error=\"1\" class=\"b-error\">" + (jade.escape(null == (jade_interp = _t("account", "error.cant_change_timezone")) ? "" : jade_interp)) + "</div></div><div class=\"line\"><button data-change-l10n=\"1\" class=\"btn green\">" + (jade.escape(null == (jade_interp = _t("account", "button.save")) ? "" : jade_interp)) + "</button></div></div></div></div><div class=\"right-block\"><h2>" + (jade.escape(null == (jade_interp = _t("account", "title.notifications")) ? "" : jade_interp)) + "</h2><div data-notification-settings=\"1\" class=\"b-settings-row\"></div><div data-notification-newsfeed-subscription=\"1\" class=\"b-settings-row activity\"></div><div data-notification-subscription=\"1\" class=\"b-settings-row b-settings-notifications\"></div><div data-settings-features=\"1\" class=\"b-settings-row b-settings-features select-output-dropdown\"></div><div data-fb-activity-settings=\"1\" class=\"b-settings-row privacy-setting\"></div><div class=\"b-settings-row meta-setting\"><div class=\"b-settings-btns\"><div class=\"b-user-complain\"><ul><li><button data-remove-account=\"data-remove-account\">" + (jade.escape(null == (jade_interp = _t("account", "button.remove_account")) ? "" : jade_interp)) + "</button></li>");
if ((hasBillingHistory === true))
{
buf.push("<li><a href=\"/#billingHistory\">" + (jade.escape(null == (jade_interp = _t("account", "button.billing_history")) ? "" : jade_interp)) + "</a></li>");
}
buf.push("</ul></div></div></div></div></div><div id=\"openx-banner-placement-accountbottom\" class=\"accountbottom-banner-openx\"></div></div>");;return buf.join("");
};