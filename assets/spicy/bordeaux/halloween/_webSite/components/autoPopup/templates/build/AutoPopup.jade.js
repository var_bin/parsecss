this["JST"] = this["JST"] || {};

this["JST"]["AutoPopup"] = function template(locals) {
var buf = [];
var jade_mixins = {};
var jade_interp;
var locals_ = (locals || {}),popupType = locals_.popupType,_t = locals_._t,data = locals_.data;
buf.push("<div class=\"b-popup-overlay\"><div class=\"b-popup-table\"><div class=\"b-popup-cell\">");
if ( popupType === "paySuccess")
{
buf.push("<div class=\"b-popup b-popup-success\"><div class=\"meta-button-wrap\"><a data-close=\"data-close\"" + (jade.attr("title", _t("autoPopup", "text.close"), true, false)) + " class=\"btn-meta btn-close\"></a></div>");
if ( data.features && (data.membership || data.offer || data.lifetimeOffer))
{
buf.push("<p class=\"title\">" + (null == (jade_interp = _t('autoPopup','text.pay-success-membership-and-features', {"{upgradepackage}":data.features, "{sitename}":data.siteName, "{br}":"<br/>"})) ? "" : jade_interp) + "</p>");
}
else if ( data.features)
{
buf.push("<p class=\"title\">" + (null == (jade_interp = _t('autoPopup', 'text.pay-success-features-only',{"{upgradepackage}":data.features,"{sitename}":data.siteName,"{br}":"<br />"})) ? "" : jade_interp) + "</p>");
}
else if ((data.membership || data.offer || data.lifetimeOffer))
{
buf.push("<p class=\"title\">" + (null == (jade_interp = _t('autoPopup', 'text.pay-success-membership-only',{"{sitename}":data.siteName,"{br}":"<br />"})) ? "" : jade_interp) + "</p>");
}
buf.push("<div class=\"btn-row\"><button data-close=\"data-close\" class=\"btn green btn-continue\">" + (jade.escape(null == (jade_interp = _t("autoPopup", "text.continue")) ? "" : jade_interp)) + "</button></div><p class=\"bolder\">" + (jade.escape(null == (jade_interp = _t("autoPopup", "text.pay-success-information")) ? "" : jade_interp)) + "</p><p>" + (jade.escape(null == (jade_interp = _t("autoPopup", "text.pay-success-description")) ? "" : jade_interp)) + "</p></div>");
}
buf.push("</div></div><div class=\"b-loader\"></div></div>");;return buf.join("");
};