module.exports = function(grunt) {
  //Init Grunt config
  var parseCSS = require('./lib/parseCSS')(),
    helpers = require('./lib/helpers')(),
    filename = '../assets/lgw/vermillion/_webSite/build/components/account/styles/styles.css';

  var pFile = parseCSS.parseFile(filename),
      metrics = pFile['metrics'];

  grunt.initConfig({
    less: {
      options: {

      },
      development: {
        files: {
          '../example/styles/s.css': '../example/styles/s.less'
        }
      }
    },
    jade: {
      development: {
        options: {
          pretty: true,
          data: {
            debug: true,
            parseObj: pFile
          }
        },
        files: {
          '../example/html/index.html': '../example/html/index.jade'
        }
      }
    }
  });

  //Load tasks
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jade');

  // Default task(s).
  grunt.registerTask('default', ['less:development', 'jade:development']);
};