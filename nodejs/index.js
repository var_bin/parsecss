'use strict';

    // node modules
var path = require('path'),
    fs = require('fs'),
    colors = require('colors'),
    // lib modules
    parseCSS = require('./lib/parseCSS')(),
    helpers = require('./lib/helpers')(),
    cssbeautify = require('./lib/cssbeautify')(),
    // load data from _themes.json
    _themes = require('../project/_themes');

/*
 * @return {object} - public methods
 *
 */

var generateJSON = function () {
  var prefix = 'styles-',
      apendix = '.min.css',
      info = '';
  /*
   * @param {Object} options - object with options
   * @param {Object} options.obj - object with themes
   * @param {String} options.objItem - path to current theme
   * @param {Bollean} options.fixPathToDist - true or false. If true -> need add options.partPathToDist to distDir. If false -> no need to add options.partPathToDist to distDir
   * @param {String} options.partPathToDist - part of path to dist. e.c '../../'
   */
  function _iteration(options) {

    var obj = options.obj,
        objItem = options.objItem,
        fixPathToDist = options.fixPathToDist || false,
        partPathToDist = options.partPathToDist || '',

        distDir = fixPathToDist ? path.join(partPathToDist, _themes[objItem]['destDir'], '/css/') : path.join(_themes[objItem]['destDir'], '/css/'),
        cCount = obj[objItem].length;

    console.log('==== Run for theme: ' + objItem + ' ====', cCount);

    console.time('generateJSON');

    obj[objItem].forEach(function(v) {
      var component = path.join(distDir, prefix + v + apendix),
        parseCSSFile = parseCSS.parseFile({
          'content': cssbeautify.beautify(component),
          'component': v,
          'themeName': objItem,
          'isStr': true
        }),
        jsonFile;

      jsonFile = helpers.parseCSSToJSON(JSON.stringify(parseCSSFile), objItem, v);

      info = 'Metrics: ' + jsonFile.yellow + ' have been created';

      console.log(info);
    });

    console.timeEnd('generateJSON');
  }

  /*
   * generate errors
   */
  function _getErrors() {
    var errorFilename = helpers.parseErrorsToJSON(JSON.stringify(parseCSS.getErrors()));

    console.log('Errors: ' + errorFilename.yellow + ' have been created');
  }

  /*
   * @param {Object} o - object with themes list
   *
   * init to get data
   */
  function init(o) {
    o = o || {};

    for (var item in o) {
      _iteration({
        'obj': o,
        'objItem': item
      });
    }

    _getErrors();
  }

  return {
    init: init
  };
}();

generateJSON.init(helpers.isComponentExists(helpers.isThemeExists(_themes), _themes));