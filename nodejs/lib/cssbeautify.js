// lib/cssbeautify.js

'use strict';

module.exports = function() {
  // node modules
  var path = require('path'),
      fs = require('fs'),
      cssbeautify = require('cssbeautify');

  // local modules
  var helpers = require('./helpers')();

  // local variables
  var cssbeautifyOptions = {
    indent: '  ',
    autosemicolon: true
  };

  function CSSBeautify() {}

  CSSBeautify.prototype.beautify = function(filename, options) {
    filename = filename || '';
    options = options || cssbeautifyOptions;

    var res = cssbeautify(helpers.readFile(filename, options));

    return res;
  };

  return new CSSBeautify();
}