'use strict';

module.exports = function() {
  // node modules
  var fs = require('fs'),
      path = require('path'),
      isThere = require('is-there');

  function Helpers() {}

  Helpers.prototype.readFile = function(filename) {
    filename = filename || '';

    var content = fs.readFileSync(filename, {'encoding': 'utf-8'});

    return content;
  };

  /*
   * @param {Object} o - object with data
   *
   * create new file with date + theme + platform + componentName + parseCSSToJSON.json
   *
   * date format: YYYY-MM-DD
   *
   * theme - any string
   *
   * filename format: date-theme.platform-componentName-parseCSSToJSON.json
   *
   * return filename
   */
  Helpers.prototype.parseCSSToJSON = function(o, theme, component) {
    o = o || {};
    theme = theme || '',
    component = component || '';

    var date = this.getDate(),
        appendix = 'parseCSSToJSON',
        ext = '.json',
        filename = '';

    filename += date + '-' + theme + '-' + component + '-' + appendix + ext;

    /*
     * Path to new folder (example):
     * path.join('../', appendix, path.sep, date) ->
     * '../parseCSSToJSON/YYYY-MM-DD/'
     *
     * Path to create file (example):
     * path.join('../', appendix, path.sep, date, path.sep, filename) ->
     * '../parseCSSToJSON/YYYY-MM-DD/YYYY-MM-DD-id-parseCSSToJSON.json'
     */
    (function() {
      Helpers._createJSON({
        'directory': path.join('../', appendix, path.sep, date),
        'filepath': path.join('../', appendix, path.sep, date, path.sep, filename),
        'o': o
      });
    })();

    return filename;
  };

  /*
   * Check theme exists
   * param {Object} o - object with all themes
   * return object with exist themes
   */
  Helpers.prototype.isThemeExists = function(o) {
    o = o || {};
    var themeExists = {};

    for(var item in o) {
      var tDir = o[item]['themeDir'];
      // think about fs.existsSync !!!
      if(fs.existsSync(tDir)) {
        console.log('Theme ' + item + ' is exists');
        themeExists[item] = item;
      }
    }
    return themeExists;
  }

  /*
   * Check component exists
   * param {Object} o - object with exist themes
   * param {Object} themes - object with all themes
   * return object with exist components in current theme
   */
  Helpers.prototype.isComponentExists = function(o, themes) {
    o = o || {},
    themes = themes || {};

    var prefix = 'styles-',
        apendix = '.min.css',
        componentsExists = {},
        // forEach creates closure, so we need this to create _this
        _this = this;

    for(var item in o) {
      var distDir = themes[item]['destDir'] + '/css/',
          components = themes[item]['config']['components'];
      componentsExists[item] = [];
      components.forEach(function(v) {
        // think about fs.existsSync !!!
        if(fs.existsSync(path.join(distDir, prefix + v + apendix)) && _this.getFileSize(path.join(distDir, prefix + v + apendix))) {
          //console.log(_this.getFileSize(path.join(distDir, prefix + v + apendix)));
          componentsExists[item].push(v);
        } else {
          return;
        }
      });
    }

    return componentsExists;
  }

  /*
   * Get current date
   * format: YYYY-MM-DD
   * e.g: 2015-07-21
   */
  Helpers.prototype.getDate = function() {
    var date = new Date(),
        month = date.getMonth() + 1,
        day = date.getDate();

    month < 10 ? month = '0' + month : month;
    day < 10 ? day = '0' + day : day;

    return date.getFullYear() + '-' + month + '-' + day;
  }

  /*
   * Get file size
   * @param {String} file - path to file
   * return string with file size
   */
  Helpers.prototype.getFileSize = function(file) {
    file = file || '';
    var content = this.readFile(file),
        fileSize = content.length;

    return fileSize;
  }

  /*
   * Convert object with errors to JSON
   * param {Object} o - object with errors
   * return filename
   */
  Helpers.prototype.parseErrorsToJSON = function(o) {
    o = o || {};

    var date = this.getDate(),
        appendix = 'parseErrorsToJSON',
        ext = '.json',
        filename = date + '-' + appendix + ext;

    /*
     * Path to new folder (example):
     * path.join('../', appendix, path.sep, date) ->
     * '../parseCSSToJSON/YYYY-MM-DD/'
     *
     * Path to create file (example):
     * path.join('../', appendix, path.sep, date, path.sep, filename) ->
     * '../parseCSSToJSON/YYYY-MM-DD/YYYY-MM-DD-id-parseCSSToJSON.json'
     */
    (function() {
      Helpers._createJSON({
        'directory': path.join('../', appendix, path.sep, date),
        'filepath': path.join('../', appendix, path.sep, date, path.sep, filename),
        'o': o
      });
    })();

    return filename;
  }

  Helpers._createJSON = function(options) {
    options = options || {};
    var directory = options.directory,
        filepath = options.filepath,
        o = options.o,
        params = options.params || {'encoding': 'utf-8'};

    if(!fs.existsSync(directory)) {
      fs.mkdirSync(directory);
      fs.writeFileSync(filepath, o, params);
    } else {
      fs.writeFileSync(filepath, o, params);
    }

    return this;
  }

  Helpers.prototype.isExists = function(file) {

    file = path.join(__dirname, file) || '';

    return isThere(file);
  }

  return new Helpers();
};