'use strict';

module.exports = function() {
  // node modules
  var fs = require('fs'),
      path = require('path'),
      helpers = require('./helpers')(),
      analyzeCSS = require('analyze-css'),
      // object for save all errors and convert to JSON
      e = {};

  function ParseCSS() {};
  /*
   * parse only one file
   * @param {String} content - absolute path to parse file
   * @param {Bollean} isStr - true or false. If true -> content = string.
   * If false -> content = absolute path to parse file
   */
  ParseCSS.prototype.parseFile = function(options) {
    // content, component, themeName, isStr
    var content = options.content,
        component = options.component,
        themeName = options.themeName,
        isStr = options.isStr || false;

    var file = '',
        res;

    if(isStr) {
      file = content;
    } else {
      file = helpers.readFile(content);
    }

    res = new analyzeCSS(file, function(err, results) {
      if(err) {
        console.log(err);
        e[themeName] = {
          "date": helpers.getDate(),
          "path": component,
          "themeName": themeName,
          "errorsInfo": err.toString()
        };
      }

      return results;
    });

    return {
      'metrics': ParseCSS._getMetrics(res),
      'offenders': ParseCSS._getOffenders(res)
    };
  };

  /*
   * return metrics
   * @param {Object} res
   */
  ParseCSS._getMetrics = function(res) {
    res = res;
    var obj = {},
        prop;

    for(prop in res['metrics']) {
      obj[prop] = res['metrics'][prop].toString();
    }

    return obj;
  };

  /*
   * return offenders
   * @param {Object} res
   */
  ParseCSS._getOffenders = function(res) {
    res = res;

    return res['offenders'];
  };

  ParseCSS.prototype.getErrors = function() {
    return e;
  }

  return new ParseCSS();
}