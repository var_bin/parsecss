var express = require('express'),
    path = require('path'),
    fs = require('fs'),
    _themes = require('../_themes'),
    helpers = require('../../nodejs/lib/helpers')(),
    router = express.Router(),
    cssbeautify = require('../../nodejs/lib/cssbeautify')(),
    conf = require('../config'), // load ../config/index.js
    // apps
    defaultPaths = conf.get('paths:default'),
    title = conf.get('title'),
    themes = helpers.isThemeExists(_themes),
    components = helpers.isComponentExists(themes, _themes),
    // errors
    errorHandler = require('../ext/error/error');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    title: title,
    paths: defaultPaths,
    themes: themes
  });
});

/* GET page with components errors */
router.get('/cErrors', function(req, res, next) {
  var date = helpers.getDate(),
      appendix = 'parseErrorsToJSON',
      ext = '.json',
      root = '../../',
      filename = date + '-' + appendix + ext,
      errorFilePath = path.join(root, appendix, path.sep, date, path.sep, filename);

  if(!helpers.isExists(errorFilePath)) {
    next(errorHandler.setCustomError(' There isn\'t a file: ' + errorFilePath, 404, 'Try to run node index.js'))
  }

  res.render('componentErrors', {
    title: title,
    paths: defaultPaths,
    currDate: date,
    paths: defaultPaths,
    content: require(errorFilePath)
  });
});

/* GET page with select theme */
router.get('/:themeName', function(req, res, next) {
  var themeName = req.params.themeName;
  res.render('theme', {
    title: title,
    theme: themeName,
    paths: defaultPaths,
    components: components[themeName],
    currDate: helpers.getDate()
  });
});

/* GET page with select component */
router.get('/:themeName/:componentName/:date', function(req, res, next) {
  var d = req.params.date, // date
      t = req.params.themeName, // theme
      c = req.params.componentName, // component
      // variables for metrics
      root = '../../',
      dirName = 'parseCSSToJSON',
      prefix = d + '-' + t + '-' + c + '-',
      appendix = 'parseCSSToJSON',
      filename = path.join(root, dirName, d, prefix + appendix),
      // variables for componentContent
      themeArr = t.split('.'),
      themePath = themeArr.map(function(v) {
        var p = '';
        if(v == 'webSite' || v == 'mobSite') {
          v = '_' + v;
        }
        p += path.join(v);

        return p;
      });
      assetsPathSpicy = path.join('../', 'assets/spicy', themePath.join('/'), 'dist/css/'),
      cPartName = 'styles-' + c + '.min.css';

  if(!helpers.isExists(filename)) {
    next(errorHandler.setCustomError(' There isn\'t a file: ' + filename, 404, 'Try to run node index.js'))
  }

  res.render('component', {
    title: title,
    theme: t,
    component: c,
    date: d,
    paths: conf.get('paths:codemirror'),
    metrics: require(filename),
    componentContent: cssbeautify.beautify(assetsPathSpicy + cPartName)
  });
});

module.exports = router;
