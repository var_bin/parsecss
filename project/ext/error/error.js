// ext/error/error.js

var http = require('http');

function setCustomError(num, msg, stack) {
  msg = msg || http.STATUS_CODE[num];
  var err = new Error(msg);
  err.status = num || 404;
  err.stack = stack || err.stack;

  return err;
}

exports.setCustomError = setCustomError;