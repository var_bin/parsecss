$(function() {
  function toggleComponents() {
    var $title = $('.js-offenders'),
        $components = $('.offenders-list');

    $title.on({
      click: function() {
        var _this = $(this),
            $currComponent = $('.offenders-list', _this);
        _this.toggleClass('active');

        $currComponent.toggleClass('is-hidden').toggleClass('is-active');
      }
    });
  }

  toggleComponents();
});