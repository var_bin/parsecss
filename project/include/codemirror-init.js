<script type="text/javascript">
  var myCodeMirror = CodeMirror.fromTextArea(document.getElementById("code"), {
      lineNumbers: true,
      lineWrapping: true,
      autoCloseBrackets: true,
      matchBrackets: true,
      showCursorWhenSelecting: true,
      mode: "css",
      theme: "monokai",
      foldGutter: true,
      gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
    });
</script>